package com.ruth.app.login.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.location.LocationManager
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Scope
import com.google.android.gms.tasks.Task
import com.google.api.services.people.v1.PeopleScopes
import com.ruth.app.App
import com.ruth.app.App.isChineseLanguage
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.base.lifecycle.AndroidDisposable
import com.ruth.app.home.activity.HomeActivity
import com.ruth.app.login.presenter.LoginPresenter
import com.ruth.app.utility.RuthUtility
import com.steelkiwi.instagramhelper.InstagramHelper
import com.steelkiwi.instagramhelper.InstagramHelperConstants
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.activity_login.*
import java.net.URL
import java.util.*

class LoginActivity : BaseActivity<LoginPresenter>(), LoginPresenter.LoginView, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private var loginType: String = ""
    private val rxPermissions: RxPermissions by lazy { RxPermissions(this) }
    private var locationManager: LocationManager? = null
    private var callbackManager: CallbackManager? = null
    private var instagramHelper: InstagramHelper? = null
    internal val RC_INTENT = 200
    private var photoUrl: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        isChineseLanguage = true

        customTextView(tvTermsCondition)

        if (utility.getBooleaPreferences(presenter.context, getString(R.string.isGoogleSignup))) {
            googlePlusSignIn()
        }
        if (utility.getBooleaPreferences(presenter.context, getString(R.string.isInstaSignup))) {
            instagramHelper = App.getInstagramHelper()
        }

        tvTermsCondition.setOnClickListener {
            presenter.openTermsConditionDialog()
        }

        ivPhone.setOnClickListener {
            loginType = "phone"
            presenter.openPhoneLogin()
        }

        ivFacebook.setOnClickListener {
            loginType = "facebook"
            if (utility.getBooleaPreferences(presenter.context, getString(R.string.isfbSignup))) {
                presenter.openView(loginType)
            } else {
                presenter.openBirthdaySelection()
            }
        }

        ivInstagram.setOnClickListener {
            loginType = "instagram"
            if (utility.getBooleaPreferences(presenter.context, getString(R.string.isInstaSignup))) {
                presenter.openView(loginType)
            } else {
                presenter.openBirthdaySelection()
            }
        }

        ivGooglePlus.setOnClickListener {
            loginType = "google"
            if (utility.getBooleaPreferences(presenter.context, getString(R.string.isGoogleSignup))) {
                presenter.openView(loginType)
            } else {
                presenter.openBirthdaySelection()
            }
        }




    }

    override fun openHome(userName: String) {
//        progressBarLogin.visibility = View.GONE
        hideProgressBar()
        progressBarLogin.isTouchable = true

        startActivity(Intent(this, HomeActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
        finish()
    }

    override fun openInvite(userName: String) {
//        progressBarLogin.visibility = View.GONE
        hideProgressBar()
        progressBarLogin.isTouchable = true

        startActivity(Intent(this, InviteFriendsActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .putExtra("header", loginType))
        finish()
    }

    private lateinit var profilePicture: URL

    override fun openFbPage() {
//        progressBarLogin.visibility = View.GONE
        hideProgressBar()
        progressBarLogin.isTouchable = true
        LoginManager.getInstance().logOut()
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile, email, user_birthday,user_friends,user_location, user_gender"))
        LoginManager.getInstance().registerCallback(callbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) {
                        val request = GraphRequest.newMeRequest(
                                loginResult.accessToken, GraphRequest.GraphJSONObjectCallback { `object`, response ->
                            //                            getFacebookFriends(loginResult.accessToken)

                            try {
                                var userId = response.jsonObject.getString("id")
                                profilePicture = URL("https://graph.facebook.com/$userId/picture?width=500&height=500")
                                var gender: String
                                if (response.jsonObject.has("gender")) {
                                    gender = response.jsonObject.getString("gender")
                                    Log.d("Facebook", " -> $profilePicture, gender - $gender")
                                }
                                Log.d("Facebook", " ->$profilePicture")

                            } catch (e: Exception) {
                                Log.d("exception", " -> " + e.message)
                            }

                            presenter.createUserFBGoogle(response.jsonObject.getString("nickname"),
                                    response.jsonObject.getString("email"),
                                    response.jsonObject.getString("id"), "facebook",
                                    profilePicture.toString())
                        })
                        val parameters = Bundle()
                        parameters.putString("fields", "id,nickname,email,gender, birthday")
                        request.parameters = parameters
                        request.executeAsync()
                    }

                    override fun onCancel() {
//                        progressBarLogin.visibility = View.GONE
                        hideProgressBar()
                        progressBarLogin.isTouchable = true
                        Toast.makeText(this@LoginActivity, getString(R.string.try_again), Toast.LENGTH_SHORT).show()
                    }

                    override fun onError(error: FacebookException) {
//                        progressBarLogin.visibility = View.GONE
                        hideProgressBar()
                        progressBarLogin.isTouchable = true
                        Toast.makeText(this@LoginActivity, getString(R.string.try_again), Toast.LENGTH_SHORT).show()
                    }
                })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == InstagramHelperConstants.INSTA_LOGIN && resultCode == Activity.RESULT_OK) {
            val user = instagramHelper!!.getInstagramUser(this)

//            Log.d("instagram", "token -> " + SharedPrefUtils.getToken(this@SelectBirthdayActivity) + "" +
//                    ", user id -> " + user.data.id)
            //getInstagramFriends(SharedPrefUtils.getToken(this@SelectBirthdayActivity), user.data.id)
            presenter.createUserInstagram(user.data.username, "", user.data.id!!, "instagram")
        } else if (requestCode == RC_INTENT) {
            Log.d("google+", "sign in result")
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            if (result.isSuccess) {
                App.acct = result.signInAccount
            } else {
                Log.d("google+", result.status.toString() + "\nmsg: " + result.status.statusMessage)
            }
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        } else {
            callbackManager?.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun hideBirthdayProgress() {
//        progressBarLogin.visibility = View.GONE
        hideProgressBar()
        progressBarLogin.isTouchable = true
    }

    override fun showBirthdayProgress() {
//        progressBarLogin.visibility = View.VISIBLE
        showProgressBar()
        progressBarLogin.isTouchable = false

    }

    override fun openInstagram() {
        instagramHelper!!.loginFromActivity(this)
    }

    /*---------------------------Google plus login-----------*/

    override fun openGooglePlus() {
//        progressBarLogin.visibility = View.VISIBLE
        showProgressBar()
        progressBarLogin.isTouchable = false
        getIdToken()
    }

    private var mGoogleApiClient: GoogleApiClient? = null

    override fun onConnected(p0: Bundle?) {
    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }

    private fun googlePlusSignIn() {
        val signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                // The serverClientId is an OAuth 2.0 web client ID
                .requestServerAuthCode("1065771002577-odus455hfaufkp427mqetonqlrarvee8.apps.googleusercontent.com")
                .requestEmail()
                .requestScopes(Scope(Scopes.PLUS_LOGIN),
                        Scope(PeopleScopes.CONTACTS_READONLY),
                        Scope(PeopleScopes.USER_EMAILS_READ),
                        Scope(PeopleScopes.USERINFO_EMAIL),
                        Scope(PeopleScopes.USER_PHONENUMBERS_READ))
                .build()


        // To connect with Google Play Services and Sign In
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions)
                .build()
    }

    fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            if (completedTask != null) {
                val account: GoogleSignInAccount?
                try {
                    account = completedTask.result
                    if (account != null) {
                        hideProgressBar()
//                        progressBarLogin.visibility = View.GONE
                        progressBarLogin.isTouchable = true
                        if (account.photoUrl != null) {
                            photoUrl = account.photoUrl.toString()
                        }
//                        presenter.createNewUser(account.displayName!!, account.email!!, account.id!!, "googleplus")
                        presenter.createUserFBGoogle(account.displayName!!, account.email!!, account.id!!, "googleplus", photoUrl!!)
                    }
                } catch (e: RuntimeException) {
//                    progressBarLogin.visibility = View.GONE
                    hideProgressBar()
                    progressBarLogin.isTouchable = true
                    Log.w("google+", "signInResult:failed code=" + e.message)
                }
            }
        } catch (e: ApiException) {
            Log.w("google+", "signInResult:failed code=" + e.statusCode)
        }
    }

    override fun onStart() {
        super.onStart()
        checkPermissions()
        if (utility.getBooleaPreferences(presenter.context, "isGoogleSignupAlready")) {
            mGoogleApiClient?.connect()
        }
    }


    // Performed on Google Sign in click
    private fun getIdToken() {
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        startActivityForResult(signInIntent, RC_INTENT)
    }


    private fun checkPermissions() {
        AndroidDisposable.create(this,
                rxPermissions.requestEach(Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.WRITE_CONTACTS,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.READ_PHONE_NUMBERS,
                        Manifest.permission.RECORD_AUDIO).toList()
                        .subscribe({
                            locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?
                            try {
                                // Request location updates
                                val permissionsOk = (rxPermissions.isGranted(Manifest.permission.ACCESS_COARSE_LOCATION) ||
                                        rxPermissions.isGranted(Manifest.permission.ACCESS_FINE_LOCATION))
                                if (permissionsOk) {
                                    presenter.getLocation(locationManager)
                                } else {
                                    AndroidDisposable.create(this,
                                            rxPermissions.requestEach(
                                                    Manifest.permission.ACCESS_COARSE_LOCATION,
                                                    Manifest.permission.ACCESS_FINE_LOCATION).toList()
                                                    .subscribe({ premissionList ->

                                                    }, { throwable ->
                                                        throwable.printStackTrace()
                                                    }))
                                }
                            } catch (ex: SecurityException) {
                                Log.d("myTag", "Security Exception, no location available")
                            }
                        }, { throwable ->
                            throwable.printStackTrace()
                        }))
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun openSelectBirthday() {
        startActivity(Intent(this, SelectBirthdayActivity::class.java)
                .putExtra("isFrom", loginType))
    }

    override fun openPhoneLogin() {
//        startActivity(Intent(this, RecommendedFriendsActivity::class.java))
        startActivity(Intent(this, LoginPhoneActivity::class.java))
    }

    override fun openTermsConditions() {
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_terms_conditions, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation

        val closeTermsCondition = dialogView.findViewById<View>(R.id.ivCloseTermsConditions) as ImageView

        closeTermsCondition.setOnClickListener {
            b.dismiss()
        }
        b.setCancelable(false)
        b.show()

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)

        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.9f).toInt()
        val dialogWindowHeight = (displayHeight * 0.7f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        b.window.attributes = layoutParams
    }


    private fun customTextView(view: TextView) {
        val terms = getString(R.string.login_to_agree_to_the_terms_conditions)
        val conditions = getString(R.string.termsConditions)

        val spanTxt = SpannableStringBuilder(terms)

        spanTxt.setSpan(UnderlineSpan(), terms.indexOf(conditions),
                terms.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spanTxt.setSpan(ForegroundColorSpan(resources.getColor(R.color.terms_textColor)),
                terms.indexOf(conditions), spanTxt.length, 0)
        view.movementMethod = LinkMovementMethod.getInstance()
        view.setText(spanTxt, TextView.BufferType.SPANNABLE)
    }

    private fun showProgressBar(){
        RuthUtility.showLoader(this@LoginActivity)
    }

    private fun hideProgressBar(){
        RuthUtility.hideLoader()
    }


}