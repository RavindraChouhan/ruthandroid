package com.ruth.app.login.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class ChangePasswordPresenter @Inject constructor() : BasePresenter<ChangePasswordPresenter.ChangePasswordView>(){

    override fun onCreate(view: ChangePasswordView) {
        super.onCreate(view)
    }

    fun goBack()
    {
        view?.goBack()
    }
    fun nextChangePassword ()
    {
        view?.nextChangePassword()
    }
    interface ChangePasswordView : BaseView {
        fun goBack()
        fun nextChangePassword()
    }

}