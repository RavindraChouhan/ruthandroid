package com.ruth.app.login.activity

import android.Manifest
import android.content.Intent
import android.location.LocationManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.base.lifecycle.AndroidDisposable
import com.ruth.app.home.activity.HomeActivity
import com.ruth.app.login.presenter.LoginPhonePresenter
import com.ruth.app.utility.RuthUtility
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.activity_login_phone_number.*

class LoginPhoneActivity : BaseActivity<LoginPhonePresenter>(), LoginPhonePresenter.LoginPhoneView {

    private var locationManager: LocationManager? = null
    private val rxPermissions: RxPermissions by lazy { RxPermissions(this) }
    private var loginAttempt: Int = 0
    private var passwordEnable: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_phone_number)
        App.getComponent().inject(this)
        presenter.onCreate(this)
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)

        etMobileNo.setOnTouchListener { v, event ->
            etMobileNo.isCursorVisible = true
            false
        }

        etPassword.setOnTouchListener { v, event ->
            etPassword.isCursorVisible = true
            false
        }

        passwordHideShow.setOnClickListener {
            passwordHideShow.switchState()
            passwordEnable = !passwordEnable

            if (passwordEnable) {
                etPassword.transformationMethod = null
                etPassword.setSelection(etPassword.length())
            } else {
                etPassword.transformationMethod = PasswordTransformationMethod()
                etPassword.setSelection(etPassword.length())
            }
        }

        val permissionsOk = (rxPermissions.isGranted(Manifest.permission.ACCESS_COARSE_LOCATION) ||
                rxPermissions.isGranted(Manifest.permission.ACCESS_FINE_LOCATION))
        if (!permissionsOk) {
            AndroidDisposable.create(this,
                    rxPermissions.requestEach(
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION).toList()
                            .subscribe({
                                presenter.getLocation(locationManager)
                            }, { throwable ->
                                throwable.printStackTrace()
                            }))
        }

        tvCreateNewAccount.setOnClickListener {
            presenter.openRegisterPhone()
        }

        btnNextPhone.setOnClickListener {
            loginAttempt += 1
            if (loginAttempt >= 5) {
                alert(getString(R.string.forgot_reset_password),
                        "",
                        getString(R.string.forgot_btn),
                        getString(R.string.cancel),
                        {
                            startActivity(Intent(this, ForgotPasswordActivity::class.java)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
                            finish()
                            overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
                        }, {})
                return@setOnClickListener
            }
            hideKeyboard()
            presenter.loginUser(etMobileNo.text.toString(), etPassword.text.toString())

        }

        ivBackLogin.setOnClickListener {
            presenter.goBack()
        }

        tvForgotPassword.setOnClickListener {
            presenter.forgotPassword()
        }

        etPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (etPassword.length() >= 6 && etMobileNo.length() >= 8) {
                    btnNextPhone.background = resources.getDrawable(R.drawable.imageview_selector_green)
                } else {
                    btnNextPhone.background = resources.getDrawable(R.drawable.imageview_selector)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        etMobileNo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (etMobileNo.length() >= 8 && etPassword.length() >= 6) {
                    btnNextPhone.background = resources.getDrawable(R.drawable.imageview_selector_green)
                } else {
                    btnNextPhone.background = resources.getDrawable(R.drawable.imageview_selector)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

//        etPassword.setOnEditorActionListener { editText, actionId, event ->
//            if ((event != null && (event.keyCode == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
//                Log.i(TAG,"Enter pressed")
//            }
//            false
//        }
    }

    override fun loginUser(userName: String?) {
        loginAttempt = 0
        startActivity(Intent(this, HomeActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
        finish()
    }

    override fun openPhoneRegister() {
        startActivity(Intent(this, SelectBirthdayActivity::class.java)
                .putExtra("isFrom", "phone"))
    }

    override fun goBack() {
        startActivity(Intent(this, LoginActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
        finish()
        overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
    }

    override fun hideLoginProgress() {
        RuthUtility.hideLoader()
    }

    override fun showLoginProgress() {
        RuthUtility.showLoader(this@LoginPhoneActivity)
    }

    override fun forgotPassword() {
        startActivity(Intent(this, ForgotPasswordActivity::class.java))
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this, LoginActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
        finish()
        overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
    }
}