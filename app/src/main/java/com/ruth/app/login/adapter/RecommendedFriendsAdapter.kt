//package com.ruth.app.login.adapter
//
//import android.content.Context
//import android.support.v7.widget.RecyclerView
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import com.ruth.app.R
//import com.ruth.app.login.model.Friends
//import com.ruth.app.login.model.RecommendedFriends
//import com.ruth.app.model.RecommandedUserDetail
//import kotlinx.android.synthetic.main.item_recommended_friends.view.*
//import kotlin.collections.ArrayList
//
//
//class RecommendedFriendsAdapter(private var context: Context,
//                                private var friendsList: ArrayList<RecommandedUserDetail>)
//    : RecyclerView.Adapter<RecommendedFriendsAdapter.RecommendedViewHolder>() {
//
//    private lateinit var recyclerView: RecyclerView
//
//    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
//        super.onAttachedToRecyclerView(recyclerView)
//        this.recyclerView = recyclerView
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecommendedViewHolder {
//        val inflater = LayoutInflater.from(parent.context)
//        val view = inflater.inflate(R.layout.item_recommended_friends, parent, false)
//        return RecommendedViewHolder(view)
//    }
//
//    override fun onBindViewHolder(holder: RecommendedViewHolder, position: Int) {
//        holder.bind(friendsList?.get(position)!!)
//    }
//
//    override fun getItemCount(): Int {
//        return friendsList?.size!!
//    }
//
//    inner class RecommendedViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//
//        fun bind(recommendedFriends: RecommendedFriends) {
//
//            if (recommendedFriends.isSelected) {
//                itemView.layoutUnselected.visibility = View.GONE
//                itemView.layoutSelected.visibility = View.VISIBLE
//            } else {
//                itemView.layoutUnselected.visibility = View.VISIBLE
//                itemView.layoutSelected.visibility = View.GONE
//            }
//
//            if (recommendedFriends.name.equals("Empty", true)) {
//                itemView.layoutEmpty.visibility = View.VISIBLE
//                itemView.layoutRecommend.visibility = View.GONE
//            } else {
//                itemView.layoutEmpty.visibility = View.GONE
//                itemView.layoutRecommend.visibility = View.VISIBLE
//
//                itemView.tvRecommendName.text = recommendedFriends.name
//                itemView.tvRecommendAccountNo.text = recommendedFriends.accountNo
//                itemView.tvRecommendFans.text = recommendedFriends.fansCount.toString()
//                itemView.tvRecommendGender.text = recommendedFriends.gender
//
//                val date = recommendedFriends.registeredDate?.split("年".toRegex())
//                itemView.tvRecommendRegisteredYear.text = /*context.getString(R.string.born_in)*/"生於" + date!![0] + "年"/*context.getString(R.string.year)*/
//                itemView.tvRecommendRegisteredDate.text = date[1]
//                itemView.tvRecommendFirstPlace.text = recommendedFriends.districtName1
//                itemView.tvRecommendSecondPlace.text = recommendedFriends.districtName2
//            }
//
//            itemView.layoutRecommend.setOnClickListener(View.OnClickListener {
//                friendsList!![position].isSelected = !friendsList!![position].isSelected
//                notifyDataSetChanged()
//            })
//        }
//    }
//
//    interface OnItemClickListener {
//        fun onItemClick(view: View, position: Int, answers: ArrayList<Friends>)
//    }
//}
