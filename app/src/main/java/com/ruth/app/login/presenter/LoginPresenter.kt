package com.ruth.app.login.presenter

import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.network.config.RuthApi
import java.util.*
import javax.inject.Inject

class LoginPresenter @Inject constructor(private val ruthApi: RuthApi) : BasePresenter<LoginPresenter.LoginView>() {


    override fun onCreate(view: LoginView) {
        super.onCreate(view)
    }

    fun openBirthdaySelection() {
//        utility.setStringPreferences(context, getString(R.string.deviceId), FirebaseInstanceId.getInstance().token)
        Log.d("deviceToken", " -> " +preferences.getDeviceId())
        view?.openSelectBirthday()
    }

    fun openTermsConditionDialog() {
        view?.openTermsConditions()
    }

    fun getLocation(locationManager: LocationManager?) {
        try {
            // Request location updates
            locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, locationListener)
        } catch (ex: SecurityException) {
            Log.d("myTag", "Security Exception, no location available")
        }
    }

    fun openPhoneLogin() {
        view?.openPhoneLogin()
    }

    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            utility.setDoublePreferences(context, getString(R.string.lat), location.latitude)
            utility.setDoublePreferences(context, getString(R.string.lng), location.longitude)
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

    fun openView(loginType: String) {
        when {
            loginType.equals("Phone", true) -> view?.openPhoneLogin()
            loginType.equals("Facebook", true) -> {
                view?.openFbPage()
            }
            loginType.equals("Instagram", true) -> {
                view?.openInstagram()
            }
            loginType.equals("Google", true) -> {
                view?.openGooglePlus()
            }
        }
    }

    fun createUserFBGoogle(userName: String, email: String, socialId: String, accType: String, profilePicture: String) {
        val gcd = Geocoder(context, Locale.getDefault())
        /*--------------Remove this on live-------------------------------------------*/
        val tempLat = 22.28552
        val tempLng = 114.15769

        utility.setDoublePreferences(context, getString(R.string.lat), tempLat)
        utility.setDoublePreferences(context, getString(R.string.lng), tempLng)

        /*------------------------------------------------------------------------------*/

        val addresses = gcd.getFromLocation(utility.getDoublePreferences(context, getString(R.string.lat)).toDouble(),
                utility.getDoublePreferences(context, getString(R.string.lng)).toDouble(), 1)

        if (addresses.size > 0) {
            if (addresses[0].countryName.equals(context.getString(R.string.Hong_Kong), true)) {
                val hm: HashMap<String, String> = HashMap()
                hm["userName"] = userName
                hm["email"] = email
                hm["socialID"] = socialId
                hm["accountType"] = accType
                hm["nickname"] = userName
                if (preferences.getDeviceId() != null){
                    hm["deviceToken"] = preferences.getDeviceId().toString()
                }else{
                    hm["deviceToken"] = FirebaseInstanceId.getInstance().token!!
                }
                hm["deviceType"] = "Android"
                hm["lat"] = utility.getDoublePreferences(context, getString(R.string.lat)).toString()
                hm["lng"] = utility.getDoublePreferences(context, getString(R.string.lng)).toString()
                hm["country"] = addresses[0].countryName
                if (profilePicture.isNotEmpty()) {
                    hm["photoUrl"] = profilePicture
                }

                ruthApi.socialSignUp(hm)
                        .doOnTerminate { view?.hideBirthdayProgress() }
                        .doOnSubscribe { view?.showBirthdayProgress() }
                        .subscribe({
                            Log.d("verify", " -> " + it.message)
                            if (it != null) {
                                if (it.status) {
                                    it.data.userToken = it.token
                                    preferences.setUserInfo(it.data)
                                    Log.d("token" , " --------> " +preferences.getUserInfo().userToken)
                                    if (it.data.isNewUser) {
                                        view?.openInvite(it.data.userName)
                                    } else {
                                        view?.hideBirthdayProgress()
                                        view?.openHome(it.data.userName)
                                    }
                                } else {
                                    view?.hideBirthdayProgress()
                                    if (App.isChineseLanguage) {
                                        view?.showToast(it.zhHant)
                                    } else {
                                        view?.showToast(it.message)
                                    }
                                }
                            }
                        }, {
                            view?.hideBirthdayProgress()
                            view?.showToast(getString(R.string.connection_error))
                            Log.d("errors", it.message)
                        })
            } else {
                view?.showToast(getString(R.string.not_available))
                return
            }
            Log.d("address", "->$addresses")
        } else {
            view?.showToast(getString(R.string.connection_error))
        }
    }

    fun createUserInstagram(userName: String, email: String, socialId: String, accType: String) {
        val gcd = Geocoder(context, Locale.getDefault())
        /*--------------Remove this on live-------------------------------------------*/
        val tempLat = 22.28552
        val tempLng = 114.15769

        utility.setDoublePreferences(context, getString(R.string.lat), tempLat)
        utility.setDoublePreferences(context, getString(R.string.lng), tempLng)

        /*------------------------------------------------------------------------------*/

        val addresses = gcd.getFromLocation(utility.getDoublePreferences(context, getString(R.string.lat)).toDouble(),
                utility.getDoublePreferences(context, getString(R.string.lng)).toDouble(), 1)

        if (addresses.size > 0) {
            if (addresses[0].countryName.equals(context.getString(R.string.Hong_Kong), true)) {
                val hm: HashMap<String, String> = HashMap()
                hm["userName"] = userName
                hm["email"] = email
                hm["socialID"] = socialId
                hm["accountType"] = accType
                hm["nickname"] = userName
                if (preferences.getDeviceId() != null){
                    hm["deviceToken"] = preferences.getDeviceId().toString()
                }else{
                    hm["deviceToken"] = FirebaseInstanceId.getInstance().token!!
                }
                hm["deviceType"] = "Android"
                hm["lat"] = utility.getDoublePreferences(context, getString(R.string.lat)).toString()
                hm["lng"] = utility.getDoublePreferences(context, getString(R.string.lng)).toString()
                hm["country"] = addresses[0].countryName

                ruthApi.socialSignUp(hm)
                        .doOnTerminate { view?.hideBirthdayProgress() }
                        .doOnSubscribe { view?.showBirthdayProgress() }
                        .subscribe({
                            Log.d("verify", " -> " + it.message)
                            if (it != null) {
                                if (it.status) {
                                    if (App.isChineseLanguage) {
                                        view?.showToast(it.zhHant)
                                    } else {
                                        view?.showToast(it.message)
                                    }
                                    it.data.userToken = it.data.userToken
                                    preferences.setUserInfo(it.data)
                                    Log.d("token", " ----------> " + preferences.getUserInfo().userToken)
                                    if (it.data.isNewUser) {
                                        view?.openInvite(it.data.userName)
                                    } else {
                                        view?.hideBirthdayProgress()
                                        view?.openHome(it.data.userName)
                                    }
                                } else {
                                    view?.hideBirthdayProgress()
                                    if (App.isChineseLanguage) {
                                        view?.showToast(it.zhHant)
                                    } else {
                                        view?.showToast(it.message)
                                    }
                                }
                            }
                        }, {
                            view?.hideBirthdayProgress()
                            view?.showToast(getString(R.string.connection_error))
                            Log.d("errors", it.message)
                        })
            } else {
                view?.showToast(getString(R.string.not_available))
                return
            }
            Log.d("address", "->$addresses")
        } else {
            view?.showToast(getString(R.string.connection_error))
        }
    }


    interface LoginView : BaseView {
        fun openSelectBirthday()
        fun openTermsConditions()
        fun openPhoneLogin()
        fun openFbPage()
        fun openHome(userName: String)
        fun openGooglePlus()
        fun openInstagram()
        fun openInvite(userName: String)
        fun hideBirthdayProgress()
        fun showBirthdayProgress()
    }
}