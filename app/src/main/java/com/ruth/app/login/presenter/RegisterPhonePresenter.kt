package com.ruth.app.login.presenter

import android.util.Log
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.network.config.RuthApi
import com.ruth.app.network.module.LoginManager
import java.util.*
import javax.inject.Inject

class RegisterPhonePresenter @Inject constructor(private val loginManager: LoginManager,
                                                 private val ruthApi: RuthApi) : BasePresenter<RegisterPhonePresenter.RegisterPhoneView>() {

    override fun onCreate(view: RegisterPhoneView) {
        super.onCreate(view)
    }

    fun openLogin() {
        view?.openLogin()
    }

    fun openTermsConditionDialog() {
        view?.openPopupDialog()
    }

    fun onClickBack() {
        view?.backPage()
    }

    fun sendOTP(username: String, phoneNumber: String) {

        if (username.isEmpty()) {
            view?.showToast(context.getString(R.string.enter_username))
            view?.stopTimer()
            return
        }

        if (phoneNumber.isEmpty()) {
            view?.showToast(context.getString(R.string.enter_phone_number))
            view?.stopTimer()
            return
        }

        if (phoneNumber.length < 8) {
            view?.showToast(context.getString(R.string.enter_correct_phone_number))
            view?.stopTimer()
            return
        }

        val isoCode = "852"/*utility.GetCountryZipCode(context)*/
//        if (isoCode.contentEquals("1")) {
//            isoCode = "91"
//        }

        utility.setStringPreferences(context, getString(R.string.isocode), isoCode)
        val phone = utility.getStringPreferences(context, getString(R.string.isocode)) + phoneNumber
        val hm: HashMap<String, String> = HashMap()
        hm["mobileNo"] = phone
        hm["type"] = "registration"
        hm["userName"] = username

        utility.setStringPreferences(context, getString(R.string.mobile_number), phone)
        loginManager.sendOTP(hm)
                .doAfterTerminate { view?.hideRegisterProgress() }
                .doOnSubscribe { view?.showRegisterProgress() }
                .subscribe({
                    Log.d("send", " -> " + it.message)
                    if (!it.status) {
                        view?.hideRegisterProgress()
                        if (App.isChineseLanguage) {
                            view?.showToast(it.zh_Hant)
                        } else {
                            view?.showToast(it.message)
                        }
                        view?.stopTimer()
                    } else {
                        view?.hideRegisterProgress()
                    }
                }, {
                    logError(it)
                    view?.hideRegisterProgress()
                    view?.stopTimer()
                    view?.showToast(context.getString(R.string.connection_error))
                })
    }

    fun verifyOTP(phoneNumber: String, otp: String) {
        if (otp.isEmpty()) {
            view?.showToast(context.getString(R.string.enter_otp))
            return
        }

        if (phoneNumber.isEmpty() || phoneNumber.length < 8) {
            view?.showToast(context.getString(R.string.enter_correct_phone_number))
            return
        }

        var isoCode = utility.getStringPreferences(context, getString(R.string.isocode))
//        if (isoCode.contentEquals("1")) {
//            isoCode = "91"
//        }
        val phone = isoCode + phoneNumber
        val hm: HashMap<String, String> = HashMap()
        hm["mobileNo"] = phone
        hm["otp"] = otp

        loginManager.verifyOTP(hm)
                .doAfterTerminate { view?.hideRegisterProgress() }
                .doOnSubscribe { view?.showRegisterProgress() }
                .subscribe({
                    Log.d("verify", " -> " + it.message)
                    view?.hideRegisterProgress()
                    if (it.status) {
                        view?.userVerified()
                    } else {
                        view?.hideRegisterProgress()
                        if (App.isChineseLanguage) {
                            view?.showToast(it.zh_Hant)
                        } else {
                            view?.showToast(it.message)
                        }
                    }
                }, {
                    logError(it)
                    view?.hideRegisterProgress()
                    view?.showToast(context.getString(R.string.connection_error))
                })
    }

    interface RegisterPhoneView : BasePresenter.BaseView {
        fun openLogin()
        fun openPopupDialog()
        fun backPage()
        fun userVerified()
        fun stopTimer()
        fun startTime()
        fun hideRegisterProgress()
        fun showRegisterProgress()
    }
}