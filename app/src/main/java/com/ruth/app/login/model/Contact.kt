package com.ruth.app.login.model

class Contact(var _ID: String?,
              var name: String?,
              var profileUrl: String?,
              var pictureUrl: String?)