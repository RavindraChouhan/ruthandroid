package com.ruth.app.login.activity

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.home.adapter.SeeAllFriendsAdapter
import com.ruth.app.login.presenter.RecommendedFriendsPresenter
import com.ruth.app.model.RecommandedUserDetail
import com.ruth.app.tutorial.activity.TutorialActivity
import kotlinx.android.synthetic.main.activity_recommended_friends.*

class RecommendedFriendsActivity : BaseActivity<RecommendedFriendsPresenter>(), RecommendedFriendsPresenter.RecommendedFriendsView {

    private var adapter: SeeAllFriendsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recommended_friends)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        presenter.getRecommendedUsers()

        //setRecommendedFriendsList()

        ivNextRecommended.setOnClickListener {
            presenter.onNext()
        }

        ivBackRecommended.setOnClickListener {
            presenter.onBack()
        }
    }

    override fun onDestroy() {
        if (invalidDialog != null) {
            invalidDialog!!.dismiss()
            invalidDialog = null
        }
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun onBackPressed() {
//        super.onBackPressed()
        onBack()
    }

    /*--------------------------------------------------------------------------------------------*/
    override fun showLoadingProgress() {
        progressBarRecommended.visibility = View.VISIBLE
    }

    override fun hideLoadingProgress() {
        progressBarRecommended.visibility = View.GONE
    }

    private var invalidDialog: AlertDialog? = null

    override fun invalidLogin() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Invalid Login")
        builder.setMessage(getString(R.string.invalid_login_error))

        // add a button
        builder.setPositiveButton(getString(R.string.log_in)) { dialog, _ ->
            presenter.clearLogin()
            startActivity(Intent(this, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            finish()
            overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
            dialog.dismiss()
        }

        if (!isFinishing && window.decorView.isShown) {
            invalidDialog = builder.create()
            invalidDialog!!.setCancelable(false)
            invalidDialog!!.show()
        }
    }

    override fun setRecommendedUsers(data: ArrayList<RecommandedUserDetail>) {
        rvRecommendedList.layoutManager = GridLayoutManager(this, 3)
        adapter = SeeAllFriendsAdapter(this, data)
        rvRecommendedList.adapter = adapter
        adapter!!.setOnItemClickListener(object : SeeAllFriendsAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {

            }

            override fun onFollowClick(view: View, position: Int) {
                data[position].isFollow = !data[position].isFollow
                if (data[position].isFollow) {
                    data[position].followers = data[position].followers + 1
                } else {
                    data[position].followers = data[position].followers - 1
                }
                presenter.followUnfollow(data[position].id, data[position].isFollow)
                adapter!!.notifyItemChanged(position)
            }
        })
    }

    override fun onNext() {
        startActivity(Intent(this, TutorialActivity::class.java))
    }

    override fun onBack() {
        alert(getString(R.string.logged_out_error),
                "",
                getString(R.string.ok_btn),
                getString(R.string.cancel),
                {
                    presenter.clearLogin()
                    startActivity(Intent(this, LoginActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
                    finish()
                    overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
                }, {})
    }

    /*--------------------------------------------------------------------------------------------*/

}