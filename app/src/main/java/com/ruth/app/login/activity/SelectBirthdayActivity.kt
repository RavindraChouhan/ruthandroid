package com.ruth.app.login.activity

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Scope
import com.google.android.gms.tasks.Task
import com.google.api.services.people.v1.PeopleScopes
import com.ruth.app.App
import com.ruth.app.BuildConfig
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.home.activity.HomeActivity
import com.ruth.app.login.model.fbfriends.FacebookFriends
import com.ruth.app.login.presenter.SelectBirthdayPresenter
import com.ruth.app.utility.Constants
import com.ruth.app.widgets.datepicker.DatePickerPopWin
import com.steelkiwi.instagramhelper.InstagramHelper
import com.steelkiwi.instagramhelper.InstagramHelperConstants
import kotlinx.android.synthetic.main.activity_birthday_screen.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.URL
import java.util.*

class SelectBirthdayActivity : BaseActivity<SelectBirthdayPresenter>(), SelectBirthdayPresenter.SelectBirthdayView, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private var thisAYear: Int = 0
    private var thisADay: Int = 0
    private var thisAMonth: Int = 0
    private var loginType: String = ""
    private var callbackManager: CallbackManager? = null
    private var instagramHelper: InstagramHelper? = null
    internal val RC_INTENT = 200
    private var photoUrl: String? = ""

    companion object {
        var selectedDob: String = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_birthday_screen)
        App.getComponent().inject(this)
        presenter.onCreate(this)
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)

        googlePlusSignIn()

        instagramHelper = App.getInstagramHelper()

        layoutBirthday.setOnClickListener {
            presenter.openDatePicker()
        }

        ivBirthdayBack.setOnClickListener {
            presenter.onClickBack()
        }

        btnNextBirthday.setOnClickListener {
            if (tvBirthdate.text.toString().isEmpty()) {
                Toast.makeText(this, getString(R.string.birthday_empty), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            loginType = intent.getStringExtra("isFrom")
            presenter.openView(loginType)
        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun openDatePicker() {
        val c = Calendar.getInstance()
        thisAYear = c.get(Calendar.YEAR) - 15
        thisADay = c.get(Calendar.DAY_OF_MONTH)
        thisAMonth = c.get(Calendar.MONTH)

        val currentDate = "" + thisAYear + "-" + (thisAMonth + 1) + "-" + thisADay
        val pickerPopWin = DatePickerPopWin.Builder(this, DatePickerPopWin.OnDatePickedListener { year, month, day, _ ->
            if (year > Calendar.getInstance().get(Calendar.YEAR)) {
                Toast.makeText(this@SelectBirthdayActivity, "Please select correct date", Toast.LENGTH_SHORT).show()
                return@OnDatePickedListener
            }

            if (Calendar.getInstance().get(Calendar.YEAR) - year < 18) {
                if (!tvBirthdate.text.isEmpty()) {
                    tvBirthdate.text = ""
                }
                btnNextBirthday.background = resources.getDrawable(R.drawable.imageview_selector)
                presenter.showErrorPopup()
            } else {
                val monthInt = utility.monthNameToInt(month)
                tvBirthdate.text = "$day/$monthInt/$year"
                selectedDob = "$day/$monthInt/$year"
                btnNextBirthday.background = resources.getDrawable(R.drawable.imageview_selector_green)
            }

        }).textConfirm(getString(R.string.done)) //text of confirm button
                .btnTextSize(16) // button text size
                .viewTextSize(40) // pick view text size
                .colorConfirm(Color.parseColor("#3183fd"))//color of confirm button
                .minYear(1900) //min year in loop
                .maxYear(c.get(Calendar.YEAR) + 1) // max year in loop
                .dateChose(currentDate) // date chose when init popwindow
                .showDayMonthYear(true)
                .build()
        pickerPopWin.showPopWin(this)
    }

    override fun openPhoneLogin() {
        startActivity(Intent(this, RegisterPhoneActivity::class.java))
    }

    override fun openHome(userName: String) {

        progressBarLoading.visibility = View.GONE
        progressBarLoading.isTouchable = true

        startActivity(Intent(this, HomeActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
        finish()
    }

    override fun openInvite(userName: String) {
        progressBarLoading.visibility = View.GONE
        progressBarLoading.isTouchable = true

        startActivity(Intent(this, InviteFriendsActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .putExtra("header", loginType))
        finish()
    }

    override fun hideBirthdayProgress() {
        progressBarLoading.visibility = View.GONE
        progressBarLoading.isTouchable = true
    }

    override fun showBirthdayProgress() {
        progressBarLoading.visibility = View.VISIBLE
        progressBarLoading.isTouchable = false

    }

    override fun backLogin() {
//        startActivity(Intent(this, LoginActivity::class.java))
        finish()
        overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
    }

    override fun showErrorPopup() {
        val dialogBuilder = Dialog(this)
        dialogBuilder.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogBuilder.setContentView(R.layout.popup_birthday_error)
        dialogBuilder.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val closeTermsCondition = dialogBuilder.findViewById<View>(R.id.ivBirthdayDone) as ImageView

        closeTermsCondition.setOnClickListener {
            dialogBuilder.dismiss()
        }
        dialogBuilder.show()

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)

        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(dialogBuilder.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.6f).toInt()
        val dialogWindowHeight = (displayHeight * 0.7f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        dialogBuilder.window.attributes = layoutParams
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
//        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    private lateinit var profilePicture: URL

    override fun openFbPage() {
        progressBarLoading.visibility = View.GONE
        progressBarLoading.isTouchable = true
        LoginManager.getInstance().logOut()
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile, email, user_birthday,user_friends,user_location, user_gender"))
        LoginManager.getInstance().registerCallback(callbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) {
                        val request = GraphRequest.newMeRequest(
                                loginResult.accessToken, GraphRequest.GraphJSONObjectCallback { `object`, response ->
                            //                            getFacebookFriends(loginResult.accessToken)

                            try {
                                var userId = response.jsonObject.getString("id")
                                profilePicture = URL("https://graph.facebook.com/$userId/picture?width=500&height=500")
                                var gender: String
                                if (response.jsonObject.has("gender")) {
                                    gender = response.jsonObject.getString("gender")
                                    Log.d("Facebook", " -> $profilePicture, gender - $gender")
                                }
                                Log.d("Facebook", " ->$profilePicture")

                            } catch (e: Exception) {
                                Log.d("exception", " -> " + e.message)
                            }

                            presenter.createNewUserPic(response.jsonObject.getString("nickname"),
                                    response.jsonObject.getString("email"),
                                    response.jsonObject.getString("id"), "facebook",
                                    profilePicture.toString())
                            utility.setBooleanPreferences(presenter.context, getString(R.string.isfbSignup), true)
                        })
                        val parameters = Bundle()
                        parameters.putString("fields", "id,nickname,email,gender, birthday")
                        request.parameters = parameters
                        request.executeAsync()
                    }

                    override fun onCancel() {
                        progressBarLoading.visibility = View.GONE
                        progressBarLoading.isTouchable = true
                        Toast.makeText(this@SelectBirthdayActivity, getString(R.string.try_again), Toast.LENGTH_SHORT).show()
                    }

                    override fun onError(error: FacebookException) {
                        progressBarLoading.visibility = View.GONE
                        progressBarLoading.isTouchable = true
                        Toast.makeText(this@SelectBirthdayActivity, getString(R.string.try_again), Toast.LENGTH_SHORT).show()
                    }
                })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == InstagramHelperConstants.INSTA_LOGIN && resultCode == Activity.RESULT_OK) {
            val user = instagramHelper!!.getInstagramUser(this)

//            Log.d("instagram", "token -> " + SharedPrefUtils.getToken(this@SelectBirthdayActivity) + "" +
//                    ", user id -> " + user.data.id)
            //getInstagramFriends(SharedPrefUtils.getToken(this@SelectBirthdayActivity), user.data.id)
            presenter.createNewUser(user.data.username, "", user.data.id!!, "instagram")
            utility.setBooleanPreferences(presenter.context, getString(R.string.isInstaSignup), true)
        } else if (requestCode == RC_INTENT) {
            Log.d("google+", "sign in result")
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            if (result.isSuccess) {
                App.acct = result.signInAccount
            } else {
                Toast.makeText(this, "Please try again", Toast.LENGTH_SHORT).show()
                //  Log.d("google+", result.status.toString() + "\nmsg: " + result.status.statusMessage)
            }
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        } else {
            callbackManager?.onActivityResult(requestCode, resultCode, data)
        }
    }


    override fun openInstagram() {
        instagramHelper!!.loginFromActivity(this)
    }

    private fun getInstagramFriends(token: String?, id: String?) {
        val call = Constants.instagramService.getInstaFriends(id!!, token!!)
        call.enqueue(object : Callback<FacebookFriends> {

            override fun onResponse(call: Call<FacebookFriends>, response: Response<FacebookFriends>) {
//                if (response.body()!!.data!!.isNotEmpty()) {
//                    Log.d("facebook", "list -> " + response.body()!!.data?.size)
//                    presenter.saveFbFriends(response.body()!!.data)
//                } else {
//                    Log.d("facebook", "No facebook friends..")
//                }
            }

            override fun onFailure(call: Call<FacebookFriends>?, t: Throwable?) {

            }
        })
    }

    private fun getFacebookFriends(accessToken: AccessToken) {
        val call = Constants.facebookService.getFriends("Bearer " + accessToken, "picture, nickname")
        call.enqueue(object : Callback<FacebookFriends> {
            override fun onResponse(call: Call<FacebookFriends>, response: Response<FacebookFriends>) {
                if (response.body()!!.data!!.isNotEmpty()) {
                    Log.d("facebook", "list -> " + response.body()!!.data?.size)
                    presenter.saveFbFriends(response.body()!!.data)
                } else {
                    Log.d("facebook", "No facebook friends..")
                }
            }

            override fun onFailure(call: Call<FacebookFriends>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

/*---------------------------Google plus login-----------*/

    override fun openGooglePlus() {
        progressBarLoading.visibility = View.VISIBLE
        progressBarLoading.isTouchable = false
        getIdToken()
//        val account = GoogleSignIn.getLastSignedInAccount(this)
//        if (account != null && !account.isExpired) {
//            //App.acct = account.serverAuthCode
//            presenter.createNewUser(account.displayName!!, account.email!!, account.id!!, "googleplus")
//        } else {
//            getIdToken()
//        }
    }

    private var mGoogleApiClient: GoogleApiClient? = null

    override fun onConnected(p0: Bundle?) {
    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }

    private fun googlePlusSignIn() {
        val signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                // The serverClientId is an OAuth 2.0 web client ID
                .requestServerAuthCode(BuildConfig.GOOGLE_SERVER_CODE)
                .requestEmail()
                .requestScopes(Scope(Scopes.PLUS_LOGIN),
                        Scope(PeopleScopes.CONTACTS_READONLY),
                        Scope(PeopleScopes.USER_EMAILS_READ),
                        Scope(PeopleScopes.USERINFO_EMAIL),
                        Scope(PeopleScopes.USER_PHONENUMBERS_READ))
                .build()


        // To connect with Google Play Services and Sign In
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions)
                .build()
    }


    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            if (completedTask != null) {
                val account: GoogleSignInAccount?
                try {
                    account = completedTask.result
                    if (account != null) {
                        progressBarLoading.visibility = View.GONE
                        progressBarLoading.isTouchable = true
                        if (account.photoUrl != null) {
                            photoUrl = account.photoUrl.toString()
                        }
                        //   presenter.createNewUser(account.displayName!!, account.email!!, account.id!!, "googleplus")
                        presenter.createNewUserPic(account.displayName!!, account.email!!, account.id!!, "googleplus", photoUrl!!)
                        utility.setBooleanPreferences(presenter.context, getString(R.string.isGoogleSignup), true)
                    }
                } catch (e: RuntimeException) {
                    progressBarLoading.visibility = View.GONE
                    progressBarLoading.isTouchable = true
                    // Log.w("google+", "signInResult:failed code=" + e.message)
                }
            }
        } catch (e: ApiException) {
            e.printStackTrace()
            //  Log.w("google+", "signInResult:failed code=" + e.statusCode)
        }
    }

    override fun onStart() {
        super.onStart()
        mGoogleApiClient?.connect()
    }

    // Performed on Google Sign in click
    private fun getIdToken() {
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        startActivityForResult(signInIntent, RC_INTENT)
    }
}