package com.ruth.app.login.model

import android.graphics.drawable.Drawable

class Friends(var name: String?,
              var email: String?,
              var phoneNumber: String?,
              var userImage: Drawable? = null,
              var selected: Boolean = true)