//package com.ruth.app.login.adapter
//
//import android.annotation.SuppressLint
//import android.content.Context
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.widget.BaseAdapter
//import com.ruth.app.R
//import com.ruth.app.login.model.RecommendedFriends
//import kotlinx.android.synthetic.main.item_recommended_friends.view.*
//
//class RecommendGridAdapter : BaseAdapter {
//    var recommendedFriends = ArrayList<RecommendedFriends>()
//    var context: Context? = null
//
//    constructor(context: Context, recommendedFriends: ArrayList<RecommendedFriends>?) : super() {
//        this.context = context
//        this.recommendedFriends = recommendedFriends!!
//    }
//
//    override fun getCount(): Int {
//        return recommendedFriends.size
//    }
//
//    override fun getItem(position: Int): Any {
//        return recommendedFriends[position]
//    }
//
//    override fun getItemId(position: Int): Long {
//        return position.toLong()
//    }
//
//    @SuppressLint("SetTextI18n")
//    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
//        val friends = this.recommendedFriends[position]
//
//        val inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//        val itemView = inflater.inflate(R.layout.item_recommended_friends, null)
//
////        Glide.with(context)
////                .load(tutorialList!![position].url)
////                .listener(object : RequestListener<String, GlideDrawable> {
////                    override fun onException(e: Exception, model: String, target: com.bumptech.glide.request.target.Target<GlideDrawable>, isFirstResource: Boolean): Boolean {
////                        return false
////                    }
////
////                    override fun onResourceReady(resource: GlideDrawable, model: String, target: Target<GlideDrawable>, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
////                        progressBarTutorial.visibility = View.GONE
////                        return false
////                    }
////                })
////                .into(ivImage)
//
//        if (friends.isSelected) {
//            itemView.layoutUnselected.visibility = View.GONE
//            itemView.layoutSelected.visibility = View.VISIBLE
//        } else {
//            itemView.layoutUnselected.visibility = View.VISIBLE
//            itemView.layoutSelected.visibility = View.GONE
//        }
//
//        if (friends.nickname.equals("Empty", true)) {
//            itemView.layoutEmpty.visibility = View.VISIBLE
//            itemView.layoutRecommend.visibility = View.GONE
//        } else {
//            itemView.layoutEmpty.visibility = View.GONE
//            itemView.layoutRecommend.visibility = View.VISIBLE
//
//            itemView.ivRecommendUser.setImageDrawable(friends.userImage)
//
//            val nickname = friends.nickname!!.split(" ")
//            itemView.tvRecommendName.text = friends.nickname!!.elementAt(0).toString()
//            itemView.tvRecommendNameSecond.text = friends.nickname!!.elementAt(1).toString()
//            itemView.tvRecommendNameThird.text = friends.nickname!!.elementAt(2).toString()
//
//            itemView.tvRecommendAccountNo.text = friends.accountNo
//            itemView.tvRecommendFans.text = friends.fansCount.toString()
//            itemView.tvRecommendGender.text = friends.gender
//
//            val insertDate = friends.registeredDate
//            val items1 = insertDate!!.split("/")
//            val d1 = items1[0]
//            val m1 = items1[1]
//            val y1 = items1[2]
//            val d = Integer.parseInt(d1)
//            val m = Integer.parseInt(m1)
//            val y = Integer.parseInt(y1)
//
////            itemView.tvRecommendRegisteredDay.text = d1
////            itemView.tvRecommendRegisteredMonth.text = m1
////            itemView.tvRecommendRegisteredYear.text = y1.elementAt(0).toString()+ "" +y1.elementAt(1).toString()
////             itemView.tvRecommendYearEnd.text = y1.elementAt(2).toString()+ "" +y1.elementAt(3).toString()
//
//            val currentString = friends.districtName1
//            val separated = currentString?.split(" ".toRegex())?.dropLastWhile { it.isEmpty() }?.toTypedArray()
//
//            if (separated != null) {
//                itemView.tvRecommendFirstPlace.text = separated[0].trim()
//                itemView.tvRecommendSecondPlace.text = separated[1].trim()
//                itemView.tvRecommendThirdPlace.text = separated[2].trim()
//                itemView.tvRecommendFourthPlace.text = separated[3].trim()
//            }
//
//            itemView.layoutRecommend.setOnClickListener(View.OnClickListener {
//                recommendedFriends[position].isSelected = !recommendedFriends[position].isSelected
//                notifyDataSetChanged()
//            })
//        }
//        return itemView
//    }
//}