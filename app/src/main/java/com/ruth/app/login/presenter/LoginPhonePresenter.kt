package com.ruth.app.login.presenter

import android.annotation.SuppressLint
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.network.config.RuthApi
import java.util.*
import javax.inject.Inject

class LoginPhonePresenter @Inject constructor(private val ruthApi: RuthApi) : BasePresenter<LoginPhonePresenter.LoginPhoneView>() {

    override fun onCreate(view: LoginPhoneView) {
        super.onCreate(view)
    }

    fun openRegisterPhone() {
        view?.openPhoneRegister()
    }

    fun goBack() {
        view?.goBack()
    }

    fun forgotPassword() {
        view?.forgotPassword()
    }

    fun getLocation(locationManager: LocationManager?) {
        try {
            locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, locationListener)
        } catch (ex: SecurityException) {
            Log.d("myTag", "Security Exception, no location available")
        }
    }

    fun loginUser(mobileNo: String, password: String) {
        if (mobileNo.isEmpty()) {
            view?.showToast(context.getString(R.string.enter_phone_number))
            return
        }

        if (password.isEmpty()) {
            view?.showToast(context.getString(R.string.enter_password))
            return
        }

        if (mobileNo.length < 8) {
            view?.showToast(context.getString(R.string.enter_correct_phone_number))
            return
        }

        var isoCode = "852"/*utility.GetCountryZipCode(context)*/
//        if (isoCode.contentEquals("1")) {
//            isoCode = "91"
//        }

        utility.setStringPreferences(context, getString(R.string.isocode), isoCode)
        val phone = utility.getStringPreferences(context, getString(R.string.isocode)) + mobileNo

        val hm: HashMap<String, String> = HashMap()
        hm["mobileNo"] = phone
        hm["password"] = password
        if (preferences.getDeviceId() != null){
            hm["deviceToken"] = preferences.getDeviceId().toString()
        }else{
            hm["deviceToken"] = FirebaseInstanceId.getInstance().token!!
        }
        hm["deviceType"] = "Android"

        ruthApi.loginPhoneNo(hm)
                .doOnTerminate { view?.hideLoginProgress() }
                .doOnSubscribe { view?.showLoginProgress() }
                .subscribe({
                    Log.d("status", "-> " + it.message)
                    if (it != null) {
                        if (it.status) {
                            Log.d("token", " -> " + it.token +
                                    " id -> " + it.data.id)
                            if (App.isChineseLanguage) {
                                view?.showToast(it.zhHant)
                            } else {
                                view?.showToast(it.message)
                            }
                            it.data.userToken = it.token
                            preferences.setUserInfo(it.data)
                            getMyProfile()
                            view?.loginUser(preferences.getUserInfo().userName)
                        } else {
                            if (App.isChineseLanguage) {
                                view?.showToast(it.zhHant)
                            } else {
                                view?.showToast(it.message)
                            }
                        }
                    }
                }, {
                    logError(it)
                    view?.hideLoginProgress()
                    view?.showToast(context.getString(R.string.connection_error))
                })
    }

    private val locationListener: LocationListener = object : LocationListener {
        @SuppressLint("MissingPermission")
        override fun onLocationChanged(location: Location) {
            utility.setDoublePreferences(context, getString(R.string.lat), location.latitude)
            utility.setDoublePreferences(context, getString(R.string.lng), location.longitude)
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

    private fun getMyProfile() {
        ruthApi.getUserInfo(preferences.getUserInfo().userToken, preferences.getUserInfo().id)
                .subscribe({
                    if (it != null) {
                        if (it.status) {
                            preferences.setUserProfile(it.data)
                        } else {
                            // view?.invalidLogin()
                        }
                    }
                }, {
                    view?.showToast(getString(R.string.connection_error))
                })
    }


    interface LoginPhoneView : BaseView {
        fun openPhoneRegister()
        fun goBack()
        fun forgotPassword()
        fun loginUser(userName: String?)
        fun hideLoginProgress()
        fun showLoginProgress()
    }
}