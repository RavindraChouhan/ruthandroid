package com.ruth.app.login.activity

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.widget.Toast
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.login.presenter.ResetPasswordPresenter
import kotlinx.android.synthetic.main.activity_reset_password.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class ResetPasswordActivity : BaseActivity<ResetPasswordPresenter>(), ResetPasswordPresenter.ResetPasswordView {

    private var resetToken: String? = null
    private var confirmPasswordEnable: Boolean = false
    private var passwordEnable: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)
        App.getComponent().inject(this)
        presenter.onCreate(this)
        resetToken = intent.getStringExtra("token")
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)

        etNewPassword.setOnTouchListener { v, event ->
            etNewPassword.isCursorVisible = true
            false
        }

        etConfirmPassword.setOnTouchListener { v, event ->
            etConfirmPassword.isCursorVisible = true
            false
        }

        passwordHideShow.setOnClickListener {
            passwordHideShow.switchState()
            passwordEnable = !passwordEnable

            if (passwordEnable) {
                etNewPassword.transformationMethod = null
                etNewPassword.setSelection(etNewPassword.length())
            } else {
                etNewPassword.transformationMethod = PasswordTransformationMethod()
                etNewPassword.setSelection(etNewPassword.length())
            }
        }

        re_passwordHideShow.setOnClickListener {
            re_passwordHideShow.switchState()
            confirmPasswordEnable = !confirmPasswordEnable

            if (confirmPasswordEnable) {
                etConfirmPassword.transformationMethod = null
                etConfirmPassword.setSelection(etConfirmPassword.length())
            } else {
                etConfirmPassword.transformationMethod = PasswordTransformationMethod()
                etConfirmPassword.setSelection(etConfirmPassword.length())
            }
        }

        btnResetPassword.setOnClickListener {
            if (etNewPassword.text.toString().isEmpty()) {
                Toast.makeText(this, getString(R.string.enter_password), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (etConfirmPassword.text.toString().isEmpty()) {
                Toast.makeText(this, getString(R.string.enter_confirm_password), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (etNewPassword.length() < 6) {
                Toast.makeText(this, getString(R.string.password_lenth_error), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!isValidPassword(etNewPassword.text.toString())) {
                Toast.makeText(this, getString(R.string.password_check), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!etNewPassword.text.toString().equals(etConfirmPassword.text.toString())) {
                Toast.makeText(this, getString(R.string.password_not_same), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            hideKeyboard()
            presenter.resetPassword(etNewPassword.text.toString(), etConfirmPassword!!.text.toString(), resetToken!!)
        }

        ivBackResetPassword.setOnClickListener {
            presenter.onClickBack()
        }

        etNewPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (etNewPassword.length() >= 6 && etConfirmPassword.length() >= 6) {
                    btnResetPassword.background = resources.getDrawable(R.drawable.imageview_selector_green)
                } else {
                    btnResetPassword.background = resources.getDrawable(R.drawable.imageview_selector)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        etConfirmPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (etConfirmPassword.length() >= 6 && etNewPassword.length() >= 6) {
                    btnResetPassword.background = resources.getDrawable(R.drawable.imageview_selector_green)
                } else {
                    btnResetPassword.background = resources.getDrawable(R.drawable.imageview_selector)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

    }

    override fun backPage() {
        finish()
        overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun resetDone() {
        startActivity(Intent(this@ResetPasswordActivity, LoginPhoneActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        finish()
    }

    fun isValidPassword(password: String): Boolean {
        val matcher: Matcher
        val PASSWORD_PATTERN = "^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}\$"
        val pattern = Pattern.compile(PASSWORD_PATTERN)
        //  pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(password)
        return matcher.matches()
    }
}