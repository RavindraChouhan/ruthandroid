package com.ruth.app.login.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import com.ruth.app.login.model.Friends
import kotlinx.android.synthetic.main.item_invite_friends.view.*

class InviteFriendsAdapter(private var context: Context,
                           private var friendsList: ArrayList<Friends>?)
    : RecyclerView.Adapter<InviteFriendsAdapter.FriendsViewHolder>() {

    private lateinit var recyclerView: RecyclerView

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FriendsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_invite_friends, parent, false)
        return FriendsViewHolder(view)
    }

    override fun onBindViewHolder(holder: FriendsViewHolder, position: Int) {

        holder.bind(friendsList?.get(position)!!)

        if (friendsList!![position].selected) {
            holder.itemView.radioSelect.setImageResource(R.drawable.green_tick)
            friendsList!![position].selected = true
        } else {
            friendsList!![position].selected = false
            holder.itemView.radioSelect.setImageResource(R.drawable.unselected_friend_bg)
        }
    }

    override fun getItemCount(): Int {
        return friendsList?.size!!
    }

    inner class FriendsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(friends: Friends) {

            itemView.setOnClickListener {
                friends.selected = !friends.selected
                checkSelectedUnselectedData(friends)
            }

            itemView.tvName.text = friends.name
            if (friends.phoneNumber == null || friends.phoneNumber == "") {
                itemView.tvPhone.visibility = View.GONE
            } else {
                itemView.tvPhone.visibility = View.VISIBLE
                itemView.tvPhone.text = friends.phoneNumber
            }

            if (friends.email == null || friends.email == "") {
                itemView.tvEmail.visibility = View.GONE
            } else {
                itemView.tvEmail.visibility = View.VISIBLE
                itemView.tvEmail.text = friends.email
            }

            if (friends.userImage == null) {
                itemView.ivUserImage.visibility = View.GONE
            } else {
                itemView.ivUserImage.visibility = View.VISIBLE
            }
        }

        private fun checkSelectedUnselectedData(friends: Friends) {
            if (friendsList!![adapterPosition].name.equals(context.getString(R.string.all), true)) {
                for (i in 0 until friendsList?.size!!) {
                    friendsList!![i].selected = friends.selected
                    notifyItemChanged(i)
                }
            } else {
                for (i in 0 until friendsList?.size!!) {
                    if (friendsList!![i].name.equals(context.getString(R.string.all), true)) {
                        friendsList!![i].selected = false
                    }
                    notifyItemChanged(i, friendsList!![i])
                    break
                }
                notifyItemChanged(adapterPosition, friends)
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int, answers: ArrayList<Friends>)
    }
}
