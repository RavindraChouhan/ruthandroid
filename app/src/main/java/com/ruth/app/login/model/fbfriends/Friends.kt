package com.ruth.app.login.model.fbfriends

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Friends(@SerializedName("picture")
        @Expose
        var picture: ProfilePicture? = null,
        @SerializedName("nickname")
        @Expose
        var name: String? = null,
        @SerializedName("id")
        @Expose
        var id: String? = null,
        var isSelected: Boolean = false
)

class ProfilePicture(
        @SerializedName("data")
        @Expose
        var data: Data? = null)

class Data(
        @SerializedName("height")
        @Expose
        var height: Int? = null,
        @SerializedName("is_silhouette")
        @Expose
        var isSilhouette: Boolean? = null,
        @SerializedName("url")
        @Expose
        var url: String? = null,
        @SerializedName("width")
        @Expose
        var width: Int? = null)

