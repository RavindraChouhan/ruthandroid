package com.ruth.app.login.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.ruth.app.R
import com.ruth.app.login.model.RecommendedFriends
import kotlinx.android.synthetic.main.item_recommended_users.view.*

class RecommendUsersAdapter : BaseAdapter {
    var recommendedFriends = ArrayList<RecommendedFriends>()
    var context: Context? = null

    constructor(context: Context, recommendedFriends: ArrayList<RecommendedFriends>?) : super() {
        this.context = context
        this.recommendedFriends = recommendedFriends!!
    }

    override fun getCount(): Int {
        return recommendedFriends.size
    }

    override fun getItem(position: Int): Any {
        return recommendedFriends[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("SetTextI18n", "ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val friends = this.recommendedFriends[position]

        val inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater.inflate(R.layout.item_recommended_users, null)

//        Glide.with(context)
//                .load(tutorialList!![position].url)
//                .listener(object : RequestListener<String, GlideDrawable> {
//                    override fun onException(e: Exception, model: String, target: com.bumptech.glide.request.target.Target<GlideDrawable>, isFirstResource: Boolean): Boolean {
//                        return false
//                    }
//                    override fun onResourceReady(resource: GlideDrawable, model: String, target: Target<GlideDrawable>, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
//                        progressBarTutorial.visibility = View.GONE
//                        return false
//                    }
//                })
//                .into(ivImage)

        if (friends.isSelected) {
            itemView.btnFollow.text = "Unfollow"
        } else {
            itemView.btnFollow.text = "Follow"
        }

        itemView.ivUserImage.setImageDrawable(friends.userImage)
        itemView.tvRecommendUserName.text = friends.name.toString()
        //itemView.tvRecommendAccountNo.text = friends.accountNo
        itemView.tvRecommendFans.text = friends.fansCount.toString()
        itemView.tvRecommendGender.text = friends.gender
        itemView.tvRegisterDate.text = friends.registeredDate
        itemView.tvRecommendAddress.text = friends.districtName1

        itemView.layoutRecommend.setOnClickListener(View.OnClickListener {
            recommendedFriends[position].isSelected = !recommendedFriends[position].isSelected
            notifyDataSetChanged()
        })

        return itemView
    }
}