package com.ruth.app.login.activity

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.home.activity.HomeActivity
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.login.presenter.SplashPresenter
import com.ruth.app.utility.RuthUtility
import java.util.*

class SplashActivity : BaseActivity<SplashPresenter>(), SplashPresenter.SplashView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        App.isChineseLanguage = true
        App.getComponent().inject(this)
        presenter.onCreate(this)
        setAppLanguage(this)
    }

    override fun openLogin() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    override fun openHome() {
        startActivity(Intent(this, HomeActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
        finish()
    }

    private fun setAppLanguage(mContext: Context) {
        val languageToLoad = "zh"

        //        if (isChineseLanguage) {
        //            languageToLoad = "zh";
        //        } else {
        //            languageToLoad = "en";
        //        }

        val locale = Locale(languageToLoad)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        mContext.resources.updateConfiguration(config, mContext.resources.displayMetrics)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}
