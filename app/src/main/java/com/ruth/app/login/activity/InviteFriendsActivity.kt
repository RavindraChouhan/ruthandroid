package com.ruth.app.login.activity

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.people.v1.People
import com.jakewharton.rxbinding2.widget.RxTextView
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.base.lifecycle.AndroidDisposable
import com.ruth.app.login.adapter.InviteFriendsAdapter
import com.ruth.app.login.model.Friends
import com.ruth.app.login.presenter.InviteFriendsPresenter
import kotlinx.android.synthetic.main.activity_invite_phone_friends.*
import java.io.IOException

class InviteFriendsActivity : BaseActivity<InviteFriendsPresenter>(), InviteFriendsPresenter.InviteFriendsView {

    private var friendsList: ArrayList<Friends>? = ArrayList()
    private var adapter: InviteFriendsAdapter? = null
    private val filtered: ArrayList<Friends>? = ArrayList()
    private var inviteFriendsType: String? = null
    private var google_serverCode: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invite_phone_friends)
        App.getComponent().inject(this)
        presenter.onCreate(this)
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)

        rvFriendsList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        inviteFriendsType = intent.getStringExtra("header")
        if (!intent.getStringExtra("header").isEmpty()) {
            when {
                intent.getStringExtra("header").equals("facebook", true) -> {
                    tvInviteHeader.text = getString(R.string.invite_fb_friends)
                    getFacebookFriends()
                }
                intent.getStringExtra("header").equals("phone", true) -> {
                    tvInviteHeader.text = getString(R.string.invite_friends)
                    getContactList()
                }
                intent.getStringExtra("header").equals("instagram", true) -> {
                    tvInviteHeader.text = getString(R.string.invite_friends)
                    getInstagramFriends()
                }
                intent.getStringExtra("header").equals("google", true) -> {
                    if (App.acct != null) {
                        utility.setStringPreferences(this, getString(R.string.google_account), App.acct?.serverAuthCode)
                        google_serverCode = App.acct!!.serverAuthCode
                    } else {
                        google_serverCode = utility.getStringPreferences(this, getString(R.string.google_account))
                    }

                    PeoplesAsync().execute(google_serverCode)
                    tvInviteHeader.text = getString(R.string.invite_google_friends)
                    getGooglePlusContactList()
                }
            }
        }

        layoutSearchFriends.setOnClickListener {
            layoutSearchFriends.visibility = View.GONE
            layoutSearchFriendsActive.visibility = View.VISIBLE
            etSearch.requestFocus()
            etSearch.isCursorVisible = true
            showKeyboard(etSearch)
        }

        tvSearchCancel.setOnClickListener {
            etSearch.text.clear()
            layoutSearchFriends.visibility = View.VISIBLE
            layoutSearchFriendsActive.visibility = View.GONE
            hideKeyboard()
        }

        skip.setOnClickListener {
            presenter.skipInvite()
        }

        tvNextInvite.setOnClickListener {
            presenter.openRecommended()
        }

        ivBackInvite.setOnClickListener {
            alert(getString(R.string.logged_out_error),
                    "",
                    getString(R.string.ok_btn),
                    getString(R.string.cancel),
                    {
                        presenter.clearLogin()
                        startActivity(Intent(this, LoginActivity::class.java)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
                        finish()
                        overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
                    }, {})
        }
    }

    private fun getInstagramFriends() {
        progressBarLoading.visibility = View.GONE
        tvEmptyText.visibility = View.VISIBLE
    }

    private fun getGooglePlusContactList() {
        if (App.acct == null) {
            progressBarLoading.visibility = View.GONE
            tvEmptyText.visibility = View.VISIBLE
        }
    }

    private fun getFacebookFriends() {
        progressBarLoading.visibility = View.GONE
        tvEmptyText.visibility = View.VISIBLE
    }

    override fun sendTextMessage() {

    }

    override fun openRecommended() {
        startActivity(Intent(this, RecommendedFriendsActivity::class.java))

//        startActivity(Intent(this, TutorialActivity::class.java))
    }

    override fun skipInvite() {
        startActivity(Intent(this, RecommendedFriendsActivity::class.java))
//        startActivity(Intent(this, HomeActivity::class.java)
//                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
//        finish()
    }

    private fun getContactList() {
        if (!friendsList!!.isEmpty()) {
            friendsList!!.clear()
        }

        friendsList?.add(0, Friends(getString(R.string.all), "", "", null))
        val phones = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC")
        while (phones!!.moveToNext()) {
            val name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
            val phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))

            val contactModel = Friends(name, "", phoneNumber, null)
            if (!friendsList!!.contains(contactModel)) {
                friendsList!!.add(contactModel)
            }
        }
        phones.close()

        if (friendsList!!.isEmpty()) {
            tvEmptyText.visibility = View.VISIBLE
            rvFriendsList.visibility = View.GONE
            progressBarLoading.visibility = View.GONE
        } else {
            tvEmptyText.visibility = View.GONE
            rvFriendsList.visibility = View.VISIBLE
            progressBarLoading.visibility = View.VISIBLE
        }

        adapter = InviteFriendsAdapter(this, friendsList!!)
        progressBarLoading.visibility = View.GONE
        rvFriendsList!!.adapter = adapter
    }


    override fun onStart() {
        super.onStart()

        AndroidDisposable.create(this, RxTextView.afterTextChangeEvents(etSearch).subscribe({ event ->
            val text = event.editable()?.toString().orEmpty()
            if (text.length >= 2) {
                filtered?.clear()
                friendsList?.filter {
                    it.name?.startsWith(text, true) ?: false
                }?.let { filtered?.addAll(it) }
                adapter = InviteFriendsAdapter(this, filtered!!)
                rvFriendsList!!.adapter = adapter
            } else {
                filtered?.clear()
                filtered?.addAll(friendsList!!)
                adapter = InviteFriendsAdapter(this, filtered!!)
                rvFriendsList!!.adapter = adapter
            }
        }, {}, {}, {}))
    }

    /*------------------------Google plus friends-----------------------------*/
    inner class PeoplesAsync : AsyncTask<String, Void, ArrayList<Friends>>() {
        override fun onPreExecute() {
            super.onPreExecute()
        }

        private var userName: String? = null
        private var userPhone: String? = null
        private var userEmail: String? = null

        override fun doInBackground(vararg params: String): ArrayList<Friends> {

            val googleFriendsList = ArrayList<Friends>()

            try {
                val peopleService = setUp(params[0])

                val response = peopleService.people().connections()
                        .list("people/me")
                        .setRequestMaskIncludeField("person.names,person.emailAddresses,person.phoneNumbers")
                        .execute()
                val connections = response.connections

                if (connections != null) {
                    googleFriendsList.add(0, Friends(getString(R.string.all), "", "", null))
                    for (person in connections) {
                        if (!person.isEmpty()) {
                            val names = person.names
                            val emailAddresses = person.emailAddresses
                            val phoneNumbers = person.phoneNumbers

                            if (phoneNumbers != null)
                                for (phoneNumber in phoneNumbers) {
                                    userPhone = phoneNumber.canonicalForm
                                    Log.d("google+", "phone: " + phoneNumber.value)
                                }

                            if (emailAddresses != null)
                                for (emailAddress in emailAddresses) {
                                    userEmail = emailAddress.value
                                    Log.d("google+", "email: " + emailAddress.value)
                                }

                            if (names != null)
                                for (name in names) {
                                    userName = name.displayName
                                }

                            if ((userName != null || userName != "") && (userEmail != null || userEmail != "")) {
                                val contactModel = Friends(
                                        userName,
                                        userEmail, "",
                                        resources.getDrawable(R.drawable.contact_placeholder))
                                googleFriendsList.add(contactModel)
                            }
                        }
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return googleFriendsList
        }

        private val APPLICATION_NAME = "Ruth"
        val clientId = "1065771002577-odus455hfaufkp427mqetonqlrarvee8.apps.googleusercontent.com"
        val clientSecret = "pWzoTfnNf2z5nNw3f61OjjEr"


        @Throws(IOException::class)
        fun setUp(serverAuthCode: String): People {
            val httpTransport = NetHttpTransport()
            val jsonFactory = JacksonFactory.getDefaultInstance()

            // Redirect URL for web based applications.
            // Can be empty too.
            val redirectUrl = "urn:ietf:wg:oauth:2.0:oob"

            // Exchange auth code for access token
            val tokenResponse = GoogleAuthorizationCodeTokenRequest(
                    httpTransport,
                    jsonFactory,
                    clientId,
                    clientSecret,
                    serverAuthCode,
                    redirectUrl).execute()
            // Then, create a GoogleCredential object using the tokens from GoogleTokenResponse
            val credential = GoogleCredential.Builder()
                    .setClientSecrets(clientId, clientSecret)
                    .setTransport(httpTransport)
                    .setJsonFactory(jsonFactory)
                    .build()

            credential.setFromTokenResponse(tokenResponse)

            // credential can then be used to access Google services
            return People.Builder(httpTransport, jsonFactory, credential)
                    .setApplicationName(APPLICATION_NAME)
                    .build()
        }

        override fun onPostExecute(googleList: ArrayList<Friends>) {
            super.onPostExecute(googleList)

            if (googleList.isEmpty()) {
                tvEmptyText.visibility = View.VISIBLE
                rvFriendsList.visibility = View.GONE
                progressBarLoading.visibility = View.GONE
            } else {
                tvEmptyText.visibility = View.GONE
                rvFriendsList.visibility = View.VISIBLE
                progressBarLoading.visibility = View.VISIBLE

                friendsList?.addAll(googleList)
                adapter = InviteFriendsAdapter(this@InviteFriendsActivity, googleList)
                progressBarLoading.visibility = View.GONE
                rvFriendsList.layoutManager = LinearLayoutManager(this@InviteFriendsActivity)
                rvFriendsList.adapter = adapter
            }
        }
    }

    override fun onBackPressed() {
//        super.onBackPressed()
        alert(getString(R.string.logged_out_error),
                "",
                getString(R.string.ok_btn),
                getString(R.string.cancel),
                {
                    presenter.clearLogin()
                    startActivity(Intent(this, LoginActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
                    finish()
                    overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
                }, {})
    }
}

