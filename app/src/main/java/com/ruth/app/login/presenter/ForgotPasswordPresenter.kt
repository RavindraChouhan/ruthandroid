package com.ruth.app.login.presenter

import android.util.Log
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.network.config.RuthApi
import java.util.*
import javax.inject.Inject

class ForgotPasswordPresenter @Inject constructor(private val ruthApi: RuthApi) : BasePresenter<ForgotPasswordPresenter.ForgotPasswordView>() {

    override fun onCreate(view: ForgotPasswordView) {
        super.onCreate(view)
    }

    fun goBack() {
        view?.goBack()
    }

    fun setForgotOTP(mobileNo: String) {

        if (mobileNo.isEmpty()) {
            view?.showToast(context.getString(R.string.enter_phone_number))
            view?.stopTimer()
            return
        }

        if (mobileNo.length < 8) {
            view?.showToast(context.getString(R.string.enter_correct_phone_number))
            view?.stopTimer()
            return
        }

        val isoCode = "852"/*utility.GetCountryZipCode(context)*/
        utility.setStringPreferences(context, getString(R.string.isocode), isoCode)

        val phone = isoCode + mobileNo

        val hm: HashMap<String, String> = HashMap()
        hm["mobileNo"] = phone
        hm["type"] = "forgot"

        ruthApi.sendOTP(hm)
                .subscribe({
                    Log.d("send", " -> " + it.message)
                    if (it != null) {
                        if (!it.status) {
                            view?.stopTimer()
                            if (App.isChineseLanguage) {
                                view?.showToast(it.zh_Hans)
                            } else {
                                view?.showToast(it.message)
                            }
                        }
                    }
                }, {
                    view?.stopTimer()
                    view?.showToast(context.getString(R.string.connection_error))
                })
    }

    fun verifyOTP(phoneNumber: String, otp: String) {
        if (otp.isEmpty()) {
            view?.showToast(context.getString(R.string.enter_otp))
            return
        }

        if (phoneNumber.isEmpty()) {
            view?.showToast(context.getString(R.string.enter_phone_number))
            return
        }

        if (phoneNumber.length < 8) {
            view?.showToast(context.getString(R.string.enter_correct_phone_number))
            return
        }

        var isoCode = utility.getStringPreferences(context, getString(R.string.isocode))
//        if (isoCode.contentEquals("1")) {
//            isoCode = "91"
//        }
        val phone = isoCode + phoneNumber
        val hm: HashMap<String, String> = HashMap()
        hm["mobileNo"] = phone
        hm["otp"] = otp

        ruthApi.verifyOTP(hm)
                .doAfterTerminate { view?.hideProgress() }
                .doOnSubscribe { view?.showProgress("Verifying", "Please Wait") }
                .subscribe({
                    Log.d("verify", " -> " + it.message)
                    if (it != null) {
                        if (it.status) {
                            view?.userVerified(phone)
                        } else {
                            if (App.isChineseLanguage) {
                                view?.showToast(it.zh_Hant)
                            } else {
                                view?.showToast(it.message)
                            }
                        }
                    }
                }, {
                    view?.hideProgress()
                    logError(it)
                    view?.showToast(context.getString(R.string.connection_error))
                })
    }

    fun getToken(phone: String) {
        val hm: HashMap<String, String> = HashMap()
        hm["mobileNo"] = phone
        ruthApi.forgotPasswordOTP(hm)
                .subscribe({
                    if (it != null) {
                        if (it.status) {
                            view?.resetPass(it.token)
                        }
                    }
                }, {
                    logError(it)
                    view?.showToast(context.getString(R.string.connection_error))
                })
    }

    interface ForgotPasswordView : BaseView {
        fun goBack()
        fun userVerified(phone: String)
        fun resetPass(token: String)
        fun stopTimer()
    }
}