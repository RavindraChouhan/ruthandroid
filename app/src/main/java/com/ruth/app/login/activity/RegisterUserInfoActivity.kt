package com.ruth.app.login.activity

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.widget.Toast
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.login.presenter.RegisterUserInfoPresenter
import com.ruth.app.utility.RuthUtility
import kotlinx.android.synthetic.main.activity_register_user_info.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class RegisterUserInfoActivity : BaseActivity<RegisterUserInfoPresenter>(), RegisterUserInfoPresenter.RegisterUserInfoView {

    private var passwordEnable: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_user_info)
        App.getComponent().inject(this)
        presenter.onCreate(this)
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)

        etEmail.setOnTouchListener { v, event ->
            etEmail.isCursorVisible = true
            false
        }

        if (!intent.getStringExtra("userName").isEmpty()) {
            etUserName.setText(intent.getStringExtra("userName"))
        }

        passwordHideShow.setOnClickListener {
            passwordHideShow.switchState()
            passwordEnable = !passwordEnable

            if (passwordEnable) {
                etPassword.transformationMethod = null
                etPassword.setSelection(etPassword.length())
            } else {
                etPassword.transformationMethod = PasswordTransformationMethod()
                etPassword.setSelection(etPassword.length())
            }
        }

        btnNextRegistered.setOnClickListener {

            if (etUserName.text.toString().isEmpty()) {
                Toast.makeText(this, getString(R.string.enter_username), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (etUserName.text.toString().length < 3) {
                Toast.makeText(this, getString(R.string.username_error), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!validUserName(etUserName.text.toString())) {
                Toast.makeText(this, getString(R.string.username_error), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (etPassword.length() < 6) {
                Toast.makeText(this, getString(R.string.password_lenth_error), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!isValidPassword(etPassword.text.toString())) {
                Toast.makeText(this, getString(R.string.password_check), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            hideKeyboard()
            presenter.registerUser(etUserName.text.toString(), etEmail.text.toString(), etPassword.text.toString())
        }

        ivBackRegister.setOnClickListener {
            presenter.onClickBack()
        }


        etUserName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (etUserName.length() >= 3 && etEmail.length() > 0 && etPassword.length() >= 6) {
                    btnNextRegistered.background = resources.getDrawable(R.drawable.imageview_selector_green)
                } else {
                    btnNextRegistered.background = resources.getDrawable(R.drawable.imageview_selector)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        etEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (etUserName.length() >= 3 && etEmail.length() > 0 && etPassword.length() >= 6) {
                    btnNextRegistered.background = resources.getDrawable(R.drawable.imageview_selector_green)
                } else {
                    btnNextRegistered.background = resources.getDrawable(R.drawable.imageview_selector)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        etPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (etUserName.length() >= 3 && etEmail.length() > 0 && etPassword.length() >= 6) {
                    btnNextRegistered.background = resources.getDrawable(R.drawable.imageview_selector_green)
                } else {
                    btnNextRegistered.background = resources.getDrawable(R.drawable.imageview_selector)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

    }

    private fun validUserName(password: String): Boolean {
        var validUserName: Boolean = true
        var counter = 0
        for (i in 0 until password.length) {
            if (Character.isLetter(password[i]))
                counter++
        }
        if (counter < 3) {
            validUserName = false
        }
        return validUserName
    }

    fun isValidPassword(password: String): Boolean {
        val matcher: Matcher
        val PASSWORD_PATTERN = "^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}\$"
        val pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(password)
        return matcher.matches()
    }

    override fun loginUser(userName: String) {
        startActivity(Intent(this, InviteFriendsActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra("header", "phone"))
        finish()
    }


    override fun hideRegisterProgress() {
        RuthUtility.hideLoader()
//        progressBarRegisterUser.visibility = View.GONE
    }

    override fun showRegisterProgress() {
        RuthUtility.showLoader(this@RegisterUserInfoActivity)
//        progressBarRegisterUser.visibility = View.VISIBLE
    }

    override fun backPage() {
        finish()
        overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
    }

    override fun redirectLogin() {
        startActivity(Intent(this, LoginActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
        finish()
    }
}