package com.ruth.app.login.model.fbfriends

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FacebookFriends(
        @SerializedName("data")
        @Expose
        var data: List<Friends>? = null,
        @SerializedName("paging")
        @Expose
        var paging: Paging,
        @SerializedName("summary")
        @Expose
        var summary: Summary)

class Paging(
        @SerializedName("cursors")
        @Expose
        var cursors: Cursors)

class Cursors(
        @SerializedName("before")
        @Expose
        var before: String,
        @SerializedName("after")
        @Expose
        var after: String)

class Summary(
        @SerializedName("total_count")
        @Expose
        var totalCount: Int)