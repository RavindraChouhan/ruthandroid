package com.ruth.app.login.activity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.support.annotation.RequiresApi
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.login.presenter.RegisterPhonePresenter
import com.ruth.app.utility.RuthUtility
import kotlinx.android.synthetic.main.activity_register_phone_number.*


class RegisterPhoneActivity : BaseActivity<RegisterPhonePresenter>(), RegisterPhonePresenter.RegisterPhoneView {

    private var isPaused = false
    private var isCancelled = false
    private var resumeFromMillis: Long = 0
    private var isClicked: Boolean = false

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_phone_number)

        val millisInFuture: Long = 60000
        val countDownInterval: Long = 1000

        App.getComponent().inject(this)
        presenter.onCreate(this)
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)


        customTextView(tvTermsCondition)

        etUserName.setOnTouchListener { v, event ->
            etUserName.isCursorVisible = true
            false
        }

        btnNextRegistered.setOnClickListener {
            stopTimer()
            // hideKeyboard()

            if (etUserName.text.toString().isEmpty()) {
                Toast.makeText(this, getString(R.string.enter_username), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (etUserName.text.toString().length < 3) {
                Toast.makeText(this, getString(R.string.username_error), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!validUserName(etUserName.text.toString())) {
                Toast.makeText(this, getString(R.string.username_error), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (etVerficationCode.text.toString().isEmpty()) {
                Toast.makeText(this, getString(R.string.enter_otp), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (etPhoneNumber.text.toString().isEmpty()) {
                Toast.makeText(this, getString(R.string.enter_phone_number), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (etUserName.text.toString().isEmpty()) {
                Toast.makeText(this, getString(R.string.enter_username), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            presenter.verifyOTP(etPhoneNumber.text.toString(), etVerficationCode.text.toString())
        }

        tvAlreadyRegistered.setOnClickListener {
            presenter.openLogin()
        }

        tvTermsCondition.setOnClickListener {
            presenter.openTermsConditionDialog()
        }

        ivBackRegister.setOnClickListener {
            presenter.onClickBack()
        }

        tvSendOtp.setOnClickListener {
            if (isCancelled) {
                isClicked = false
            }

            if (!etVerficationCode.text.isEmpty()) {
                etVerficationCode.text.clear()
            }

            if (etPhoneNumber.text.toString().isEmpty()) {
                Toast.makeText(this, getString(R.string.enter_phone_number), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (etPhoneNumber.text.toString().length < 8) {
                Toast.makeText(this, getString(R.string.enter_correct_phone_number), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!isClicked) {
                isClicked = true
                timer(millisInFuture, countDownInterval).start()
                isCancelled = false
                isPaused = false
                hideKeyboard()
                presenter.sendOTP(etUserName.text.toString(), etPhoneNumber.text.toString())
            }
        }


        etPhoneNumber.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                isCancelled = true
                isPaused = false
                isClicked = false

                checkDataEntered()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })


        etUserName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                checkDataEntered()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        etVerficationCode.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                checkDataEntered()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

    }

    private fun checkDataEntered() {
        if (etPhoneNumber.length() >= 8 && etUserName.length() >= 3 && etVerficationCode.length() >= 6) {
            btnNextRegistered.background = resources.getDrawable(R.drawable.imageview_selector_green)
        } else {
            btnNextRegistered.background = resources.getDrawable(R.drawable.imageview_selector)
        }
    }

    override fun startTime() {
        isCancelled = true
        isPaused = false
        isClicked = false
    }

    override fun openLogin() {
        startActivity(Intent(this, LoginPhoneActivity::class.java))
//                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
        finish()
    }

    override fun backPage() {
        finish()
        overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
    }

    override fun hideRegisterProgress() {
        RuthUtility.hideLoader()
//        progressBarRegisterPhone.visibility = View.GONE
    }

    override fun showRegisterProgress() {
        RuthUtility.showLoader(this@RegisterPhoneActivity)
//        progressBarRegisterPhone.visibility = View.VISIBLE
    }

    override fun userVerified() {
        startActivity(Intent(this, RegisterUserInfoActivity::class.java)
//                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .putExtra("userName", etUserName.text.toString()))
    }

    override fun stopTimer() {
        isCancelled = true
        isPaused = false
        isClicked = false
    }

    override fun openPopupDialog() {
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_terms_conditions, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation


        val closeTermsCondition = dialogView.findViewById<View>(R.id.ivCloseTermsConditions) as ImageView

        closeTermsCondition.setOnClickListener {
            b.dismiss()
        }
        b.show()
        b.setCancelable(false)

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)

        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.8f).toInt()
        val dialogWindowHeight = (displayHeight * 0.6f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        b.window.attributes = layoutParams
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
        finish()
    }

    //Countdown timer
    private fun timer(millisInFuture: Long, countDownInterval: Long): CountDownTimer {
        return object : CountDownTimer(millisInFuture, countDownInterval) {
            @RequiresApi(Build.VERSION_CODES.M)
            override fun onTick(millisUntilFinished: Long) {
                val timeRemaining = "${millisUntilFinished / 1000} s"
                tvSendOtp.setTextColor(resources.getColor(R.color.timer_text))
                tvSendOtp.text = timeRemaining
                if (timeRemaining == "1 s") {
                    isCancelled = true
                    isPaused = false
                    isClicked = false
                }
                when {
                    isPaused -> {
                        tvSendOtp.setTextColor(resources.getColor(R.color.white))
                        tvSendOtp.text = getString(R.string.resend)
                        // To ensure start timer from paused time
                        resumeFromMillis = millisUntilFinished
                        cancel()
                    }
                    isCancelled -> {
                        tvSendOtp.setTextColor(resources.getColor(R.color.white))
                        tvSendOtp.text = getString(R.string.resend)
                        cancel()
                    }
                    else -> tvSendOtp.text = timeRemaining
                }
            }

            @RequiresApi(Build.VERSION_CODES.M)
            override fun onFinish() {
                tvSendOtp.setTextColor(resources.getColor(R.color.white))
                tvSendOtp.text = getString(R.string.resend)
            }
        }
    }

    private fun customTextView(view: TextView) {
        val terms = getString(R.string.register_terms_and_conditions)
        val conditions = getString(R.string.termsConditions)

        val spanTxt = SpannableStringBuilder(terms)

        spanTxt.setSpan(UnderlineSpan(), terms.indexOf(conditions),
                terms.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spanTxt.setSpan(ForegroundColorSpan(resources.getColor(R.color.terms_textColor)), terms.indexOf(conditions), spanTxt.length, 0)
        view.movementMethod = LinkMovementMethod.getInstance()
        view.setText(spanTxt, TextView.BufferType.SPANNABLE)
    }

    private fun validUserName(password: String): Boolean {
        var validUserName: Boolean = true
        var counter = 0
        for (i in 0 until password.length) {
            if (Character.isLetter(password[i]))
                counter++
        }
        if (counter < 3) {
            validUserName = false
        }
        return validUserName
    }
}