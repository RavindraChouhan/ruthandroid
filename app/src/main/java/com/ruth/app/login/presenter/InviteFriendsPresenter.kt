package com.ruth.app.login.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class InviteFriendsPresenter @Inject constructor() : BasePresenter<InviteFriendsPresenter.InviteFriendsView>() {

    override fun onCreate(view: InviteFriendsView) {
        super.onCreate(view)

    }

    fun openRecommended() {
        view?.openRecommended()
    }

    fun skipInvite() {
        view?.skipInvite()
    }

    fun sendMessage(inviteFriendsType: String?) {
        if (inviteFriendsType.equals("facebook", true)) {

        } else if (inviteFriendsType.equals("phone", true)) {
            view?.sendTextMessage()
        }
    }

    fun clearLogin() {
        preferences.clearPreferences()
    }

    interface InviteFriendsView : BaseView {
        fun openRecommended()
        fun skipInvite()
        fun sendTextMessage()
    }
}