package com.ruth.app.login.activity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.support.annotation.RequiresApi
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.login.presenter.ForgotPasswordPresenter
import kotlinx.android.synthetic.main.activity_forgot_password.*

class ForgotPasswordActivity : BaseActivity<ForgotPasswordPresenter>(), ForgotPasswordPresenter.ForgotPasswordView {

    private var isPaused = false
    private var isCancelled = false
    private var resumeFromMillis: Long = 0

    private var isClicked: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        App.getComponent().inject(this)
        presenter.onCreate(this)
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)

        etMobileNo.setOnTouchListener { v, event ->
            etMobileNo.isCursorVisible = true
            false
        }

        val millisInFuture: Long = 60000
        val countDownInterval: Long = 1000

        ivBackForgotPassword.setOnClickListener {
            presenter.goBack()
        }

        btnForgotPassword.setOnClickListener {

            if (etMobileNo.text.toString().isEmpty()) {
                Toast.makeText(this, getString(R.string.enter_phone_number), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (etVerficationCode.text.toString().isEmpty()) {
                Toast.makeText(this, getString(R.string.enter_otp), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            hideKeyboard()
            stopTimer()
            presenter.verifyOTP(etMobileNo.text.toString(), etVerficationCode.text.toString())
        }

        tvSendOtp.setOnClickListener {
            if (isCancelled) {
                isClicked = false
            }

            if (!etVerficationCode.text.isEmpty()) {
                etVerficationCode.text.clear()
            }

            if (etMobileNo.text.isEmpty() || etMobileNo.text.length > 10) {
                Toast.makeText(this, getString(R.string.enter_phone_number), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (!isClicked) {
                isClicked = true
//                if (etMobileNo.text.length == 10) {
                timer(millisInFuture, countDownInterval).start()
                isCancelled = false
                isPaused = false
                hideKeyboard()
                presenter.setForgotOTP(etMobileNo.text.toString())
//                }
            }
        }

//        etVerficationCode.setOnClickListener {
//            isCancelled = true
//            isPaused = false
//            isClicked = false
//        }
//
        etVerficationCode.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (etMobileNo.length() >= 8 && etVerficationCode.length() >= 6) {
                    btnForgotPassword.background = resources.getDrawable(R.drawable.imageview_selector_green)
                } else {
                    btnForgotPassword.background = resources.getDrawable(R.drawable.imageview_selector)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        etMobileNo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                isCancelled = true
                isPaused = false
                isClicked = false

                if (etMobileNo.length() >= 8 && etVerficationCode.length() >= 6) {
                    btnForgotPassword.background = resources.getDrawable(R.drawable.imageview_selector_green)
                } else {
                    btnForgotPassword.background = resources.getDrawable(R.drawable.imageview_selector)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }

    override fun stopTimer() {
        isCancelled = true
        isPaused = false
        isClicked = false
    }

    override fun goBack() {
        finish()
        overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
    }

    override fun userVerified(phone: String) {
        if (etVerficationCode.text.toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.verify_phone), Toast.LENGTH_SHORT).show()
            return
        }
        presenter.getToken(phone)
    }

    //Countdown timer
    private fun timer(millisInFuture: Long, countDownInterval: Long): CountDownTimer {
        return object : CountDownTimer(millisInFuture, countDownInterval) {
            @RequiresApi(Build.VERSION_CODES.M)
            override fun onTick(millisUntilFinished: Long) {
                val timeRemaining = "${millisUntilFinished / 1000} s"
                tvSendOtp.setTextColor(resources.getColor(R.color.timer_text))
                tvSendOtp.text = timeRemaining
                if (timeRemaining == "1 s") {
                    isCancelled = true
                    isPaused = false
                    isClicked = false
                }

                when {
                    isPaused -> {
                        tvSendOtp.setTextColor(resources.getColor(R.color.white))
                        tvSendOtp.text = getString(R.string.resend)
                        // To ensure start timer from paused time
                        resumeFromMillis = millisUntilFinished
                        cancel()
                    }
                    isCancelled -> {
                        tvSendOtp.setTextColor(resources.getColor(R.color.white))
                        tvSendOtp.text = getString(R.string.resend)
                        cancel()
                    }
                    else -> tvSendOtp.text = timeRemaining
                }
            }

            @RequiresApi(Build.VERSION_CODES.M)
            override fun onFinish() {
                tvSendOtp.setTextColor(resources.getColor(R.color.white))
                tvSendOtp.text = getString(R.string.resend)
            }
        }
    }

    override fun resetPass(token: String) {
        startActivity(Intent(this@ForgotPasswordActivity, ResetPasswordActivity::class.java).putExtra("token", token))
        finish()
    }
}