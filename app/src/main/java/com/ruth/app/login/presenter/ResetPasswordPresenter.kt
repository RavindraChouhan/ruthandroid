package com.ruth.app.login.presenter

import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.network.config.RuthApi
import javax.inject.Inject

class ResetPasswordPresenter @Inject constructor(private val ruthApi: RuthApi) : BasePresenter<ResetPasswordPresenter.ResetPasswordView>() {

    override fun onCreate(view: ResetPasswordView) {
        super.onCreate(view)

    }

    fun resetPassword(password: String, confirmPassword: String, resetToken: String) {

        if (!password.equals(confirmPassword, true)) {
            view?.showToast(context.getString(R.string.password_not_same))
        } else {
            var hm = HashMap<String, String>()
            hm["token"] = resetToken
            hm["password"] = password
            ruthApi.resetPassword(hm)
                    .subscribe({
                        if (it != null) {
                            if (it.status) {
                                view?.resetDone()
                            } else {
                                if (App.isChineseLanguage) {
                                    view?.showToast(it.zh_Hant)
                                } else {
                                    view?.showToast(it.message)
                                }
                            }
                        }
                    }, {
                        logError(it)
                        view?.showToast(context.getString(R.string.connection_error))
                    })
        }
    }


    fun onClickBack() {
        view?.backPage()
    }


    interface ResetPasswordView : BaseView {
        fun resetDone()
        fun backPage()
    }
}