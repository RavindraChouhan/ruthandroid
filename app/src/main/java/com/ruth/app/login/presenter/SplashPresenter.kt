package com.ruth.app.login.presenter

import android.os.Handler
import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject
import com.google.firebase.iid.InstanceIdResult
import com.google.android.gms.tasks.OnSuccessListener



class SplashPresenter @Inject constructor() : BasePresenter<SplashPresenter.SplashView>() {

    private var mDelayHandler: Handler? = null
    private val splashDelay: Long = 3000 //3 seconds

    private val mRunnable: Runnable = Runnable {
        if (preferences.getUserInfo() != null) {
            view?.openHome()
        } else {
            view?.openLogin()
        }
    }

    override fun onCreate(view: SplashView) {
        super.onCreate(view)

        mDelayHandler = Handler()
        //Navigate with delay
        mDelayHandler!!.postDelayed(mRunnable, splashDelay)
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {instanceIdResult ->
            val newToken = instanceIdResult.token
            preferences.setDeviceId(newToken)
            Log.d("deviceToken", "newToken -> $newToken")
        }
    }

    interface SplashView : BaseView {
        fun openLogin()
        fun openHome()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }
    }
}