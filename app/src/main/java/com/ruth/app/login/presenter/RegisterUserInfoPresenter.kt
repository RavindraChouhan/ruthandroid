package com.ruth.app.login.presenter

import android.location.Geocoder
import android.util.Log
import android.widget.Toast
import com.google.firebase.iid.FirebaseInstanceId
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.login.activity.SelectBirthdayActivity
import com.ruth.app.network.config.RuthApi
import java.util.*
import javax.inject.Inject


class RegisterUserInfoPresenter @Inject constructor(private val ruthApi: RuthApi) :
        BasePresenter<RegisterUserInfoPresenter.RegisterUserInfoView>() {

    override fun onCreate(view: RegisterUserInfoView) {
        super.onCreate(view)
    }

    fun onClickBack() {
        view?.backPage()
    }

    fun registerUser(userName: String, email: String, password: String) {
        if (!utility.isEmailValid(email)) {
            Toast.makeText(context, context.getString(R.string.valid_email), Toast.LENGTH_SHORT).show()
            return
        }

        val gcd = Geocoder(context, Locale.getDefault())
        /*--------------Remove this on live-------------------------------------------*/
        val tempLat = 22.28552
        val tempLng = 114.15769

        utility.setDoublePreferences(context, getString(R.string.lat), tempLat)
        utility.setDoublePreferences(context, getString(R.string.lng), tempLng)
        /*------------------------------------------------------------------------------*/

        val addresses = gcd.getFromLocation(utility.getDoublePreferences(context, getString(R.string.lat)).toDouble(),
                utility.getDoublePreferences(context, getString(R.string.lng)).toDouble(), 1)
        Log.d("address", "->$addresses")

        if (addresses.size > 0) {
            if (addresses[0].countryName.equals(context.getString(R.string.Hong_Kong), true)) {
                val hm: HashMap<String, String> = HashMap()
                hm["mobileNo"] = utility.getStringPreferences(context, getString(R.string.mobile_number))
                hm["userName"] = userName
                hm["email"] = email
                hm["password"] = password
                if (preferences.getDeviceId() != null){
                    hm["deviceToken"] = preferences.getDeviceId().toString()
                }else{
                    hm["deviceToken"] = FirebaseInstanceId.getInstance().token!!
                }
                hm["deviceType"] = "Android"
                hm["lat"] = utility.getDoublePreferences(context, getString(R.string.lat)).toString()
                hm["lng"] = utility.getDoublePreferences(context, getString(R.string.lng)).toString()
                hm["country"] = addresses[0].countryName
                hm["dob"] = SelectBirthdayActivity.selectedDob

                ruthApi.registerUser(hm)
                        .doOnTerminate { view?.hideRegisterProgress() }
                        .doOnSubscribe { view?.showRegisterProgress() }
                        .subscribe({
                            Log.d("verify", " -> " + it.message)
                            if (it != null) {
                                if (it.status) {
                                    if (App.isChineseLanguage) {
                                        view?.showToast(it.zhHant)
                                    } else {
                                        view?.showToast(it.message)
                                    }
                                    if (it.message.contains("exists", true)) {
                                        view?.redirectLogin()
                                    } else {
                                        it.data.userToken = it.data.userToken
                                        preferences.setUserInfo(it.data)
//                                    utility.setStringPreferences(context, "userName", preferences.getUserInfo().userName)
                                        view?.loginUser(preferences.getUserInfo().userName)
                                    }
                                } else {
                                    if (App.isChineseLanguage) {
                                        view?.showToast(it.zhHant)
                                    } else {
                                        view?.showToast(it.message)
                                    }
                                    view?.hideRegisterProgress()
                                }
                            }
                        }, {
                            view?.hideRegisterProgress()
                            Log.e("error", it.message)
                            view?.showToast(context.getString(R.string.connection_error))
                        })
            } else {
                view?.showToast(getString(R.string.not_available))
                return
            }

        } else {
            view?.showToast(getString(R.string.connection_error))
        }
    }

    interface RegisterUserInfoView : BasePresenter.BaseView {
        fun backPage()
        //        fun openInviteFriends()
        fun redirectLogin()

        fun loginUser(userName: String)
        fun hideRegisterProgress()
        fun showRegisterProgress()
    }
}