package com.ruth.app.login.model

import android.graphics.drawable.Drawable

class RecommendedFriends(var name: String?,
                         var userImage: Drawable? = null,
                         var fansCount: Int?,
                         var registeredDate: String?,
                         var districtName1: String?,
                         var districtName2: String?,
                         var accountNo : String,
                         var gender : String,
                         var isSelected: Boolean = true)