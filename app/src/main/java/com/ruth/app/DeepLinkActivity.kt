package com.ruth.app

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.ruth.app.login.activity.SplashActivity

class DeepLinkActivity : AppCompatActivity() {
    private var mContext: Context? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deep_link)
        mContext = this
        onNewIntent(intent)
    }

    override fun onNewIntent(intent: Intent) {
        val action = intent.action
        var data = intent.dataString
        try {
            if (Intent.ACTION_VIEW == action && data != null) {
                data = data.replace("ruth://", "")
                Log.d("deeplink", " -> $data")

                startActivity(Intent(this, SplashActivity::class.java))

//                if (data.contains("SessionId")) {
//                    val splitData = data.split("\\?".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//                    val videoSession = splitData[1]
//                    val sessionValue = videoSession.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//                    val sessionId = sessionValue[1]
//                    val splittokenId = sessionId.split("&".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//                    val id = splittokenId[0]
//                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}