package com.ruth.app.network.config

import com.ruth.app.home.eightbutton.model.MapModel
import com.ruth.app.home.model.EightButtons
import com.ruth.app.home.model.HomeBanners
import com.ruth.app.home.model.RuthChannelDetail
import com.ruth.app.home.model.RuthChannels
import com.ruth.app.login.model.fbfriends.FacebookFriends
import com.ruth.app.model.*
import com.ruth.app.sharing.model.VipPayment
import retrofit2.Call
import retrofit2.http.*
import rx.Observable

interface Api {

    @POST("user/sendOTP")
    fun sendOTP(@Body body: Map<String, String>): Observable<SendOTPResponse>

    @POST("user/verifyOTP")
    fun verifyOTP(@Body body: Map<String, String>): Observable<VerifyOTPResponse>

    @POST("user/mobile/signup")
    fun registerUser(@Body body: Map<String, String>): Observable<UserInfo>

    @POST("user/socialSignUp")
    fun socialSignUp(@Body userInfo: Map<String, String>): Observable<UserInfo>

    @POST("user/mobile/login")
    fun loginPhoneNo(@Body userInfo: Map<String, String>): Observable<UserInfo>

    @POST("user/mobile/forgotPassword")
    fun forgotPasswordOTP(@Body body: Map<String, String>): Observable<ForgotPassword>

    @POST("user/mobile/resetPassword")
    fun resetPassword(@Body body: Map<String, String>): Observable<ForgotPassword>

    @GET("tutorials/getAll")
    fun tutorial(@Header("Authorization") token: String): Observable<Tutorials>

    @GET("user/mobile/me/{id}")
    fun getUserInfo(@Header("Authorization") token: String, @Path("id") id: String): Observable<UserProfile>

    @GET("user/mobile/me/{id}")
    fun getUserWithBroadcast(@Header("Authorization") token: String, @Path("id") id: String
                             , @Query("broadcastId") broadcastId: String): Observable<UserProfile>

//    @GET("/user/mobile/details/{id}")
//    fun getUserInfo(@Header("Authorization") token: String, @Path("id") id: String): Observable<UserProfile>

    @POST("user/mobile/update")
    fun updateUserInfo(@Header("Authorization") token: String,
                       @Body body: Map<String, @JvmSuppressWildcards Any>): Observable<UserProfile>

    @GET("user/followUsers")
    fun getRecommanded(@Header("Authorization") token: String): Observable<RecommandedUser>

    @GET("settings/mobile/get")
    fun getSettings(@Header("Authorization") token: String): Observable<RuthSettings>


    /*------------------------------------------HOME----------------------------------------------*/

    @GET("advertisements/getAll")
    fun getBanners(): Observable<HomeBanners>

    @GET("effects/getAll")
    fun getEightButtonEffect(): Observable<EightButtons>

    @GET("channels/getAll")
    fun getChannelsList(@Header("Authorization") token: String): Observable<RuthChannels>

    @GET("channels/{channelId}")
    fun getChannelById(@Header("Authorization") token: String, @Path("channelId") channelId: String): Observable<RuthChannelDetail>

    /*----------------------------------BROADCAST VIDEOS------------------------------------------*/

    @POST("user/mobile/broadcast")
    fun uploadBroadcastInfo(@Body body: Map<String, @JvmSuppressWildcards Any>): Observable<BroadcastInfo>

    @GET("stickers/mobile/get")
    fun getLiveStickers(@Header("Authorization") token: String): Observable<Stickers>

    @GET("user/mobile/broadcastById/{id}")
    fun getPastBroadcast(@Header("Authorization") token: String, @Path("id") id: String): Observable<PastChannelBroadcastData>

    @GET("user/mobile/broadcast/gifts/{broadcastId}")
    fun getUsersGifts(@Header("Authorization") token: String, @Path("broadcastId") broadcastId: String): Observable<UserGifts>

    @GET("user/mobile/broadcast/views/{broadcastId}")
    fun getLiveViewers(@Header("Authorization") token: String,
                       @Path("broadcastId") broadcastId: String): Observable<LiveUsersData>

    @GET("broadcasts/viewer/{broadcastId}")
    fun getEndStreamingInfo(@Header("Authorization") token: String,
                            @Path("broadcastId") broadcastId: String): Observable<EndStreamingInfo>

    @GET("broadcasts/invites/{broadcastId}")
    fun getInvitedUsers(@Header("Authorization") token: String,
                        @Path("broadcastId") broadcastId: String): Observable<InvitedFans>

    @POST("broadcasts/invite-user")
    fun postInviteFans(@Header("Authorization") token: String,
                       @Body fansBody: Map<String, @JvmSuppressWildcards Any>): Observable<PostInvitedFans>

    @GET("broadcasts/custom-order/{broadcastId}")
    fun getCustomOrders(@Header("Authorization") token: String,
                        @Path("broadcastId") broadcastId: String): Observable<CustomOrdersGift>

    @PUT("broadcasts/update-custom-order")
    fun postOrderStatus(@Header("Authorization") token: String,
                        @Body fansBody: Map<String, @JvmSuppressWildcards Any>): Observable<CustomOrderStatus>

    @GET("user/mobile/broadcast/application")
    fun getVerificationCode(@Header("Authorization") token: String): Observable<VerificationStreaming>


    /*----------------------------Subscribe Channels - Follow Unfollow User-----------------------*/

    @POST("channels/mobile/update")
    fun postSubscribe(@Header("Authorization") token: String,
                      @Body body: Map<String, @JvmSuppressWildcards Any>): Observable<PostResponse>

    @POST("user/mobile/update")
    fun followUnfollowUser(@Header("Authorization") token: String,
                           @Body body: Map<String, @JvmSuppressWildcards Any>): Observable<PostResponse>


    /*----------------------------------------Facebook Friends------------------------------------*/
    @GET("v2.11/me/friends")
    fun getFriends(@Header("Authorization") token: String, @Query("fields") data: String): Call<FacebookFriends>


    /*----------------------------------------Instagram Friends-----------------------------------*/
    @GET("{userid}/follows?access_token=")
    fun getInstaFriends(@Path("userid") userid: String,
                        @Query("access_token") access_token: String): Call<FacebookFriends>

    /*-----------------------------------------Map------------------------------------------------*/
    @GET("directions/json")
    fun getDirection(@Query("origin") origin: String,
                     @Query("destination") destination: String,
                     @Query("key") maps_key: String): Call<MapModel>

    /*-----------------------------------------Vip payment------------------------------------------------*/
    @POST("vips/mobile/add")
    fun postVippayment(@Header("Authorization") token: String,
                       @Body body: Map<String, @JvmSuppressWildcards Any>): Observable<VipPayment>


}