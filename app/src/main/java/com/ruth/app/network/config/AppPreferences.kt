package com.ruth.app.network.config

import android.content.Context
import com.google.gson.Gson
import com.ruth.app.R
import com.ruth.app.home.model.ChannelBroadcasterData
import com.ruth.app.home.model.RuthChannelsData
import com.ruth.app.login.model.fbfriends.Friends
import com.ruth.app.model.StickerType
import com.ruth.app.model.UserInfo
import com.ruth.app.model.UserProfileData
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppPreferences @Inject constructor(private val context: Context, private val gson: Gson) {
    private val preferences = context
            .getSharedPreferences("Ruth", Context.MODE_PRIVATE)

    // string resources are overkill here.
    private fun getKey(resId: Int) = context.getString(resId)

    fun clearPreferences() = preferences.edit().clear().apply()

    fun setUserInfo(userModel: UserInfo.Data) =
            preferences.edit().putString(getKey(R.string.userinfo), gson.toJson(userModel)).apply()

    fun getUserInfo() =
            gson.fromJson(preferences.getString(getKey(R.string.userinfo), ""), UserInfo.Data::class.java)

    fun setUserProfile(userProfileData: UserProfileData) =
            preferences.edit().putString(getKey(R.string.userprofile), gson.toJson(userProfileData)).apply()

    fun getUserProfile() =
            gson.fromJson(preferences.getString(getKey(R.string.userprofile), ""), UserProfileData::class.java)

    fun saveStickers(stickerType: StickerType) =
            preferences.edit().putString(getKey(R.string.stickers), gson.toJson(stickerType)).apply()

    fun getStickers() =
            gson.fromJson(preferences.getString(getKey(R.string.stickers), ""), StickerType::class.java)


    fun setDeviceId(number: String) =
            preferences.edit().putString(getKey(R.string.deviceId), number).apply()

    fun getDeviceId(): String? = preferences.getString(getKey(R.string.deviceId), null)

    fun setFacebookUserInfo(friends: List<Friends>?) =
            preferences.edit().putString(getKey(R.string.facebookinfo), gson.toJson(friends)).apply()

    fun getFacebookUserInfo() =
            gson.fromJson(preferences.getString(getKey(R.string.facebookinfo), ""), Friends::class.java)


    fun setAllChannels(channelData: ArrayList<RuthChannelsData>) =
            preferences.edit().putString(getKey(R.string.allchannels), gson.toJson(channelData)).apply()

    fun getAllChannels() =
            gson.fromJson(preferences.getString(getKey(R.string.allchannels), ""), RuthChannelsData::class.java)


    fun setBroadcasterInfo(channelBroadcasterData: ChannelBroadcasterData?) =
            preferences.edit().putString(getKey(R.string.channel_broadcaster), gson.toJson(channelBroadcasterData)).apply()

    fun getBroadcasterInfo() =
            gson.fromJson(preferences.getString(getKey(R.string.channel_broadcaster), ""), ChannelBroadcasterData::class.java)


//  fun getOTPstatus() =
//    preferences.getBoolean(getKey(R.string.isOtpSent), false)
//
//  fun setOTPStatus(status: Boolean) =
//    preferences.edit().putBoolean(getKey(R.string.isOtpSent), status).apply()
//
//  fun isLoggedInUber() = preferences.getBoolean(getKey(R.string.isUberLoginDone), false)
//
//  fun setLoggedInUber(isLoggedIn: Boolean) =
//    preferences.edit().putBoolean(getKey(R.string.isUberLoginDone), isLoggedIn).apply()
//
//  fun getUserNumber() =
//    preferences.getString(getKey(R.string.phone_number), "")
//
//  fun setUserNumber(number: String) =
//    preferences.edit().putString(getKey(R.string.phone_number), number).apply()
//
//  fun setAuthToken(token: String) = preferences.edit().putString(getKey(R.string.authToken), token).apply()
//
//  fun getAuthToken(): String? = preferences.getString(getKey(R.string.authToken), null)
//
//  fun setGlobalSettings(settings: GlobalSettings) = preferences.edit().putString(getKey(R.string.settings), gson.toJson(settings)).apply()
//
//  fun setUberLoginResponse(loginResponse: UberLoginResponse) =
//    preferences.edit().putString(getKey(R.string.uberLoginResponse), gson.toJson(loginResponse)).apply()
//
//  fun getUberLoginResponse() =
//    gson.fromJson(preferences.getString(getKey(R.string.uberLoginResponse), ""), UberLoginResponse::class.java)
//
//  fun putLocationCategories(locationCategories: LocationCategories) =
//    preferences.edit().putString(getKey(R.string.location_categories), gson.toJson(locationCategories)).apply()
//
//  fun getLocationCategories() =
//    gson.fromJson(preferences.getString(getKey(R.string.location_categories), null), LocationCategories::class.java)
//
//  fun removeLocationCategories() =
//    preferences.edit().remove(getKey(R.string.location_categories)).apply()
//
//  fun isCashBack() = preferences.getBoolean(getKey(R.string.cashBack), false)
//
//  fun setCashBack(isLoggedIn: Boolean) =
//          preferences.edit().putBoolean(getKey(R.string.cashBack), isLoggedIn).apply()
//
//  fun isCash():Boolean = preferences.getBoolean(getKey(R.string.cashBack), false)
//
//  fun setCash(isLoggedIn: Boolean) =
//          preferences.edit().putBoolean(getKey(R.string.cashBack), isLoggedIn).apply()

}