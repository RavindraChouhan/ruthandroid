package com.ruth.app.network

import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter
import android.support.v4.content.LocalBroadcastManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.adapter.rxjava.HttpException
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*

fun <T> Observable<T>.toMainThread() = this.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())


fun <T> Call<T>.toObservable() = Observable.create<T> {
    this.enqueue(object : Callback<T> {
        override fun onResponse(call: Call<T>, response: Response<T>) {
            if (response.isSuccessful) {
                it.onNext(response.body())
                it.onCompleted()
            } else {
                it.onError(HttpException(response))
            }
        }

        override fun onFailure(call: Call<T>, t: Throwable) {
            it.onError(t)
        }
    })
}

fun registerReceiver(context: Context, receiver: BroadcastReceiver, filter: String) {
    LocalBroadcastManager.getInstance(context).registerReceiver(receiver, IntentFilter(filter))
}

fun unregisterReceiver(context: Context, receiver: BroadcastReceiver) {
    LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver)
}

fun hideKeyboard(view: View?) {
    view?.let {
        val inputManager = it.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(it.windowToken, 0)
    }
}

fun deg2rad(deg: Double): Double {
    return deg * Math.PI / 180.0
}

fun rad2deg(rad: Double): Double {
    return rad * 180 / Math.PI
}

fun getDistance(sourceLat: Double, sourceLng: Double, destinationLat: Double, destinationLng: Double): Double {
    val theta = sourceLng - destinationLng
    var dist = Math.sin(deg2rad(sourceLat)) * Math.sin(deg2rad(destinationLat)) + Math.cos(deg2rad(sourceLat)) * Math.cos(deg2rad(destinationLat)) * Math.cos(deg2rad(theta))
    dist = Math.acos(dist)
    dist = rad2deg(dist)
    dist *= 60.0 * 1.1515
    return dist
}

//fun getUserCountryISO(context: Context) =
//  context.telephonyManager.networkCountryIso.toUpperCase()

//Use this property to disable some features like custom animations when running tests
val isRunningTest: Boolean by lazy {
    try {
        Class.forName("android.support.test.espresso.Espresso")
        true
    } catch (e: ClassNotFoundException) {
        false
    }
}

//fun getMapViewBounds(coordinates: List<LatLng>): LatLngBounds {
//  val builder = LatLngBounds.builder()
//  for (coord in coordinates) {
//    builder.include(coord)
//  }
//  return builder.build()
//}

fun convertStringToDate(dateString: String, dateFormat: String): Date {
    return SimpleDateFormat(dateFormat).parse(dateString)
}

fun setDateIntoCalenderAndGet(date: Date): Calendar {
    val cal = Calendar.getInstance()
    cal.time = date
    return cal
}