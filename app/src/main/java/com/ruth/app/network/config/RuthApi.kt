package com.ruth.app.network.config

import android.support.annotation.NonNull
import com.ruth.app.network.config.Api
import com.ruth.app.network.toMainThread
import javax.inject.Inject

class RuthApi @Inject constructor(private val api: Api) {

    fun sendOTP(@NonNull number: Map<String, String>) = api.sendOTP(number).toMainThread()!!

    fun verifyOTP(@NonNull number: Map<String, String>) = api.verifyOTP(number).toMainThread()!!

    fun registerUser(@NonNull userInfo: Map<String, String>) = api.registerUser(userInfo).toMainThread()!!

    fun socialSignUp(@NonNull userInfo: Map<String, String>) = api.socialSignUp(userInfo).toMainThread()!!

    fun loginPhoneNo(@NonNull userInfo: Map<String, String>) = api.loginPhoneNo(userInfo).toMainThread()!!

    fun forgotPasswordOTP(@NonNull number: Map<String, String>) = api.forgotPasswordOTP(number).toMainThread()!!

    fun resetPassword(@NonNull number: Map<String, String>) = api.resetPassword(number).toMainThread()!!

    fun tutorials(token: String) = api.tutorial(token).toMainThread()!!

    fun getUserInfo(token: String, userId: String) = api.getUserInfo(token, userId).toMainThread()!!

    fun updateUserInfo(token: String, info: Map<String, @JvmSuppressWildcards Any>) = api.updateUserInfo(token, info).toMainThread()!!

    fun recommandedUsers(token: String) = api.getRecommanded(token).toMainThread()!!

    fun getSettings(token: String) = api.getSettings(token).toMainThread()!!

    fun getUserWithBroadcast(token: String, userId: String, broadcastId: String) = api.getUserWithBroadcast(token, userId, broadcastId).toMainThread()!!


    /*-----------------------------------------Home-----------------------------------------------*/
    fun getHomeBanners() = api.getBanners().toMainThread()!!

    fun getButtonEffect() = api.getEightButtonEffect().toMainThread()!!

    fun getAllChannels(token: String) = api.getChannelsList(token).toMainThread()!!

    fun getChannelById(token: String, channelId: String) = api.getChannelById(token, channelId).toMainThread()!!

    fun getPastBroadcastInfo(token: String, channelId: String) = api.getPastBroadcast(token, channelId).toMainThread()!!

    fun getVerificationCode(token: String) = api.getVerificationCode(token).toMainThread()!!

    /*----------------------------------Live Streaming--------------------------------------------*/

    fun uploadBroadcastInfo(@NonNull broadcastInfo: Map<String, @JvmSuppressWildcards Any>) = api.uploadBroadcastInfo(broadcastInfo).toMainThread()!!

    fun getLiveStickers(token: String) = api.getLiveStickers(token).toMainThread()!!

    fun getUsersGifts(token: String, broadcastId: String) = api.getUsersGifts(token, broadcastId).toMainThread()!!

    fun getLiveViewers(token: String, broadcastId: String) = api.getLiveViewers(token, broadcastId).toMainThread()!!

    fun getEndStreamingInfo(token: String, broadcastId: String) = api.getEndStreamingInfo(token, broadcastId).toMainThread()!!

    fun getInvitedUsers(token: String, broadcastId: String) = api.getInvitedUsers(token, broadcastId).toMainThread()!!

    fun postInviteFans(token: String, fansUserId: Map<String, @JvmSuppressWildcards Any>) = api.postInviteFans(token, fansUserId).toMainThread()!!

    fun getCustomOrders(token: String, broadcastId: String) = api.getCustomOrders(token, broadcastId).toMainThread()!!

    fun postOrderStatus(token: String, customOrderData: Map<String, @JvmSuppressWildcards Any>) = api.postOrderStatus(token, customOrderData).toMainThread()!!

    /*----------------------------Subscribe Channels - Follow Unfollow User-----------------------*/

    fun postSubscribe(token: String, @NonNull subscribeInfo: Map<String, @JvmSuppressWildcards Any>) = api.postSubscribe(token, subscribeInfo).toMainThread()!!

    fun followUnfollowUser(token: String, @NonNull followUnfollowInfo: Map<String, @JvmSuppressWildcards Any>) = api.followUnfollowUser(token, followUnfollowInfo).toMainThread()!!

    fun postPayment(token: String, @NonNull paymentInfo: Map<String, @JvmSuppressWildcards Any>) = api.postVippayment(token, paymentInfo).toMainThread()!!

}