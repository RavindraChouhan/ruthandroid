package com.ruth.app.network.config

import com.ruth.app.App
import com.ruth.app.broadcaster.activity.AllBroadcastActivity
import com.ruth.app.broadcaster.activity.BroadcastDetailActivity
import com.ruth.app.home.activity.HomeActivity
import com.ruth.app.home.activity.SeeAllFriends
import com.ruth.app.home.eightbutton.activity.*
import com.ruth.app.home.eightbutton.fragment.BiblePagerFragment
import com.ruth.app.home.eightbutton.fragment.RuthKnowsFragment
import com.ruth.app.home.eightbutton.fragment.VipShareFragment
import com.ruth.app.home.eightbutton.videoplayer.RuthStoryPlayer
import com.ruth.app.home.eightbutton.videoplayer.RuthVideoPlayer
import com.ruth.app.home.fragment.*
import com.ruth.app.home.maps.activity.FilterActivity
import com.ruth.app.home.userprofileview.account.fragment.UserAccountFragment
import com.ruth.app.home.userprofileview.settings.SettingsActivity
import com.ruth.app.livestreaming.activity.*
import com.ruth.app.livestreaming.fragment.FilterListFragment
import com.ruth.app.livestreaming.fragment.StickerFragment
import com.ruth.app.login.activity.*
import com.ruth.app.network.module.ApiModule
import com.ruth.app.otheruser.OtherUserLiveFragment
import com.ruth.app.otheruser.OtherUserProfileActivity
import com.ruth.app.rank.activity.RankHomeActivity
import com.ruth.app.rank.common.CommanFragment
import com.ruth.app.rank.fragment.FansFragment
import com.ruth.app.rank.fragment.GiftFragment
import com.ruth.app.rank.fragment.PopularityFragment
import com.ruth.app.rank.fragment.RichFragment
import com.ruth.app.sharing.activity.ShareActivity
import com.ruth.app.tutorial.activity.TutorialActivity
import com.ruth.app.video.activity.VideoPlayerActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApiModule::class))
interface AppComponent {

    fun inject(app: App)
    fun inject(loginActivity: LoginActivity)
    fun inject(homeActivity: HomeActivity)
    fun inject(splashActivity: SplashActivity)
    fun inject(loginWithPhone: LoginPhoneActivity)
    fun inject(registerPhoneActivity: RegisterPhoneActivity)
    fun inject(selectBirthdayActivity: SelectBirthdayActivity)
    fun inject(inviteFriendsActivity: InviteFriendsActivity)
    fun inject(forgotPasswordActivity: ForgotPasswordActivity)
    fun inject(tutorialActivity: TutorialActivity)
    fun inject(resetPasswordActivity: ResetPasswordActivity)
    fun inject(recommendedFriendsActivity: RecommendedFriendsActivity)
    fun inject(registerUserInfoActivity: RegisterUserInfoActivity)
    fun inject(rankHomeActivity: RankHomeActivity)
    fun inject(broadcastDetailActivity: BroadcastDetailActivity)
    fun inject(mapsActivity: MapsActivity)
    fun inject(ruthStoryActivity: RuthStoryActivity)
    fun inject(sitnTalkActivity: SitnTalkActivity)
    fun inject(ruthVideoActivity: RuthVideoActivity)
    fun inject(liveVideoPlayerActivity: LiveVideoPlayerActivity)
    fun inject(enterBroadcastingDetailsActivity: EnterBroadcastingDetailsActivity)
    fun inject(otherUserProfileActivity: OtherUserProfileActivity)
    fun inject(bibleActivity: BibleActivity)
    fun inject(ruthKnowsActivity: RuthKnowsActivity)
    fun inject(vipShareActivity: VipShareActivity)
    fun inject(bibleDetailActivity: BibleDetailActivity)
    fun inject(settingsActivity: SettingsActivity)
    fun inject(allBroadcastActivity: AllBroadcastActivity)
    fun inject(filterActivity: FilterActivity)
    fun inject(pastVideoPlayerActivity: PastVideoPlayerActivity)
    fun inject(shareActivity: ShareActivity)
    fun inject(seeAllFriends: SeeAllFriends)
    fun inject(homeFragment: HomeFragment)
    fun inject(searchFragment: SearchFragment)
    fun inject(favoriteFragment: FavoriteFragment)
    fun inject(userProfileFragment: UserProfileFragment)
    fun inject(videosFragment: VideosFragment)
    fun inject(liveFragment: LiveFragment)
    fun inject(giftFragment: GiftFragment)
    fun inject(richFragment: RichFragment)
    fun inject(popularityFragment: PopularityFragment)
    fun inject(fansFragment: FansFragment)
    fun inject(commanFragment: CommanFragment)
    fun inject(userAccountFragment: UserAccountFragment)
    fun inject(vipShareFragment: VipShareFragment)
    fun inject(filterListFragment: FilterListFragment)
    fun inject(stickerFragment: StickerFragment)
    fun inject(biblePagerFragment: BiblePagerFragment)
    fun inject(ruthKnowsFragment: RuthKnowsFragment)
    fun inject(otherUserLiveFragment: OtherUserLiveFragment)
    fun inject(favoriteListFragment: FavoriteListFragment)
    fun inject(podcastsFragment: PodcastsFragment)
    fun inject(videoPlayerActivity: VideoPlayerActivity)
    fun inject(ruthVideoPlayer: RuthVideoPlayer)
    fun inject(ruthStoryPlayer: RuthStoryPlayer)
    fun inject(articleDetailActivity: ArticleDetailActivity)
    fun inject(broadcastingNewLiveActivity: BroadcastingNewLiveActivity)
}