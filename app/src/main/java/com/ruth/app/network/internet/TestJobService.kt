package com.ruth.app.network.internet

import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager.CONNECTIVITY_ACTION
import android.support.v4.content.LocalBroadcastManager
import android.widget.Toast
import com.ruth.app.utility.RuthUtility


internal class TestJobService : JobService(), ConnectivityReceiver.ConnectivityReceiverListener {

    private var mConnectivityReceiver: ConnectivityReceiver? = null

    override fun onCreate() {
        super.onCreate()
        mConnectivityReceiver = ConnectivityReceiver(this)
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        return START_NOT_STICKY
    }

    override fun onStartJob(params: JobParameters): Boolean {
        registerReceiver(mConnectivityReceiver, IntentFilter(CONNECTIVITY_ACTION))
        return true
    }

    override fun onStopJob(params: JobParameters): Boolean {
        unregisterReceiver(mConnectivityReceiver)
        return true
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(Intent("internetConnection")
                .putExtra("isConnected", isConnected))



//        if (isConnected) {
//            Toast.makeText(applicationContext, "Good! Connected to Internet", Toast.LENGTH_SHORT).show()
//        } else {
//            Toast.makeText(applicationContext, "Sorry! Not connected to internet", Toast.LENGTH_SHORT).show()
////            RuthUtility.internetDialog(applicationContext) //show
//        }
    }
}