package com.ruth.app.network.rx

import android.support.design.widget.TextInputLayout
import android.text.TextUtils
import android.widget.EditText
import android.widget.TextView
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Observable

object RxViewUtils {
    fun nonEmptyChecker(inputLayout: TextInputLayout, errorText: String): Observable<Boolean> {

        val correctOnStart = !TextUtils.isEmpty(inputLayout.editText!!.text.toString())

        return RxTextView.afterTextChangeEvents(inputLayout.editText!!)
                .skip(1)
                .map { event ->
                    val text = if (event.editable() != null) event.editable()!!.toString() else ""
                    val correct = !TextUtils.isEmpty(text)
                    inputLayout.error = errorText
                    inputLayout.isErrorEnabled = !correct
                    correct
                }
                .startWith(correctOnStart)
    }


    fun nonEmptyChecker(inputLayout: EditText): Observable<Boolean> {

        val correctOnStart = !TextUtils.isEmpty(inputLayout.text.toString())

        return RxTextView.afterTextChangeEvents(inputLayout)
                .skip(1)
                .map { event ->
                    val text = if (event.editable() != null) event.editable()!!.toString() else ""
                    val correct = !TextUtils.isEmpty(text)
                    correct
                }
                .startWith(correctOnStart)
    }

    fun nonEmptyChecker(inputLayout: TextView): Observable<Boolean> {

        val correctOnStart = !TextUtils.isEmpty(inputLayout.text.toString())

        return RxTextView.afterTextChangeEvents(inputLayout)
                .skip(1)
                .map { event ->
                    val text = if (event.editable() != null) event.editable()!!.toString() else ""
                    val correct = !TextUtils.isEmpty(text)
                    correct
                }
                .startWith(correctOnStart)
    }
}