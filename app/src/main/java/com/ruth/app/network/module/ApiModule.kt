package com.ruth.app.network.module

import android.content.Context
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.ruth.app.BuildConfig
import com.ruth.app.network.config.Api
import com.ruth.app.utility.Utility
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class ApiModule(val context: Context) {

    @Provides
    @Singleton
    fun provideClient(preferences: Utility): OkHttpClient {
        val builder = OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
//                .addInterceptor {
//                    it.proceed(it.request()
//                            .newBuilder()
//                            .addHeader("Authorization", "Bearer "
//                                    + preferences.getStringPreferences(context, context.getString(R.string.deviceId)))
//                            .build())
//                }
        if (BuildConfig.DEBUG) {
            builder.addNetworkInterceptor(StethoInterceptor())
        }
        return builder.build()
    }

    private fun getClient(): OkHttpClient {
        return OkHttpClient.Builder()
                .writeTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.MINUTES)
                .build()
    }


    @Provides
    @Singleton
    fun provideApi(gson: Gson, client: OkHttpClient) = Retrofit.Builder()
            .client(getClient())
            .baseUrl(provideHostUrl())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
//            .client(client)
            .build()
            .create(Api::class.java)

    @Provides
    @Singleton
    fun provideGson() = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()

    private fun provideHostUrl() =
            if (BuildConfig.DEBUG) BuildConfig.BASE_URL else BuildConfig.BASE_URL

    @Provides
    fun provideContext() = context
}