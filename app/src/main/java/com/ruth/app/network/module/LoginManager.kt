package com.ruth.app.network.module

import android.content.Context
import android.util.Log
import com.ruth.app.R
import com.ruth.app.network.config.RuthApi
import com.ruth.app.utility.Utility
import javax.inject.Inject

class LoginManager @Inject constructor(private val preferences: Utility,
                                       private val ruthApi: RuthApi,
                                       private val context: Context) {


    fun verifyOTP(otp: HashMap<String, String>) =
            ruthApi.verifyOTP(otp)
                    .doOnNext {

                    }.doOnError { Log.d("Error", "Verify otp error") }


    fun sendOTP(number: Map<String, String>) =
            ruthApi.sendOTP(number)
                    .doOnNext {
                        preferences.setStringPreferences(context,
                                context.getString(R.string.userNumber), number["mobileNo"])

                    }.doOnError {
                        Log.d("Error", "Send otp error")
                    }
}