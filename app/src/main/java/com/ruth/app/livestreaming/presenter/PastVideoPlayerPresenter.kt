package com.ruth.app.livestreaming.presenter

import android.util.Log
import android.widget.Toast
import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.network.config.RuthApi
import javax.inject.Inject

class PastVideoPlayerPresenter @Inject constructor(private val ruthApi: RuthApi) : BasePresenter<PastVideoPlayerPresenter.PastVideoPlayerView>() {

    fun getPastBroadcast(channelId: String) {
        view?.showLoadingProgress()
        ruthApi.getPastBroadcastInfo(preferences.getUserInfo().userToken, channelId)
                .onErrorReturn { null }
                .doOnError {
                    view?.hideLoadingProgress()
                    Log.d("error", "-> $it")
                }
                .subscribe {
                    view?.showLoadingProgress()
                    if (it != null) {
                        if (it.status) {
                            view?.playVideos(it.data.broadcastUrl)
                            view?.hideLoadingProgress()
                        } else if (it.message.contains(context.getString(R.string.device_login), true)
                                || it.zhHans.contains(context.getString(R.string.device_login), true)
                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
                            view?.invalidLogin()
                            view?.hideLoadingProgress()
                        } else {
                            view?.hideLoadingProgress()
                            view?.finishActivity()
                        }
                    }
                }
    }

    fun getBroadcasterDetail(broadcasterId: String) {
        ruthApi.getUserInfo(preferences.getUserInfo().userToken, broadcasterId)
                .subscribe {
                    if (it != null) {
                        if (it.status) {
                            //        view?.setBroadcasterInfo(it.data)
                            Log.d("status", "->" + it.data.userName)
                        } else if (it.message.contains(context.getString(R.string.device_login), true)
                                || it.zhHans.contains(context.getString(R.string.device_login), true)
                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
                            view?.invalidLogin()
                        } else {

                        }
                    }
                }
    }

    fun clearLogin() {
        val tempFb = utility.getBooleaPreferences(context, getString(R.string.isfbSignup))
        val tempGoogle = utility.getBooleaPreferences(context, getString(R.string.isGoogleSignup))
        val tempInsta = utility.getBooleaPreferences(context, getString(R.string.isInstaSignup))

        preferences.clearPreferences()

        utility.setBooleanPreferences(context, getString(R.string.isfbSignup), tempFb)
        utility.setBooleanPreferences(context, getString(R.string.isGoogleSignup), tempGoogle)
        utility.setBooleanPreferences(context, getString(R.string.isInstaSignup), tempInsta)

        view?.clearPreferences()
    }

    fun followUnfollow(followingId: String, isFollowed: Boolean) {
        val hm = HashMap<String, @JvmSuppressWildcards Any>()
        hm["type"] = "follow"
        hm["isFollow"] = isFollowed
        hm["followingId"] = followingId
        hm["userId"] = preferences.getUserInfo().id

        ruthApi.followUnfollowUser(preferences.getUserInfo().userToken, hm)
                .subscribe {
                    if (it != null) {
                        if (it.status) {

                        } else if (it.message.contains(context.getString(R.string.device_login), true)
                                || it.zhHans.contains(context.getString(R.string.device_login), true)
                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
                            view?.invalidLogin()
                        } else {
                            Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
    }

    interface PastVideoPlayerView : BaseView {
        fun invalidLogin()
        fun clearPreferences()
        fun showLoadingProgress()
        fun hideLoadingProgress()
        fun playVideos(broadcastUrl: String)
        fun finishActivity()
        //   fun setBroadcasterInfo(data: UserProfileData)
    }
}