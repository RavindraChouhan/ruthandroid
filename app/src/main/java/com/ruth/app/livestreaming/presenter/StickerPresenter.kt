package com.ruth.app.livestreaming.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class StickerPresenter @Inject constructor() : BasePresenter<StickerPresenter.StickerView>() {

    interface StickerView : BaseView {

    }
}