package com.ruth.app.livestreaming.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.ruth.app.R
import com.ruth.app.home.activity.HomeActivity
import com.ruth.app.model.StickersData
import kotlinx.android.synthetic.main.item_sticker_cell.view.*

class StickerAdapter(val context: Context, private val stickersList: ArrayList<StickersData>) : RecyclerView.Adapter<StickerAdapter.StickerViewHolder>() {

    private var onItemClickListener: OnItemClickListener? = null
    private var selectedPosition: Int? = null
    private var stickerCount: Int = 0
    private var pointsEarned: Int? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StickerViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.item_sticker_cell, parent, false)
        return StickerViewHolder(view)
    }

    override fun getItemCount(): Int {
        return stickersList.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: StickerViewHolder, position: Int) {

        holder.bind(stickersList[position])

        holder.itemView.setOnClickListener {
            if (selectedPosition == position) {
                return@setOnClickListener
            }
            stickerCount = 0
            if (stickersList[position].giftType.equals(context.getString(R.string.gold_points), true)) {
                if (stickersList[position].giftValue <= HomeActivity.goldPoints) {
                    pointsEarned = HomeActivity.goldPoints
                    stickerCount = HomeActivity.goldPoints / stickersList[position].giftValue
                    selectedPosition = position
                } else {
                    selectedPosition = null
                    Toast.makeText(context, "You don't have enough gold coins", Toast.LENGTH_SHORT).show()
                }
            } else if (stickersList[position].giftType.equals(context.getString(R.string.silver_points), true)) {
                if (stickersList[position].giftValue <= HomeActivity.silverPoints) {
                    pointsEarned = HomeActivity.silverPoints
                    stickerCount = HomeActivity.silverPoints / stickersList[position].giftValue
                    selectedPosition = position
                } else {
                    selectedPosition = null
                    Toast.makeText(context, "You don't have enough silver coins", Toast.LENGTH_SHORT).show()
                }
            } else if (stickersList[position].giftType.equals(context.getString(R.string.coin_points), true)) {
                if (stickersList[position].giftValue <= HomeActivity.yuvanPoints) {
                    pointsEarned = HomeActivity.yuvanPoints
                    stickerCount = (HomeActivity.yuvanPoints / stickersList[position].giftValue)
                    selectedPosition = position
                } else {
                    selectedPosition = null
                    Toast.makeText(context, "You don't have enough coins", Toast.LENGTH_SHORT).show()
                }
            }
            holder.itemView.tvStickerCount.text = stickerCount.toString()
            notifyDataSetChanged()
        }

        if (position == selectedPosition) {
            holder.itemView.layoutCount.visibility = View.VISIBLE
        } else {
            holder.itemView.layoutCount.visibility = View.GONE
        }

        holder.itemView.increaseCount.setOnClickListener {
            stickerCount += 1
            if (stickerCount > 0) {
                holder.itemView.decreaseCount.visibility = View.VISIBLE
            }
            holder.itemView.tvStickerCount.text = stickerCount.toString()
        }

        holder.itemView.decreaseCount.setOnClickListener {
            if (stickerCount == 1) {
                stickerCount = 1
            } else {
                stickerCount -= 1
            }
            holder.itemView.tvStickerCount.text = stickerCount.toString()
        }

        holder.itemView.btnSendFilter.setOnClickListener {
            val totalStickers = stickerCount
            if ((totalStickers * stickersList[position].giftValue) <= pointsEarned!!) {
                onItemClickListener!!.onItemClick(holder.itemView, position, totalStickers)
            } else {
                Toast.makeText(context, "You cannot send the gifts more than " + (pointsEarned!! / stickersList[position].giftValue), Toast.LENGTH_SHORT).show()
            }
            selectedPosition = null
            notifyDataSetChanged()
        }
    }

    inner class StickerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(stickersData: StickersData) {
            Glide.with(context).load(stickersData.image)
                    .apply(RequestOptions().error(R.drawable.logoeye)
                            .placeholder(R.drawable.background))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            itemView.progressBarSticker.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            itemView.progressBarSticker.visibility = View.GONE
                            return false
                        }

                    })
                    .into(itemView.imgSticker)

            itemView.tvPointsValue.text = stickersData.giftValue.toString()
            when {
                stickersData.giftType.equals(context.getString(R.string.gold_points), true) -> {
                    itemView.ivPointsType.setImageDrawable(context.getDrawable(R.drawable.icon_user_in_got_yellow))
                }
                stickersData.giftType.equals(context.getString(R.string.silver_points), true) -> {
                    itemView.ivPointsType.setImageDrawable(context.getDrawable(R.drawable.icon_user_in_got_silver))
                }
                stickersData.giftType.equals(context.getString(R.string.coin_points), true) -> {
                    itemView.ivPointsType.setImageDrawable(context.getDrawable(R.drawable.icon_user_yuan))
                }
            }
            itemView.lblStickerName.text = stickersData.title
        }
    }

    fun setOnItemClickListener(onItemClick: OnItemClickListener) {
        this.onItemClickListener = onItemClick
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int, totalStickers: Int)
    }
}


