package com.ruth.app.livestreaming.dialogs

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.jakewharton.rxbinding2.widget.RxTextView
import com.ruth.app.R
import com.ruth.app.base.lifecycle.AndroidDisposable
import com.ruth.app.livestreaming.activity.BroadcastingNewLiveActivity
import com.ruth.app.livestreaming.activity.LiveVideoPlayerActivity
import com.ruth.app.livestreaming.adapter.UsersViewersAdapter
import com.ruth.app.model.ViewerData
import com.ruth.app.utility.withArguments
import com.ruth.app.widgets.bottomsheet.FullBottomSheetFragment

class UserViewersDialog : FullBottomSheetFragment() {

    private lateinit var rvGifts: RecyclerView
    private lateinit var tvNoGifts: TextView
    private lateinit var tvSearchCancel: TextView
    private lateinit var btnSearch: ImageView
    private lateinit var btnCloseGifts: ImageView
    private lateinit var etSearch: EditText
    private lateinit var tvDialogHeader: TextView
    private lateinit var layoutSearchGifts: RelativeLayout
    private var usersViewersList: ArrayList<ViewerData>? = ArrayList()
    private val isFromStreaming: String? by lazy { arguments?.getString(UserViewersDialog.IS_FROM) }
    private val filtered: ArrayList<ViewerData>? = ArrayList()
    private lateinit var adapter: UsersViewersAdapter

    companion object {
        const val IS_FROM = "IS_FROM"
    }

    fun newInstance(isFrom: String): UserViewersDialog {
        val args = Bundle()
        args.putString(IS_FROM, isFrom)
//        usersViewersList = usersViewerList
        return UserViewersDialog().withArguments(args)
    }


    override fun onCreateDialogView(savedInstanceState: Bundle?, parent: ViewGroup): View {
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_gifts, parent, false)
        rvGifts = view.findViewById(R.id.rvGifts)
        tvNoGifts = view.findViewById(R.id.tvNoGifts)
        btnSearch = view.findViewById(R.id.btnSearch)
        layoutSearchGifts = view.findViewById(R.id.layoutSearchGifts)
        tvSearchCancel = view.findViewById(R.id.tvSearchCancel)
        etSearch = view.findViewById(R.id.etSearch)
        btnCloseGifts = view.findViewById(R.id.btnCloseGifts)
        tvDialogHeader = view.findViewById(R.id.tvDialogHeader)

        tvDialogHeader.text = "Viewers"

        btnSearch.setOnClickListener {
            layoutSearchGifts.visibility = View.VISIBLE
        }

        btnCloseGifts.setOnClickListener {
            dismiss()
        }


        tvSearchCancel.setOnClickListener {
            layoutSearchGifts.visibility = View.GONE
            hideKeyboard()
            etSearch.setText("")
        }

        usersViewersList!!.clear()
        if (isFromStreaming.equals("Broadcasting", true)) {
            usersViewersList = BroadcastingNewLiveActivity.usersViewersList
        } else if (isFromStreaming.equals("StreamingPlayer", true)) {
            usersViewersList = LiveVideoPlayerActivity.usersViewersList
        }

        if (usersViewersList!!.isNotEmpty()) {
            rvGifts.visibility = View.VISIBLE
            tvNoGifts.visibility = View.GONE
            adapter = UsersViewersAdapter(context!!, usersViewersList!!)
            rvGifts.adapter = adapter
            adapter.setOnItemClickListener(object : UsersViewersAdapter.OnItemClickListener {
                override fun onItemClick(view: View, position: Int) {

                }

                override fun onFollowClick(view: View, position: Int) {
                    val isFollowed: Boolean = !usersViewersList!![position].isFollow
                    usersViewersList!![position].isFollow = isFollowed
                    adapter.notifyItemChanged(position)

                    usersViewersList!![position].followers = usersViewersList!![position].followers
                    LocalBroadcastManager.getInstance(context!!).sendBroadcast(Intent("updateFollower")
                            .putExtra("followerId", usersViewersList!![position].id)
                            .putExtra("isFollowed", isFollowed))
                }
            })
        } else {
            rvGifts.visibility = View.GONE
            tvNoGifts.visibility = View.VISIBLE
        }
        return view
    }

    override fun onStart() {
        super.onStart()
        AndroidDisposable.create(this, RxTextView.afterTextChangeEvents(etSearch).subscribe({ event ->
            val text = event.editable()?.toString().orEmpty()
            if (text.length >= 2) {
                filtered?.clear()
                usersViewersList?.filter {
                    it.name?.startsWith(text, true) ?: false
                }?.let { filtered?.addAll(it) }
                adapter = UsersViewersAdapter(context!!, filtered!!)
                rvGifts!!.adapter = adapter
            } else {
                filtered?.clear()
                filtered?.addAll(usersViewersList!!)
                adapter = UsersViewersAdapter(context!!, filtered!!)
                rvGifts!!.adapter = adapter
            }
        }, {}, {}, {}))
    }
}