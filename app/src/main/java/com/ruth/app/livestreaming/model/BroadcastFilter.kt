package com.ruth.app.livestreaming.model

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parceler
import kotlinx.android.parcel.Parcelize


@Parcelize
class BroadcastFilter(var filterId: String,
                      var filterName: String,
                      var filterIcon: Drawable? = null,
                      var filterType: Drawable?) : Parcelable {

    companion object : Parceler<BroadcastFilter> {
        // private val mMyDrawable: Drawable? = null

        override fun BroadcastFilter.write(out: Parcel, flags: Int) {
            // Convert Drawable to Bitmap first:
            val bitmap = (filterIcon as BitmapDrawable).bitmap as Bitmap
            // Serialize bitmap as Parcelable:
            out.writeParcelable(bitmap, flags)
        }

        override fun create(parcel: Parcel): BroadcastFilter = TODO()


    }
}