package com.ruth.app.livestreaming.adapter

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import com.ruth.app.livestreaming.model.BroadcastFilter
import kotlinx.android.synthetic.main.item_filter_cell.view.*

class FilterAdapter(var context: Context, var broadcastFilterList: ArrayList<BroadcastFilter>?) : RecyclerView.Adapter<FilterAdapter.FiltersViewHolder>() {

    private lateinit var recyclerView: RecyclerView
    private var onItemClickListener: OnItemClickListener? = null
    private var selectedPosition: Int? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FiltersViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_filter_cell, parent, false)
        return FiltersViewHolder(view)
    }

    override fun getItemCount(): Int {
        return broadcastFilterList?.size!!
    }

    override fun onBindViewHolder(holder: FiltersViewHolder, position: Int) {
        holder.bind(broadcastFilterList?.get(position)!!)

        holder.itemView.setOnClickListener {
            selectedPosition = position
            onItemClickListener!!.onItemClick(holder.itemView, position)
            notifyDataSetChanged()
        }

        if (position == selectedPosition) {
            holder.itemView.ivFilter.setBackgroundResource(R.drawable.ic_selected_filter_counter)
        } else {
            holder.itemView.ivFilter.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent))
        }
    }

    inner class FiltersViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(broadcastFilter: BroadcastFilter) {
            itemView.ivFilter.setImageDrawable(broadcastFilter.filterIcon)
            itemView.lblFilterName.text = broadcastFilter.filterName
        }
    }

    fun setOnItemClickListener(onItemClick: OnItemClickListener) {
        this.onItemClickListener = onItemClick
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }

}


