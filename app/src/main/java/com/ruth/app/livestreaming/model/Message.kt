package com.ruth.app.livestreaming.model

class Message private constructor(var message: String, var username: String, var photoUrl :String) {
    var type: Int = 0

    class Builder(type: Int) {
        private var mType: Int = 0
        private var mUsername: String = ""
        private var mMessage: String = ""
        private var mPhotoUrl: String = ""

        init {
            mType = type
        }

        fun username(username: String): Builder {
            mUsername = username
            return this
        }

        fun message(message: String): Builder {
            mMessage = message
            return this
        }

        fun photoUrl(photoUrl: String): Builder {
            mPhotoUrl = photoUrl
            return this
        }

        fun build(): Message {
            val message = Message("", "", "")
            message.type = mType
            message.username = mUsername
            message.message = mMessage
            message.photoUrl = mPhotoUrl
            return message
        }
    }

    companion object {
        val TYPE_MESSAGE = 0
    }
}