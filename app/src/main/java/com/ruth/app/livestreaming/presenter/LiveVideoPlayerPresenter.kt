package com.ruth.app.livestreaming.presenter

import android.os.Handler
import android.util.Log
import android.widget.Toast
import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.model.StreamingInfo
import com.ruth.app.model.UserGiftData
import com.ruth.app.model.UserProfileData
import com.ruth.app.model.ViewerData
import com.ruth.app.network.config.RuthApi
import com.ruth.app.utility.RuthUtility
import javax.inject.Inject

class LiveVideoPlayerPresenter @Inject constructor(var ruthApi: RuthApi) : BasePresenter<LiveVideoPlayerPresenter.LiveVideoPlayerView>() {

    fun getBroadcasterDetail(broadcasterId: String, broadcastId: String) {
        ruthApi.getUserWithBroadcast(preferences.getUserInfo().userToken, broadcasterId, broadcastId)
                .subscribe {
                    if (it != null) {
                        if (it.status) {
                            Log.d("internet", "else getHomeBanners")
                            view?.hideVideoProgress()
                            view?.setUserProfile(it.data)
                            Log.d("status", "->" + it.data.userName)
                        } else if (it.message.contains(context.getString(R.string.device_login), true)
                                || it.zhHans.contains(context.getString(R.string.device_login), true)
                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
                            view?.invalidLogin()
                            view?.hideVideoProgress()
                        } else {
                            view?.hideVideoProgress()
                        }
                    }
                }
//        ruthApi.getUserInfo(preferences.getUserInfo().userToken, broadcasterId)
//                .subscribe {
//                    if (it != null) {
//                        if (it.status) {
//                            view?.hideVideoProgress()
//                            view?.setUserProfile(it.data)
//                            Log.d("status", "->" + it.data.userName)
//                        } else if (it.message.contains(context.getString(R.string.device_login), true)
//                                || it.zhHans.contains(context.getString(R.string.device_login), true)
//                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
//                            view?.invalidLogin()
//                            view?.hideVideoProgress()
//                        } else {
//                            view?.hideVideoProgress()
//                        }
//                    }
//                }
/*------*/
//        ruthApi.getUserWithBroadcast(preferences.getUserInfo().userToken, broadcasterId, broadcastId)
//                .subscribe {
//                    if (it != null) {
//                        if (it.status) {
//                            view?.hideVideoProgress()
//                            view?.setUserProfile(it.data)
//                            Log.d("status", "->" + it.data.userName)
//                        } else if (it.message.contains(context.getString(R.string.device_login), true)
//                                || it.zhHans.contains(context.getString(R.string.device_login), true)
//                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
//                            view?.invalidLogin()
//                            view?.hideVideoProgress()
//                        } else {
//                            view?.hideVideoProgress()
//                        }
//                    }
//                }
    }


    fun followUnfollow(followingId: String, isFollowed: Boolean) {
        val hm = HashMap<String, @JvmSuppressWildcards Any>()
        hm["type"] = "follow"
        hm["isFollow"] = isFollowed
        hm["followingId"] = followingId
        hm["userId"] = preferences.getUserInfo().id

        ruthApi.followUnfollowUser(preferences.getUserInfo().userToken, hm)
                .subscribe {
                    if (it != null) {
                        if (it.status) {

                        } else if (it.message.contains(context.getString(R.string.device_login), true)
                                || it.zhHans.contains(context.getString(R.string.device_login), true)
                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
                            view?.invalidLogin()
                        } else {
                            Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
    }

    fun clearLogin() {
        val tempFb = utility.getBooleaPreferences(context, getString(R.string.isfbSignup))
        val tempGoogle = utility.getBooleaPreferences(context, getString(R.string.isGoogleSignup))
        val tempInsta = utility.getBooleaPreferences(context, getString(R.string.isInstaSignup))

        preferences.clearPreferences()

        utility.setBooleanPreferences(context, getString(R.string.isfbSignup), tempFb)
        utility.setBooleanPreferences(context, getString(R.string.isGoogleSignup), tempGoogle)
        utility.setBooleanPreferences(context, getString(R.string.isInstaSignup), tempInsta)

    }

    fun getUsersGifts(broadcastId: String) {
        var giftsList: ArrayList<UserGiftData> = ArrayList()
        ruthApi.getUsersGifts(preferences.getUserInfo().userToken, broadcastId)
                .subscribe {
                    if (it != null) {
                        if (it.status) {
                            Log.d("internet", "else getUserGiftsApi")
                            giftsList = it.data!!
                            view?.updateGifts(giftsList)
                            Log.d("status", " -> " + it.data)
                        } else {
                            Log.d("status", " -> $it")
                        }
                    }
                }
    }


    fun getViewerCount(broadcastId: String) {
        ruthApi.getLiveViewers(preferences.getUserInfo().userToken, broadcastId)
                .onErrorReturn { (null) }
                .doOnError {
                    Log.d("Error", "error" + it.message)
                }
                .subscribe {
                    if (it != null) {
                        if (it.status) {
                            Log.d("internet", "else getViewerCountApi")
                            if (it.data!!.isNotEmpty()) {
                                view?.updateViewers(it.data!![0].userData!!)
                            }
                        } else {
                            Log.d("status", " -> $it")
                        }
                    }
                }
    }

    private var mDelayHandler: Handler? = null
    private val splashDelay: Long = 2000 //2 seconds
    private var broadcastId: String = ""

    fun showEndStreamingData(broadcastStreamId: String) {
        if (utility.isConnectingToInternet(context)) {
            mDelayHandler = Handler()
            //Navigate with delay
            broadcastId = broadcastStreamId
            mDelayHandler!!.postDelayed(mRunnable, splashDelay)
        } else {
            val thread = Thread(Runnable {
                val internetConnection = utility.checkInternetWithoutDialog(context)
                if (internetConnection) {
                    mDelayHandler = Handler()
                    //Navigate with delay
                    broadcastId = broadcastStreamId
                    mDelayHandler!!.postDelayed(mRunnable, splashDelay)
                } else {
                    showEndStreamingData(broadcastStreamId)
                    mDelayHandler!!.removeCallbacks(mRunnable)
                }
            })
            thread.start()
        }
    }

    private val mRunnable: Runnable = Runnable {
        ruthApi.getEndStreamingInfo(preferences.getUserInfo().userToken, broadcastId)
                .onErrorReturn { (null) }
                .doOnError {
                    Log.d("Error", "error" + it.message)
                }
                .subscribe {
                    if (it != null) {
                        if (it.status) {
                            Log.d("internet", "else mRunnable")
                            view?.hideVideoProgress()
                            view!!.updateStreamingInfo(it.data)
                        } else {
                            Log.d("status", " -> $it")
                        }
                    }
                }
    }

    fun postOrderStatus(customOrderId: String, position: Int) {
        val hm = HashMap<String, @JvmSuppressWildcards Any>()
        hm["type"] = "completed"
        hm["customId"] = customOrderId

        ruthApi.postOrderStatus(preferences.getUserInfo().userToken, hm)
                .onErrorReturn { (null) }
                .doOnError {
                    Log.d("Error", "error" + it.message)
                }
                .subscribe {
                    if (it != null) {
                        if (it.status) {
//                            view!!.updateOrders(position)
                            Log.d("orderStatus", " -> " + it.data)
                        } else {
                            Toast.makeText(context, "" + it.message, Toast.LENGTH_SHORT).show()
                            Log.d("status", " -> $it")
                        }
                    }
                }
    }

    fun getMyProfile() {
        ruthApi.getUserInfo(preferences.getUserInfo().userToken, preferences.getUserInfo().id)
                .subscribe({
                    if (it != null) {
                        if (it.status) {
                            Log.d("internet", "else getProfileApi")
                            preferences.setUserProfile(it.data)
                            if (it.data.pointEarn!!.isNotEmpty()) {
                                RuthUtility.updatePoints(context, it.data.pointEarn)
                                view!!.updatePointsData()
                            }
                        } else {
                            // view?.invalidLogin()
                        }
                    }
                }, {
                    view?.showToast(getString(R.string.connection_error))
                })
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }
        Runtime.getRuntime().gc()
    }

    interface LiveVideoPlayerView : BaseView {
        fun invalidLogin()
        fun showVideoProgress()
        fun hideVideoProgress()
        fun setUserProfile(data: UserProfileData)
        fun updateGifts(giftsList: ArrayList<UserGiftData>)
        fun updateViewers(giftsList: java.util.ArrayList<ViewerData>)
        fun updateStreamingInfo(data: StreamingInfo?)
        fun showNoInternetDialog()
        fun updateOrders(position: Int)
        fun updatePointsData()
    }
}