package com.ruth.app.livestreaming.activity

import android.Manifest
import android.annotation.SuppressLint
import android.content.*
import android.content.res.Configuration
import android.graphics.Color
import android.hardware.Camera
import android.net.Uri
import android.opengl.GLSurfaceView
import android.os.*
import android.support.design.widget.TabLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.view.animation.AccelerateInterpolator
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.livebroadcasting.ILiveVideoBroadcaster
import com.livebroadcasting.LiveVideoBroadcaster
import com.ruth.app.App
import com.ruth.app.BuildConfig
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.broadcaster.activity.BroadcastDetailActivity
import com.ruth.app.home.activity.HomeActivity
import com.ruth.app.livestreaming.activity.EnterBroadcastingDetailsActivity.Companion.broadcastFilter
import com.ruth.app.livestreaming.adapter.*
import com.ruth.app.livestreaming.dialogs.UserReceivedGiftsDialog
import com.ruth.app.livestreaming.dialogs.UserViewersDialog
import com.ruth.app.livestreaming.model.BroadcastFilter
import com.ruth.app.livestreaming.presenter.BroadcastingLivePresenter
import com.ruth.app.login.activity.LoginActivity
import com.ruth.app.model.*
import com.ruth.app.utility.RuthUtility
import com.ruth.app.widgets.emojianim.ParticleSystem
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_live_video_broadcaster_new.*
import kotlinx.android.synthetic.main.view_chat_broadcast.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class BroadcastingNewLiveActivity : BaseActivity<BroadcastingLivePresenter>(), BroadcastingLivePresenter.LiveVideoBroadcastingView, View.OnClickListener {

    internal var mIsRecording = false
    private var mTimer: Timer? = null
    private var mElapsedTime: Long = 0
    private var mTimerHandler: TimerHandler? = null
    private var mLiveVideoBroadcasterServiceIntent: Intent? = null
    private var mGLView: GLSurfaceView? = null
    private var mLiveVideoBroadcaster: ILiveVideoBroadcaster? = null
    private var mBroadcastControlButton: Button? = null

    //chat messages
    private val mMessages = ArrayList<com.ruth.app.livestreaming.model.Message>()
    private var adapter: ChatAdapter? = null
    private var zoomout: Animation? = null

    private var mDelayHandler: Handler? = Handler()
    private val splashDelay: Long = 300 //0.3 second
    private var currentTime: String = ""
    private var receivedOrdersAdapter: ReceivedOrdersAdapter? = null
    private val customOrderList: ArrayList<HashMap<String, @JvmSuppressWildcards Any>>? = ArrayList()
    private var givenCustomOrdersAdapter: GivenCustomOrdersAdapter? = null

    companion object {
        internal const val CONNECTION_LOST = 2
        internal const val INCREASE_TIMER = 1
        var usersViewersList: ArrayList<ViewerData> = ArrayList()
        var usersGiftList: ArrayList<UserGiftData> = ArrayList()
        // val broadcastFilter: ArrayList<BroadcastFilter>? = ArrayList()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Hide title
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        //binding on resume not to having leaked service connection
        mLiveVideoBroadcasterServiceIntent = Intent(this, LiveVideoBroadcaster::class.java)
        //this makes service do its job until done
        startService(mLiveVideoBroadcasterServiceIntent)

        App.component.inject(this)
        presenter.onCreate(this)
        setContentView(R.layout.activity_live_video_broadcaster_new)
        presenter.getUsersGifts(intent.getStringExtra("broadcastId"))
        presenter.getViewerCount(intent.getStringExtra("broadcastId"))
        presenter.getInvitedUsers(intent.getStringExtra("broadcastId"))
        presenter.getCustomOrders(intent.getStringExtra("broadcastId")/*"5bb5de09498124732e098a45"*/)
//        presenter.getCustomOrders("5bb5de09498124732e098a45")

        mTimerHandler = TimerHandler()
        mBroadcastControlButton = findViewById(R.id.toggle_broadcasting)
        // Configure the GLSurfaceView. This will start the Renderer thread, with an
        // appropriate EGL activity.
        mGLView = findViewById<GLSurfaceView>(R.id.cameraPreview_surfaceView)
        if (mGLView != null) {
            mGLView!!.setEGLContextClientVersion(2) // select GLES 2.0
        }

        setUpSocket()
        setUpFilters()

        rvMessagesList.layoutManager = LinearLayoutManager(this)
        adapter = ChatAdapter(this, mMessages)
        rvMessagesList.adapter = adapter

        etChatMessage.setOnEditorActionListener(TextView.OnEditorActionListener { v, id, event ->
            if (id == R.id.send || id == EditorInfo.IME_NULL) {
                com.ruth.app.network.hideKeyboard(v)
                attemptSend()
                return@OnEditorActionListener true
            }
            false
        })

        btnSendMessage.setOnClickListener {
            hideKeyboard()
            attemptSend()
        }

        ivRotateCamera.setOnClickListener(this)
        ivChat.setOnClickListener(this)
        ivFilter.setOnClickListener(this)
        ivOrderList.setOnClickListener(this)
        ivInvitation.setOnClickListener(this)
        ivCandleStatus.setOnClickListener(this)
        ivStopBroadcasting.setOnClickListener(this)
        layoutReceivedGifts.setOnClickListener(this)
        layoutViewCount.setOnClickListener(this)
        mRootView.setOnClickListener(this)

        zoomout = AnimationUtils.loadAnimation(this, R.anim.zoom_out_anim)

        startCountDown()
        LocalBroadcastManager.getInstance(this).registerReceiver(updateFilter, IntentFilter("updateFilter"))
        LocalBroadcastManager.getInstance(this).registerReceiver(updateFollower, IntentFilter("updateFollower"))

        if (broadcastFilter!!.isNotEmpty()) {
            val filterSelected = intent.getBooleanExtra("filterSelected", false)
            if (filterSelected) {
                if (intent.getIntExtra("selectedFilter", 0) != null) {
                    funSetFilters(intent.getIntExtra("selectedFilter", 0))
                }
            }
        }

        if (mLiveVideoBroadcaster != null) {
            if ((intent.getIntExtra("cameraRotate", 0)) != null) {
//                val cameraValue = intent.getIntExtra("cameraRotate", 0)
                mLiveVideoBroadcaster!!.changeCamera()
//                if (cameraValue == 0) {
//                    mLiveVideoBroadcaster?.openCamera(Camera.CameraInfo.CAMERA_FACING_BACK)
//                } else {
//                    mLiveVideoBroadcaster?.openCamera(Camera.CameraInfo.CAMERA_FACING_FRONT)
//                }
            }
        }
    }

    private fun startCountDown() {
        object : CountDownTimer(10000, 1000) {
            override fun onFinish() {
                lblCounter.visibility = View.GONE
                lblCounter.text = ""
            }

            override fun onTick(millisUntilFinished: Long) {
                var count = lblCounter.text.toString().toInt()
                count -= 1
                lblCounter.text = count.toString()
                lblCounter.startAnimation(zoomout)
                if (count == 1) {
                    toggleBroadcasting()
                }
            }
        }.start()
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.ivStopBroadcasting) {
            resetButtonsBackground()
            stopBroadcastingLive()
        }

        if (v?.id == R.id.layoutReceivedGifts) {
            if (usersGiftList.size > 0) {
                UserReceivedGiftsDialog().newInstance("Broadcasting")
                        .show(this.supportFragmentManager, UserReceivedGiftsDialog::javaClass.name)
            } else {
                Toast.makeText(this, "No gifts received yet..", Toast.LENGTH_SHORT).show()
            }
        }

        if (v?.id == R.id.layoutViewCount) {
            if (usersViewersList.size > 0) {
                UserViewersDialog().newInstance("Broadcasting")
                        .show(this.supportFragmentManager, UserViewersDialog::javaClass.name)
            } else {
                Toast.makeText(this, "No viewers yet..", Toast.LENGTH_SHORT).show()
            }
        }

        if (v?.id == R.id.ivCandleStatus) {
            candleStartStop()
        }

        if (v?.id == R.id.mRootView) {
            lytOptionSelector.visibility = View.VISIBLE
            layoutChat.visibility = View.GONE
            layoutFilter.visibility = View.GONE
            resetButtonsBackground()
        }

        if (v?.id == R.id.ivChat) {
            resetButtonsBackground()
            bgChat.setColorFilter(ContextCompat.getColor(this, R.color.red))

            mDelayHandler!!.postDelayed({
                lytOptionSelector.visibility = View.GONE
                layoutChat.visibility = View.VISIBLE
            }, splashDelay)
        }

        if (v?.id == R.id.ivFilter) {
            resetButtonsBackground()
            bgFilter.setColorFilter(ContextCompat.getColor(this, R.color.red))

            mDelayHandler!!.postDelayed({
                lytOptionSelector.visibility = View.GONE
                layoutFilter.visibility = View.VISIBLE
            }, splashDelay)
        }

        if (v?.id == R.id.ivOrderList) {
            resetButtonsBackground()
            bgOrderList.setColorFilter(ContextCompat.getColor(this, R.color.red))

            mDelayHandler!!.postDelayed({
                //                presenter.getCustomOrders("5bb5de09498124732e098a45")
                if (ordersList!!.isNotEmpty()) {
                    showOrderPopup()
                } else {
                    Toast.makeText(this, "No orders received yet..", Toast.LENGTH_SHORT).show()
                    resetButtonsBackground()
                }

            }, splashDelay)

        }

        if (v?.id == R.id.ivInvitation) {
            resetButtonsBackground()
            ivInvitation.setImageResource(R.drawable.selected_speaker)

            mDelayHandler!!.postDelayed({
                if (invitedFansList!!.isNotEmpty()) {
                    showInvitationPopup()
                } else {
                    Toast.makeText(this, "No invitations received yet..", Toast.LENGTH_SHORT).show()
                    resetButtonsBackground()
                }
                // presenter.getInvitedUsers(intent.getStringExtra("broadcastId")/*"5bb202768f4ac93d5c9042a3"*/)
            }, splashDelay)
        }

        if (v?.id == R.id.ivRotateCamera) {
            resetButtonsBackground()
            bgRotateCamera.setColorFilter(ContextCompat.getColor(this, R.color.red))

            mDelayHandler!!.postDelayed({
                changeCamera(v)
            }, splashDelay)
        }
    }

    private fun resetButtonsBackground() {
        bgChat.setColorFilter(ContextCompat.getColor(this, R.color.green))
        bgFilter.setColorFilter(ContextCompat.getColor(this, R.color.green))
        bgOrderList.setColorFilter(ContextCompat.getColor(this, R.color.green))
        ivInvitation.setImageResource(R.drawable.unselected_speaker)
        bgRotateCamera.setColorFilter(ContextCompat.getColor(this, R.color.green))
    }

    override fun onStart() {
        super.onStart()
        //this lets activity bind
        try {
            bindService(mLiveVideoBroadcasterServiceIntent, mConnection, 0)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onPause() {
        //hide dialog if visible not to create leaked window exception
        mLiveVideoBroadcaster!!.pause()
//        Log.d("pause", " --------")
        super.onPause()
    }


    override fun onStop() {
        unbindService(mConnection)
//        Log.d("onStop", " --------")
        super.onStop()
    }

    override fun onDestroy() {
        triggerStopRecording()
//        Log.d("destroy", " ------")
        super.onDestroy()
    }

    override fun onBackPressed() {
        stopBroadcastingLive()
        leave()
        //  super.onBackPressed()
    }

    /*---------------------------------------START LIVE BROADCASTING------------------------------*/
    fun changeCamera(v: View) {
        if (mLiveVideoBroadcaster != null) {
            mLiveVideoBroadcaster!!.changeCamera()
            resetButtonsBackground()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            LiveVideoBroadcaster.PERMISSIONS_REQUEST -> {
                if (mLiveVideoBroadcaster!!.isPermissionGranted) {
                    mLiveVideoBroadcaster!!.openCamera(Camera.CameraInfo.CAMERA_FACING_BACK)
                } else {
                    if ((ActivityCompat.shouldShowRequestPermissionRationale(this,
                                    Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                                    Manifest.permission.RECORD_AUDIO))) {
                        mLiveVideoBroadcaster!!.requestPermission()
                    } else {
                        AlertDialog.Builder(this@BroadcastingNewLiveActivity)
                                .setTitle(R.string.permission)
                                .setMessage(getString(R.string.app_doesnot_work_without_permissions))
                                .setPositiveButton(android.R.string.yes) { dialog, which ->
                                    try {
                                        //Open the specific App Info page:
                                        val intent = Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                        intent.data = Uri.parse("package:" + applicationContext.packageName)
                                        startActivity(intent)
                                    } catch (e: ActivityNotFoundException) {
                                        //e.printStackTrace();
                                        //Open the generic Apps page:
                                        val intent = Intent(android.provider.Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS)
                                        startActivity(intent)
                                    }
                                }
                                .show()
                    }
                }
                return
            }
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (newConfig.orientation === Configuration.ORIENTATION_LANDSCAPE ||
                newConfig.orientation === Configuration.ORIENTATION_PORTRAIT) {
            mLiveVideoBroadcaster!!.setDisplayOrientation()
        }
    }

    fun toggleBroadcasting() {
        if (!mIsRecording) {
            if (mLiveVideoBroadcaster != null) {
                if (!mLiveVideoBroadcaster!!.isConnected) {
                    val streamName: String
                    if (!intent.getStringExtra("streamId").isEmpty()) {
                        streamName = intent.getStringExtra("streamId")
                    } else {
                        streamName = "live_DefaultUsername"
                    }
                    Log.d("streamName", "->$streamName")

                    object : AsyncTask<String, String, Boolean>() {
                        override fun onPreExecute() {
                            stopBroadcastingSocket(true)
                            toggle_broadcasting.visibility = View.GONE
                            if (!isFinishing && window.decorView.isShown) {
                                RuthUtility.showLoader(this@BroadcastingNewLiveActivity)
                            }
                            presenter.updateLiveStatus(intent.getStringExtra("broadcastId"), true)

                        }

                        override fun doInBackground(vararg url: String): Boolean {
                            return mLiveVideoBroadcaster!!.startBroadcasting(url[0])
                        }

                        override fun onPostExecute(result: Boolean) {
                            mIsRecording = result
                            if (result) {
                                RuthUtility.hideLoader()
                                mBroadcastControlButton!!.visibility = View.GONE
                                mStreamLiveStatus.visibility = View.VISIBLE
                                ivStopBroadcasting.visibility = View.VISIBLE
                                lytOptionSelector.visibility = View.VISIBLE
                                layoutHeaderToolbar.visibility = View.VISIBLE
                                layoutBottomToolbar.visibility = View.VISIBLE
//                                mBroadcastControlButton!!.setText(R.string.stop_broadcasting)
                                startTimer()//start the recording duration
                            } else {
                                // Snackbar.make(mRootView, R.string.stream_not_started, Snackbar.LENGTH_LONG).show();
                                triggerStopRecording()
                            }
                        }
                    }.execute(BuildConfig.RTMP_BASE_URL + streamName)
                } else {
                    // Snackbar.make(mRootView, R.string.streaming_not_finished, Snackbar.LENGTH_LONG).show();
                }
            } else {
                // Snackbar.make(mRootView, R.string.oopps_shouldnt_happen, Snackbar.LENGTH_LONG).show();
            }
        } else {
            stopBroadcastingSocket(false)
            triggerStopRecording()
        }
    }

    private fun stopBroadcastingLive() {
        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_end_live_streaming, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val btnOk = dialogView.findViewById<View>(R.id.btnOk) as TextView
        val btnCancel = dialogView.findViewById<View>(R.id.btnCancel) as TextView

        btnOk.setOnClickListener {
            b.dismiss()
            stopBroadcastingSocket(false)
            triggerStopRecording()
        }

        btnCancel.setOnClickListener {
            b.dismiss()
        }

        b.setCancelable(false)
        b.show()

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val displayWidth = displayMetrics.widthPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.9f).toInt()
        layoutParams.width = dialogWindowWidth
        b.window.attributes = layoutParams
        b.window.setGravity(Gravity.CENTER_VERTICAL)
    }

    fun triggerStopRecording() {
        if (mIsRecording) {
            stopTimer()
            stopBroadcastingSocket(false)
            mLiveVideoBroadcaster!!.stopBroadcasting()
            presenter.updateLiveStatus(intent.getStringExtra("broadcastId"), false)
            mBroadcastControlButton!!.setText(R.string.start_broadcasting)
            mStreamLiveStatus.visibility = View.GONE
            lytOptionSelector.visibility = View.GONE
            layoutHeaderToolbar.visibility = View.GONE
            layoutBottomToolbar.visibility = View.GONE
            Handler().postDelayed((Runnable { finish() }), 1000)
        }
        mIsRecording = false
    }

    //This method starts a mTimer and updates the textview to show elapsed time for recording
    fun startTimer() {
        if (mTimer == null) {
            mTimer = Timer()
        }
        mElapsedTime = 0
        mTimer!!.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                mElapsedTime += 1 //increase every sec
                mTimerHandler!!.obtainMessage(INCREASE_TIMER).sendToTarget()
                if (mLiveVideoBroadcaster == null || !mLiveVideoBroadcaster!!.isConnected) {
                    mTimerHandler!!.obtainMessage(CONNECTION_LOST).sendToTarget()
                }
            }
        }, 0, 1000)
    }

    private fun stopTimer() {
        if (mTimer != null) {
            this.mTimer!!.cancel()
        }
        this.mTimer = null
        this.mElapsedTime = 0
    }

    private inner class TimerHandler : Handler() {
        @SuppressLint("SetTextI18n")
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                INCREASE_TIMER -> {
                    currentTime = getDurationString(mElapsedTime.toInt())
                    //   mStreamLiveStatus.text = getString(R.string.live_indicator) + " - " + getDurationString(mElapsedTime.toInt())
                    if (isCandleOn) {
                        tvCurrentTime.text = getDurationString(mElapsedTime.toInt())
                    }

                }
                CONNECTION_LOST -> {
                    stopBroadcastingSocket(false)
                    triggerStopRecording()
                    AlertDialog.Builder(this@BroadcastingNewLiveActivity)
                            .setMessage(R.string.broadcast_connection_lost)
                            .setPositiveButton(android.R.string.yes, null)
                            .show()
                }
            }
        }
    }

    fun getDurationString(second: Int): String {
        var seconds = second

        if (seconds < 0 || seconds > 2000000)
        //there is an codec problem and duration is not set correctly,so display meaningfull string
            seconds = 0
        val hours = seconds / 3600
        val minutes = seconds % 3600 / 60
        seconds %= 60

        /*return if (hours == 0)
            twoDigitString(minutes) + ":" + twoDigitString(seconds)
        else
            twoDigitString(hours) + ":" + twoDigitString(minutes) + ":" + twoDigitString(seconds)
*/
        return if (hours != 0) {
            String.format("%02d:%02d:%02d", hours, minutes, seconds)
        } else {
            String.format("%02d:%02d", minutes, seconds)
        }
    }

    private fun twoDigitString(number: Int): String {
        if (number == 0) {
            return "00"
        }
        if (number / 10 == 0) {
            return "0$number"
        }
        return (number).toString()
    }

    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private val mConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName,
                                        service: IBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            val binder = service as LiveVideoBroadcaster.LocalBinder
            if (mLiveVideoBroadcaster == null) {
                mLiveVideoBroadcaster = binder.service
                Log.d("view", "---------$mGLView")
                mLiveVideoBroadcaster!!.init(this@BroadcastingNewLiveActivity, mGLView)
                mLiveVideoBroadcaster!!.setAdaptiveStreaming(true)
            }

            if ((intent.getIntExtra("cameraRotate", 0)) != null) {
                val cameraValue = intent.getIntExtra("cameraRotate", 0)
                if (cameraValue == 0) {
                    mLiveVideoBroadcaster?.openCamera(Camera.CameraInfo.CAMERA_FACING_BACK)
                } else {
                    mLiveVideoBroadcaster?.openCamera(Camera.CameraInfo.CAMERA_FACING_FRONT)
                }
            }
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mLiveVideoBroadcaster = null
        }
    }
    /*---------------------------------------END LIVE BROADCASTING--------------------------------*/


    /*---------------------------------------START OF SOCKET--------------------------------------*/
    private var mSocket: Socket? = null
    private var isConnected: Boolean? = true

    private fun attemptSend() {

        if (!mSocket!!.connected()) return

        val message = etChatMessage.text.toString().trim({ it <= ' ' })
        if (TextUtils.isEmpty(message)) {
            etChatMessage.requestFocus()
            return
        }

        etChatMessage.setText("")

        val postData = JSONObject()
        try {
            postData.put("broadcastId", intent.getStringExtra("broadcastId"))
            postData.put("userId", intent.getStringExtra("userId"))
            postData.put("message", message)
            postData.put("type", "comment")

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        mSocket!!.emit("COMMENTS", postData)
    }

    private fun sendFilters(filterName: String) {

        if (!mSocket!!.connected()) return

        val postData = JSONObject()
        try {
            postData.put("broadcastId", intent.getStringExtra("broadcastId"))
            postData.put("filterId", filterName)
            postData.put("type", "filter")
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        mSocket!!.emit("FILTER", postData)
    }

    private fun stopBroadcastingSocket(liveStatus: Boolean) {

        if (!mSocket!!.connected()) return

        val postData = JSONObject()
        try {
            postData.put("isLiveStatus", liveStatus)
            postData.put("broadcasterUserId", preferences.getUserInfo().id)
            postData.put("broadcastId", intent.getStringExtra("broadcastId"))
            if (!liveStatus) {
                if (tvStreamLiveViewers.text.isNotEmpty()) {
                    postData.put("broadcastViewCount", tvStreamLiveViewers.text.toString())
                }
                postData.put("broadcastEndTime", currentTime.trim())
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        mSocket!!.emit("LIVE", postData)
    }

    private fun setUpSocket() {
        val app = this.application as App
        mSocket = app.socket
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
        mSocket!!.on("RECEIVE_MESSAGE", onNewMessage)
        mSocket!!.on("RECEIVE_LIKE", onNewLike)
        mSocket!!.on("RECEIVE_VIEWS", onNewView)
        mSocket!!.on("RECEIVE_STICKER", onNewSticker)
        mSocket!!.on("RECEIVE_EMOJI", onNewEmoji)
        mSocket!!.on("RECEIVE_ELITE", onGetEliteUser)
        mSocket!!.on("RECEIVE_CUSTOM_ORDER", onReceiveGift)
        mSocket!!.on("RECEIVE_COMPLETE_CUSTOM_ORDER", onCompleteCustomOrder)
        mSocket!!.connect()
    }

    private val onConnect = Emitter.Listener {
        this.runOnUiThread {
            if (!isConnected!!) {
                isConnected = true
            }
        }
    }

    private val onDisconnect = Emitter.Listener {
        this.runOnUiThread {
            Log.i("socket", "disconnected")
            isConnected = false
        }
    }

    private val onConnectError = Emitter.Listener {
        this.runOnUiThread {
            Log.e("socket", "Error connecting")
        }
    }

    private val onNewMessage = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            try {
                val data = args[0] as JSONObject
                val username: String
                val message: String
                val photoUrl: String
                val broadcastId: String
                try {
                    username = data.getString("username")
                    message = data.getString("message")
                    photoUrl = data.getString("photoUrl")
                    broadcastId = data.getString("broadcastId")
                    if (broadcastId == intent.getStringExtra("broadcastId")) {
                        addMessage(username, message, photoUrl, "chat")
                    }
                } catch (e: JSONException) {
                    Log.e("socket", e.message)
                    return@Runnable
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private val onReceiveGift = Emitter.Listener { args ->
        this.runOnUiThread {
            try {
                val data = args[0] as JSONObject
                val username: String
                val message: String
                val broadcastId: String
                val photoUrl: String
                try {
                    username = data.getString("username")
                    message = data.getString("message")
                    photoUrl = data.getString("photoUrl")
                    broadcastId = data.getString("broadcastId")
                    if (broadcastId == intent.getStringExtra("broadcastId")) {
                        addMessage(username, message, photoUrl, "customGift")
                        presenter.getCustomOrders(broadcastId)
                    }
                } catch (e: JSONException) {
                    Log.e(TAG, e.message)
                    return@runOnUiThread
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun addMessage(username: String, message: String, photoUrl: String, messageType: String) {
        mMessages.add(com.ruth.app.livestreaming.model.Message.Builder(com.ruth.app.livestreaming.model.Message.TYPE_MESSAGE)
                .username(username)
                .message(message)
                .photoUrl(photoUrl).build())
        try {
            adapter!!.notifyChat(messageType, mMessages.size - 1)
            //adapter!!.notifyItemInserted(mMessages.size - 1)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        scrollToBottom(mMessages.size - 1)
    }

    private val onNewLike = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            try {
                val data = args[0] as JSONObject
//                Log.d("args", "data :- $data")
                val broadcastId: String
                try {
                    broadcastId = data.getString("broadcastId")
                    if (broadcastId == intent.getStringExtra("broadcastId")) {
                        val ps = ParticleSystem(this, 1, R.drawable.icon_like, 2000)
                        ps.setScaleRange(1.3f, 1.3f)
                        ps.setSpeedModuleAndAngleRange(0.07f, 0.20f, 270, 270)
                        ps.setFadeOut(400, AccelerateInterpolator())
                        ps.emit(lytLike, 1)
                        Handler().postDelayed({
                            ps.stopEmitting()
                            ps.cancel()
                        }, 2000)
                    }
                } catch (e: JSONException) {
                    Log.e(TAG, e.message)
                    return@Runnable
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private val onNewEmoji = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            try {
                val data = args[0] as JSONObject
//                Log.d("args", "-data :- $data")
                val broadcastId: String
                try {
                    broadcastId = data.getString("broadcastId")
                    if (broadcastId == intent.getStringExtra("broadcastId")) {
                        val ps = ParticleSystem(this, 1, R.drawable.scary_emoji, 2000)
                        ps.setScaleRange(1.3f, 1.3f)
                        ps.setSpeedModuleAndAngleRange(0.07f, 0.20f, 270, 270)
                        ps.setFadeOut(400, AccelerateInterpolator())
                        ps.emit(lytLike, 1)
                        Handler().postDelayed({
                            ps.stopEmitting()
                            ps.cancel()
                        }, 2000)
                    }
                } catch (e: JSONException) {
                    Log.e(TAG, e.message)
                    return@Runnable
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private val onNewView = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            try {
                val data = args[0] as JSONObject
//                Log.d("args", "-data :- $data")
                val broadcastId: String
                val viewCount: Int
                try {
                    broadcastId = data.getString("broadcastId")
                    if (broadcastId == intent.getStringExtra("broadcastId")) {
                        viewCount = data.getInt("viewCount")
                        presenter.getViewerCount(intent.getStringExtra("broadcastId"))
//                        Log.d("viewCount", "receive -> $viewCount")
                        if (viewCount >= 1) {
                            // layoutLiveCount.visibility = android.view.View.VISIBLE
                        } else {
                            //layoutLiveCount.visibility = android.view.View.GONE
                        }
                        tvViewersCount.text = RuthUtility.getCounts(viewCount)
                        tvStreamLiveViewers.text = RuthUtility.getCounts(viewCount)
                    }

                } catch (e: JSONException) {
                    Log.e(TAG, e.message)
                    return@Runnable
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private val onNewSticker = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            try {
                val data = args[0] as JSONObject
                val broadcastId: String
                try {
                    broadcastId = data.getString("broadcastId")
                    if (broadcastId == intent.getStringExtra("broadcastId")) {
                        var giftCount = data.getInt("giftCount")
                        tvGiftCount.text = RuthUtility.getCounts(giftCount)
                        presenter.getUsersGifts(intent.getStringExtra("broadcastId"))
                        android.widget.Toast.makeText(this, "StickerReceived", android.widget.Toast.LENGTH_SHORT).show()
                    }
                } catch (e: JSONException) {
                    Log.e("socket", e.message)
                    return@Runnable
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private val onGetEliteUser = Emitter.Listener { args ->
        this.runOnUiThread {
            try {
                val data = args[0] as JSONArray
                var liveUsers: JSONObject
                var giftData: JSONObject
                val liveViewerList: ArrayList<LiveViewers> = ArrayList()

                for (i in 0 until data.length()) {
                    val jsonData = data[i] as JSONObject
                    giftData = jsonData.getJSONObject("giftId")
                    liveUsers = jsonData.getJSONObject("userId")
                    val liveViewers = LiveViewers(GiftId(giftData.getString("_id"), giftData.getString("giftType")),
                            UserId(liveUsers.getString("_id"), liveUsers.getString("photoUrl")),
                            jsonData.getInt("count"))
                    liveViewerList.add(liveViewers)
                }

                listUsers.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
                val liveViewersAdapter = LiveViewersAdapter(this, liveViewerList)
                listUsers.adapter = liveViewersAdapter

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private val onCompleteCustomOrder = Emitter.Listener { args ->
        this.runOnUiThread {
            try {
                val data = args[0] as JSONObject
                var comment: String = ""
                var currentTime: String = ""

                try {
                    presenter.getCustomOrders(intent.getStringExtra("broadcastId"))
                    comment = data.getString("comment")
                    currentTime = data.getString("currentTime")

                    presenter.getMyProfile()
                    if (data.getBoolean("isCompleted")) {
//                        Log.e("status", "completed -> " + data.getBoolean("isCompleted"))
                        layoutOrder.visibility = View.VISIBLE
                        tvOrderCompleted.text = comment

                        val currentSystemTime = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(System.currentTimeMillis()).toString()
                        val diff = RuthUtility.printTimeDifference(currentTime, currentSystemTime, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")

                        tvOrderTime.text = diff
                    }

                } catch (e: Exception) {
                    Log.e(TAG, e.message)
                    return@runOnUiThread
                }
//                Log.d("data", " -> $data")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

//    private fun setOrdersList() {
//        givenCustomOrdersAdapter = GivenCustomOrdersAdapter(this, customOrderList)
//        rvOrdersList.adapter = givenCustomOrdersAdapter
//        givenCustomOrdersAdapter!!.setOnItemClickListener(object : GivenCustomOrdersAdapter.OnItemClickListener {
//            override fun onItemClick(view: View, position: Int) {
//                presenter.postOrderStatus(customOrderList!![position]["customOrderId"].toString(), position)
//            }
//        })
//    }

    private fun leave() {
        mSocket!!.disconnect()
        mSocket!!.connect()
    }

    /*---------------------------------------END OF SOCKET--------------------------------------*/

    private fun scrollToBottom(position: Int) {
//        rvMessagesList.scrollToPosition(position)
    }

    /*----------------------------------------UI UPDATE-------------------------------------------*/
    private var isCandleOn: Boolean = false
//    private val broadcastFilter: ArrayList<BroadcastFilter>? = ArrayList()

    private fun candleStartStop() {
        if (isCandleOn) {
            isCandleOn = false
            ivCandleStatus.setImageResource(R.drawable.icon_candle_off)
            tvCurrentTime.setShadowLayer(3.0f, 2.0f, 2.0f, Color.parseColor("#d0021b"))
            tvCandleOffTime.visibility = View.VISIBLE
            tvCurrentTime.visibility = View.GONE
            tvCandleOffTime.text = "未開始"
        } else {
            isCandleOn = true
            ivCandleStatus.setImageResource(R.drawable.icon_candle_on)
            tvCurrentTime.setShadowLayer(3.0f, 2.0f, 2.0f, Color.parseColor("#35fd08"))
            tvCandleOffTime.visibility = View.GONE
            tvCurrentTime.visibility = View.VISIBLE
        }
    }

    private fun setUpFilters() {
        broadcastFilter?.clear()
        broadcastFilter?.add(BroadcastFilter("1", "None",
                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                        R.drawable.filter_none),
                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.transparent_bg)))
        broadcastFilter?.add(BroadcastFilter("2", "Filter 1",
                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                        R.drawable.filter_1),
                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.filter_image_purple)))
        broadcastFilter?.add(BroadcastFilter("3", "Filter 2",
                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                        R.drawable.filter_2),
                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.filter_image_green)))
        broadcastFilter?.add(BroadcastFilter("4", "Filter 3",
                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                        R.drawable.filter_3),
                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.filter_image_red)))

        val filterPagerAdapter = FilterPagerAdapter(supportFragmentManager, broadcastFilter)
        viewPagerFilter!!.adapter = filterPagerAdapter
        tabFilter.setupWithViewPager(viewPagerFilter)

        tabFilter.getTabAt(0)!!.setText(R.string.all)
        tabFilter.getTabAt(1)!!.setText(R.string.classic)
        tabFilter.getTabAt(2)!!.setText(R.string.horrible)
        tabFilter.getTabAt(3)!!.setText(R.string.cute)
        tabFilter.getTabAt(4)!!.setText(R.string.interesting)
        tabFilter.getTabAt(5)!!.setText(R.string.strange)

        tabFilter.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                val position = tab.position

                when (position) {
                    0 -> {
                        broadcastFilter?.clear()
                        broadcastFilter?.add(BroadcastFilter("1", "None",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_none),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.transparent_bg)))
                        broadcastFilter?.add(BroadcastFilter("2", "Filter 1",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_1),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.filter_image_purple)))
                        broadcastFilter?.add(BroadcastFilter("3", "Filter 2",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_2),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.filter_image_green)))
                        broadcastFilter?.add(BroadcastFilter("4", "Filter 3",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_3),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.filter_image_red)))
                    }
                    1 -> {
                        broadcastFilter?.clear()
                        broadcastFilter?.add(BroadcastFilter("1", "None",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_none),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.transparent_bg)))
                        broadcastFilter?.add(BroadcastFilter("2", "Filter 1",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_1),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.filter_image_purple)))
                        broadcastFilter?.add(BroadcastFilter("3", "Filter 2",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_2),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.filter_image_green)))
                    }
                    2 -> {
                        broadcastFilter?.clear()
                        broadcastFilter?.add(BroadcastFilter("1", "None",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_none),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.transparent_bg)))
                        broadcastFilter?.add(BroadcastFilter("2", "Filter 1",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_1),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.filter_image_purple)))
                        broadcastFilter?.add(BroadcastFilter("3", "Filter 2",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_2),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.filter_image_green)))
                    }
                    3 -> {
                        broadcastFilter?.clear()
                        broadcastFilter?.add(BroadcastFilter("1", "None",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_none),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.transparent_bg)))
                        broadcastFilter?.add(BroadcastFilter("3", "Filter 2",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_2),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.filter_image_green)))
                        broadcastFilter?.add(BroadcastFilter("4", "Filter 3",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_3),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.filter_image_red)))
                    }
                    4 -> {
                        broadcastFilter?.clear()
                        broadcastFilter?.add(BroadcastFilter("1", "None",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_none),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.transparent_bg)))
                        broadcastFilter?.add(BroadcastFilter("2", "Filter 1",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_1),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.filter_image_purple)))
                        broadcastFilter?.add(BroadcastFilter("3", "Filter 2",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_2),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.filter_image_green)))
                    }
                    5 -> {
                        broadcastFilter?.clear()
                        broadcastFilter?.add(BroadcastFilter("1", "None",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_none),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.transparent_bg)))
                        broadcastFilter?.add(BroadcastFilter("2", "Filter 1",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_1),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.filter_image_purple)))
                        broadcastFilter?.add(BroadcastFilter("3", "Filter 2",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_2),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.filter_image_green)))
                        broadcastFilter?.add(BroadcastFilter("4", "Filter 3",
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity,
                                        R.drawable.filter_3),
                                ContextCompat.getDrawable(this@BroadcastingNewLiveActivity, R.drawable.filter_image_red)))
                    }
                }
            }
        })
    }

    private val updateFilter = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val position = intent!!.getIntExtra("filterIcon", 0)
            funSetFilters(position)
        }
    }

    fun funSetFilters(selectedPosition: Int) {
        if (broadcastFilter!![selectedPosition].filterName.equals("None", true)) {
            ivFilterImage.visibility = View.GONE
        } else {
            ivFilterImage.visibility = View.VISIBLE
            ivFilterImage.setImageDrawable(broadcastFilter!![selectedPosition].filterType)
        }
        sendFilters(broadcastFilter!![selectedPosition].filterId)
    }

    private fun showOrderPopup() {
        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_streaming_orders, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val ivCloseOrders = dialogView.findViewById<View>(R.id.ivCloseOrders) as ImageView
        val rvOrdersList = dialogView.findViewById<View>(R.id.rvOrdersList) as RecyclerView

        receivedOrdersAdapter = ReceivedOrdersAdapter(this, ordersList)
        rvOrdersList.adapter = receivedOrdersAdapter
        receivedOrdersAdapter!!.setOnItemClickListener(object : ReceivedOrdersAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                if (!ordersList!![position].isRejected && !ordersList!![position].isCompleted) {
                    b.dismiss()
                    if (ordersList!![position].giftData.giftType.equals(getString(R.string.gold_points), true)) {
                        checkOrderStatus(ordersList!![position].giftData.giftValue, HomeActivity.goldPoints, position)
                    }

                    if (ordersList!![position].giftData.giftType.equals(getString(R.string.silver_points), true)) {
                        checkOrderStatus(ordersList!![position].giftData.giftValue, HomeActivity.silverPoints, position)
                    }

                    if (ordersList!![position].giftData.giftType.equals(getString(R.string.coin_points), true)) {
                        checkOrderStatus(ordersList!![position].giftData.giftValue, HomeActivity.yuvanPoints, position)
                    }
                } else {
                    Toast.makeText(this@BroadcastingNewLiveActivity, "This order is been rejected or completed.", Toast.LENGTH_SHORT).show()
                }
            }
        })

        ivCloseOrders.setOnClickListener {
            b.dismiss()
            resetButtonsBackground()
        }

        b.setCancelable(false)
        b.show()

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.9f).toInt()
        val dialogWindowHeight = (displayHeight * 0.9f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        b.window.attributes = layoutParams
        b.window.setGravity(Gravity.CENTER_VERTICAL)
    }

    private fun checkOrderStatus(giftData: Int, giftTypeValue: Int, position: Int) {
        if (giftData <= giftTypeValue) {
            presenter.postOrderStatus(ordersList!![position].id, position)
        } else {
            Toast.makeText(this@BroadcastingNewLiveActivity, "You don't have enough coins to reject this order",
                    Toast.LENGTH_SHORT).show()
        }
    }

    override fun updatePointsData(pointEarn: List<PointsEarned>) {
        RuthUtility.updatePoints(this, pointEarn)
    }

    private fun showInvitationPopup() {
        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_streaming_invitations, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val ivCloseOrders = dialogView.findViewById<View>(R.id.ivCloseOrders) as ImageView
        val rvInvitationList = dialogView.findViewById<View>(R.id.rvInvitationList) as RecyclerView
        val btnInviteAll = dialogView.findViewById<View>(R.id.btnInviteAll) as TextView
        val btnInviteSelected = dialogView.findViewById<View>(R.id.btnInviteSelected) as TextView
        val selectedFansId: ArrayList<String> = ArrayList()

        val adapter = UsersInvitationAdapter(this, invitedFansList)
        rvInvitationList.adapter = adapter
        adapter.setOnItemClickListener(object : UsersInvitationAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                val isInvited: Boolean = !invitedFansList!![position].isInvite
                invitedFansList!![position].isInvite = isInvited
                selectedFansId.add(invitedFansList!![position].id)
                adapter.notifyItemChanged(position)
            }
        })

        ivCloseOrders.setOnClickListener {
            b.dismiss()
            resetButtonsBackground()
        }

        btnInviteAll.setOnClickListener {
            for (i in 0 until invitedFansList!!.size) {
                if (invitedFansList!![i].isLiveStatus && !invitedFansList!![i].isInvite) {
                    selectedFansId.add(invitedFansList!![i].id)
                }
            }
            presenter.inviteUser(selectedFansId, intent.getStringExtra("broadcastId"))
        }

        btnInviteSelected.setOnClickListener {
            presenter.inviteUser(selectedFansId, intent.getStringExtra("broadcastId"))
        }

        b.setCancelable(false)
        b.show()

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val displayWidth = displayMetrics.widthPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.9f).toInt()
        layoutParams.width = dialogWindowWidth
        b.window.attributes = layoutParams
        b.window.setGravity(Gravity.CENTER_VERTICAL)
    }

    private val updateFollower = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val followerId = intent!!.getStringExtra("followerId")
            val isFollowed = intent.getBooleanExtra("isFollowed", false)
            presenter.followUnfollow(followerId, isFollowed)
        }
    }

    override fun updateOrdersData(data: ArrayList<BroadcastCustomOrder>?) {
        ordersList!!.clear()
        ordersList = data
//        receivedOrdersAdapter!!.notifyDataSetChanged()
//        showOrderPopup()
    }

    override fun updateFansData(data: ArrayList<InvitedFansData>?) {
        invitedFansList!!.clear()
        invitedFansList = data
//        showInvitationPopup()
    }

    override fun updateGifts(giftsList: ArrayList<UserGiftData>) {
        tvGiftCount.text = RuthUtility.getCounts(giftsList.size)
        usersGiftList = giftsList
    }

    override fun updateViewers(viewerList: ArrayList<ViewerData>) {
        usersViewersList = viewerList
        tvViewersCount.text = RuthUtility.getCounts(viewerList.size)
    }

    override fun updateOrders(position: Int) {
        Toast.makeText(this@BroadcastingNewLiveActivity, "You have rejected this order",
                Toast.LENGTH_SHORT).show()
        ordersList!![position].isRejected = true
//        receivedOrdersAdapter!!.notifyItemChanged(position)
    }

    private fun updateFollowerList(isFollowed: Boolean, followerCount: Int) {
        BroadcastDetailActivity.liveUpcomingBroadcastList!![intent.getIntExtra("position", 0)].broadcasterData.isFollow = isFollowed
        BroadcastDetailActivity.liveUpcomingBroadcastList!![intent.getIntExtra("position", 0)].broadcasterData.followers = followerCount

        /*-----------------------------------Updating past list-----------------------------------*/
        for (i in 0 until BroadcastDetailActivity.pastBroadcastList!!.size) {
            if (BroadcastDetailActivity.pastBroadcastList!![i].broadcasterData.id == BroadcastDetailActivity.liveUpcomingBroadcastList!![intent.getIntExtra("position", 0)].broadcasterData.id) {
                BroadcastDetailActivity.pastBroadcastList!![i].broadcasterData.isFollow = isFollowed
                BroadcastDetailActivity.pastBroadcastList!![i].broadcasterData.followers = followerCount
            }
        }

        /*---------------------------------Updating live list-------------------------------------*/
        if (BroadcastDetailActivity.liveUpcomingBroadcastList!!.isNotEmpty()) {
            for (i in 0 until BroadcastDetailActivity.liveUpcomingBroadcastList!!.size) {
                if (BroadcastDetailActivity.liveUpcomingBroadcastList!![i].broadcasterData.id == BroadcastDetailActivity.liveUpcomingBroadcastList!![intent.getIntExtra("position", 0)].broadcasterData.id) {
                    BroadcastDetailActivity.liveUpcomingBroadcastList!![i].broadcasterData.isFollow = isFollowed
                    BroadcastDetailActivity.liveUpcomingBroadcastList!![i].broadcasterData.followers = followerCount
                }
            }
        }
    }

    /*--------------------------------------------------------------------------------------------*/
    private var invalidDialog: android.app.AlertDialog? = null
    private var invitedFansList: ArrayList<InvitedFansData>? = ArrayList()
    private var ordersList: ArrayList<BroadcastCustomOrder>? = ArrayList()

    override fun invalidLogin() {

        val builder = android.app.AlertDialog.Builder(this)
        builder.setTitle("Invalid Login")
        builder.setMessage(getString(R.string.invalid_login_error))

        // add a button
        builder.setPositiveButton(getString(R.string.log_in)) { dialog, which ->
            presenter.clearLogin()
            startActivity(Intent(this, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            finish()
            overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
            dialog.dismiss()
        }

        if (!isFinishing && window.decorView.isShown) {
            invalidDialog = builder.create()
            invalidDialog!!.setCancelable(false)
            invalidDialog!!.show()
        }
    }
}