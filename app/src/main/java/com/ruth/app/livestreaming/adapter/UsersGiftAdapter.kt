package com.ruth.app.livestreaming.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ruth.app.R
import com.ruth.app.model.UserGiftData
import com.ruth.app.otheruser.OtherUserProfileActivity
import com.ruth.app.utility.RuthUtility
import kotlinx.android.synthetic.main.item_gifts_viewers.view.*

class UsersGiftAdapter(val context: Context, private val usersGiftList: ArrayList<UserGiftData>) :
        RecyclerView.Adapter<UsersGiftAdapter.UsersGiftViewHolder>() {

    private var isFollow: Boolean = false
    private var onItemClicklistener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersGiftViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.item_gifts_viewers, parent, false)
        return UsersGiftViewHolder(view)
    }

    override fun getItemCount(): Int {
        return usersGiftList.size
    }

    override fun onBindViewHolder(holder: UsersGiftViewHolder, position: Int) {
        holder.bind(position, usersGiftList[position])
        if (position == 0) {
            holder.itemView.lytBottom.visibility = View.GONE
            holder.itemView.lytTop.visibility = View.VISIBLE
        } else if (position == 1) {
            holder.itemView.lytBottom.visibility = View.VISIBLE
            holder.itemView.lytTop.visibility = View.GONE
            holder.itemView.imgFire.visibility = View.VISIBLE
            holder.itemView.imgFire.setImageResource(R.drawable.ic_fire_blue)
            holder.itemView.lblPosition.visibility = View.GONE
        } else if (position == 2) {
            holder.itemView.lytBottom.visibility = View.VISIBLE
            holder.itemView.lytTop.visibility = View.GONE
            holder.itemView.imgFire.visibility = View.VISIBLE
            holder.itemView.imgFire.setImageResource(R.drawable.ic_fire_gray)
            holder.itemView.lblPosition.visibility = View.GONE
        } else {
            holder.itemView.lytBottom.visibility = View.VISIBLE
            holder.itemView.lytTop.visibility = View.GONE
            holder.itemView.imgFire.visibility = View.GONE
            holder.itemView.lblPosition.visibility = View.VISIBLE
            holder.itemView.lblPosition.text = (position + 1).toString()
        }

        holder.itemView.img_follow.setOnClickListener {
            if (isFollow) {
                isFollow = false
                holder.itemView.img_follow.setImageResource(R.drawable.icon_follow_button)
            } else {
                isFollow = true
                holder.itemView.img_follow.setImageResource(R.drawable.icon_follow)
            }
        }

        holder.itemView.img_follow.setOnClickListener {
            var followerCount = usersGiftList[position].userData.followers

            if (usersGiftList[position].userData.isFollow) {
                followerCount -= 1
                holder.itemView.img_follow.setImageResource(R.drawable.icon_follow_button)
            } else {
                followerCount += 1
                holder.itemView.img_follow.setImageResource(R.drawable.icon_follow)
            }
            usersGiftList[position].userData.followers = followerCount
            holder.itemView.tvGiftUserFollowers.text = RuthUtility.getCounts(usersGiftList[position].userData.followers) + context.getString(R.string.followers)
            onItemClicklistener!!.onFollowClick(holder.itemView.img_follow, position)
        }

//        holder.itemView.layoutUser.setOnClickListener {
//            context.startActivity(Intent(context, OtherUserProfileActivity::class.java))
//        }
    }

    inner class UsersGiftViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(position: Int, userGiftData: UserGiftData) {

            if (position == 0) {
                Glide.with(context).load(userGiftData.userData.photoUrl)
                        .apply(RequestOptions().error(R.drawable.com_facebook_profile_picture_blank_square)
                                .placeholder(R.drawable.com_facebook_profile_picture_blank_square))
                        .into(itemView.ivTopUserImage)
            } else {
                Glide.with(context).load(userGiftData.userData.photoUrl)
                        .apply(RequestOptions().error(R.drawable.com_facebook_profile_picture_blank_square)
                                .placeholder(R.drawable.com_facebook_profile_picture_blank_square))
                        .into(itemView.ivBottomUserImage)
            }
            itemView.tvGiftUserName.text = userGiftData.userData.name
            itemView.tvGiftUserFollowers.text = "HKD " +RuthUtility.getCounts(userGiftData.count)

            if (userGiftData.userData.isFollow) {
                itemView.img_follow.setImageResource(R.drawable.icon_follow)
            } else {
                itemView.img_follow.setImageResource(R.drawable.icon_follow_button)
            }
        }
    }

    fun setOnItemClickListener(onItemClick: OnItemClickListener) {
        this.onItemClicklistener = onItemClick
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
        fun onFollowClick(view: View, position: Int)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}
