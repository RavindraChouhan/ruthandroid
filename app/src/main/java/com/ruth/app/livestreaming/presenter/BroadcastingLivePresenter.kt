package com.ruth.app.livestreaming.presenter

import android.util.Log
import android.widget.Toast
import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.model.*
import com.ruth.app.network.config.RuthApi
import com.ruth.app.utility.RuthUtility
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class BroadcastingLivePresenter @Inject constructor(var ruthApi: RuthApi) : BasePresenter<BroadcastingLivePresenter.LiveVideoBroadcastingView>() {

    fun updateLiveStatus(broadcastId: String, isLive: Boolean) {

        val hm = HashMap<String, @JvmSuppressWildcards Any>()
        hm["broadcasterId"] = preferences.getUserInfo().id
        hm["isLive"] = isLive
        hm["isActive"] = isLive
        hm["_id"] = broadcastId
        if (isLive) {
            hm["broadcastStartTime"] = RuthUtility.dateToString(Calendar.getInstance().time)
        } else {
            hm["broadcastEndTime"] = RuthUtility.dateToString(Calendar.getInstance().time)
        }

        ruthApi.uploadBroadcastInfo(hm)
                .onErrorReturn { (null) }
                .subscribe {
                    if (it != null) {
                        if (it.status) {

                        } else {

                        }
                    }
                }
    }

    fun getUsersGifts(broadcastId: String)/*: ArrayList<UserGiftData> */ {
        var giftsList: ArrayList<UserGiftData> = ArrayList()
        ruthApi.getUsersGifts(preferences.getUserInfo().userToken, broadcastId)
                .subscribe {
                    if (it.status) {
                        giftsList = it.data!!
                        view?.updateGifts(giftsList)
                        Log.d("status", " -> " + it.data)
                    } else {
                        Log.d("status", " -> $it")
                    }
                }
    }

    fun followUnfollow(followingId: String, isFollowed: Boolean) {
        val hm = HashMap<String, @JvmSuppressWildcards Any>()
        hm["type"] = "follow"
        hm["isFollow"] = isFollowed
        hm["followingId"] = followingId
        hm["userId"] = preferences.getUserInfo().id

        ruthApi.followUnfollowUser(preferences.getUserInfo().userToken, hm)
                .subscribe {
                    if (it != null) {
                        if (it.status) {

                        } else if (it.message.contains(context.getString(R.string.device_login), true)
                                || it.zhHans.contains(context.getString(R.string.device_login), true)
                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
                            view?.invalidLogin()
                        } else {
                            Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
    }

    fun getViewerCount(broadcastId: String) {
        ruthApi.getLiveViewers(preferences.getUserInfo().userToken, broadcastId)
                .onErrorReturn { (null) }
                .doOnError {
                    Log.d("Error", "error" + it.message)
                }
                .subscribe {
                    if (it != null) {
                        if (it.status) {
                            if (it.data!!.isNotEmpty()) {
                                view?.updateViewers(it.data!![0].userData!!)
                            }
                        } else {
                            Log.d("status", " -> $it")
                        }
                    }
                }
    }

    fun getInvitedUsers(broadcastId: String) {
        ruthApi.getInvitedUsers(preferences.getUserInfo().userToken, broadcastId)
                .onErrorReturn { (null) }
                .doOnError {
                    Log.d("Error", "error" + it.message)
                }
                .subscribe {
                    if (it != null) {
                        if (it.status) {
                            view?.updateFansData(it.data)
                        } else {
                            Log.d("status", " -> $it")
                        }
                    }
                }
    }

    private lateinit var userIds: Array<String>
    fun inviteUser(selectedFansId: ArrayList<String>, broadcastId: String) {

        for (i in 0 until selectedFansId.size) {
            userIds = arrayOf(selectedFansId[i])
        }

        val hashMap: HashMap<String, @JvmSuppressWildcards Any> = HashMap()
        hashMap["broadcastId"] = broadcastId
        hashMap["userIds"] = userIds

        ruthApi.postInviteFans(preferences.getUserInfo().userToken, hashMap)
                .subscribe {
                    if (it != null) {
                        if (it.status) {
                            Log.d("inviteUsers", "status -> " + it.message)
                        } else if (it.message.contains(context.getString(R.string.device_login), true)
                                || it.zhHans.contains(context.getString(R.string.device_login), true)
                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
                            view?.invalidLogin()
                        } else {
                            Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
    }

    fun getCustomOrders(broadcastId: String) {
        ruthApi.getCustomOrders(preferences.getUserInfo().userToken, broadcastId)
                .onErrorReturn { (null) }
                .doOnError {
                    Log.d("Error", "error" + it.message)
                }
                .subscribe {
                    if (it != null) {
                        if (it.status) {
                            if (it.data!!.isNotEmpty()) {
                                view?.updateOrdersData(it.data)
                            }
                        } else {
                            Log.d("status", " -> $it")
                        }
                    }
                }
    }

    fun clearLogin() {
        val tempFb = utility.getBooleaPreferences(context, getString(R.string.isfbSignup))
        val tempGoogle = utility.getBooleaPreferences(context, getString(R.string.isGoogleSignup))
        val tempInsta = utility.getBooleaPreferences(context, getString(R.string.isInstaSignup))

        preferences.clearPreferences()

        utility.setBooleanPreferences(context, getString(R.string.isfbSignup), tempFb)
        utility.setBooleanPreferences(context, getString(R.string.isGoogleSignup), tempGoogle)
        utility.setBooleanPreferences(context, getString(R.string.isInstaSignup), tempInsta)

    }

    fun postOrderStatus(customOrderId: String, position: Int) {
        val hm = java.util.HashMap<String, @JvmSuppressWildcards Any>()
        hm["type"] = "rejected"
        hm["customId"] = customOrderId

        ruthApi.postOrderStatus(preferences.getUserInfo().userToken, hm)
                .onErrorReturn { (null) }
                .doOnError {
                    Log.d("Error", "error" + it.message)
                }
                .subscribe {
                    if (it != null) {
                        if (it.status) {
                            view!!.updateOrders(position)
                            getMyProfile()
                            Log.d("orderStatus", " -> " + it.data)
                        } else {
                            Toast.makeText(context, "" + it.message, Toast.LENGTH_SHORT).show()
                            Log.d("status", " -> $it")
                        }
                    }
                }
    }

    fun getMyProfile() {
        ruthApi.getUserInfo(preferences.getUserInfo().userToken, preferences.getUserInfo().id)
                .subscribe({
                    if (it != null) {
                        if (it.status) {
                            preferences.setUserProfile(it.data)
                            if (it.data.pointEarn!!.isNotEmpty()) {
                                RuthUtility.updatePoints(context, it.data.pointEarn)
                                // view!!.updatePointsData(it.data.pointEarn!!)
                            }
                        } else {
                            // view?.invalidLogin()
                        }
                    }
                }, {
                    view?.showToast(getString(R.string.connection_error))
                })
    }

    interface LiveVideoBroadcastingView : BaseView {
        fun updateGifts(giftsList: ArrayList<UserGiftData>)
        fun updateViewers(giftsList: ArrayList<ViewerData>)
        fun invalidLogin()
        fun updateFansData(data: ArrayList<InvitedFansData>?)
        fun updateOrdersData(data: ArrayList<BroadcastCustomOrder>?)
        fun updateOrders(position: Int)
        fun updatePointsData(pointEarn: List<PointsEarned>)
    }
}