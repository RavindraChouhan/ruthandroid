package com.ruth.app.livestreaming.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.fragment.BaseFragment
import com.ruth.app.home.activity.HomeActivity
import com.ruth.app.livestreaming.activity.LiveVideoPlayerActivity
import com.ruth.app.livestreaming.adapter.StickerAdapter
import com.ruth.app.livestreaming.presenter.StickerPresenter
import com.ruth.app.model.PositiveNegative
import com.ruth.app.model.StickersData
import io.socket.client.Socket
import io.socket.emitter.Emitter.Listener
import kotlinx.android.synthetic.main.fragment_sticker.view.*
import org.json.JSONException
import org.json.JSONObject

class StickerFragment : BaseFragment<StickerPresenter>(), StickerPresenter.StickerView {

    private var adapter: StickerAdapter? = null
    private var layoutManager: LinearLayoutManager? = null

    //Stickers list
    private var stickerList: ArrayList<StickersData>? = ArrayList()
    private val allStickers: ArrayList<StickersData>? = ArrayList()
    private val combinedStickers: ArrayList<PositiveNegative>? = ArrayList()
    private var totalStickerSent: Int? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_sticker, container, false)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        setUpSocket()

        val position = LiveVideoPlayerActivity.SUB_STICKER_TYPE
        when {
            LiveVideoPlayerActivity.STICKER_TYPE.equals("backpack", true) -> {
                view.tvEmptyState.visibility = View.VISIBLE
                view.rvComment.visibility = View.GONE
                view.tvEmptyState.text = "No sticker bought yet.."
            }
            LiveVideoPlayerActivity.STICKER_TYPE.equals("positive", true) -> {
                allStickers?.clear()
                combinedStickers?.clear()

                if (combinedStickers?.isEmpty()!!) {
                    for (i in 0 until preferences.getStickers().positive!!.size) {
                        allStickers!!.addAll(preferences.getStickers().positive!![i].stickersData!!)
                    }
                    combinedStickers.add(0, PositiveNegative(getString(R.string.all), allStickers))
                    combinedStickers.addAll(preferences.getStickers().positive!!)
                }

                stickerList?.clear()
                stickerList?.addAll(combinedStickers[position].stickersData!!)

            }
            LiveVideoPlayerActivity.STICKER_TYPE.equals("negative", true) -> {
                allStickers?.clear()
                combinedStickers?.clear()

                if (combinedStickers?.isEmpty()!!) {
                    for (i in 0 until preferences.getStickers().negative!!.size) {
                        allStickers!!.addAll(preferences.getStickers().negative!![i].stickersData!!)
                    }
                    combinedStickers.add(0, PositiveNegative(getString(R.string.all), allStickers))
                    combinedStickers.addAll(preferences.getStickers().negative!!)
                }

                stickerList?.clear()
                stickerList?.addAll(combinedStickers[position].stickersData!!)
            }
            LiveVideoPlayerActivity.STICKER_TYPE.equals("specificOrder", true) -> {
                allStickers?.clear()
                combinedStickers?.clear()

                if (combinedStickers?.isEmpty()!!) {
                    for (i in 0 until preferences.getStickers().specificOrder!!.size) {
                        allStickers!!.addAll(preferences.getStickers().specificOrder!![i].stickersData!!)
                    }
                    combinedStickers.add(0, PositiveNegative(getString(R.string.all), allStickers))
                    combinedStickers.addAll(preferences.getStickers().specificOrder!!)
                }

                stickerList?.clear()
                stickerList?.addAll(combinedStickers[position].stickersData!!)
            }


        }

        adapter = StickerAdapter(this.activity!!, stickerList!!)
        layoutManager = LinearLayoutManager(this.activity, LinearLayoutManager.HORIZONTAL, false)
        view.rvComment!!.layoutManager = layoutManager
        view.rvComment!!.adapter = adapter
        adapter!!.setOnItemClickListener(object : StickerAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int, totalStickers: Int) {
                totalStickerSent = totalStickers
                for (i in 0 until totalStickers) {
                    attemptSticker(stickerList!![position]._id)
                }

//                if (preferences.getUserProfile().pointEarn!!.isNotEmpty()) {
//                    for (i in 0 until preferences.getUserProfile().pointEarn!!.size) {
//                        if (preferences.getUserProfile().pointEarn!![i].id == stickerList!![position].giftType) {
//                            if (preferences.getUserProfile().pointEarn!![i].total >= stickerList!![position].giftValue) {
//                                attemptSticker(stickerList!![position]._id)
//                            } else {
//                                Toast.makeText(activity, "You don't have enough coins..", Toast.LENGTH_SHORT).show()
//                            }
//                            break
//                        } else {
//                            Toast.makeText(activity, "You don't have enough coins..", Toast.LENGTH_SHORT).show()
//                        }
//                    }
//                } else {
//                    Toast.makeText(activity, "You don't have enough coins..", Toast.LENGTH_SHORT).show()
//                }
            }
        })
        return view
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            fragmentManager!!.beginTransaction().detach(this).attach(this).commit()
        }
    }

    /*------------------------------------START OF SOCKET-----------------------------------------*/

    private var mSocket: Socket? = null
    private var isConnected: Boolean? = true

    private fun attemptSticker(stickerId: String) {

        if (!mSocket!!.connected()) return

        val postData = JSONObject()
        try {
            postData.put("broadcastId", LiveVideoPlayerActivity.BROADCAST_ID)
            postData.put("userId", preferences.getUserInfo().id)
            postData.put("stickerId", stickerId)
            postData.put("type", "sticker")

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        mSocket!!.emit("STICKER", postData)
    }

    private fun setUpSocket() {
        val app = activity?.application as App
        mSocket = app.socket
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
        mSocket!!.on("RECEIVE_STICKER", onNewSticker)
        mSocket!!.connect()
    }

    private val onConnect = Listener {
        activity?.runOnUiThread {
            if (!isConnected!!) {
                isConnected = true
            }
        }
    }

    private val onDisconnect = Listener {
        activity?.runOnUiThread {
            Log.i(TAG, "disconnected")
            isConnected = false
        }
    }

    private val onConnectError = Listener {
        activity?.runOnUiThread {
            Log.e(TAG, "Error connecting")
        }
    }

    private val onNewSticker = Listener { args ->
        activity?.runOnUiThread(Runnable {
            try {
                val data = args[0] as JSONObject
                val giftCount: Int
                val giftType: String
                val giftPointsLeft: Int
                val userProfileData = preferences.getUserProfile()
                var stickerSent: Int = 0
                try {
                    giftCount = data.getInt("giftCount")
                    giftType = data.getString("giftType")
                    giftPointsLeft = data.getInt("giftPointsLeft")

                    if (preferences.getUserInfo().id == data.getString("userId")) {
                        if (totalStickerSent!! > 1) {
                            Toast.makeText(activity, "$totalStickerSent Stickers Sent", Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(activity, "Sticker Sent", Toast.LENGTH_SHORT).show()
                        }

                        if (giftType.equals(getString(R.string.gold_points), true)) {
                            HomeActivity.goldPoints = giftPointsLeft
                        }

                        if (giftType.equals(getString(R.string.silver_points), true)) {
                            HomeActivity.silverPoints = giftPointsLeft
                        }

                        if (giftType.equals(getString(R.string.coin_points), true)) {
                            HomeActivity.yuvanPoints = giftPointsLeft
                        }
                        updateView(giftCount)

//                        if (preferences.getUserProfile().pointEarn!!.isNotEmpty()) {
//                            for (i in 0 until preferences.getUserProfile().pointEarn!!.size) {
//                                if (preferences.getUserProfile().pointEarn!![i].id == giftType) {
//                                    userProfileData!!.pointEarn!![i].total = giftPointsLeft
//                                    preferences.setUserProfile(userProfileData)
//                                    updateView(giftCount)
//                                    break
//                                }
//                            }
//                        } else {
//                            userProfileData!!.pointEarn!![0].id = giftType
//                            userProfileData.pointEarn!![0].total = giftPointsLeft
//                            preferences.setUserProfile(userProfileData)
//                            updateView(giftCount)
//                        }
                    }
                } catch (e: JSONException) {
                    Log.e(TAG, e.message)
                    return@Runnable
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private fun updateView(giftCount: Int) {
        LocalBroadcastManager.getInstance(activity!!).sendBroadcast(Intent("updatePoints")
                .putExtra("broadcastersGiftCount", giftCount))
    }

    private fun leave() {
        mSocket!!.disconnect()
        mSocket!!.connect()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        leave()
    }

    /*--------------------------------------------------------------------------------------------*/

}

