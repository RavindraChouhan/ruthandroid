package com.ruth.app.livestreaming.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.ruth.app.livestreaming.fragment.FilterListFragment
import com.ruth.app.livestreaming.model.BroadcastFilter

class FilterPagerAdapter(fm: FragmentManager?, var filterList: ArrayList<BroadcastFilter>?) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return FilterListFragment()/*.newInstance(filterList!!)*/
    }

    override fun getCount(): Int {
        return 6
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return super.getPageTitle(position)
    }

//    override fun saveState(): Parcelable {
//        val bundle = saveState() as Bundle
//        bundle.putParcelableArray("states", null) // Never maintain any states from the base class to avoid TransactionTooLargeException
//        return bundle
//    }
}