//package com.ruth.app.livestreaming.activity
//
//import android.Manifest
//import android.app.Activity
//import android.content.BroadcastReceiver
//import android.content.Context
//import android.content.Intent
//import android.content.IntentFilter
//import android.content.pm.PackageManager
//import android.graphics.Bitmap
//import android.graphics.BitmapFactory
//import android.hardware.Camera
//import android.net.Uri
//import android.os.Build
//import android.os.Bundle
//import android.provider.MediaStore
//import android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
//import android.support.design.widget.TabLayout
//import android.support.v4.app.ActivityCompat
//import android.support.v4.content.ContextCompat
//import android.support.v4.content.LocalBroadcastManager
//import android.text.Editable
//import android.text.TextWatcher
//import android.util.DisplayMetrics
//import android.view.Surface
//import android.view.SurfaceHolder
//import android.view.View
//import android.view.WindowManager
//import android.view.inputmethod.InputMethodManager
//import android.widget.ImageView
//import android.widget.LinearLayout
//import android.widget.Toast
//import com.ruth.app.App
//import com.ruth.app.R
//import com.ruth.app.base.activity.BaseActivity
//import com.ruth.app.livestreaming.adapter.FilterPagerAdapter
//import com.ruth.app.livestreaming.model.BroadcastFilter
//import com.ruth.app.livestreaming.presenter.EnterBroadcastingDetailsPresenter
//import com.ruth.app.widgets.CustomDialog
//import kotlinx.android.synthetic.main.activity_enter_broadcasting_details.*
//import java.io.*
//
//class EnterBroadcastingDetailsActivity : BaseActivity<EnterBroadcastingDetailsPresenter>(),
//        EnterBroadcastingDetailsPresenter.EnterBroadcastingDetailsView, View.OnClickListener, SurfaceHolder.Callback {
//
//    private val REQUEST_PERMISSIONS = 100
////    private val broadcastFilter: ArrayList<BroadcastFilter>? = ArrayList()
//    private lateinit var dialogs: CustomDialog
//
//    /*---------------------Camera------------------------------*/
//    private var surfaceHolder: SurfaceHolder? = null
//
//    companion object {
//        val broadcastFilter: ArrayList<BroadcastFilter>? = ArrayList()
//    }
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//
//        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
//        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
//        overridePendingTransition(R.anim.anim_enter_from_bottom, R.anim.anim_exit_from_bottom)
//        setContentView(R.layout.activity_enter_broadcasting_details)
//        App.component.inject(this)
//        presenter.onCreate(this)
//        dialogs = CustomDialog(this)
//        captureImageCallback()
//
//        etTitle.setOnTouchListener { v, event ->
//            etTitle.isCursorVisible = true
//            false
//        }
//
//        ivBackBroadcast.setOnClickListener(this)
//        ivStartBroadcasting.setOnClickListener(this)
//        ivGetCoverImage.setOnClickListener(this)
//        ivCoverImage.setOnClickListener(this)
//        ivFilter.setOnClickListener(this)
//        ivNextFilter.setOnClickListener(this)
//        ivChangeCamera.setOnClickListener(this)
//
//        etTitle.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(s: Editable?) {
//                if (etTitle.length() > 0 && etTag.length() > 0) {
//                    ivStartBroadcasting.background = ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.imageview_selector_green)
//                } else {
//                    ivStartBroadcasting.background = ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.imageview_selector)
//                }
//            }
//
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
//
//            }
//
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//
//            }
//        })
//
//        etTag.addTextChangedListener(object : TextWatcher {
//            private var len: Int = 0
//            override fun afterTextChanged(s: Editable?) {
//
//                if (etTitle.length() > 0 && etTag.length() > 0) {
//                    ivStartBroadcasting.background = ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.imageview_selector_green)
//                } else {
//                    ivStartBroadcasting.background = ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.imageview_selector)
//                }
//            }
//
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
//                val str = etTag.text.toString()
//                len = str.length
//            }
//
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//
//            }
//        })
//
//        LocalBroadcastManager.getInstance(this).registerReceiver(updateFilter, IntentFilter("updateFilter"))
//    }
//
//    private fun captureImageCallback() {
//        surfaceHolder = imgSurface.holder
//        surfaceHolder!!.addCallback(this)
//        surfaceHolder!!.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS)
//    }
//
//    override fun hideProgressLoading() {
//        if (!isFinishing && window.decorView.isShown) {
//            dialogs.dismiss()
//        }
//    }
//
//    override fun finishBroadcasting() {
//        if (!isFinishing && window.decorView.isShown) {
//            dialogs.dismiss()
//        }
//        finish()
//    }
//
//    override fun onClick(v: View?) {
//        if (v?.id == R.id.ivBackBroadcast) {
//            finish()
//        }
//
//        if (v?.id == R.id.ivChangeCamera) {
//            camera!!.stopPreview()
//            camera!!.release()
//            if (flag == 0) {
//                flag = 1
//            } else {
//                flag = 0
//            }
//            surfaceCreated(surfaceHolder!!)
//        }
//
//        if (v?.id == R.id.ivStartBroadcasting) {
//            if (mPicCaptureUri != null) {
//                val uploadedImageUrl = selectedImage(mPicCaptureUri!!)
//
//                if (etTitle.text.toString().isEmpty()) {
//                    Toast.makeText(this, getString(R.string.enter_title), Toast.LENGTH_SHORT).show()
//                    return
//                }
//
//                if (etTag.text.toString().isEmpty()) {
//                    Toast.makeText(this, getString(R.string.enter_tag), Toast.LENGTH_SHORT).show()
//                    return
//                }
//
//                if (uploadedImageUrl.isEmpty()) {
//                    if (!isFinishing && window.decorView.isShown) {
//                        dialogs.show()
//                    }
////                    progressBarBroadcast.visibility = View.VISIBLE
//                    hideKeyboard()
//                    uploadCoverImage()
//                    return
//                }
//                if (!isFinishing && window.decorView.isShown) {
//                    dialogs.show()
//                }
//                hideKeyboard()
//                presenter.uploadBroadcastInfo(uploadedImageUrl, title = etTitle.text.toString().trim(),
//                        tag = etTag.text.toString().trim())
//
//            } else {
//                Toast.makeText(this, getString(R.string.select_image_error), Toast.LENGTH_SHORT).show()
//            }
//        }
//
//        if (v?.id == R.id.ivGetCoverImage) {
//            hideKeyboard()
//            if (utility.permissionForStorageAndCamera(this)) {
//                openSelectImagePopup()
//            }
//        }
//
//        if (v?.id == R.id.ivCoverImage) {
//            hideKeyboard()
//            if (utility.permissionForStorageAndCamera(this)) {
//                openSelectImagePopup()
//            }
//        }
//
//        if (v?.id == R.id.ivFilter) {
//            if (layoutFilter.visibility == View.VISIBLE) {
//                layoutFilter.visibility = View.GONE
//                layoutBroadcastDetail.visibility = View.VISIBLE
//            } else {
//                layoutFilter.visibility = View.VISIBLE
//                layoutBroadcastDetail.visibility = View.GONE
//                hideKeyboard()
//            }
//
//            setUpFilters()
//        }
//
//        if (v?.id == R.id.ivNextFilter) {
//            layoutFilter.visibility = View.GONE
//            layoutBroadcastDetail.visibility = View.VISIBLE
//        }
//    }
//
//    private fun setUpFilters() {
//        broadcastFilter?.clear()
//        broadcastFilter?.add(BroadcastFilter("1", "None",
//                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                        R.drawable.filter_none),
//                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.transparent_bg)))
//        broadcastFilter?.add(BroadcastFilter("2", "Filter 1",
//                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                        R.drawable.filter_1),
//                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.filter_image_purple)))
//        broadcastFilter?.add(BroadcastFilter("3", "Filter 2",
//                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                        R.drawable.filter_2),
//                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.filter_image_green)))
//        broadcastFilter?.add(BroadcastFilter("4", "Filter 3",
//                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                        R.drawable.filter_3),
//                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.filter_image_red)))
//
//        val filterPagerAdapter = FilterPagerAdapter(supportFragmentManager, broadcastFilter)
//        viewPagerFilter!!.adapter = filterPagerAdapter
//        tabFilter.setupWithViewPager(viewPagerFilter)
//
//
//        tabFilter.getTabAt(0)!!.setText(R.string.all)
//        tabFilter.getTabAt(1)!!.setText(R.string.classic)
//        tabFilter.getTabAt(2)!!.setText(R.string.horrible)
//        tabFilter.getTabAt(3)!!.setText(R.string.cute)
//        tabFilter.getTabAt(4)!!.setText(R.string.interesting)
//        tabFilter.getTabAt(5)!!.setText(R.string.strange)
//
//        tabFilter.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
//            override fun onTabReselected(tab: TabLayout.Tab?) {
//
//            }
//
//            override fun onTabUnselected(tab: TabLayout.Tab?) {
//
//            }
//
//            override fun onTabSelected(tab: TabLayout.Tab) {
//                val position = tab.position
//
//                when (position) {
//                    0 -> {
//                        broadcastFilter?.clear()
//                        broadcastFilter?.add(BroadcastFilter("1", "None",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_none),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.transparent_bg)))
//                        broadcastFilter?.add(BroadcastFilter("2", "Filter 1",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_1),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.filter_image_purple)))
//                        broadcastFilter?.add(BroadcastFilter("3", "Filter 2",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_2),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.filter_image_green)))
//                        broadcastFilter?.add(BroadcastFilter("4", "Filter 3",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_3),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.filter_image_red)))
//                    }
//                    1 -> {
//                        broadcastFilter?.clear()
//                        broadcastFilter?.add(BroadcastFilter("1", "None",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_none),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.transparent_bg)))
//                        broadcastFilter?.add(BroadcastFilter("2", "Filter 1",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_1),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.filter_image_purple)))
//                        broadcastFilter?.add(BroadcastFilter("3", "Filter 2",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_2),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.filter_image_green)))
//                    }
//                    2 -> {
//                        broadcastFilter?.clear()
//                        broadcastFilter?.add(BroadcastFilter("1", "None",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_none),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.transparent_bg)))
//                        broadcastFilter?.add(BroadcastFilter("2", "Filter 1",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_1),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.filter_image_purple)))
//                        broadcastFilter?.add(BroadcastFilter("3", "Filter 2",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_2),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.filter_image_green)))
//                    }
//                    3 -> {
//                        broadcastFilter?.clear()
//                        broadcastFilter?.add(BroadcastFilter("1", "None",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_none),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.transparent_bg)))
//                        broadcastFilter?.add(BroadcastFilter("3", "Filter 2",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_2),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.filter_image_green)))
//                        broadcastFilter?.add(BroadcastFilter("4", "Filter 3",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_3),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.filter_image_red)))
//                    }
//                    4 -> {
//                        broadcastFilter?.clear()
//                        broadcastFilter?.add(BroadcastFilter("1", "None",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_none),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.transparent_bg)))
//                        broadcastFilter?.add(BroadcastFilter("2", "Filter 1",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_1),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.filter_image_purple)))
//                        broadcastFilter?.add(BroadcastFilter("3", "Filter 2",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_2),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.filter_image_green)))
//                    }
//                    5 -> {
//                        broadcastFilter?.clear()
//                        broadcastFilter?.add(BroadcastFilter("1", "None",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_none),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.transparent_bg)))
//                        broadcastFilter?.add(BroadcastFilter("2", "Filter 1",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_1),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.filter_image_purple)))
//                        broadcastFilter?.add(BroadcastFilter("3", "Filter 2",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_2),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.filter_image_green)))
//                        broadcastFilter?.add(BroadcastFilter("4", "Filter 3",
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity,
//                                        R.drawable.filter_3),
//                                ContextCompat.getDrawable(this@EnterBroadcastingDetailsActivity, R.drawable.filter_image_red)))
//                    }
//                }
//            }
//        })
//    }
//
//    /*---------------------------------UPLOAD IMAGE TO AWS----------------------------------------*/
//
//    private fun uploadCoverImage() {
//        val thread = Thread(Runnable {
//            Runtime.getRuntime().gc()
//            val internetConnection = utility.checkInternetWithoutDialog(this)
//            if (internetConnection) {
//                val checkImageUploaded = selectedImage(mPicCaptureUri!!)
//                if (!checkImageUploaded.isEmpty()) {
//                    val uploadedImageUrl = selectedImage(mPicCaptureUri!!)
//                    presenter.uploadBroadcastInfo(uploadedImageUrl, title = etTitle.text.toString().trim(),
//                            tag = etTag.text.toString().trim())
//                } else {
//                    uploadCoverImage()
//                }
//            } else {
//                uploadCoverImage()
//            }
//        })
//        thread.start()
//    }
//
//    override fun startBroadcasting(streamName: String, broadcastId: String, userId: String) {
////        progressBarBroadcast.visibility = View.GONE
//        if (!isFinishing && window.decorView.isShown) {
////            dialogs.dismiss()
//        }
//
////        startActivity(Intent(this, BroadcastingLiveActivity::class.java)
////                .putExtra("streamId", streamName)
////                .putExtra("broadcastId", broadcastId)
////                .putExtra("userId", userId))
////        finish()
//
//        startActivity(Intent(this, BroadcastingNewLiveActivity::class.java)
//                .putExtra("streamId", streamName)
//                .putExtra("broadcastId", broadcastId)
//                .putExtra("userId", userId)
//                .putExtra("cameraRotate", flag)
//                .putExtra("selectedFilter", selectedFilter)
//                .putExtra("filterSelected", filterSelected))
//        finish()
//    }
//
//    private fun openSelectImagePopup() {
//        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
//        val inflater = this.layoutInflater
//        val dialogView = inflater.inflate(R.layout.popup_take_picture, null)
//        dialogBuilder.setView(dialogView)
//        val b = dialogBuilder.create()
//        b.window.attributes.windowAnimations = R.style.DialogAnimation
//        b.window.setBackgroundDrawableResource(R.color.transparent)
//
//        val layoutGallery = dialogView.findViewById<View>(R.id.layoutGallery) as LinearLayout
//        val layoutCamera = dialogView.findViewById<View>(R.id.layoutCamera) as LinearLayout
//        val ivGallery = dialogView.findViewById<View>(R.id.ivGallery) as ImageView
//        val ivCamera = dialogView.findViewById<View>(R.id.ivCamera) as ImageView
//        val popupClose = dialogView.findViewById<View>(R.id.popupClose) as ImageView
//
//        layoutCamera.setOnClickListener {
//            ivGallery.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.unselected_phone))
//            ivCamera.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.selected_camera))
//            b.dismiss()
//            cameraIntent()
//        }
//
//        layoutGallery.setOnClickListener {
//            ivGallery.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.selected_phone))
//            ivCamera.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.unselected_camera))
//            b.dismiss()
//            galleryIntent()
//        }
//
//        popupClose.setOnClickListener {
//            b.dismiss()
//        }
//
//        b.setCancelable(false)
//        b.show()
//
//        val displayMetrics = DisplayMetrics()
//        windowManager.defaultDisplay.getMetrics(displayMetrics)
//        b.window.setBackgroundDrawableResource(R.color.transparent)
//
//        val displayWidth = displayMetrics.widthPixels
//        val displayHeight = displayMetrics.heightPixels
//        val layoutParams = WindowManager.LayoutParams()
//        layoutParams.copyFrom(b.window.attributes)
//
//        val dialogWindowWidth = (displayWidth * 1f).toInt()
//        val dialogWindowHeight = (displayHeight * 1f).toInt()
//        layoutParams.width = dialogWindowWidth
//        layoutParams.height = dialogWindowHeight
//        b.window.attributes = layoutParams
//    }
//
//    private var mPicCaptureUri: Uri? = null
//
//    private fun galleryIntent() {
//        val intent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
//        startActivityForResult(intent, 2)
//    }
//
//
//    private fun cameraIntent() {
//        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        mPicCaptureUri = utility.getOutputMediaFileUri(MEDIA_TYPE_IMAGE, this)
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, mPicCaptureUri)
//        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
//        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
//        startActivityForResult(intent, 1)
//    }
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//
//        if (resultCode == RESULT_OK) {
//            if (requestCode == 1) {
//                if (mPicCaptureUri != null) {
//                    ivCoverImage.visibility = View.VISIBLE
//                    ivGetCoverImage.visibility = View.GONE
//                    ivCoverImage.setImageURI(mPicCaptureUri)
//                } else {
//                    ivCoverImage.visibility = View.GONE
//                    ivGetCoverImage.visibility = View.VISIBLE
//                    Toast.makeText(this, "Error while capturing Image", Toast.LENGTH_LONG).show()
//                }
//            } else if (requestCode == 2) {
//                val selectedImage = data?.data
//                mPicCaptureUri = selectedImage
//                val filePath = arrayOf(MediaStore.Images.Media.DATA)
//                val c = this.contentResolver?.query(selectedImage, filePath, null, null, null)
//                c!!.moveToFirst()
//                val columnIndex = c.getColumnIndex(filePath[0])
//                val picturePath = c.getString(columnIndex)
//                c.close()
//                val thumbnail = BitmapFactory.decodeFile(picturePath)
//                if (thumbnail != null) {
//                    ivCoverImage.visibility = View.VISIBLE
//                    ivGetCoverImage.visibility = View.GONE
//                    ivCoverImage.setImageBitmap(thumbnail)
//                } else {
//                    ivCoverImage.visibility = View.GONE
//                    ivGetCoverImage.visibility = View.VISIBLE
//                }
//            }
//        }
//    }
//
//    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
//        if (requestCode == REQUEST_PERMISSIONS) {
//            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
//                openSelectImagePopup()
//            } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
//                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
//                    ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), REQUEST_PERMISSIONS)
//                }
//            } else if (grantResults[1] == PackageManager.PERMISSION_DENIED) {
//                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//                    ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_PERMISSIONS)
//                }
//            }
//        }
//    }
//
//    /*--------------------------------------------------------------------------------------------*/
//
//    private var tempFile: String? = null
//    private var myBase64Image: String? = null
//    private var bm: Bitmap? = null
//    private var coverImageUrl: String = ""
//
//    private fun selectedImage(imageUri: Uri): String {
//        val outputUri = imageUri
//        var imageStream: InputStream? = null
//        try {
//            imageStream = this.contentResolver.openInputStream(outputUri)
//        } catch (e: FileNotFoundException) {
//            e.printStackTrace()
//        }
//
//        try {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                bm = BitmapFactory.decodeStream(imageStream)
//                val file = storeImage(bm!!)
//                tempFile = utility.compressImage(file!!.absolutePath)
//                bm = null
//            } else {
//                myBase64Image = utility.getUriPath(this, outputUri)
//                tempFile = myBase64Image
//                tempFile = utility.compressImage(tempFile!!)
//            }
//        } catch (e: NullPointerException) {
//            e.printStackTrace()
//
//        }
//        myBase64Image = tempFile
//        try {
//            coverImageUrl = awsUtility.uploadCoverImage(myBase64Image!!, "coverImage")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        return coverImageUrl
//    }
//
//    private fun storeImage(image: Bitmap): File? {
//        val pictureFile = utility.getOutputMediaFile(1) ?: return null
//        try {
//            bm = image
//            val fos = FileOutputStream(pictureFile)
//            image.compress(Bitmap.CompressFormat.JPEG, 60, fos)
//            fos.close()
//            return pictureFile
//        } catch (e: FileNotFoundException) {
//            e.printStackTrace()
//        } catch (e: IOException) {
//            e.printStackTrace()
//        }
//
//        return pictureFile
//    }
//
//    override fun onBackPressed() {
//        super.onBackPressed()
//        finish()
//    }
//
//
//    override fun hideKeyboard() {
//        val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
//        //Find the currently focused view, so we can grab the correct window token from it.
//        var view = this.currentFocus
//        //If no view currently has focus, create a new one, just so we can grab a window token from it
//        if (view == null) {
//            view = View(this)
//        }
//        imm.hideSoftInputFromWindow(view.windowToken, 0)
//    }
//
//    //------------------SURFACE CREATED FIRST TIME--------------------//
//    private var flag = 0
//    private var camera: Camera? = null
//
//    override fun surfaceCreated(arg0: SurfaceHolder) {
//        try {
//            if (flag == 0) {
//                camera = Camera.open(0)
//            } else {
//                camera = Camera.open(1)
//            }
//        } catch (e: RuntimeException) {
//            e.printStackTrace()
//            return
//        }
//
//        try {
//            val param: Camera.Parameters? = camera!!.parameters
//            val sizes = param!!.supportedPictureSizes
//            //get diff to get perfect preview sizes
//            val displayMetrics = DisplayMetrics()
//            windowManager.defaultDisplay.getMetrics(displayMetrics)
//            val height = displayMetrics.heightPixels
//            val width = displayMetrics.widthPixels
//            val diff = (height * 1000 / width).toLong()
//            var cdistance = Integer.MAX_VALUE.toLong()
//            var idx = 0
//            for (i in sizes.indices) {
//                val value = (sizes[i].width * 1000).toLong() / sizes[i].height
//                if (value > diff && value < cdistance) {
//                    idx = i
//                    cdistance = value
//                }
//            }
//            val cs = sizes[idx]
//            param.setPreviewSize(cs.width, cs.height)
//            param.setPictureSize(cs.width, cs.height)
//            if (camera != null) {
//                camera!!.cancelAutoFocus()
//                if (param == null) return
//                if (param.focusMode !== Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE) {
//                    param.focusMode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE
//                    param.focusAreas = null
//                    param.meteringAreas = null
//                }
//            }
//            camera!!.parameters = param
//            setCameraDisplayOrientation(0)
//            camera!!.setPreviewDisplay(surfaceHolder)
//            camera!!.startPreview()
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return
//        }
//    }
//
//    override fun surfaceDestroyed(arg0: SurfaceHolder) {
//        try {
//            camera!!.stopPreview()
//            camera!!.release()
//            camera = null
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//    }
//
//    override fun surfaceChanged(surfaceHolder: SurfaceHolder, i: Int, i1: Int, i2: Int) {
//        refreshCamera()
//    }
//
//    private fun setCameraDisplayOrientation(cameraId: Int) {
//        val info = Camera.CameraInfo()
//        Camera.getCameraInfo(cameraId, info)
//        var rotation = windowManager.defaultDisplay.rotation
//        if (Build.MODEL.equals("Nexus 6", ignoreCase = true) && flag == 1) {
//            rotation = Surface.ROTATION_180
//        }
//        var degrees = 0
//        when (rotation) {
//            Surface.ROTATION_0 -> degrees = 0
//            Surface.ROTATION_90 -> degrees = 90
//            Surface.ROTATION_180 -> degrees = 180
//            Surface.ROTATION_270 -> degrees = 270
//        }
//        var result: Int
//        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
//            result = (info.orientation + degrees) % 360
//            result = (360 - result) % 360 // compensate the mirror
//        } else {
//            result = (info.orientation - degrees + 360) % 360
//        }
//        camera!!.setDisplayOrientation(result)
//    }
//
//    private fun refreshCamera() {
//        if (surfaceHolder!!.surface == null) {
//            return
//        }
//        try {
//            camera!!.stopPreview()
//            val param = camera!!.parameters
//            refreshCameraPreview(param)
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//    }
//
//    private fun refreshCameraPreview(param: Camera.Parameters) {
//        try {
//            camera!!.parameters = param
//            setCameraDisplayOrientation(0)
//
//            camera!!.setPreviewDisplay(surfaceHolder)
//            camera!!.startPreview()
//
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//
//    }
//
//    //------------------SURFACE OVERRIDE METHODS END--------------------//
//
//    private var selectedFilter: Int? = 0
//    private var filterSelected: Boolean = false
//    private val updateFilter = object : BroadcastReceiver() {
//        override fun onReceive(context: Context?, intent: Intent?) {
//            filterSelected = true
//            val position = intent!!.getIntExtra("filterIcon", 0)
//            if (!broadcastFilter!![position].filterName.equals("None", true)) {
//                selectedFilter = position
//            }
//
//        }
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(updateFilter)
//    }
//
//}