package com.ruth.app.livestreaming.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.ruth.app.R
import com.ruth.app.model.BroadcastCustomOrder
import com.ruth.app.utility.RuthUtility
import kotlinx.android.synthetic.main.item_streaming_orders.view.*
import java.text.SimpleDateFormat
import java.util.*


class ReceivedOrdersAdapter(var context: Context,
                            private var ordersList: ArrayList<BroadcastCustomOrder>?) : RecyclerView.Adapter<ReceivedOrdersAdapter.ViewHolder>() {

    private var onItemClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.item_streaming_orders, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(ordersList!![position], context)
        holder.itemView.ivOrderStatus.setOnClickListener {
            holder.itemView.ivOrderStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_follow))
            onItemClickListener!!.onItemClick(holder.itemView, position)
        }

//        when {
//            ordersList!![position].isCompleted -> {
//                holder.itemView.ivOrderStatus.visibility = View.VISIBLE
//                holder.itemView.tvTimeLimit.visibility = View.GONE
//                holder.itemView.ivOrderStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_follow))
//            }
//            ordersList!![position].isRejected -> {
//                holder.itemView.ivOrderStatus.visibility = View.GONE
//                holder.itemView.tvTimeLimit.visibility = View.GONE
//            }
//            else -> {
//                holder.itemView.ivOrderStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.reject_offer))
//                holder.itemView.tvTimeLimit.visibility = View.VISIBLE
//                holder.itemView.tvTimeLimit.visibility = View.VISIBLE
//            }
//        }
    }

    override fun getItemCount(): Int {
        return ordersList!!.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private var timeLimit: String = ""
        private var diff: String = ""

        fun bind(broadcastCustomOrder: BroadcastCustomOrder, mContext: Context) {
            Glide.with(mContext).load(broadcastCustomOrder.userData.photoUrl)
                    .apply(RequestOptions().error(R.drawable.com_facebook_profile_picture_blank_square)
                            .placeholder(R.drawable.com_facebook_profile_picture_blank_square))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            itemView.progressBarImageLoading.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            itemView.progressBarImageLoading.visibility = View.GONE
                            return false
                        }

                    })
                    .into(itemView.ivProfileImage)

            itemView.tvUserName.text = broadcastCustomOrder.userData.name
            itemView.tvUsersOrder.text = broadcastCustomOrder.comment

            if (adapterPosition % 2 == 0) {
                itemView.ivHeaderIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.purple_order))
            } else {
                itemView.ivHeaderIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.orange_order))
            }


            if (broadcastCustomOrder.orderCompletionTime == 0) {
                itemView.tvTimeLimit.text = "Unlimited Time"
            } else {
                if (broadcastCustomOrder.currentTime != null) {
                    timeLimit = RuthUtility.getTimeLimit(broadcastCustomOrder.currentTime, broadcastCustomOrder.orderCompletionTime)
                    val currentTime = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(System.currentTimeMillis()).toString()
                    if (currentTime == timeLimit || currentTime > timeLimit) {
                        itemView.tvTimeLimit.text = "Time limit ends"
                    } else {
                        diff = RuthUtility.printTimeDifference(currentTime, timeLimit, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                        itemView.tvTimeLimit.text = diff
                    }
                }
            }

            when {
                broadcastCustomOrder.isCompleted -> {
                    itemView.ivOrderStatus.visibility = View.VISIBLE
                    itemView.tvTimeLimit.visibility = View.GONE
                    itemView.ivOrderStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.icon_follow))
                }
                broadcastCustomOrder.isRejected -> {
                    itemView.ivOrderStatus.visibility = View.GONE
                    itemView.tvTimeLimit.visibility = View.GONE
                }
                else -> {
                    itemView.ivOrderStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.reject_offer))
                    itemView.tvTimeLimit.visibility = View.VISIBLE
                    itemView.tvTimeLimit.visibility = View.VISIBLE
                }
            }
        }
    }

    fun setOnItemClickListener(onItemClickList: OnItemClickListener) {
        this.onItemClickListener = onItemClickList
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}
