package com.ruth.app.livestreaming.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class FilterListPresenter @Inject constructor() : BasePresenter<FilterListPresenter.FilterFragmentView>() {

    interface FilterFragmentView : BaseView {

    }
}