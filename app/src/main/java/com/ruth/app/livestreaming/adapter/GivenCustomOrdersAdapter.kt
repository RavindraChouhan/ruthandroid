package com.ruth.app.livestreaming.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import kotlinx.android.synthetic.main.item_custom_orders.view.*
import java.util.*

class GivenCustomOrdersAdapter(var context: Context, var ordersList: ArrayList<HashMap<String, @JvmSuppressWildcards Any>>?) : RecyclerView.Adapter<GivenCustomOrdersAdapter.ViewHolder>() {

    private var onItemClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.item_custom_orders, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return ordersList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(ordersList!![position])

        holder.itemView.ivOrderCompleted.setOnClickListener {
            onItemClickListener!!.onItemClick(holder.itemView, position)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(orders: HashMap<String, @JvmSuppressWildcards Any>) {
            itemView.tvOrdersGiven.text = orders["message"].toString()
        }
    }

    fun setOnItemClickListener(onItemClickList: OnItemClickListener) {
        this.onItemClickListener = onItemClickList
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}
