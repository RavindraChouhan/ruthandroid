package com.ruth.app.livestreaming.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.design.widget.TabLayout
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.view.View.GONE
import android.view.View.OnClickListener
import android.view.animation.AccelerateInterpolator
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer.DecoderInitializationException
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil.DecoderQueryException
import com.google.android.exoplayer2.source.*
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.MappingTrackSelector.MappedTrackInfo
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.PlaybackControlView
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.util.Util
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.broadcaster.activity.BroadcastDetailActivity
import com.ruth.app.home.activity.HomeActivity
import com.ruth.app.home.userprofileview.activity.VIPActivity
import com.ruth.app.livestreaming.adapter.ChatAdapter
import com.ruth.app.livestreaming.adapter.GivenCustomOrdersAdapter
import com.ruth.app.livestreaming.adapter.LiveViewersAdapter
import com.ruth.app.livestreaming.adapter.StickerPagerAdapter
import com.ruth.app.livestreaming.dialogs.UserReceivedGiftsDialog
import com.ruth.app.livestreaming.dialogs.UserViewersDialog
import com.ruth.app.livestreaming.model.BroadcastFilter
import com.ruth.app.livestreaming.model.Message
import com.ruth.app.livestreaming.presenter.LiveVideoPlayerPresenter
import com.ruth.app.login.activity.LoginActivity
import com.ruth.app.model.*
import com.ruth.app.otheruser.OtherUserProfileActivity
import com.ruth.app.utility.RuthUtility
import com.ruth.app.utility.broadcastingstreaming.livevideoplayer.DefaultExtractorsFactoryForFLV
import com.ruth.app.utility.broadcastingstreaming.livevideoplayer.EventLogger
import com.ruth.app.utility.broadcastingstreaming.livevideoplayer.RtmpDataSource
import com.ruth.app.widgets.emojianim.ParticleSystem
import de.hdodenhof.circleimageview.CircleImageView
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_live_video_player.*
import kotlinx.android.synthetic.main.view_chat_broadcast.*
import org.jetbrains.anko.textColor
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.net.CookieHandler
import java.net.CookieManager
import java.net.CookiePolicy
import java.text.SimpleDateFormat
import java.util.HashMap
import kotlin.collections.ArrayList
import kotlin.collections.set

class LiveVideoPlayerActivity : BaseActivity<LiveVideoPlayerPresenter>(),
        LiveVideoPlayerPresenter.LiveVideoPlayerView,
        OnClickListener, ExoPlayer.EventListener, PlaybackControlView.VisibilityListener {

    private var mainHandler: Handler? = null
    private var eventLogger: EventLogger? = null
    private var mediaDataSourceFactory: DataSource.Factory? = null
    private var player: SimpleExoPlayer? = null
    private var trackSelector: DefaultTrackSelector? = null
    private var needRetrySource: Boolean = false
    private var shouldAutoPlay: Boolean = false
    private var resumeWindow: Int = 0
    private var resumePosition: Long = 0
    private var rtmpDataSourceFactory: RtmpDataSource.RtmpDataSourceFactory? = null
    private var isCandleOn: Boolean = false
    private var isNegative: Boolean = false
    private var isComments: Boolean = true
    private var isError: Boolean = false

    // Activity lifecycle
    private var userAgent: String? = null

    //chat messages
    private val mMessages = ArrayList<Message>()
    private var adapter: ChatAdapter? = null
    lateinit var layoutManager: LinearLayoutManager
    private lateinit var favoritePagerAdapter: StickerPagerAdapter
    private var channelBroadcasterData: UserProfileData? = null

    //Stickers list
    private val stickerItemList: ArrayList<PositiveNegative> = ArrayList()
    private var positiveSticker: ArrayList<PositiveNegative>? = ArrayList()
    private var specificOrder: ArrayList<PositiveNegative>? = ArrayList()
    private var negativeSticker: ArrayList<PositiveNegative>? = ArrayList()
    private var favoriteList: ArrayList<StickersData>? = ArrayList()
    private var allStickers: ArrayList<StickersData>? = ArrayList()
    private var startTime: String = ""
    private var handler: Handler = Handler(Looper.getMainLooper())
    private var totalGiftCount: Int = 0
    private var totalStreamingTime: String? = ""
    private var totalViewCount: String? = ""
    private var invalidDialog: AlertDialog? = null
    private var broadcasterId: String = ""
    private var orderTimeLimit: Int = 0
    private val broadcastFilter: ArrayList<BroadcastFilter>? = ArrayList()
    private val customOrderList: ArrayList<HashMap<String, @JvmSuppressWildcards Any>>? = ArrayList()
    private var givenCustomOrdersAdapter: GivenCustomOrdersAdapter? = null
    private lateinit var runnable: Runnable
    private var mDelayHandler: Handler? = Handler()
    private val splashDelay: Long = 300 //0.3 second
    private var isBack: Boolean = false

    companion object {
        var BROADCAST_ID: String = ""
        var STICKER_TYPE: String = ""
        var SUB_STICKER_TYPE: Int = 0

        val PREFER_EXTENSION_DECODERS = "prefer_extension_decoders"
        private val BANDWIDTH_METER = DefaultBandwidthMeter()
        private val DEFAULT_COOKIE_MANAGER: CookieManager

        init {
            DEFAULT_COOKIE_MANAGER = CookieManager()
            DEFAULT_COOKIE_MANAGER.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER)
        }

        private fun isBehindLiveWindow(e: ExoPlaybackException): Boolean {
            if (e.type != ExoPlaybackException.TYPE_SOURCE) {
                return false
            }
            var cause: Throwable? = e.sourceException
            while (cause != null) {
                if (cause is BehindLiveWindowException) {
                    return true
                }
                cause = cause.cause
            }
            return false
        }

        var usersViewersList: ArrayList<ViewerData> = ArrayList()
        var usersGiftList: ArrayList<UserGiftData> = ArrayList()
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userAgent = Util.getUserAgent(this, "ExoPlayerDemo")
        shouldAutoPlay = true
        clearResumePosition()
        mediaDataSourceFactory = buildDataSourceFactory(true)
        rtmpDataSourceFactory = RtmpDataSource.RtmpDataSourceFactory()
        mainHandler = Handler()
        if (CookieHandler.getDefault() !== DEFAULT_COOKIE_MANAGER) {
            CookieHandler.setDefault(DEFAULT_COOKIE_MANAGER)
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)
        setContentView(R.layout.activity_live_video_player)
        App.component.inject(this)
        presenter.onCreate(this)

        setUpSocket()
        setUpFilterList()
        broadcasterId = intent.getStringExtra("broadcasterId")
        BROADCAST_ID = intent.getStringExtra("broadcastId")
        presenter.getUsersGifts(intent.getStringExtra("broadcastId")/*"5b92567aad43682a9373450c"*/)
        presenter.getViewerCount(intent.getStringExtra("broadcastId")/*"5ba22aa6cfb96e1a288598e0"*/)
        presenter.getBroadcasterDetail(intent.getStringExtra("broadcasterId"), intent.getStringExtra("broadcastId"))
        setData()
        sendViewer(intent.getStringExtra("broadcastId"), preferences.getUserInfo().id, true, false)

        val layoutManager = LinearLayoutManager(this)
        layoutManager.stackFromEnd = true
        rvMessagesList.layoutManager = layoutManager
        adapter = ChatAdapter(this, mMessages)
        rvMessagesList.adapter = adapter

        etChatMessage.setOnEditorActionListener(TextView.OnEditorActionListener { v, id, event ->
            if (id == R.id.send || id == EditorInfo.IME_NULL) {
                attemptSend()
                hideKeyboard()
                return@OnEditorActionListener true
            }
            false
        })

        positiveSticker?.addAll(preferences.getStickers().positive!!)
        negativeSticker?.addAll(preferences.getStickers().negative!!)
        specificOrder?.addAll(preferences.getStickers().specificOrder!!)

        /*Hide follow button for own userid*/
        if (intent.getStringExtra("broadcasterId") == preferences.getUserInfo().id) {
            ivLiveFollowUnfollow.visibility = GONE
        } else {
            ivLiveFollowUnfollow.visibility = View.VISIBLE
        }

        rootView.setOnClickListener(this)
        simpleExoPlayerView.setControllerVisibilityListener(this)
        simpleExoPlayerView.requestFocus()
        imgCandle.setOnClickListener(this)
        lytOptionSelector.setOnClickListener(this)
        imgBack.setOnClickListener(this)
        btnChat.setOnClickListener(this)
        lytLiveGift.setOnClickListener(this)
        lytOrderList.setOnClickListener(this)
        lblBackpack.setOnClickListener(this)
        lblPositiveComments.setOnClickListener(this)
        lblNegativeComments.setOnClickListener(this)
        lytUserProfile.setOnClickListener(this)
        lytEmojis.setOnClickListener(this)
        lytLike.setOnClickListener(this)
        ivLiveFollowUnfollow.setOnClickListener(this)
        tvSendCustomGift.setOnClickListener(this)
        layoutTotalGifts.setOnClickListener(this)
        layoutUserViews.setOnClickListener(this)
        tvUnlimitedLimit.setOnClickListener(this)
        tvFiveMinLimit.setOnClickListener(this)
        tvHighestLimit.setOnClickListener(this)
        tvTerms.setOnClickListener(this)
        customTextView(tvTerms)
        btnSendMessage.setOnClickListener {
            attemptSend()
            hideKeyboard()
        }

        play(null)

        LocalBroadcastManager.getInstance(this).registerReceiver(updatePoints, IntentFilter("updatePoints"))
        checkInternetConnection()
    }

    override fun onClick(v: View) {
        if (v.id == R.id.tvTerms) {
            openTermsConditions()
        }

        if (v.id == R.id.tvUnlimitedLimit) {
            orderTimeLimit = 0
            tvUnlimitedLimit.setTextColor(ContextCompat.getColor(this, R.color.white))
            tvFiveMinLimit.setTextColor(ContextCompat.getColor(this, R.color.black))
            tvHighestLimit.setTextColor(ContextCompat.getColor(this, R.color.black))
        }

        if (v.id == R.id.tvFiveMinLimit) {
            orderTimeLimit = 5
            tvUnlimitedLimit.setTextColor(ContextCompat.getColor(this, R.color.black))
            tvFiveMinLimit.setTextColor(ContextCompat.getColor(this, R.color.white))
            tvHighestLimit.setTextColor(ContextCompat.getColor(this, R.color.black))
        }

        if (v.id == R.id.tvHighestLimit) {
            orderTimeLimit = 30
            tvUnlimitedLimit.setTextColor(ContextCompat.getColor(this, R.color.black))
            tvFiveMinLimit.setTextColor(ContextCompat.getColor(this, R.color.black))
            tvHighestLimit.setTextColor(ContextCompat.getColor(this, R.color.white))
        }

        if (v.id == R.id.layoutUserViews) {
            if (usersViewersList.size > 0) {
                UserViewersDialog().newInstance("StreamingPlayer")
                        .show(this.supportFragmentManager, UserViewersDialog::javaClass.name)
            } else {
                Toast.makeText(this, "No viewers yet..", Toast.LENGTH_SHORT).show()
            }
        }

        if (v.id == R.id.layoutTotalGifts) {
            if (usersGiftList.size > 0) {
                UserReceivedGiftsDialog().newInstance("StreamingPlayer")
                        .show(this.supportFragmentManager, UserReceivedGiftsDialog::javaClass.name)
            } else {
                Toast.makeText(this, "No gifts received yet..", Toast.LENGTH_SHORT).show()
            }
        }

        if (v.id == R.id.imgCandle) {
            candleStartStop()
        }

        if (v.id == R.id.imgBack) {
            Log.d("progress", "back")
            isBack = true
            leave()
            if (intent.getStringExtra("broadcastId") != null) {
                sendViewer(intent.getStringExtra("broadcastId"), preferences.getUserInfo().id, false, true)
            }
        }

        if (v.id == R.id.lblBackpack) {
            backPackCommentSticker()
        }

        if (v.id == R.id.lblPositiveComments) {
            positiveCommentSticker()
        }

        if (v.id == R.id.lblNegativeComments) {
            negativeCommentSticker()
        }

        if (v.id == R.id.rootView) {
            rootViewImpl()
        }

        if (v.id == R.id.btnChat) {
            resetButtonsBackground()
            bgChat.setColorFilter(ContextCompat.getColor(this, R.color.red))

            mDelayHandler!!.postDelayed({
                lytOptionSelector.visibility = View.GONE
                chatList.visibility = View.VISIBLE
            }, splashDelay)
        }

        if (v.id == R.id.lytLiveGift) {
            resetButtonsBackground()
            bgLiveGift.setColorFilter(ContextCompat.getColor(this, R.color.red))

            mDelayHandler!!.postDelayed({
                giftPress()
            }, splashDelay)
        }

        if (v.id == R.id.lytOrderList) {
            resetButtonsBackground()
            bgOrderList.setColorFilter(ContextCompat.getColor(this, R.color.red))

            mDelayHandler!!.postDelayed({
                specificOrderSticker()
            }, splashDelay)
        }

        if (v.id == R.id.lytEmojis) {
            resetButtonsBackground()
            bgEmojis.setColorFilter(ContextCompat.getColor(this, R.color.red))
            emitEmoji()
        }

        if (v.id == R.id.lytLike) {
            resetButtonsBackground()
            bgLike.setColorFilter(ContextCompat.getColor(this, R.color.red))
            sendLikes()
        }

        if (v.id == R.id.lytUserProfile) {
            showUserProfile()
        }

        if (v.id == R.id.ivLiveFollowUnfollow) {
            followUnfollowUser()
        }
        if (v.id == R.id.tvSendCustomGift) {
            sendCustomGift()
        }
    }

    /*---------------------------------------ON CLICK METHODS-------------------------------------*/
    private fun candleStartStop() {
        if (isCandleOn) {
            isCandleOn = false
            imgCandle.setImageResource(R.drawable.icon_candle_off)
            lblCurrent.setShadowLayer(3.0f, 2.0f, 2.0f, Color.parseColor("#d0021b"))
            lblCurrent.text = "未開始"
        } else {
            startTime = RuthUtility.convertSecondsToHMmSs(player!!.currentPosition)
            if (startTime.isNotEmpty()) {
                isCandleOn = true
                imgCandle.setImageResource(R.drawable.icon_candle_on)
                lblCurrent.setShadowLayer(3.0f, 2.0f, 2.0f, Color.parseColor("#35fd08"))
                lblCurrent.text = startTime
            }
        }
    }

    private fun giftPress() {
        isComments = true
        isNegative = false
        lytOptionSelector.visibility = GONE
        lytAddComments.visibility = GONE
        lytSticker.visibility = View.VISIBLE
        lytStickerList.visibility = View.VISIBLE
        lblBackpack.visibility = View.VISIBLE

        lblBackpack.text = resources.getString(R.string.backpack)
        lblPositiveComments.text = resources.getString(R.string.positive_comments)
        lblNegativeComments.text = resources.getString(R.string.negative_comments)

        lblBackpack.textColor = this.resources.getColor(R.color.white)
        lblPositiveComments.textColor = this.resources.getColor(R.color.gray)
        lblNegativeComments.textColor = this.resources.getColor(R.color.gray)

        STICKER_TYPE = "backpack"
        setupTabLayout()
    }

    private fun positiveCommentSticker() {
        if (!isComments) {
            lytStickerList.visibility = View.VISIBLE
            lytAddComments.visibility = GONE
            STICKER_TYPE = "specificOrder"
        } else {
            STICKER_TYPE = "positive"
        }
        lblNegativeComments.setTextColor(ContextCompat.getColor(this, R.color.gray))
        lblBackpack.setTextColor(ContextCompat.getColor(this, R.color.gray))
        lblPositiveComments.setTextColor(ContextCompat.getColor(this, R.color.white))
        isNegative = false

        setupTabLayout()
    }

    private fun backPackCommentSticker() {
        if (!isComments) {
            lytStickerList.visibility = View.VISIBLE
            lytAddComments.visibility = GONE
            STICKER_TYPE = "specificOrder"
        } else {
            STICKER_TYPE = "backpack"
        }

        lblBackpack.setTextColor(ContextCompat.getColor(this, R.color.white))
        lblNegativeComments.setTextColor(ContextCompat.getColor(this, R.color.gray))
        lblPositiveComments.setTextColor(ContextCompat.getColor(this, R.color.gray))
        isNegative = false

        setupTabLayout()
    }

    private fun negativeCommentSticker() {
        if (isComments) {
            isNegative = true
            STICKER_TYPE = "negative"
            setupTabLayout()
        } else {
            lytStickerList.visibility = GONE
            lytAddComments.visibility = View.VISIBLE
            STICKER_TYPE = "customOrder"
            setCustomGift()
        }
        lblNegativeComments.setTextColor(ContextCompat.getColor(this, R.color.white))
        lblPositiveComments.setTextColor(ContextCompat.getColor(this, R.color.gray))
        lblBackpack.setTextColor(ContextCompat.getColor(this, R.color.gray))
    }

    private fun sendCustomGift() {
        if (STICKER_TYPE.equals("customOrder", true)) {
            val customGift = preferences.getStickers().customGift

            if (etCustomGift.text.toString().isEmpty()) {
                Toast.makeText(this, "Action cannot be empty", Toast.LENGTH_SHORT).show()
                return
            }

            if (customGift!!.giftType.equals(getString(R.string.gold_points), true)) {
                checkGiftApplicable(customGift.giftValue, tvGoldCoins.text.toString(), customGift._id)
            }

            if (customGift.giftType.equals(getString(R.string.silver_points), true)) {
                checkGiftApplicable(customGift.giftValue, tvSilverCoins.text.toString(), customGift._id)
            }

            if (customGift.giftType.equals(getString(R.string.coin_points), true)) {
                checkGiftApplicable(customGift.giftValue, tvYuanCoins.text.toString(), customGift._id)
            }
        }
    }

    private fun checkGiftApplicable(giftValue: String, giftTypeValue: String, _id: String) {
        if (giftValue <= giftTypeValue) {
            sendGift(etCustomGift.text.toString().trim(), BROADCAST_ID, preferences.getUserInfo().id,
                    _id, "CustomGift", orderTimeLimit, SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(System.currentTimeMillis()).toString())
        } else {
            Toast.makeText(this, "Please get some points", Toast.LENGTH_SHORT).show()
        }
        etCustomGift.setText("")
        hideKeyboard()
    }

    private fun setCustomGift() {
        if (STICKER_TYPE.equals("customOrder", true)) {
            val customGift = preferences.getStickers().customGift

            if (customGift!!.giftType.equals(getString(R.string.gold_points), true)) {
                ivGiftType.setImageResource(R.drawable.icon_user_in_got_yellow)
            }

            if (customGift.giftType.equals(getString(R.string.silver_points), true)) {
                ivGiftType.setImageResource(R.drawable.icon_user_in_got_silver)
            }

            if (customGift.giftType.equals(getString(R.string.coin_points), true)) {
                ivGiftType.setImageResource(R.drawable.icon_user_yuan)
            }
            tvGiftValue.text = customGift.giftValue
        }
    }

    private fun rootViewImpl() {
        if (lytOptionSelector.visibility != View.VISIBLE) {
            lytOptionSelector.visibility = View.VISIBLE
            chatList.visibility = GONE
            lytSticker.visibility = GONE
        }

        bgChat.setColorFilter(ContextCompat.getColor(this, R.color.green))
        bgLiveGift.setColorFilter(ContextCompat.getColor(this, R.color.green))
        bgOrderList.setColorFilter(ContextCompat.getColor(this, R.color.green))
        bgLike.setColorFilter(ContextCompat.getColor(this, R.color.green))
        bgEmojis.setColorFilter(ContextCompat.getColor(this, R.color.green))
        hideKeyboard()
    }

    private fun specificOrderSticker() {
        lytOptionSelector.visibility = GONE
        lytSticker.visibility = View.VISIBLE
        lytAddComments.visibility = GONE
        lytStickerList.visibility = View.VISIBLE

        lblBackpack.visibility = View.GONE
        lblPositiveComments.text = resources.getString(R.string.specify_order)
        lblNegativeComments.text = resources.getString(R.string.custom_order)

        lblPositiveComments.textColor = ContextCompat.getColor(this, R.color.white)
        lblNegativeComments.textColor = ContextCompat.getColor(this, R.color.gray)

        isComments = false
        STICKER_TYPE = "specificOrder"
        setupTabLayout()
    }

    private fun followUnfollowUser() {
        val isFollowed: Boolean = !BroadcastDetailActivity.liveUpcomingBroadcastList!![intent.getIntExtra("position", 0)].broadcasterData.isFollow
        presenter.followUnfollow(intent.getStringExtra("broadcasterId"), isFollowed)

        var followerCount = BroadcastDetailActivity.liveUpcomingBroadcastList!![intent.getIntExtra("position", 0)].broadcasterData.followers
        if (isFollowed) {
            followerCount += 1
            ivLiveFollowUnfollow.setImageResource(R.drawable.icon_follow)
        } else {
            followerCount -= 1
            ivLiveFollowUnfollow.setImageResource(R.drawable.icon_follow_button)
        }
        channelBroadcasterData!!.isFollow = isFollowed
        broadcasterFans.text = followerCount.toString() + " " + getString(R.string.followers)
        updateFollowerList(isFollowed, followerCount)

    }

    /*---------------------------------------START LIVE STREAMING---------------------------------*/

    public override fun onNewIntent(intent: Intent) {
        releasePlayer()
        shouldAutoPlay = true
        clearResumePosition()
        setIntent(intent)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            play(null)
        } else {
            showToast(R.string.storage_permission_denied)
            finish()
        }
    }

    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        // Show the controls on any key event.
        simpleExoPlayerView.hideController()
        // If the event was not handled then see if the player view can handle it as a media key event.
        return super.dispatchKeyEvent(event) || simpleExoPlayerView.dispatchMediaKeyEvent(event)
    }

    // Internal methods

    private fun initializePlayer(rtmpUrl: String) {
        try {
            val intent = intent
            val needNewPlayer = player == null
            if (needNewPlayer) {

                val preferExtensionDecoders = intent.getBooleanExtra(PREFER_EXTENSION_DECODERS, false)
                @SimpleExoPlayer.ExtensionRendererMode val extensionRendererMode = if (useExtensionRenderers())
                    if (preferExtensionDecoders)
                        SimpleExoPlayer.EXTENSION_RENDERER_MODE_PREFER
                    else
                        SimpleExoPlayer.EXTENSION_RENDERER_MODE_ON
                else
                    SimpleExoPlayer.EXTENSION_RENDERER_MODE_OFF
                val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(BANDWIDTH_METER)
                trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)
                player = ExoPlayerFactory.newSimpleInstance(this, trackSelector!!, DefaultLoadControl(), null, extensionRendererMode)
                //   player = ExoPlayerFactory.newSimpleInstance(this, trackSelector,
                //           new DefaultLoadControl(new DefaultAllocator(true, C.DEFAULT_BUFFER_SEGMENT_SIZE),  500, 1500, 500, 1500),
                //           null, extensionRendererMode);
                player!!.addListener(this)

                eventLogger = EventLogger(trackSelector)
                player!!.addListener(eventLogger)
                player!!.setAudioDebugListener(eventLogger)
                player!!.setVideoDebugListener(eventLogger)
                player!!.setMetadataOutput(eventLogger)

                simpleExoPlayerView.setPlayer(player)
                player!!.playWhenReady = shouldAutoPlay
                // debugViewHelper = DebugTextViewHelper(player, debugTextView)
                //debugViewHelper!!.start()
            }
            if (needNewPlayer || needRetrySource) {
                //  String action = intent.getAction();
                val uris: Array<Uri?>
                val extensions: Array<String?>

                uris = arrayOfNulls(1)
                uris[0] = Uri.parse(rtmpUrl)
                extensions = arrayOfNulls(1)
                extensions[0] = ""
                if (Util.maybeRequestReadExternalStoragePermission(this, *uris)) {
                    // The player will be reinitialized if the permission is granted.
                    return
                }
                val mediaSources = arrayOfNulls<MediaSource?>(uris.size)
                for (i in uris.indices) {
                    mediaSources[i] = buildMediaSource(uris[i], extensions[i])
                }
                val mediaSource = if (mediaSources.size == 1)
                    mediaSources[0]
                else
                    ConcatenatingMediaSource(*mediaSources)
                val haveResumePosition = resumeWindow != C.INDEX_UNSET
                if (haveResumePosition) {
                    player!!.seekTo(resumeWindow, resumePosition)
                }
                player!!.prepare(mediaSource, !haveResumePosition, false)
                needRetrySource = false
            }
        } catch (e: Exception) {
            Log.d("exception", "->" + e.message)
        }
    }

    private fun buildMediaSource(uri: Uri?, overrideExtension: String?): MediaSource {
        val type = if (TextUtils.isEmpty(overrideExtension))
            Util.inferContentType(uri)
        else
            Util.inferContentType(".$overrideExtension")
        Log.d("exoplayer", "uri ->" + uri!!.scheme + " type -> " + type)
        when (type) {
            C.TYPE_SS -> return SsMediaSource(uri, buildDataSourceFactory(false),
                    DefaultSsChunkSource.Factory(mediaDataSourceFactory), mainHandler, eventLogger)
            C.TYPE_DASH -> return DashMediaSource(uri, buildDataSourceFactory(false),
                    DefaultDashChunkSource.Factory(mediaDataSourceFactory), mainHandler, eventLogger)
            C.TYPE_HLS -> return HlsMediaSource(uri, mediaDataSourceFactory, mainHandler, eventLogger)
            C.TYPE_OTHER -> return if (uri!!.scheme == "rtmp") {
                ExtractorMediaSource(uri, rtmpDataSourceFactory, DefaultExtractorsFactoryForFLV(),
                        mainHandler, eventLogger)
            } else {
                ExtractorMediaSource(uri, mediaDataSourceFactory, DefaultExtractorsFactory(),
                        mainHandler, eventLogger)
            }
            else -> {
                throw IllegalStateException("Unsupported type: $type")
            }
        }
    }

    private fun releasePlayer() {
        if (player != null) {
            shouldAutoPlay = player!!.playWhenReady
            updateResumePosition()
            player!!.release()
            player = null
            trackSelector = null
            //trackSelectionHelper = null;
            eventLogger = null
        }
    }

    private fun updateResumePosition() {
        resumeWindow = player!!.currentWindowIndex
        resumePosition = if (player!!.isCurrentWindowSeekable)
            Math.max(0, player!!.currentPosition)
        else
            C.TIME_UNSET
    }

    private fun clearResumePosition() {
        resumeWindow = C.INDEX_UNSET
        resumePosition = C.TIME_UNSET
    }

    /**
     * Returns a new DataSource factory.
     *
     * @param useBandwidthMeter Whether to set [.BANDWIDTH_METER] as a listener to the new
     * DataSource factory.
     * @return A new DataSource factory.
     */
    private fun buildDataSourceFactory(useBandwidthMeter: Boolean): DataSource.Factory {
        return buildDataSourceFactory(if (useBandwidthMeter) BANDWIDTH_METER else null)
    }

    // ExoPlayer.EventListener implementation

    override fun onLoadingChanged(isLoading: Boolean) {
        Log.d("loading", "->$isLoading")
    }

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        if (playbackState == ExoPlayer.STATE_ENDED) {
            showControls()
        }
        if (playbackState == ExoPlayer.STATE_BUFFERING) {
            Log.d("progress", "STATE_BUFFERING")
            showVideoProgress()

        }
        if (playbackState == ExoPlayer.STATE_IDLE) {
            Log.d("progress", "STATE_IDLE")
            if (!isError) {
                showVideoProgress()
            } else {
                hideVideoProgress()
            }
        }
        if (playbackState == ExoPlayer.STATE_READY) {
            Log.d("progress", "STATE_READY")
            hideVideoProgress()
            startTime = RuthUtility.convertSecondsToHMmSs(player!!.currentPosition)
        }
    }

    override fun onPositionDiscontinuity() {
        if (needRetrySource) {
            // This will only occur if the user has performed a seek whilst in the error state. Update the
            // resume position so that if the user then retries, playback will resume from the position to
            // which they seeked.
            updateResumePosition()
        }
    }


    override fun onTimelineChanged(timeline: Timeline?, manifest: Any?) {
        Log.d("timeline", "manifest")
    }

    @SuppressLint("StringFormatInvalid")
    override fun onPlayerError(e: ExoPlaybackException) {
//        hideVideoProgress()
        Log.d("error", "->" + e)
        //videoStartControlLayout.setVisibility(View.VISIBLE)
        var errorString: String? = null
        if (e.type == ExoPlaybackException.TYPE_RENDERER) {
            val cause = e.rendererException
            if (cause is DecoderInitializationException) {
                // Special case for decoder initialization failures.
                val decoderInitializationException = cause as DecoderInitializationException
                if (decoderInitializationException.decoderName == null) {
                    if (decoderInitializationException.cause is DecoderQueryException) {
                        errorString = getString(R.string.error_querying_decoders)
                    } else if (decoderInitializationException.secureDecoderRequired) {
                        errorString = getString(R.string.error_no_secure_decoder,
                                decoderInitializationException.mimeType)
                    } else {
                        errorString = getString(R.string.error_no_decoder,
                                decoderInitializationException.mimeType)
                    }
                } else {
                    errorString = getString(R.string.error_instantiating_decoder,
                            decoderInitializationException.decoderName)
                }
            }
        }
        if (errorString != null) {
            showToast(errorString)
        } else {
            Log.d("progress", "Error")
            isError = true
            hideVideoProgress()
//            showErrorDialog()
        }

        needRetrySource = true
        if (isBehindLiveWindow(e)) {
            clearResumePosition()
            play(null)
        } else {
            updateResumePosition()
            showControls()
        }
    }

    private fun showErrorDialog() {
        hideVideoProgress()
        val builder = android.app.AlertDialog.Builder(this)
        builder.setMessage(getString(R.string.problem_in_video))
        builder.setPositiveButton(getString(R.string.ok_btn)) { dialog, which ->
            dialog.dismiss()
            hideVideoProgress()
            finish()
        }

        if (!isFinishing && window.decorView.isShown) {
            val dialog = builder.create()
            dialog.setCancelable(false)
            dialog.show()
        }
    }

    override fun onTracksChanged(trackGroups: TrackGroupArray, trackSelections: TrackSelectionArray) {
        val mappedTrackInfo = trackSelector!!.currentMappedTrackInfo
        if (mappedTrackInfo != null) {
            if ((mappedTrackInfo.getTrackTypeRendererSupport(C.TRACK_TYPE_VIDEO) == MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS)) {
                Toast.makeText(applicationContext, R.string.error_unsupported_video, Toast.LENGTH_LONG).show()

            }
            if ((mappedTrackInfo.getTrackTypeRendererSupport(C.TRACK_TYPE_AUDIO) == MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS)) {
                Toast.makeText(applicationContext, R.string.error_unsupported_audio, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun showControls() {
        // debugRootView.visibility = View.VISIBLE
    }

    private fun buildDataSourceFactory(bandwidthMeter: DefaultBandwidthMeter?): DataSource.Factory {
        return DefaultDataSourceFactory(this, bandwidthMeter,
                buildHttpDataSourceFactory(bandwidthMeter))
    }

    private fun buildHttpDataSourceFactory(bandwidthMeter: DefaultBandwidthMeter?): HttpDataSource.Factory {
        return DefaultHttpDataSourceFactory(userAgent, bandwidthMeter)
    }

    private fun useExtensionRenderers(): Boolean {
        return BuildConfig.FLAVOR == "withExtensions"
    }

    fun play(view: View?) {
        val url = com.ruth.app.BuildConfig.RTMP_BASE_URL + intent.getStringExtra("streamName")
        initializePlayer(url)
        //videoStartControlLayout.visibility = View.GONE
    }
/*---------------------------------------END LIVE STREAMING-----------------------------------*/


    public override fun onPause() {
        checkInternetConnection()
        super.onPause()
        if (Util.SDK_INT <= 23) {
            releasePlayer()
        }
    }

    public override fun onStop() {
        super.onStop()
        if (Util.SDK_INT > 23) {
            releasePlayer()
        }
    }

    override fun onVisibilityChange(visibility: Int) {

    }

    override fun onDestroy() {
        if (invalidDialog != null) {
            invalidDialog!!.dismiss()
            invalidDialog = null
        }
        if (intent.getStringExtra("broadcastId") != null) sendViewer(intent.getStringExtra("broadcastId"),
                preferences.getUserInfo().id, false, false)
        leave()
        super.onDestroy()
    }

/*---------------------------------------START OF SOCKET--------------------------------------*/

    private var mSocket: Socket? = null
    private var isConnected: Boolean? = true

    private fun attemptSend() {

        if (!mSocket!!.connected()) return

        val message = etChatMessage.text.toString().trim({ it <= ' ' })
        if (TextUtils.isEmpty(message)) {
            etChatMessage.requestFocus()
            return
        }

        etChatMessage.setText("")

        val postData = JSONObject()
        try {
            postData.put("broadcastId", intent.getStringExtra("broadcastId"))
            postData.put("userId", preferences.getUserInfo().id)
            postData.put("message", message)
            postData.put("type", "comment")

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        mSocket!!.emit("COMMENTS", postData)
    }

    private fun sendGift(customOrderText: String, broadcastId: String, userId: String, giftId: String, type: String,
                         orderCompletionTime: Int, currentTime: String) {
        if (!mSocket!!.connected()) return

        val postData = JSONObject()
        try {
            postData.put("message", customOrderText)
            postData.put("broadcastId", broadcastId)
            postData.put("userId", userId)
            postData.put("giftId", giftId)
            postData.put("messageType", type)
            postData.put("orderCompletionTime", orderCompletionTime)
            postData.put("currentTime", currentTime)
            postData.put("endOrderTime", RuthUtility.getTimeLimit(currentTime, orderCompletionTime))

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        mSocket!!.emit("CUSTOM_ORDER", postData)
        hideKeyboard()
    }

    private fun emitEmoji() {
        if (!mSocket!!.connected()) return

        val postData = JSONObject()
        try {
            postData.put("broadcastId", intent.getStringExtra("broadcastId") /*"5b5ab2317ea1dc4485f963ed"*/)
            postData.put("userId", preferences.getUserInfo().id /*"5b3d93961cea7d57360e0dec"*/)
            postData.put("type", "emoji")

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        mSocket!!.emit("EMOJI", postData)

    }

    private fun sendLikes() {
        if (!mSocket!!.connected()) return

        val postData = JSONObject()
        try {
            postData.put("broadcastId", intent.getStringExtra("broadcastId") /*"5b5ab2317ea1dc4485f963ed"*/)
            postData.put("userId", preferences.getUserInfo().id /*"5b3d93961cea7d57360e0dec"*/)
            postData.put("type", "like")

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        mSocket!!.emit("LIKES", postData)
    }

    private fun setUpSocket() {
        val app = this.application as App
        mSocket = app.socket
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
        mSocket!!.on("RECEIVE_MESSAGE", onNewMessage)
        mSocket!!.on("RECEIVE_LIKE", onNewLike)
        mSocket!!.on("RECEIVE_EMOJI", onNewEmoji)
        mSocket!!.on("RECEIVE_VIEWS", onNewView)
        mSocket!!.on("RECEIVE_ELITE", onGetEliteUser)
        mSocket!!.on("RECEIVE_CUSTOM_ORDER", onReceiveGift)
        mSocket!!.on("RECEIVE_LIVE", onLiveEnded)
        mSocket!!.on("RECEIVE_FILTER", onFilterChange)
        mSocket!!.on("RECEIVE_COMPLETE_CUSTOM_ORDER", onCompleteCustomOrder)
        mSocket!!.connect()
    }

    private val onConnect = Emitter.Listener {
        this.runOnUiThread {
            if (!isConnected!!) {
                isConnected = true
            }
        }
    }

    private val onDisconnect = Emitter.Listener {
        this.runOnUiThread {
            Log.e(TAG, "Socket disconnected")
            isConnected = false
        }
    }

    private val onConnectError = Emitter.Listener {
        this.runOnUiThread {
            Log.e(TAG, "Socket connecting")
        }
    }

    private val onNewMessage = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            try {
                val data = args[0] as JSONObject
                val username: String
                val message: String
                val broadcastId: String
                val photoUrl: String
                try {
                    username = data.getString("username")
                    message = data.getString("message")
                    photoUrl = data.getString("photoUrl")
                    broadcastId = data.getString("broadcastId")
                    if (broadcastId == intent.getStringExtra("broadcastId")) {
                        addMessage(username, message, photoUrl, "chat")
                    }
                } catch (e: JSONException) {
                    Log.e(TAG, e.message)
                    return@Runnable
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private val onNewLike = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            try {
                val data = args[0] as JSONObject
                val broadcastId: String
                try {
                    broadcastId = data.getString("broadcastId")
                    if (broadcastId == intent.getStringExtra("broadcastId")) {
                        val ps = ParticleSystem(this, 1, R.drawable.icon_like, 2000)
                        ps.setScaleRange(1.3f, 1.3f)
                        ps.setSpeedModuleAndAngleRange(0.07f, 0.20f, 270, 270)
                        ps.setFadeOut(400, AccelerateInterpolator())
                        ps.emit(lytLike, 1)
                        Handler().postDelayed({
                            ps.stopEmitting()
                            ps.cancel()
                            bgLike.setColorFilter(ContextCompat.getColor(this, R.color.green))
                        }, 2000)
                    }
                } catch (e: JSONException) {
                    Log.e(TAG, e.message)
                    return@Runnable
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private var emoji = intArrayOf(R.drawable.eye,
            R.drawable.scary_emoji,
            R.drawable.ghost,
            R.drawable.fire)

    private val onNewEmoji = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            try {
                val data = args[0] as JSONObject
                val broadcastId: String
                try {
                    broadcastId = data.getString("broadcastId")
                    if (broadcastId == intent.getStringExtra("broadcastId")) {
                        for (i in 0 until emoji.size) {
                            val ps = ParticleSystem(this, 1, emoji[i], 2000)
                            ps.setScaleRange(1.3f, 1.3f)
                            ps.setSpeedModuleAndAngleRange(0.07f, 0.20f, 270, 270)
                            ps.setFadeOut(400, AccelerateInterpolator())
                            ps.emit(lytEmojis, 1)
                            Handler().postDelayed({
                                ps.stopEmitting()
                                ps.cancel()
                                bgEmojis.setColorFilter(ContextCompat.getColor(this, R.color.green))
                            }, 2000)
                        }
                    }
                } catch (e: JSONException) {
                    Log.e(TAG, e.message)
                    return@Runnable
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private fun addMessage(username: String, message: String, photoUrl: String, messageType: String) {
        mMessages.add(Message.Builder
        (Message.TYPE_MESSAGE).username(username).message(message).photoUrl(photoUrl).build())
        try {
            adapter!!.notifyChat(messageType, mMessages.size - 1)
            // adapter!!.notifyItemInserted(mMessages.size - 1)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        scrollToBottom(mMessages.size - 1)
    }

    private val onNewView = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            try {
                val data = args[0] as JSONObject
                Log.d("args", "-data :- $data")
                val broadcastId: String
                val viewCount: Int
                try {
                    broadcastId = data.getString("broadcastId")
                    if (broadcastId == intent.getStringExtra("broadcastId")) {
                        viewCount = data.getInt("viewCount")
                        if (viewCount != 0) {
                            tvViewCount.text = RuthUtility.getCounts(viewCount)
                            presenter.getViewerCount(intent.getStringExtra("broadcastId")/*"5ba22aa6cfb96e1a288598e0"*/)
                            val userPostData = JSONObject()
                            try {
                                userPostData.put("broadcastId", broadcastId)
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                            mSocket!!.emit("ELITE_USER", userPostData)
                        }
                    }

                } catch (e: JSONException) {
                    Log.e(TAG, e.message)
                    return@Runnable
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private val onReceiveGift = Emitter.Listener { args ->
        this.runOnUiThread {
            try {
                val data = args[0] as JSONObject
                val userId: String
                val username: String
                val message: String
                val broadcastId: String
                val photoUrl: String
                val giftPointsLeft: Int
                val customOrderId: String
                val userProfileData = preferences.getUserProfile()
                try {
                    userId = data.getString("userId")
                    username = data.getString("username")
                    message = data.getString("message")
                    photoUrl = data.getString("photoUrl")
                    broadcastId = data.getString("broadcastId")
                    giftPointsLeft = data.getInt("giftPointsLeft")
                    customOrderId = data.getString("customOrderId")
                    if (preferences.getStickers().customGift!!.giftType.equals(getString(R.string.gold_points), true)) {
                        HomeActivity.goldPoints = giftPointsLeft
                    }

                    if (preferences.getStickers().customGift!!.giftType.equals(getString(R.string.silver_points), true)) {
                        HomeActivity.silverPoints = giftPointsLeft
                    }

                    if (preferences.getStickers().customGift!!.giftType.equals(getString(R.string.coin_points), true)) {
                        HomeActivity.yuvanPoints = giftPointsLeft
                    }
                    setData()

                    if (broadcastId == intent.getStringExtra("broadcastId")) {
                        addMessage(username, message, photoUrl, "customGift")
                    }
                    if (userId == preferences.getUserInfo().id) {
                        val hm = HashMap<String, @JvmSuppressWildcards Any>()
                        hm["customOrderId"] = customOrderId
                        hm["message"] = message

                        customOrderList!!.add(hm)
                        setOrdersList()
                    }
                } catch (e: JSONException) {
                    Log.e(TAG, e.message)
                    return@runOnUiThread
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun setOrdersList() {
        givenCustomOrdersAdapter = GivenCustomOrdersAdapter(this, customOrderList)
        rvOrdersList.adapter = givenCustomOrdersAdapter
        givenCustomOrdersAdapter!!.setOnItemClickListener(object : GivenCustomOrdersAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                presenter.postOrderStatus(customOrderList!![position]["customOrderId"].toString(), position)
            }
        })
    }

    private val onFilterChange = Emitter.Listener { args ->
        this.runOnUiThread {
            try {
                val data = args[0] as JSONObject
                val broadcastId = data.getString("broadcastId")
                val filterId = data.getString("filterId")
                if (broadcastId == intent.getStringExtra("broadcastId")) {
                    for (i in 0 until broadcastFilter!!.size) {
                        if (broadcastFilter[i].filterId == filterId) {
                            ivFilterImage.visibility = View.VISIBLE
                            ivFilterImage.setImageDrawable(broadcastFilter[i].filterType)
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun updateLiveEnded(broadcastEndTime: String) {
        runnable = Runnable {
            kotlin.run {
                if (!isBack) {
                    if (player != null && player!!.currentPosition == null) {
                        return@run
                    }
                    startTime = RuthUtility.convertSecondsToHMmSs(player!!.currentPosition + 1000)
                    if (broadcastEndTime == startTime) {
                        showLiveEndedDialog()
                        handler.removeCallbacks(runnable)
                    } else {
                        handler.postDelayed(runnable, 1000)
                    }
                }
            }
        }
        handler.postDelayed(runnable, 3000)
    }

    private val onCompleteCustomOrder = Emitter.Listener { args ->
        this.runOnUiThread {
            try {
                val data = args[0] as JSONObject
                var customOrderId: String = ""
                var userId: String = ""
//                Log.e("data", " -> $data")
                customOrderId = data.getString("_id")
                userId = data.getString("userId")
                if (userId == preferences.getUserInfo().id) {

                    if (data.getBoolean("isCompleted")) {
//                        Log.e("status", "completed -> " + data.getBoolean("isCompleted"))
                    }

                    if (data.getBoolean("isRejected")) {
                        Toast.makeText(this, "Your order has been rejected by the user", Toast.LENGTH_SHORT).show()
//                        Log.e("status", "completed -> " + data.getBoolean("isRejected"))
                    }

                    presenter.getMyProfile()

                    for (i in 0 until customOrderList!!.size) {
                        if (customOrderList[i]["customOrderId"]!! == customOrderId) {
                            customOrderList.removeAt(i)
                            givenCustomOrdersAdapter!!.notifyDataSetChanged()
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun updatePointsData() {
        setData()
    }

    private fun showLiveEndedDialog() {
        if (!isBack) {
            val dialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
            val inflater = this.layoutInflater
            val dialogView = inflater.inflate(R.layout.popup_livestreaming_end, null)
            dialogBuilder.setView(dialogView)
            val b = dialogBuilder.create()
            b.window.attributes.windowAnimations = R.style.DialogAnimation
            b.window.setBackgroundDrawableResource(R.color.transparent)

            val btnOk = dialogView.findViewById<View>(R.id.btnOk) as TextView
            val ivUserImage = dialogView.findViewById<View>(R.id.ivUserImage) as CircleImageView
            val tvTotalViewCount = dialogView.findViewById<View>(R.id.tvTotalViewCount) as TextView
            val tvTotalGiftCount = dialogView.findViewById<View>(R.id.tvTotalGiftCount) as TextView
            val tvTotalStreamTime = dialogView.findViewById<View>(R.id.tvTotalStreamTime) as TextView

            val layoutViews = dialogView.findViewById<View>(R.id.layoutViews) as LinearLayout
            val layoutGifts = dialogView.findViewById<View>(R.id.layoutGifts) as LinearLayout
            val layoutTime = dialogView.findViewById<View>(R.id.layoutTime) as LinearLayout

            Glide.with(this).load(channelBroadcasterData!!.photoUrl)
                    .apply(RequestOptions().error(R.drawable.logoeye).placeholder(R.drawable.logoeye))
                    .into(ivUserImage)

            if (totalViewCount!!.isNotEmpty()) {
                layoutViews.visibility = View.VISIBLE
            }
            if (totalGiftCount > 0) {
                layoutGifts.visibility = View.VISIBLE
            }
            if (totalStreamingTime!!.isNotEmpty()) {
                layoutTime.visibility = View.VISIBLE
            }
            tvTotalViewCount.text = totalViewCount.toString()
            tvTotalGiftCount.text = totalGiftCount.toString()
            tvTotalStreamTime.text = totalStreamingTime.toString()

            btnOk.setOnClickListener {
                b.dismiss()
                leave()
                if (intent.getStringExtra("broadcastId") != null) {
                    sendViewer(intent.getStringExtra("broadcastId"), preferences.getUserInfo().id, false, false)
                }
                finish()
            }

            b.setCancelable(false)
            b.show()

            val displayMetrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(displayMetrics)
            b.window.setBackgroundDrawableResource(R.color.transparent)

            val displayWidth = displayMetrics.widthPixels
            val layoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(b.window.attributes)

            layoutParams.width = (displayWidth * 0.8f).toInt()
            layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
            layoutParams.gravity = Gravity.CENTER_VERTICAL
            b.window.attributes = layoutParams
        }
    }

    private val onGetEliteUser = Emitter.Listener { args ->
        this.runOnUiThread {
            try {
                val data = args[0] as JSONArray
                var liveUsers: JSONObject
                var giftData: JSONObject
                val liveViewerList: ArrayList<LiveViewers> = ArrayList()

                for (i in 0 until data.length()) {
                    val jsonData = data[i] as JSONObject
                    giftData = jsonData.getJSONObject("giftId")
                    liveUsers = jsonData.getJSONObject("userId")
                    val liveViewers = LiveViewers(GiftId(giftData.getString("_id"), giftData.getString("giftType")),
                            UserId(liveUsers.getString("_id"), liveUsers.getString("photoUrl")),
                            jsonData.getInt("count"))
                    liveViewerList.add(liveViewers)
                }

                rvPointsUser.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
                val liveViewersAdapter = LiveViewersAdapter(this, liveViewerList)
                rvPointsUser.adapter = liveViewersAdapter

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private val onLiveEnded = Emitter.Listener { args ->
        this.runOnUiThread {
            try {
                val data = args[0] as JSONObject
                var broadcastEndTime: String
                var broadcastId: String
                android.os.Handler().postDelayed({
                    try {
                        broadcastEndTime = data.getString("broadcastEndTime")
                        broadcastId = data.getString("broadcastId")
                        totalViewCount = data.getString("broadcastViewCount")
                        if (broadcastId == intent.getStringExtra("broadcastId")) {
                            totalStreamingTime = broadcastEndTime
                            if (player != null) {
                                startTime = RuthUtility.convertSecondsToHMmSs(player!!.currentPosition)
                                if (broadcastEndTime == startTime) {
                                    showLiveEndedDialog()
                                } else {
                                    updateLiveEnded(broadcastEndTime)
                                }
                            }
                        }
                    } catch (e: JSONException) {
                        Log.e(TAG, e.message)
                        return@postDelayed
                    }
                }, 1000)


            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    private fun scrollToBottom(position: Int) {
//          rvMessagesList.scrollToPosition(position)
    }

    private fun leave() {
        mSocket!!.disconnect()
        mSocket!!.connect()
    }

    private fun sendViewer(broadcastId: String, userId: String, isViewing: Boolean, isBack: Boolean) {
        if (!mSocket!!.connected()) return
        val postData = JSONObject()
        try {
            postData.put("broadcastId", broadcastId)
            postData.put("userId", userId)
            postData.put("isViewing", isViewing)
            postData.put("type", "view")
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        mSocket!!.emit("VIEWS", postData)

        if (isBack) {
            Log.d("progress", "back")
            showVideoProgress()
            presenter.showEndStreamingData(intent.getStringExtra("broadcastId"))
        }
    }
    /*-----------------------------------------END OF SOCKET--------------------------------------*/

    override fun hideKeyboard() {
        val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = this.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun invalidLogin() {

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Invalid Login")
        builder.setMessage(getString(R.string.invalid_login_error))

        // add a button
        builder.setPositiveButton(getString(R.string.log_in)) { dialog, which ->
            presenter.clearLogin()
            startActivity(Intent(this, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            finish()
            overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
            dialog.dismiss()
        }

        if (!isFinishing && window.decorView.isShown) {
            invalidDialog = builder.create()
            invalidDialog!!.setCancelable(false)
            invalidDialog!!.show()
        }
    }

    override fun showVideoProgress() {
     //   RuthUtility.showLoader(this@LiveVideoPlayerActivity)
    }

    override fun hideVideoProgress() {
        RuthUtility.hideLoader()
    }

    override fun setUserProfile(data: UserProfileData) {
        channelBroadcasterData = data
        Glide.with(this).load(data.photoUrl)
                .apply(RequestOptions().error(R.drawable.logoeye).placeholder(R.drawable.logoeye))
                .into(profile_image)
        when {
            !data.nickname.isEmpty() -> broadcasterName.text = data.nickname
            else -> broadcasterName.text = data.userName
        }

        if (data.isFollow) {
            ivLiveFollowUnfollow.setImageResource(R.drawable.icon_follow)
        } else {
            ivLiveFollowUnfollow.setImageResource(R.drawable.icon_follow_button)
        }

        tvGiftCount.text = data.giftCount.toString()
        broadcasterFans.text = data.followers.toString() + " " + getString(R.string.followers)
    }

    /*--------------------------------------------------------------------------------------------*/
    private fun setupTabLayout() {

        stickerItemList.clear()
        allStickers?.clear()
        when {
            STICKER_TYPE.equals("backpack", true) -> {
                stickerItemList.add(0, PositiveNegative(getString(R.string.all), allStickers))
            }

            STICKER_TYPE.equals("positive", true) -> {
                positiveSticker?.let { stickerItemList.addAll(it) }
                for (i in 0 until stickerItemList.size) {
                    allStickers!!.addAll(stickerItemList[i].stickersData!!)
                }
                stickerItemList.add(0, PositiveNegative(getString(R.string.all), allStickers))
            }
            STICKER_TYPE.equals("specificOrder", true) -> {
                specificOrder?.let { stickerItemList.addAll(it) }
                for (i in 0 until stickerItemList.size) {
                    allStickers!!.addAll(stickerItemList[i].stickersData!!)
                }
                stickerItemList.add(0, PositiveNegative(getString(R.string.all), allStickers))
            }
            STICKER_TYPE.equals("negative", true) -> {
                negativeSticker?.let { stickerItemList.addAll(it) }
                for (i in 0 until stickerItemList.size) {
                    allStickers!!.addAll(stickerItemList[i].stickersData!!)
                }
                stickerItemList.add(0, PositiveNegative(getString(R.string.all), allStickers))
            }
        }

        favoriteList?.clear()
        for (i in 0 until stickerItemList.size) {
            stickerItemList[i].stickersData?.let { favoriteList!!.addAll(it) }
        }

        favoritePagerAdapter = StickerPagerAdapter(supportFragmentManager, stickerItemList)
        rvComment!!.adapter = favoritePagerAdapter
        tabs.setupWithViewPager(rvComment)

        for (i in 0 until stickerItemList.size) {
            tabs.getTabAt(i)?.text = stickerItemList[i]._id
        }

        tabs.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                SUB_STICKER_TYPE = tab.position
            }
        })
    }

    private val updatePoints = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            setData()
            presenter.getUsersGifts(BROADCAST_ID)
            totalGiftCount += intent!!.getIntExtra("broadcastersGiftCount", 0)
            tvGiftCount.text = intent.getIntExtra("broadcastersGiftCount", 0).toString()
        }
    }

    private fun setData() {
        tvGoldCoins.text = RuthUtility.getCounts(HomeActivity.goldPoints)
        tvSilverCoins.text = RuthUtility.getCounts(HomeActivity.silverPoints)
        tvYuanCoins.text = RuthUtility.getCounts(HomeActivity.yuvanPoints)
    }

/*--------------------------------------------POPUPS------------------------------------------*/

    @SuppressLint("SetTextI18n")
    private fun showUserProfile() {

        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_new_user_layout, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation
        b.window.setBackgroundDrawableResource(R.color.transparent)

        b.setCancelable(true)
        b.show()

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
        layoutParams.gravity = Gravity.CENTER_VERTICAL
        b.window.attributes = layoutParams

        val back = dialogView.findViewById<View>(R.id.img_back) as ImageView
        val ivProfileImage = dialogView.findViewById<View>(R.id.ivProfileImage) as CircleImageView
        val tvUserLevel = dialogView.findViewById<View>(R.id.tvUserLevel) as TextView
        val tvNickName = dialogView.findViewById<View>(R.id.tvNickName) as TextView
        val tvUsername = dialogView.findViewById<View>(R.id.tvUsername) as TextView
        val tvAboutUser = dialogView.findViewById<View>(R.id.tvAboutUser) as TextView
        val tvFansCount = dialogView.findViewById<View>(R.id.tvFansCount) as TextView
        val btnFollowUser = dialogView.findViewById<View>(R.id.layoutFollowUser) as RelativeLayout
        val tvFollowUser = dialogView.findViewById<View>(R.id.btnFollowUser) as TextView
        val btnSeeUserProfile = dialogView.findViewById<View>(R.id.btnSeeUserProfile) as TextView
        val btnMessage = dialogView.findViewById<View>(R.id.btnMessage) as TextView

        if (channelBroadcasterData!!.isFollow) {
            tvFollowUser.text = this.getString(R.string.unfollow) + " " + channelBroadcasterData!!.userName
        } else {
            tvFollowUser.text = this.getString(R.string.follow) + " " + channelBroadcasterData!!.userName
        }

        back.setOnClickListener {
            b.dismiss()
        }

        btnSeeUserProfile.setOnClickListener {
            b.dismiss()
        }

        btnSeeUserProfile.setOnClickListener {
            startActivity(Intent(this, OtherUserProfileActivity::class.java)
                    .putExtra("otherUserId", intent.getStringExtra("broadcasterId")))
            finish()
        }

        btnFollowUser.setOnClickListener {
            val isFollowed: Boolean = !channelBroadcasterData!!.isFollow
            var followerCount = channelBroadcasterData!!.followers

            if (isFollowed) {
                followerCount += 1
                tvFollowUser.text = this.getString(R.string.unfollow) + " " + channelBroadcasterData!!.userName
                ivLiveFollowUnfollow.setImageResource(R.drawable.icon_follow)
            } else {
                if (followerCount > 0) {
                    followerCount -= 1
                } else {
                    followerCount = 0
                }
                tvFollowUser.text = this.getString(R.string.follow) + " " + channelBroadcasterData!!.userName
                ivLiveFollowUnfollow.setImageResource(R.drawable.icon_follow_button)
            }
            channelBroadcasterData!!.isFollow = !channelBroadcasterData!!.isFollow
            channelBroadcasterData!!.followers = followerCount
            tvFansCount.text = RuthUtility.getCounts(followerCount)
            broadcasterFans.text = followerCount.toString() + " " + getString(R.string.followers)
            updateFollowerList(isFollowed, followerCount)
            presenter.followUnfollow(intent.getStringExtra("broadcasterId"), !isFollowed)
        }

        tvNickName.text = channelBroadcasterData!!.nickname
        tvUsername.text = "@" + channelBroadcasterData!!.userName
        tvAboutUser.text = channelBroadcasterData!!.about
        tvFansCount.text = RuthUtility.getCounts(channelBroadcasterData!!.followers)
        tvUserLevel.text = RuthUtility.getCounts(channelBroadcasterData!!.level)
        Glide.with(this).load(channelBroadcasterData!!.photoUrl)
                .apply(RequestOptions().error(R.drawable.logoeye).placeholder(R.drawable.logoeye))
                .into(ivProfileImage)
    }

    override fun updateViewers(viewerList: ArrayList<ViewerData>) {
        usersViewersList = viewerList
        tvViewCount.text = RuthUtility.getCounts(viewerList.size)
    }

    override fun updateStreamingInfo(data: StreamingInfo?) {
        hideVideoProgress()
        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_broadcast_end, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val btnConfirmEnd = dialogView.findViewById<View>(R.id.btnConfirmEnd) as Button
        val tvGiftsPresented = dialogView.findViewById<View>(R.id.tvGiftsPresented) as TextView
        val tvLikes = dialogView.findViewById<View>(R.id.tvLikes) as TextView
        val tvTimePresent = dialogView.findViewById<View>(R.id.tvTimePresent) as TextView
        val tvOrdersPresented = dialogView.findViewById<View>(R.id.tvOrdersPresented) as TextView

        tvGiftsPresented.text = RuthUtility.getCounts(data!!.giftCount)
        tvLikes.text = RuthUtility.getCounts(data.likes)
        if (data.spendingTime != null) {
            tvTimePresent.text = RuthUtility.printDifference(data.spendingTime.viewStartTime,
                    data.spendingTime.viewEndTime, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").replace("-", "")
        }
        tvOrdersPresented.text = RuthUtility.getCounts(data.customOrder)

        btnConfirmEnd.setOnClickListener {
            hideVideoProgress()
            b.dismiss()
            if (lytOptionSelector.visibility == View.VISIBLE) {
            } else {
                lytOptionSelector.visibility = View.VISIBLE
                chatList.visibility = GONE
                lytSticker.visibility = GONE
                hideKeyboard()
            }
            leave()
            if (intent.getStringExtra("broadcastId") != null) {
                sendViewer(intent.getStringExtra("broadcastId"),
                        preferences.getUserInfo().id, false, false)
            }
            startActivity(Intent(this, VIPActivity::class.java))
            finish()
        }

        b.setCancelable(false)
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val displayWidth = displayMetrics.widthPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        layoutParams.width = (displayWidth * 0.8f).toInt()
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
        layoutParams.gravity = Gravity.CENTER_VERTICAL
        b.window.attributes = layoutParams

        if (!isFinishing) {
            hideVideoProgress()
            b.show()
        }
    }

    override fun showNoInternetDialog() {
//        Toast.makeText(this, "connection lost", Toast.LENGTH_SHORT).show()
        Log.d("check", "1")
        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_internet_connection, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val btnOk = dialogView.findViewById<View>(R.id.btnOk) as TextView

        btnOk.setOnClickListener {
            leave()
            if (intent.getStringExtra("broadcastId") != null) {
                sendViewer(intent.getStringExtra("broadcastId"),
                        preferences.getUserInfo().id, false, false)
            }
            b.dismiss()
            finish()
        }

        b.setCancelable(false)
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val displayWidth = displayMetrics.widthPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.9f).toInt()
        layoutParams.width = dialogWindowWidth
        b.window.attributes = layoutParams
        b.window.setGravity(Gravity.CENTER_VERTICAL)
        if (!isFinishing) {
            b.show()
        }
    }

    override fun updateOrders(position: Int) {
        customOrderList!!.removeAt(position)
        givenCustomOrdersAdapter!!.notifyDataSetChanged()
    }

    /*--------------------------------------------------------------------------------------------*/

    private fun updateFollowerList(isFollowed: Boolean, followerCount: Int) {
        BroadcastDetailActivity.liveUpcomingBroadcastList!![intent.getIntExtra("position", 0)].broadcasterData.isFollow = isFollowed
        BroadcastDetailActivity.liveUpcomingBroadcastList!![intent.getIntExtra("position", 0)].broadcasterData.followers = followerCount

        /*-----------------------------------Updating past list-----------------------------------*/
        for (i in 0 until BroadcastDetailActivity.pastBroadcastList!!.size) {
            if (BroadcastDetailActivity.pastBroadcastList!![i].broadcasterData.id == BroadcastDetailActivity.liveUpcomingBroadcastList!![intent.getIntExtra("position", 0)].broadcasterData.id) {
                BroadcastDetailActivity.pastBroadcastList!![i].broadcasterData.isFollow = isFollowed
                BroadcastDetailActivity.pastBroadcastList!![i].broadcasterData.followers = followerCount
            }
        }

        /*---------------------------------Updating live list-------------------------------------*/
        if (BroadcastDetailActivity.liveUpcomingBroadcastList!!.isNotEmpty()) {
            for (i in 0 until BroadcastDetailActivity.liveUpcomingBroadcastList!!.size) {
                if (BroadcastDetailActivity.liveUpcomingBroadcastList!![i].broadcasterData.id == BroadcastDetailActivity.liveUpcomingBroadcastList!![intent.getIntExtra("position", 0)].broadcasterData.id) {
                    BroadcastDetailActivity.liveUpcomingBroadcastList!![i].broadcasterData.isFollow = isFollowed
                    BroadcastDetailActivity.liveUpcomingBroadcastList!![i].broadcasterData.followers = followerCount
                }
            }
        }
    }

    override fun updateGifts(giftsList: ArrayList<UserGiftData>) {
        usersGiftList = giftsList
        tvGiftCount.text = RuthUtility.getCounts(giftsList.size)
    }

    private fun customTextView(view: TextView) {
        val terms = getString(R.string.termsConditions)
        val spanTxt = SpannableStringBuilder(terms)
        spanTxt.setSpan(UnderlineSpan(), terms.indexOf(terms), terms.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spanTxt.setSpan(ForegroundColorSpan(ContextCompat.getColor(this, R.color.terms_textColor)),
                terms.indexOf(terms), spanTxt.length, 0)
        view.movementMethod = LinkMovementMethod.getInstance()
        view.setText(spanTxt, TextView.BufferType.SPANNABLE)
    }

    private fun openTermsConditions() {
        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_terms_conditions, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val closeTermsCondition = dialogView.findViewById<View>(R.id.ivCloseTermsConditions) as ImageView

        closeTermsCondition.setOnClickListener {
            b.dismiss()
        }
        b.setCancelable(false)
        b.show()

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)

        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.9f).toInt()
        val dialogWindowHeight = (displayHeight * 0.7f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        b.window.attributes = layoutParams
    }

    private fun resetButtonsBackground() {
        bgChat.setColorFilter(ContextCompat.getColor(this, R.color.green))
        bgLiveGift.setColorFilter(ContextCompat.getColor(this, R.color.green))
        bgOrderList.setColorFilter(ContextCompat.getColor(this, R.color.green))
        bgLike.setColorFilter(ContextCompat.getColor(this, R.color.green))
        bgEmojis.setColorFilter(ContextCompat.getColor(this, R.color.green))
    }

    /*-------------------------------------FILTER LIST--------------------------------------------*/
    private fun setUpFilterList() {
        broadcastFilter?.clear()
        broadcastFilter?.add(BroadcastFilter("1", "None",
                ContextCompat.getDrawable(this@LiveVideoPlayerActivity,
                        R.drawable.filter_none),
                ContextCompat.getDrawable(this@LiveVideoPlayerActivity, R.drawable.transparent_bg)))
        broadcastFilter?.add(BroadcastFilter("2", "Filter 1",
                ContextCompat.getDrawable(this@LiveVideoPlayerActivity,
                        R.drawable.filter_1),
                ContextCompat.getDrawable(this@LiveVideoPlayerActivity, R.drawable.filter_image_purple)))
        broadcastFilter?.add(BroadcastFilter("3", "Filter 2",
                ContextCompat.getDrawable(this@LiveVideoPlayerActivity,
                        R.drawable.filter_2),
                ContextCompat.getDrawable(this@LiveVideoPlayerActivity, R.drawable.filter_image_green)))
        broadcastFilter?.add(BroadcastFilter("4", "Filter 3",
                ContextCompat.getDrawable(this@LiveVideoPlayerActivity,
                        R.drawable.filter_3),
                ContextCompat.getDrawable(this@LiveVideoPlayerActivity, R.drawable.filter_image_red)))
    }

    /*--------------------------------------------------------------------------------------------*/
    private fun checkInternetConnection() {
        Handler().postDelayed({
            if (utility.checkInternetWithoutDialog(this)) {
                Log.d("internet", " net")
                checkInternetConnection()
            } else {
                Log.d("internet", " no net")
                releasePlayer()
                clearResumePosition()
                showNoInternetDialog()
            }
        }, 3000)
    }
}

//private fun openTemp() {
//    val dialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
//    val inflater = this.layoutInflater
//    val dialogView = inflater.inflate(R.layout.popup_new_user_layout, null)
//    dialogBuilder.setView(dialogView)
//    val b = dialogBuilder.create()
//    val back = dialogView.findViewById<View>(R.id.img_back) as ImageView
//
//    back.setOnClickListener {
//        b.dismiss()
//    }
//
//    b.setCancelable(false)
//    b.show()
//
//    val displayMetrics = DisplayMetrics()
//    windowManager.defaultDisplay.getMetrics(displayMetrics)
//    b.window.setBackgroundDrawableResource(R.color.transparent)
//
//    val displayWidth = displayMetrics.widthPixels
//    val layoutParams = WindowManager.LayoutParams()
//    layoutParams.copyFrom(b.window.attributes)
//
//    layoutParams.width = (displayWidth * 0.7f).toInt()
//    layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
//    layoutParams.gravity = Gravity.CENTER_VERTICAL
//    b.window.attributes = layoutParams
//}

//private fun userProfileNextPage() {
//    val dialog = Dialog(this, R.style.Theme_Dialog)
//    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//    dialog.window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
//    dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//    dialog.setContentView(R.layout.popup_user_profile_second)
//
//    val ivProfileImage = dialog.findViewById<View>(R.id.ivProfileImage) as CircleImageView
//    val tvNickName = dialog.findViewById<View>(R.id.tvNickName) as TextView
//    val tvUsername = dialog.findViewById<View>(R.id.tvUsername) as TextView
//    val tvAboutUser = dialog.findViewById<View>(R.id.tvAboutUser) as TextView
//    val tvFansCount = dialog.findViewById<View>(R.id.tvFansCount) as TextView
//
//    tvNickName.text = channelBroadcasterData!!.nickname
//    tvUsername.text = "@" + channelBroadcasterData!!.userName
//    tvAboutUser.text = channelBroadcasterData!!.about
//    tvFansCount.text = RuthUtility.getCounts(channelBroadcasterData!!.followers)
//    Glide.with(this).load(channelBroadcasterData!!.photoUrl)
//            .apply(RequestOptions().error(R.drawable.logoeye).placeholder(R.drawable.logoeye))
//            .into(ivProfileImage)
//
//    dialog.findViewById<View>(R.id.btn_chat).setOnClickListener {
//        dialog.dismiss()
//    }
//    dialog.findViewById<View>(R.id.btn_done).setOnClickListener {
//        dialog.dismiss()
//    }
//    dialog.show()
//}




