package com.ruth.app.livestreaming.model

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class StickerInt(var stickerName: String?,
                 var img: Int?) : Parcelable
