package com.ruth.app.livestreaming.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.fragment.BaseFragment
import com.ruth.app.home.presenter.FavoriteListPresenter
import com.ruth.app.livestreaming.activity.EnterBroadcastingDetailsActivity
import com.ruth.app.livestreaming.adapter.FilterAdapter
import com.ruth.app.livestreaming.model.BroadcastFilter
import com.ruth.app.utility.withArguments
import kotlinx.android.synthetic.main.fragment_list_filter.view.*

class FilterListFragment : BaseFragment<FavoriteListPresenter>(), FavoriteListPresenter.FavoriteFragmentView {

   // private val broadcastFilter: ArrayList<BroadcastFilter>? by lazy { arguments?.getParcelableArrayList<BroadcastFilter>(FILTERS) }
    private var filterAdapter: FilterAdapter? = null

    companion object {
        const val FILTERS = "FILTERS"
    }

//    fun newInstance(data: ArrayList<BroadcastFilter>): FilterListFragment {
//        val args = Bundle()
//        args.putParcelableArrayList(FILTERS, data)
//        return FilterListFragment().withArguments(args)
//    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_list_filter, container, false)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        view.rvFilter.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        filterAdapter = FilterAdapter(this.activity!!, EnterBroadcastingDetailsActivity.broadcastFilter)
        view.rvFilter!!.adapter = filterAdapter
        filterAdapter!!.setOnItemClickListener(object : FilterAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                LocalBroadcastManager.getInstance(activity!!).sendBroadcast(Intent("updateFilter")
                        .putExtra("filterIcon", position))
            }
        })

        return view
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            fragmentManager!!.beginTransaction().detach(this).attach(this).commit()
        }
    }

}