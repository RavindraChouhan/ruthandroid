package com.ruth.app.livestreaming.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.ruth.app.R
import com.ruth.app.model.InvitedFansData
import kotlinx.android.synthetic.main.item_streaming_invitation.view.*

class UsersInvitationAdapter(val mContext: Context, private var invitedFansList: ArrayList<InvitedFansData>?)
    : RecyclerView.Adapter<UsersInvitationAdapter.ViewHolder>() {

    private var onItemClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(mContext)
        val view = inflater.inflate(R.layout.item_streaming_invitation, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return invitedFansList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(invitedFansList!![position], mContext)

        holder.itemView.ivInvite.setOnClickListener {
            if (invitedFansList!![position].isInvite) {
                holder.itemView.ivInvited.visibility = View.VISIBLE
                holder.itemView.ivInvite.visibility = View.GONE
            } else {
                holder.itemView.ivInvited.visibility = View.GONE
                holder.itemView.ivInvite.visibility = View.VISIBLE
            }
            onItemClickListener!!.onItemClick(holder.itemView, position)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(invitedFansData: InvitedFansData, mContext: Context) {

            Glide.with(mContext).load(invitedFansData.photoUrl)
                    .apply(RequestOptions().error(R.drawable.com_facebook_profile_picture_blank_square)
                            .placeholder(R.drawable.com_facebook_profile_picture_blank_square))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            itemView.progressBarImageLoading.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            itemView.progressBarImageLoading.visibility = View.GONE
                            return false
                        }

                    })
                    .into(itemView.ivProfileImage)

            itemView.tvUserName.text = invitedFansData.name
            if (invitedFansData.isLiveStatus) {
                itemView.tvUsersStatus.text = mContext.getString(R.string.online)
            } else {
                itemView.tvUsersStatus.text = mContext.getString(R.string.offline)
            }

            if (invitedFansData.isLiveStatus) {
                if (invitedFansData.isInvite) {
                    itemView.ivInvited.visibility = View.VISIBLE
                    itemView.ivInvite.visibility = View.GONE
                } else {
                    itemView.ivInvited.visibility = View.GONE
                    itemView.ivInvite.visibility = View.VISIBLE
                }
            } else {
                itemView.ivInvited.visibility = View.GONE
                itemView.ivInvite.visibility = View.GONE
            }
        }
    }

    fun setOnItemClickListener(onItemClick: OnItemClickListener) {
        this.onItemClickListener = onItemClick
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}
