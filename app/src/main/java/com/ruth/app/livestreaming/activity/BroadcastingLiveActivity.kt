//package com.ruth.app.livestreaming.activity
//
//import android.Manifest
//import android.annotation.SuppressLint
//import android.content.ActivityNotFoundException
//import android.content.ComponentName
//import android.content.Intent
//import android.content.ServiceConnection
//import android.content.res.Configuration
//import android.hardware.Camera
//import android.net.Uri
//import android.opengl.GLSurfaceView
//import android.os.*
//import android.support.v4.app.ActivityCompat
//import android.support.v4.widget.ContentLoadingProgressBar
//import android.support.v7.app.AlertDialog
//import android.support.v7.widget.LinearLayoutManager
//import android.text.TextUtils
//import android.util.Log
//import android.view.View
//import android.view.WindowManager
//import android.view.animation.AccelerateInterpolator
//import android.view.inputmethod.EditorInfo
//import android.widget.Button
//import android.widget.TextView
//import com.livebroadcasting.ILiveVideoBroadcaster
//import com.livebroadcasting.LiveVideoBroadcaster
//import com.ruth.app.App
//import com.ruth.app.BuildConfig
//import com.ruth.app.R
//import com.ruth.app.base.activity.BaseActivity
//import com.ruth.app.livestreaming.adapter.ChatAdapter
//import com.ruth.app.livestreaming.presenter.BroadcastingLivePresenter
//import com.ruth.app.login.activity.LoginActivity
//import com.ruth.app.model.InvitedFansData
//import com.ruth.app.model.UserGiftData
//import com.ruth.app.model.ViewerData
//import com.ruth.app.utility.RuthUtility
//import com.ruth.app.widgets.CustomDialog
//import com.ruth.app.widgets.emojianim.ParticleSystem
//import io.socket.client.Socket
//import io.socket.emitter.Emitter
//import kotlinx.android.synthetic.main.activity_live_video_broadcaster.*
//import kotlinx.android.synthetic.main.view_chat_broadcast.*
//import org.json.JSONException
//import org.json.JSONObject
//import java.util.*
//
//class BroadcastingLiveActivity : BaseActivity<BroadcastingLivePresenter>(), BroadcastingLivePresenter.LiveVideoBroadcastingView, View.OnClickListener {
//
//    internal var mIsRecording = false
//    private var mTimer: Timer? = null
//    private var mElapsedTime: Long = 0
//    private var mTimerHandler: TimerHandler? = null
//    private var mLiveVideoBroadcasterServiceIntent: Intent? = null
//    private var mGLView: GLSurfaceView? = null
//    private var mLiveVideoBroadcaster: ILiveVideoBroadcaster? = null
//    private var mBroadcastControlButton: Button? = null
//
//    //chat messages
//    private val mMessages = ArrayList<com.ruth.app.livestreaming.model.Message>()
//    private var adapter: ChatAdapter? = null
//    private lateinit var dialogs: CustomDialog
//
//    companion object {
//        internal const val CONNECTION_LOST = 2
//        internal const val INCREASE_TIMER = 1
//    }
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        // Hide title
//        //requestWindowFeature(Window.FEATURE_NO_TITLE);
//        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
//        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
//        //binding on resume not to having leaked service connection
//        mLiveVideoBroadcasterServiceIntent = Intent(this, LiveVideoBroadcaster::class.java)
//        //this makes service do its job until done
//        startService(mLiveVideoBroadcasterServiceIntent)
//
//        App.component.inject(this)
//        presenter.onCreate(this)
//
//        setContentView(R.layout.activity_live_video_broadcaster)
//        mTimerHandler = TimerHandler()
//        mBroadcastControlButton = findViewById(R.id.toggle_broadcasting)
//        // Configure the GLSurfaceView. This will start the Renderer thread, with an
//        // appropriate EGL activity.
//        mGLView = findViewById<GLSurfaceView>(R.id.cameraPreview_surfaceView)
//        if (mGLView != null) {
//            mGLView!!.setEGLContextClientVersion(2) // select GLES 2.0
//        }
//
//        setUpSocket()
//
//        rvMessagesList.layoutManager = LinearLayoutManager(this)
//        adapter = ChatAdapter(this, mMessages)
//        rvMessagesList.adapter = adapter
//
//        etChatMessage.setOnEditorActionListener(TextView.OnEditorActionListener { v, id, event ->
//            if (id == R.id.send || id == EditorInfo.IME_NULL) {
//                com.ruth.app.network.hideKeyboard(v)
//                attemptSend()
//                return@OnEditorActionListener true
//            }
//            false
//        })
//
//        btnSendMessage.setOnClickListener {
//            hideKeyboard()
//            attemptSend()
//        }
//        ivStopBroadcasting.setOnClickListener(this)
//        dialogs = CustomDialog(this)
//    }
//
//    override fun onClick(v: View?) {
//        if (v?.id == R.id.ivStopBroadcasting) {
//            stopBroadcastingLive()
//        }
//    }
//
//    override fun onStart() {
//        super.onStart()
//        //this lets activity bind
//        try {
//            bindService(mLiveVideoBroadcasterServiceIntent, mConnection, 0)
//        } catch (ex: Exception) {
//            ex.printStackTrace()
//        }
//    }
//
//    override fun onPause() {
//        //hide dialog if visible not to create leaked window exception
//        mLiveVideoBroadcaster!!.pause()
////        Log.d("pause", " --------")
//        super.onPause()
//    }
//
//
//    override fun onStop() {
//        unbindService(mConnection)
////        Log.d("onStop", " --------")
//        super.onStop()
//    }
//
//    override fun onDestroy() {
//        triggerStopRecording()
////        Log.d("destroy", " ------")
//        super.onDestroy()
//    }
//
//    override fun onBackPressed() {
//        stopBroadcastingLive()
//        leave()
//        //  super.onBackPressed()
//    }
//
//
//    /*---------------------------------------START LIVE BROADCASTING------------------------------*/
//    fun changeCamera(v: View) {
//        if (mLiveVideoBroadcaster != null) {
//            mLiveVideoBroadcaster!!.changeCamera()
//        }
//    }
//
//    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
//        when (requestCode) {
//            LiveVideoBroadcaster.PERMISSIONS_REQUEST -> {
//                if (mLiveVideoBroadcaster!!.isPermissionGranted) {
//                    mLiveVideoBroadcaster!!.openCamera(Camera.CameraInfo.CAMERA_FACING_BACK)
//                } else {
//                    if ((ActivityCompat.shouldShowRequestPermissionRationale(this,
//                                    Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(this,
//                                    Manifest.permission.RECORD_AUDIO))) {
//                        mLiveVideoBroadcaster!!.requestPermission()
//                    } else {
//                        AlertDialog.Builder(this@BroadcastingLiveActivity)
//                                .setTitle(R.string.permission)
//                                .setMessage(getString(R.string.app_doesnot_work_without_permissions))
//                                .setPositiveButton(android.R.string.yes) { dialog, which ->
//                                    try {
//                                        //Open the specific App Info page:
//                                        val intent = Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
//                                        intent.data = Uri.parse("package:" + applicationContext.packageName)
//                                        startActivity(intent)
//                                    } catch (e: ActivityNotFoundException) {
//                                        //e.printStackTrace();
//                                        //Open the generic Apps page:
//                                        val intent = Intent(android.provider.Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS)
//                                        startActivity(intent)
//                                    }
//                                }
//                                .show()
//                    }
//                }
//                return
//            }
//        }
//    }
//
//    override fun onConfigurationChanged(newConfig: Configuration) {
//        super.onConfigurationChanged(newConfig)
//        if (newConfig.orientation === Configuration.ORIENTATION_LANDSCAPE ||
//                newConfig.orientation === Configuration.ORIENTATION_PORTRAIT) {
//            mLiveVideoBroadcaster!!.setDisplayOrientation()
//        }
//    }
//
//    fun toggleBroadcasting(v: View) {
//        if (!mIsRecording) {
//            if (mLiveVideoBroadcaster != null) {
//                if (!mLiveVideoBroadcaster!!.isConnected) {
//                    val streamName: String
//                    if (!intent.getStringExtra("streamId").isEmpty()) {
//                        streamName = intent.getStringExtra("streamId")
//                    } else {
//                        streamName = "live_DefaultUsername"
//                    }
//                    Log.d("streamName", "->$streamName")
//
//                    object : AsyncTask<String, String, Boolean>() {
//                        lateinit var progressBar: ContentLoadingProgressBar
//                        override fun onPreExecute() {
//                            stopBroadcastingSocket(true)
//                            toggle_broadcasting.visibility = View.GONE
////                            progressBarStartBroadcast.visibility = View.VISIBLE
//                            if (!isFinishing && window.decorView.isShown) {
//                                dialogs.show()
//                            }
//
//                            presenter.updateLiveStatus(intent.getStringExtra("broadcastId"), true)
//                            progressBar = ContentLoadingProgressBar(this@BroadcastingLiveActivity)
//                            progressBar.show()
//                        }
//
//                        override fun doInBackground(vararg url: String): Boolean {
//                            return mLiveVideoBroadcaster!!.startBroadcasting(url[0])
//                        }
//
//                        override fun onPostExecute(result: Boolean) {
//                            progressBar.hide()
//                            mIsRecording = result
//                            if (result) {
////                                progressBarStartBroadcast.visibility = View.GONE
//
//                                if (!isFinishing && window.decorView.isShown) {
//                                    dialogs.dismiss()
//                                }
//                                mBroadcastControlButton!!.visibility = View.GONE
//                                mStreamLiveStatus.visibility = View.VISIBLE
//                                headerLive.visibility = View.VISIBLE
//                                ivStopBroadcasting.visibility = View.VISIBLE
//                                layoutChat.visibility = View.VISIBLE
////                                mBroadcastControlButton!!.setText(R.string.stop_broadcasting)
//                                startTimer()//start the recording duration
//                            } else {
//                                // Snackbar.make(mRootView, R.string.stream_not_started, Snackbar.LENGTH_LONG).show();
//                                triggerStopRecording()
//                            }
//                        }
//                    }.execute(BuildConfig.RTMP_BASE_URL + streamName)
//                } else {
//                    // Snackbar.make(mRootView, R.string.streaming_not_finished, Snackbar.LENGTH_LONG).show();
//                }
//            } else {
//                // Snackbar.make(mRootView, R.string.oopps_shouldnt_happen, Snackbar.LENGTH_LONG).show();
//            }
//        } else {
//            stopBroadcastingSocket(false)
//            triggerStopRecording()
//        }
//    }
//
//    private fun stopBroadcastingLive() {
//        val builder = android.app.AlertDialog.Builder(this)
//        builder.setTitle(getString(R.string.stop_broadcasting))
//        builder.setMessage(getString(R.string.stop_broadcasting_message))
//        builder.setPositiveButton(getString(R.string.yes)) { dialog, _ ->
//            dialog.dismiss()
//            stopBroadcastingSocket(false)
//            triggerStopRecording()
//        }
//        builder.setNegativeButton(getString(R.string.no)) { dialog, _ ->
//            dialog.dismiss()
//        }
//
//        if (!isFinishing && window.decorView.isShown) {
//            val dialog = builder.create()
//            dialog.setCancelable(false)
//            dialog.show()
//        }
//    }
//
//    fun triggerStopRecording() {
//        if (mIsRecording) {
//            stopTimer()
//            stopBroadcastingSocket(false)
//            mLiveVideoBroadcaster!!.stopBroadcasting()
//            presenter.updateLiveStatus(intent.getStringExtra("broadcastId"), false)
//            mBroadcastControlButton!!.setText(R.string.start_broadcasting)
//            mStreamLiveStatus.visibility = View.GONE
//            Handler().postDelayed((Runnable { finish() }), 1000)
//        }
//        mIsRecording = false
//    }
//
//    //This method starts a mTimer and updates the textview to show elapsed time for recording
//    fun startTimer() {
//        if (mTimer == null) {
//            mTimer = Timer()
//        }
//        mElapsedTime = 0
//        mTimer!!.scheduleAtFixedRate(object : TimerTask() {
//            override fun run() {
//                mElapsedTime += 1 //increase every sec
//                mTimerHandler!!.obtainMessage(INCREASE_TIMER).sendToTarget()
//                if (mLiveVideoBroadcaster == null || !mLiveVideoBroadcaster!!.isConnected) {
//                    mTimerHandler!!.obtainMessage(CONNECTION_LOST).sendToTarget()
//                }
//            }
//        }, 0, 1000)
//    }
//
//    private fun stopTimer() {
//        if (mTimer != null) {
//            this.mTimer!!.cancel()
//        }
//        this.mTimer = null
//        this.mElapsedTime = 0
//    }
//
//    private inner class TimerHandler : Handler() {
//        @SuppressLint("SetTextI18n")
//        override fun handleMessage(msg: Message) {
//            when (msg.what) {
//                INCREASE_TIMER -> mStreamLiveStatus.text = getString(R.string.live_indicator) + " - " + getDurationString(mElapsedTime.toInt())
//                CONNECTION_LOST -> {
//                    stopBroadcastingSocket(false)
//                    triggerStopRecording()
//                    AlertDialog.Builder(this@BroadcastingLiveActivity)
//                            .setMessage(R.string.broadcast_connection_lost)
//                            .setPositiveButton(android.R.string.yes, null)
//                            .show()
//                }
//            }
//        }
//    }
//
//    fun getDurationString(second: Int): String {
//        var seconds = second
//
//        if (seconds < 0 || seconds > 2000000)
//        //there is an codec problem and duration is not set correctly,so display meaningfull string
//            seconds = 0
//        val hours = seconds / 3600
//        val minutes = seconds % 3600 / 60
//        seconds %= 60
//
//        /*return if (hours == 0)
//            twoDigitString(minutes) + ":" + twoDigitString(seconds)
//        else
//            twoDigitString(hours) + ":" + twoDigitString(minutes) + ":" + twoDigitString(seconds)
//*/
//        return if (hours != 0) {
//            String.format("%02d:%02d:%02d", hours, minutes, seconds)
//        } else {
//            String.format("%02d:%02d", minutes, seconds)
//        }
//    }
//
//    private fun twoDigitString(number: Int): String {
//        if (number == 0) {
//            return "00"
//        }
//        if (number / 10 == 0) {
//            return "0$number"
//        }
//        return (number).toString()
//    }
//
//    /**
//     * Defines callbacks for service binding, passed to bindService()
//     */
//    private val mConnection = object : ServiceConnection {
//        override fun onServiceConnected(className: ComponentName,
//                                        service: IBinder) {
//            // We've bound to LocalService, cast the IBinder and get LocalService instance
//            val binder = service as LiveVideoBroadcaster.LocalBinder
//            if (mLiveVideoBroadcaster == null) {
//                mLiveVideoBroadcaster = binder.service
//                Log.d("view", "---------$mGLView")
//                mLiveVideoBroadcaster!!.init(this@BroadcastingLiveActivity, mGLView)
//                mLiveVideoBroadcaster!!.setAdaptiveStreaming(true)
//            }
//            mLiveVideoBroadcaster?.openCamera(Camera.CameraInfo.CAMERA_FACING_FRONT)
//        }
//
//        override fun onServiceDisconnected(arg0: ComponentName) {
//            mLiveVideoBroadcaster = null
//        }
//    }
//    /*---------------------------------------END LIVE BROADCASTING--------------------------------*/
//
//
//    /*---------------------------------------START OF SOCKET--------------------------------------*/
//    private var mSocket: Socket? = null
//    private var isConnected: Boolean? = true
//
//    private fun attemptSend() {
//
//        if (!mSocket!!.connected()) return
//
//        val message = etChatMessage.text.toString().trim({ it <= ' ' })
//        if (TextUtils.isEmpty(message)) {
//            etChatMessage.requestFocus()
//            return
//        }
//
//        etChatMessage.setText("")
//
//        val postData = JSONObject()
//        try {
//            postData.put("broadcastId", intent.getStringExtra("broadcastId"))
//            postData.put("userId", intent.getStringExtra("userId"))
//            postData.put("message", message)
//            postData.put("type", "comment")
//
//        } catch (e: JSONException) {
//            e.printStackTrace()
//        }
//
//        mSocket!!.emit("COMMENTS", postData)
//    }
//
//    private fun stopBroadcastingSocket(liveStatus: Boolean) {
//
//        if (!mSocket!!.connected()) return
//
//        val postData = JSONObject()
//        try {
//            postData.put("isLiveStatus", liveStatus)
//            postData.put("broadcasterUserId", preferences.getUserInfo().id)
//            postData.put("broadcastId", intent.getStringExtra("broadcastId"))
//            if (!liveStatus) {
//                if (tvStreamLiveViewers.text.isNotEmpty()) {
//                    postData.put("broadcastViewCount", tvStreamLiveViewers.text.toString())
//                }
//                postData.put("broadcastEndTime", mStreamLiveStatus.text.toString().replace("生活 - ", "").trim())
//            }
//        } catch (e: JSONException) {
//            e.printStackTrace()
//        }
//        Log.d("LIVE", "EMIT")
//        mSocket!!.emit("LIVE", postData)
//    }
//
//    private fun setUpSocket() {
//        val app = this.application as App
//        mSocket = app.socket
//        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
//        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
//        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
//        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
//        mSocket!!.on("RECEIVE_MESSAGE", onNewMessage)
//        mSocket!!.on("RECEIVE_LIKE", onNewLike)
//        mSocket!!.on("RECEIVE_VIEWS", onNewView)
//        mSocket!!.on("RECEIVE_STICKER", onNewSticker)
//        mSocket!!.on("RECEIVE_EMOJI", onNewEmoji)
//        mSocket!!.on("RECEIVE_ELITE", onGetEliteUser)
//        mSocket!!.on("RECEIVE_CUSTOM_ORDER", onReceiveGift)
//        mSocket!!.connect()
//    }
//
//    private val onConnect = Emitter.Listener {
//        this.runOnUiThread {
//            if (!isConnected!!) {
//                isConnected = true
//            }
//        }
//    }
//
//    private val onDisconnect = Emitter.Listener {
//        this.runOnUiThread {
//            Log.i("socket", "disconnected")
//            isConnected = false
//        }
//    }
//
//    private val onConnectError = Emitter.Listener {
//        this.runOnUiThread {
//            Log.e("socket", "Error connecting")
//        }
//    }
//
//    private val onNewMessage = Emitter.Listener { args ->
//        this.runOnUiThread(Runnable {
//            try {
//                val data = args[0] as JSONObject
//                val username: String
//                val message: String
//                val photoUrl: String
//                val broadcastId: String
//                try {
//                    username = data.getString("username")
//                    message = data.getString("message")
//                    photoUrl = data.getString("photoUrl")
//                    broadcastId = data.getString("broadcastId")
//                    if (broadcastId == intent.getStringExtra("broadcastId")) {
//                        addMessage(username, message, photoUrl, "chat")
//                    }
//                } catch (e: JSONException) {
//                    Log.e("socket", e.message)
//                    return@Runnable
//                }
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//        })
//    }
//
//    private val onReceiveGift = Emitter.Listener { args ->
//        this.runOnUiThread {
//            try {
//                val data = args[0] as JSONObject
//                val username: String
//                val message: String
//                val broadcastId: String
//                val photoUrl: String
//                try {
//                    username = data.getString("username")
//                    message = data.getString("message")
//                    photoUrl = data.getString("photoUrl")
//                    broadcastId = data.getString("broadcastId")
//
//                    if (broadcastId == intent.getStringExtra("broadcastId")) {
//                        addMessage(username, message, photoUrl, "customGift")
//                    }
//                } catch (e: JSONException) {
//                    Log.e(TAG, e.message)
//                    return@runOnUiThread
//                }
//
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//        }
//    }
//
//    private fun addMessage(username: String, message: String, photoUrl: String, messageType: String) {
//        mMessages.add(com.ruth.app.livestreaming.model.Message.Builder(com.ruth.app.livestreaming.model.Message.TYPE_MESSAGE)
//                .username(username)
//                .message(message)
//                .photoUrl(photoUrl).build())
//        try {
//            adapter!!.notifyChat(messageType, mMessages.size - 1)
//            //adapter!!.notifyItemInserted(mMessages.size - 1)
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        scrollToBottom(mMessages.size - 1)
//    }
//
//    private val onNewLike = Emitter.Listener { args ->
//        this.runOnUiThread(Runnable {
//            try {
//                val data = args[0] as JSONObject
//                Log.d("args", "-data :- $data")
//                val broadcastId: String
//                try {
//                    broadcastId = data.getString("broadcastId")
//                    if (broadcastId == intent.getStringExtra("broadcastId")) {
//                        val ps = ParticleSystem(this, 1, R.drawable.icon_like, 2000)
//                        ps.setScaleRange(1.3f, 1.3f)
//                        ps.setSpeedModuleAndAngleRange(0.07f, 0.20f, 270, 270)
//                        ps.setFadeOut(400, AccelerateInterpolator())
//                        ps.emit(lytLike, 1)
//                        Handler().postDelayed({
//                            ps.stopEmitting()
//                            ps.cancel()
//                        }, 2000)
//                    }
//                } catch (e: JSONException) {
//                    Log.e(TAG, e.message)
//                    return@Runnable
//                }
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//        })
//    }
//
//    private val onNewEmoji = Emitter.Listener { args ->
//        this.runOnUiThread(Runnable {
//            try {
//                val data = args[0] as JSONObject
//                Log.d("args", "-data :- $data")
//                val broadcastId: String
//                try {
//                    broadcastId = data.getString("broadcastId")
//                    if (broadcastId == intent.getStringExtra("broadcastId")) {
//                        val ps = ParticleSystem(this, 1, R.drawable.scary_emoji, 2000)
//                        ps.setScaleRange(1.3f, 1.3f)
//                        ps.setSpeedModuleAndAngleRange(0.07f, 0.20f, 270, 270)
//                        ps.setFadeOut(400, AccelerateInterpolator())
//                        ps.emit(lytLike, 1)
//                        Handler().postDelayed({
//                            ps.stopEmitting()
//                            ps.cancel()
//                        }, 2000)
//                    }
//                } catch (e: JSONException) {
//                    Log.e(TAG, e.message)
//                    return@Runnable
//                }
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//        })
//    }
//
//    private val onGetEliteUser = Emitter.Listener { args ->
//        this.runOnUiThread {
//            try {
//                val data = args[0] as JSONObject
//                Log.d("elite", " -> $data")
//
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//        }
//    }
//
//    private val onNewView = Emitter.Listener { args ->
//        this.runOnUiThread(Runnable {
//            try {
//                val data = args[0] as JSONObject
//                Log.d("args", "-data :- $data")
//                val broadcastId: String
//                val viewCount: Int
//                try {
//                    broadcastId = data.getString("broadcastId")
//                    if (broadcastId == intent.getStringExtra("broadcastId")) {
//                        viewCount = data.getInt("viewCount")
//                        Log.d("viewCount", "receive -> $viewCount")
//                        if (viewCount >= 1) {
//                            layoutLiveCount.visibility = android.view.View.VISIBLE
//                        } else {
//                            layoutLiveCount.visibility = android.view.View.GONE
//                        }
//                        tvStreamLiveViewers.text = RuthUtility.getCounts(viewCount)
//                    }
//
//                } catch (e: JSONException) {
//                    Log.e(TAG, e.message)
//                    return@Runnable
//                }
//
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//        })
//    }
//
//    private val onNewSticker = Emitter.Listener { args ->
//        this.runOnUiThread(Runnable {
//            try {
//                val data = args[0] as JSONObject
//                val broadcastId: String
//                try {
//                    broadcastId = data.getString("broadcastId")
//                    if (broadcastId == intent.getStringExtra("broadcastId")) {
//                        android.widget.Toast.makeText(this, "StickerReceived", android.widget.Toast.LENGTH_SHORT).show()
//                    }
//                } catch (e: JSONException) {
//                    Log.e("socket", e.message)
//                    return@Runnable
//                }
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//        })
//    }
//
//    private fun leave() {
//        mSocket!!.disconnect()
//        mSocket!!.connect()
//    }
//
//    /*---------------------------------------END OF SOCKET--------------------------------------*/
//
//    private fun scrollToBottom(position: Int) {
////        rvMessagesList.scrollToPosition(position)
//    }
//
//    /*--------------------------------------------------------------------------------------------*/
//    private var invalidDialog: android.app.AlertDialog? = null
//    private var usersGiftList: ArrayList<UserGiftData> = ArrayList()
//
//    override fun invalidLogin() {
//
//        val builder = android.app.AlertDialog.Builder(this)
//        builder.setTitle("Invalid Login")
//        builder.setMessage(getString(R.string.invalid_login_error))
//
//        // add a button
//        builder.setPositiveButton(getString(R.string.log_in)) { dialog, which ->
//            presenter.clearLogin()
//            startActivity(Intent(this, LoginActivity::class.java)
//                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
//            finish()
//            overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
//            dialog.dismiss()
//        }
//
//        if (!isFinishing && window.decorView.isShown) {
//            invalidDialog = builder.create()
//            invalidDialog!!.setCancelable(false)
//            invalidDialog!!.show()
//        }
//    }
//
//    override fun updateGifts(giftsList: ArrayList<UserGiftData>) {
//        usersGiftList = giftsList
//    }
//
//    private var usersViewersList: java.util.ArrayList<ViewerData> = java.util.ArrayList()
//    override fun updateViewers(viewerList: ArrayList<ViewerData>) {
//        usersViewersList = viewerList
//    }
//
//    private var invitedFansList: ArrayList<InvitedFansData>? = null
//    override fun updateFansData(data: ArrayList<InvitedFansData>?) {
//        invitedFansList!!.clear()
//        invitedFansList = data
//    }
//}