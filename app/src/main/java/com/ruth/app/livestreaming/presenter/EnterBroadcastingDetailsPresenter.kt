package com.ruth.app.livestreaming.presenter

import com.ruth.app.App
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.network.config.RuthApi
import java.util.*
import javax.inject.Inject

class EnterBroadcastingDetailsPresenter @Inject constructor(var ruthApi: RuthApi) : BasePresenter<EnterBroadcastingDetailsPresenter.EnterBroadcastingDetailsView>() {

    override fun onCreate(view: EnterBroadcastingDetailsView) {
        super.onCreate(view)
    }

    fun uploadBroadcastInfo(uploadedImageUrl: String, title: String, tag: String) {
        var uniqueId = UUID.randomUUID().toString()
        uniqueId = uniqueId.replace("-", "_")

        val hm = HashMap<String, @JvmSuppressWildcards Any>()
        hm["broadcasterId"] = preferences.getUserInfo().id
        hm["coverUrl"] = uploadedImageUrl
        hm["broadcastTitle"] = title
        hm["broadcastTag"] = tag
        hm["broadcastStreamId"] = "live_$uniqueId"

        ruthApi.uploadBroadcastInfo(hm)
                .subscribe {
                    if (it != null) {
                        if (it.status) {
                            view?.startBroadcasting("live_$uniqueId", it.data.id, preferences.getUserInfo().id)
                        } else {
                            //view?.hideProgressLoading()
                            view?.finishBroadcasting()
                            if (App.isChineseLanguage) {
                                view?.showToast(it.zhHant)
                            } else {
                                view?.showToast(it.message)
                            }
                        }
                    }
                }
    }

    interface EnterBroadcastingDetailsView : BaseView {
        fun startBroadcasting(streamName: String, broadcastId: String, userId: String)
        fun hideProgressLoading()
        fun finishBroadcasting()
    }
}