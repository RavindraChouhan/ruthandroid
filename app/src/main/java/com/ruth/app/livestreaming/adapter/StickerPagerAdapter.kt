package com.ruth.app.livestreaming.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.ruth.app.livestreaming.fragment.StickerFragment
import com.ruth.app.model.PositiveNegative

class StickerPagerAdapter(fm: FragmentManager?, var stickerList: ArrayList<PositiveNegative>) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return StickerFragment()
    }

    override fun getCount(): Int {
        return stickerList.size
    }

}