package com.ruth.app.livestreaming.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ruth.app.R
import com.ruth.app.livestreaming.model.Message
import kotlinx.android.synthetic.main.item_message_cell.view.*
import java.util.*

class ChatAdapter(var context: Context, var mMessages: ArrayList<Message>) : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {

    private var mUsernameColors: IntArray? = null
    private var messageT:String =""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var layout = -1
        when (viewType) {
            Message.TYPE_MESSAGE -> layout = R.layout.item_message_cell
        }
        val v = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        //val remind = position % 2
        if (messageT.equals("customGift",true)) {
            viewHolder.setLayoutBack(false)
        } else {
            viewHolder.setLayoutBack(true)
        }
        val message = mMessages[position]
        viewHolder.setMessage(message.message)
        viewHolder.setUsername(message.username, position)
        if (message.photoUrl.isNotEmpty()) {
            Glide.with(context).load(message.photoUrl)
                    .apply(RequestOptions().error(R.drawable.com_facebook_profile_picture_blank_square)
                            .placeholder(R.drawable.com_facebook_profile_picture_blank_square))
                    .into(viewHolder.itemView.ivChatUser)
        } else {
            viewHolder.itemView.ivChatUser.setImageDrawable(context.resources.getDrawable(R.drawable.com_facebook_profile_picture_blank_square))
        }
    }

    override fun getItemCount(): Int {
        return mMessages.size
    }

    override fun getItemViewType(position: Int): Int {
        return mMessages[position].type
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val mUsernameView: TextView?
        private val mMessageView: TextView?
        private val layoutChat: LinearLayout?

        init {

            mUsernameView = itemView.findViewById(R.id.username) as TextView
            mMessageView = itemView.findViewById(R.id.message) as TextView
            layoutChat = itemView.findViewById(R.id.lytChat) as LinearLayout

            mUsernameColors = context.resources.getIntArray(R.array.username_colors)
        }

        fun setUsername(username: String, position: Int) {
            if (null == mUsernameView) return
            mUsernameView.text = username
            mUsernameView.setTextColor(getUsernameColor(position))
        }

        fun setMessage(message: String) {
            if (null == mMessageView) return
            mMessageView.text = message
        }

        fun setLayoutBack(isOdd: Boolean) {
            if (isOdd) {
                layoutChat?.background = context.resources.getDrawable(R.drawable.black_transparent_button)
            } else {
                layoutChat?.background = context.resources.getDrawable(R.drawable.green_button)
            }
        }

        private fun getUsernameColor(position: Int): Int {
            val remind = position % 7
            return mUsernameColors!![remind]
        }
    }

    fun notifyChat(messageType: String, i: Int) {
        this.messageT = messageType
        notifyItemInserted(i)
    }
}