package com.ruth.app.livestreaming.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ruth.app.R
import com.ruth.app.model.LiveViewers
import kotlinx.android.synthetic.main.item_user.view.*

class LiveViewersAdapter(var context: Context, var liveViewersList: ArrayList<LiveViewers>) : RecyclerView.Adapter<LiveViewersAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_user, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(liveViewersList[position])

    }

    override fun getItemCount(): Int {
        return liveViewersList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(liveViewers: LiveViewers) {

            when {
                liveViewers.giftId.giftType.equals(context.getString(R.string.gold_points), true) -> {
                    itemView.ivPointType.setImageDrawable(context.getDrawable(R.drawable.icon_user_in_got_yellow))
                }
                liveViewers.giftId.giftType.equals(context.getString(R.string.silver_points), true) -> {
                    itemView.ivPointType.setImageDrawable(context.getDrawable(R.drawable.icon_user_in_got_silver))
                }
                liveViewers.giftId.giftType.equals(context.getString(R.string.coin_points), true) -> {
                    itemView.ivPointType.setImageDrawable(context.getDrawable(R.drawable.icon_user_yuan))
                }
            }

            Glide.with(context).load(liveViewers.userId.photoUrl)
                    .apply(RequestOptions().error(R.drawable.com_facebook_profile_picture_blank_square)
                            .placeholder(R.drawable.com_facebook_profile_picture_blank_square))
                    .into(itemView.imgUser)
        }
    }
}