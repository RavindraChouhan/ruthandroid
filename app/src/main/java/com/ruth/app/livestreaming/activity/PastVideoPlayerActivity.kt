package com.ruth.app.livestreaming.activity

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.View.OnClickListener
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer.DecoderInitializationException
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil.DecoderQueryException
import com.google.android.exoplayer2.source.*
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.MappingTrackSelector.MappedTrackInfo
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.PlaybackControlView
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.util.Util
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.broadcaster.activity.BroadcastDetailActivity
import com.ruth.app.home.model.ChannelBroadcasterData
import com.ruth.app.livestreaming.presenter.PastVideoPlayerPresenter
import com.ruth.app.login.activity.LoginActivity
import com.ruth.app.utility.RuthUtility
import com.ruth.app.utility.broadcastingstreaming.livevideoplayer.DefaultExtractorsFactoryForFLV
import com.ruth.app.utility.broadcastingstreaming.livevideoplayer.EventLogger
import com.ruth.app.utility.broadcastingstreaming.livevideoplayer.RtmpDataSource
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_past_video_player.*
import java.net.CookieHandler
import java.net.CookieManager
import java.net.CookiePolicy

class PastVideoPlayerActivity : BaseActivity<PastVideoPlayerPresenter>(),
        PastVideoPlayerPresenter.PastVideoPlayerView,
        OnClickListener, ExoPlayer.EventListener, PlaybackControlView.VisibilityListener {

    private var mainHandler: Handler? = null
    private var eventLogger: EventLogger? = null
    private var mediaDataSourceFactory: DataSource.Factory? = null
    private var player: SimpleExoPlayer? = null
    private var trackSelector: DefaultTrackSelector? = null
    private var needRetrySource: Boolean = false
    private var shouldAutoPlay: Boolean = false
    private var resumeWindow: Int = 0
    private var resumePosition: Long = 0
    private var rtmpDataSourceFactory: RtmpDataSource.RtmpDataSourceFactory? = null
    private var channelBroadcasterData: ChannelBroadcasterData? = null

    // Activity lifecycle
    private var userAgent: String? = null

    companion object {

        val PREFER_EXTENSION_DECODERS = "prefer_extension_decoders"
        private val BANDWIDTH_METER = DefaultBandwidthMeter()
        private val DEFAULT_COOKIE_MANAGER: CookieManager

        init {
            DEFAULT_COOKIE_MANAGER = CookieManager()
            DEFAULT_COOKIE_MANAGER.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER)
        }

        private fun isBehindLiveWindow(e: ExoPlaybackException): Boolean {
            if (e.type != ExoPlaybackException.TYPE_SOURCE) {
                return false
            }
            var cause: Throwable? = e.sourceException
            while (cause != null) {
                if (cause is BehindLiveWindowException) {
                    return true
                }
                cause = cause!!.cause
            }
            return false
        }
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userAgent = Util.getUserAgent(this, "ExoPlayerDemo")
        shouldAutoPlay = true
        clearResumePosition()
        mediaDataSourceFactory = buildDataSourceFactory(true)
        rtmpDataSourceFactory = RtmpDataSource.RtmpDataSourceFactory()
        mainHandler = Handler()
        if (CookieHandler.getDefault() !== DEFAULT_COOKIE_MANAGER) {
            CookieHandler.setDefault(DEFAULT_COOKIE_MANAGER)
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE)
//        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_past_video_player)
        App.component.inject(this)
        presenter.onCreate(this)
//        presenter.getBroadcasterDetail(intent.getStringExtra("broadcasterId"))

        channelBroadcasterData = BroadcastDetailActivity.pastBroadcastList!![intent.getIntExtra("position", 0)].broadcasterData
        setBroadcasterData(channelBroadcasterData!!)

        simpleExoPlayerView.setControllerVisibilityListener(this)
        simpleExoPlayerView.requestFocus()
        rootView.setOnClickListener(this)
        imgBack.setOnClickListener(this)
        lytUserProfile.setOnClickListener(this)
        ivPastFollowUnfollow.setOnClickListener(this)

        if (intent.getStringExtra("broadcasterId") == preferences.getUserInfo().id) {
            ivPastFollowUnfollow.visibility = View.GONE
        } else {
            ivPastFollowUnfollow.visibility = View.VISIBLE
        }

        play(null)

    }

    override fun onClick(v: View) {
        if (v.id == R.id.imgBack) {
            finish()
        }
        if (v.id == R.id.lytUserProfile) {
            userProfile()
        }
        if (v.id == R.id.btnSend) {
            //updateChat()
        }

        if (v.id == R.id.ivPastFollowUnfollow) {
            val isFollowed: Boolean = !BroadcastDetailActivity.pastBroadcastList!![intent.getIntExtra("position", 0)].broadcasterData.isFollow
            presenter.followUnfollow(intent.getStringExtra("broadcasterId"), isFollowed)
            var followerCount = BroadcastDetailActivity.pastBroadcastList!![intent.getIntExtra("position", 0)].broadcasterData.followers
            if (isFollowed) {
                followerCount += 1
                ivPastFollowUnfollow.setImageResource(R.drawable.icon_follow)
            } else {
                followerCount -= 1
                ivPastFollowUnfollow.setImageResource(R.drawable.icon_follow_button)
            }
            tvBroadcasterFans.text = followerCount.toString() + " " + getString(R.string.followers)
            BroadcastDetailActivity.pastBroadcastList!![intent.getIntExtra("position", 0)].broadcasterData.isFollow = isFollowed
            BroadcastDetailActivity.pastBroadcastList!![intent.getIntExtra("position", 0)].broadcasterData.followers = followerCount

            /*------------------------Updating past list-----------------------------*/
            if (BroadcastDetailActivity.pastBroadcastList!!.isNotEmpty()) {
                for (i in 0 until BroadcastDetailActivity.pastBroadcastList!!.size) {
                    if (BroadcastDetailActivity.pastBroadcastList!![i].broadcasterData.id == BroadcastDetailActivity.pastBroadcastList!![intent.getIntExtra("position", 0)].broadcasterData.id) {
                        BroadcastDetailActivity.pastBroadcastList!![i].broadcasterData.isFollow = isFollowed
                        BroadcastDetailActivity.pastBroadcastList!![i].broadcasterData.followers = followerCount
                    }
                }
            }

            /*------------------------Updating live and upcoming list-----------------------------*/
            if (BroadcastDetailActivity.liveUpcomingBroadcastList!!.isNotEmpty()) {
                for (i in 0 until BroadcastDetailActivity.liveUpcomingBroadcastList!!.size) {
                    if (BroadcastDetailActivity.liveUpcomingBroadcastList!![i].broadcasterData.id == BroadcastDetailActivity.pastBroadcastList!![intent.getIntExtra("position", 0)].broadcasterData.id) {
                        BroadcastDetailActivity.liveUpcomingBroadcastList!![i].broadcasterData.isFollow = isFollowed
                        BroadcastDetailActivity.liveUpcomingBroadcastList!![i].broadcasterData.followers = followerCount
                    }
                }
            }
        }
    }

    private fun setBroadcasterData(data: ChannelBroadcasterData) {
        Glide.with(this).load(data.photoUrl)
                .apply(RequestOptions().error(R.drawable.com_facebook_profile_picture_blank_square)
                        .placeholder(R.drawable.com_facebook_profile_picture_blank_square))
                .into(ivProfileImagePast)
        when {
            !data.name.isEmpty() -> tvBroadcasterName.text = data.name
            else -> tvBroadcasterName.text = data.userName
        }

        if (data.isFollow) {
            ivPastFollowUnfollow.setImageResource(R.drawable.icon_follow)
        } else {
            ivPastFollowUnfollow.setImageResource(R.drawable.icon_follow_button)
        }

        tvBroadcasterFans.text = data.followers.toString() + " " + getString(R.string.followers)
    }


    public override fun onPause() {
        super.onPause()
        if (Util.SDK_INT <= 23) {
            releasePlayer()
        }
    }

    public override fun onStop() {
        super.onStop()
        if (Util.SDK_INT > 23) {
            releasePlayer()
        }
    }

    /*---------------------------------------START LIVE STREAMING---------------------------------*/

    public override fun onNewIntent(intent: Intent) {
        releasePlayer()
        shouldAutoPlay = true
        clearResumePosition()
        setIntent(intent)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            play(null)
        } else {
            showToast(R.string.storage_permission_denied)
            finish()
        }
    }

    // Activity input

    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        // Show the controls on any key event.
        simpleExoPlayerView.hideController()
        // If the event was not handled then see if the player view can handle it as a media key event.
        return super.dispatchKeyEvent(event) || simpleExoPlayerView.dispatchMediaKeyEvent(event)
    }


    // PlaybackControlView.VisibilityListener implementation

    override fun onVisibilityChange(visibility: Int) {
        // debugRootView.setVisibility(visibility)
    }

    // Internal methods

    private fun initializePlayer(rtmpUrl: String) {
        val intent = intent
        val needNewPlayer = player == null
        if (needNewPlayer) {

            val preferExtensionDecoders = intent.getBooleanExtra(PREFER_EXTENSION_DECODERS, false)
            @SimpleExoPlayer.ExtensionRendererMode val extensionRendererMode = if (useExtensionRenderers())
                if (preferExtensionDecoders)
                    SimpleExoPlayer.EXTENSION_RENDERER_MODE_PREFER
                else
                    SimpleExoPlayer.EXTENSION_RENDERER_MODE_ON
            else
                SimpleExoPlayer.EXTENSION_RENDERER_MODE_OFF
            val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(BANDWIDTH_METER)
            trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)
            player = ExoPlayerFactory.newSimpleInstance(this, trackSelector!!, DefaultLoadControl(), null, extensionRendererMode)
            //   player = ExoPlayerFactory.newSimpleInstance(this, trackSelector,
            //           new DefaultLoadControl(new DefaultAllocator(true, C.DEFAULT_BUFFER_SEGMENT_SIZE),  500, 1500, 500, 1500),
            //           null, extensionRendererMode);
            player!!.addListener(this)

            eventLogger = EventLogger(trackSelector)
            player!!.addListener(eventLogger)
            player!!.setAudioDebugListener(eventLogger)
            player!!.setVideoDebugListener(eventLogger)
            player!!.setMetadataOutput(eventLogger)

            simpleExoPlayerView.setPlayer(player)
            player!!.playWhenReady = shouldAutoPlay
            // debugViewHelper = DebugTextViewHelper(player, debugTextView)
            //debugViewHelper!!.start()
        }
        if (needNewPlayer || needRetrySource) {
            //  String action = intent.getAction();
            val uris: Array<Uri?>
            val extensions: Array<String?>

            uris = arrayOfNulls(1)
            uris[0] = Uri.parse(rtmpUrl)
            extensions = arrayOfNulls(1)
            extensions[0] = ""
            if (Util.maybeRequestReadExternalStoragePermission(this, *uris)) {
                // The player will be reinitialized if the permission is granted.
                return
            }
            val mediaSources = arrayOfNulls<MediaSource?>(uris.size)
            for (i in uris.indices) {
                mediaSources[i] = buildMediaSource(uris[i], extensions[i])
            }
            val mediaSource = if (mediaSources.size == 1)
                mediaSources[0]
            else
                ConcatenatingMediaSource(*mediaSources)
            val haveResumePosition = resumeWindow != C.INDEX_UNSET
            if (haveResumePosition) {
                player!!.seekTo(resumeWindow, resumePosition)
            }
            player!!.prepare(mediaSource, !haveResumePosition, false)
            needRetrySource = false
        }
    }


    private fun buildMediaSource(uri: Uri?, overrideExtension: String?): MediaSource {
        val type = if (TextUtils.isEmpty(overrideExtension))
            Util.inferContentType(uri)
        else
            Util.inferContentType(".$overrideExtension")
        when (type) {
            C.TYPE_SS -> return SsMediaSource(uri, buildDataSourceFactory(false),
                    DefaultSsChunkSource.Factory(mediaDataSourceFactory), mainHandler, eventLogger)
            C.TYPE_DASH -> return DashMediaSource(uri, buildDataSourceFactory(false),
                    DefaultDashChunkSource.Factory(mediaDataSourceFactory), mainHandler, eventLogger)
            C.TYPE_HLS -> return HlsMediaSource(uri, mediaDataSourceFactory, mainHandler, eventLogger)
            C.TYPE_OTHER -> return if (uri!!.scheme == "rtmp") {
                ExtractorMediaSource(uri, rtmpDataSourceFactory, DefaultExtractorsFactoryForFLV(),
                        mainHandler, eventLogger)
            } else {
                ExtractorMediaSource(uri, mediaDataSourceFactory, DefaultExtractorsFactory(),
                        mainHandler, eventLogger)
            }
            else -> {
                throw IllegalStateException("Unsupported type: $type")
            }
        }
    }

    private fun releasePlayer() {
        if (player != null) {
            shouldAutoPlay = player!!.playWhenReady
            updateResumePosition()
            player!!.release()
            player = null
            trackSelector = null
            eventLogger = null
        }
    }

    private fun updateResumePosition() {
        resumeWindow = player!!.currentWindowIndex
        resumePosition = if (player!!.isCurrentWindowSeekable)
            Math.max(0, player!!.currentPosition)
        else
            C.TIME_UNSET
    }

    private fun clearResumePosition() {
        resumeWindow = C.INDEX_UNSET
        resumePosition = C.TIME_UNSET
    }

    /**
     * Returns a new DataSource factory.
     *
     * @param useBandwidthMeter Whether to set [.BANDWIDTH_METER] as a listener to the new
     * DataSource factory.
     * @return A new DataSource factory.
     */
    private fun buildDataSourceFactory(useBandwidthMeter: Boolean): DataSource.Factory {
        return buildDataSourceFactory(if (useBandwidthMeter) BANDWIDTH_METER else null)
    }

    /**
     * Returns a new HttpDataSource factory.
     *
     * @param useBandwidthMeter Whether to set [.BANDWIDTH_METER] as a listener to the new
     * DataSource factory.
     * @return A new HttpDataSource factory.
     */
    private fun buildHttpDataSourceFactory(useBandwidthMeter: Boolean): HttpDataSource.Factory {
        return buildHttpDataSourceFactory(if (useBandwidthMeter) BANDWIDTH_METER else null)
    }

    // ExoPlayer.EventListener implementation

    override fun onLoadingChanged(isLoading: Boolean) {
        Log.d("loading", "->$isLoading")
    }

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        if (playbackState == ExoPlayer.STATE_ENDED) {
            showControls()
        }
        if (playbackState == ExoPlayer.STATE_BUFFERING) {
//            RuthUtility.showLoader(this@PastVideoPlayerActivity)

//            progressBarVideoPlayer.visibility = View.VISIBLE
        }
        if (playbackState == ExoPlayer.STATE_IDLE) {
//            RuthUtility.showLoader(this@PastVideoPlayerActivity)

//            progressBarVideoPlayer.visibility = View.VISIBLE
        }
        if (playbackState == ExoPlayer.STATE_READY) {
            RuthUtility.hideLoader()

//            progressBarVideoPlayer.visibility = View.GONE
        }
    }

    override fun onPositionDiscontinuity() {
        if (needRetrySource) {
            updateResumePosition()
        }
    }


    override fun onTimelineChanged(timeline: Timeline?, manifest: Any?) {
        Log.d("timeline", "manifest")
    }


    override fun onPlayerError(e: ExoPlaybackException) {
        //videoStartControlLayout.setVisibility(View.VISIBLE)
        var errorString: String? = null
        if (e.type == ExoPlaybackException.TYPE_RENDERER) {
            val cause = e.rendererException
            if (cause is DecoderInitializationException) {
                // Special case for decoder initialization failures.
                val decoderInitializationException = cause as DecoderInitializationException
                if (decoderInitializationException.decoderName == null) {
                    if (decoderInitializationException.cause is DecoderQueryException) {
                        errorString = getString(R.string.error_querying_decoders)
                    } else if (decoderInitializationException.secureDecoderRequired) {
                        errorString = getString(R.string.error_no_secure_decoder,
                                decoderInitializationException.mimeType)
                    } else {
                        errorString = getString(R.string.error_no_decoder,
                                decoderInitializationException.mimeType)
                    }
                } else {
                    errorString = getString(R.string.error_instantiating_decoder,
                            decoderInitializationException.decoderName)
                }
            }
        }
        if (errorString != null) {
            showToast(errorString)
        }
        needRetrySource = true
        if (isBehindLiveWindow(e)) {
            clearResumePosition()
            play(null)
        } else {
            updateResumePosition()
            showControls()
        }
    }

    override fun onTracksChanged(trackGroups: TrackGroupArray, trackSelections: TrackSelectionArray) {
        val mappedTrackInfo = trackSelector!!.currentMappedTrackInfo
        if (mappedTrackInfo != null) {
            if ((mappedTrackInfo.getTrackTypeRendererSupport(C.TRACK_TYPE_VIDEO) == MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS)) {
                Toast.makeText(applicationContext, R.string.error_unsupported_video, Toast.LENGTH_LONG).show()

            }
            if ((mappedTrackInfo.getTrackTypeRendererSupport(C.TRACK_TYPE_AUDIO) == MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS)) {
                Toast.makeText(applicationContext, R.string.error_unsupported_audio, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun showControls() {
        // debugRootView.visibility = View.VISIBLE
    }

    private fun buildDataSourceFactory(bandwidthMeter: DefaultBandwidthMeter?): DataSource.Factory {
        return DefaultDataSourceFactory(this, bandwidthMeter,
                buildHttpDataSourceFactory(bandwidthMeter))
    }

    private fun buildHttpDataSourceFactory(bandwidthMeter: DefaultBandwidthMeter?): HttpDataSource.Factory {
        return DefaultHttpDataSourceFactory(userAgent, bandwidthMeter)
    }

    private fun useExtensionRenderers(): Boolean {
        return BuildConfig.FLAVOR == "withExtensions"
    }

    fun play(view: View?) {
        if (intent.getStringExtra("broadcastId") != null) {
            if (!intent.getStringExtra("broadcastUrl").isEmpty()) {
                val URL = intent.getStringExtra("broadcastUrl")
                initializePlayer(URL)
            } else {
                showLoadingProgress()
                presenter.getPastBroadcast(intent.getStringExtra("broadcastId"))
            }
        }
    }

    /*---------------------------------------END LIVE STREAMING-----------------------------------*/

    private fun userProfile() {
        val dialog = Dialog(this, R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.popup_user_profile)

        val ivBroadcasterImage = dialog.findViewById<View>(R.id.ivBroadcasterImage) as CircleImageView
        val tvBroadcasterName = dialog.findViewById<View>(R.id.tvBroadcasterName) as TextView

        Glide.with(this).load(channelBroadcasterData!!.photoUrl)
                .apply(RequestOptions().error(R.drawable.com_facebook_profile_picture_blank_square)
                        .placeholder(R.drawable.com_facebook_profile_picture_blank_square))
                .into(ivBroadcasterImage)
        if (!channelBroadcasterData!!.name.isEmpty()) {
            tvBroadcasterName.text = channelBroadcasterData!!.name
        } else {
            tvBroadcasterName.text = channelBroadcasterData!!.userName
        }

        dialog.findViewById<View>(R.id.btnSend).setOnClickListener {
            userProfileNextPage()
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun userProfileNextPage() {
        val dialog = Dialog(this, R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.popup_user_profile_second)
        dialog.findViewById<View>(R.id.btn_chat).setOnClickListener {
            dialog.dismiss()
        }
        dialog.findViewById<View>(R.id.btn_done).setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    override fun hideKeyboard() {
        val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = this.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private var invalidDialog: AlertDialog? = null

    override fun invalidLogin() {

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Invalid Login")
        builder.setMessage(getString(R.string.invalid_login_error))

        // add a button
        builder.setPositiveButton(getString(R.string.log_in)) { dialog, _ ->
            presenter.clearLogin()
            startActivity(Intent(this, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            finish()
            overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
            dialog.dismiss()
        }

        if (!isFinishing && window.decorView.isShown) {
            invalidDialog = builder.create()
            invalidDialog!!.setCancelable(false)
            invalidDialog!!.show()
        }
    }

    override fun onDestroy() {
        if (invalidDialog != null) {
            invalidDialog!!.dismiss()
            invalidDialog = null
        }
        super.onDestroy()
    }

    override fun clearPreferences() {
        startActivity(Intent(this, LoginActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        finish()
    }

    override fun showLoadingProgress() {
        RuthUtility.showLoader(this@PastVideoPlayerActivity)

//        progressBarVideoPlayer.visibility = View.VISIBLE
    }

    override fun hideLoadingProgress() {
        RuthUtility.hideLoader()
//        progressBarVideoPlayer.visibility = View.GONE
    }

    override fun playVideos(broadcastUrl: String) {
        initializePlayer(broadcastUrl)
    }

    override fun finishActivity() {
        finish()
    }
}

