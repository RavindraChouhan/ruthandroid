package com.ruth.app.home.eightbutton.videoplayer

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ruth.app.R
import com.ruth.app.home.adapter.EmojisHomeAdapter
import kotlinx.android.synthetic.main.item_ruth_video_cell.view.*

class RuthVideoPlayerAdapter(private val mContext: Context,
                             private val videoPlayerList: ArrayList<String>) : RecyclerView.Adapter<RuthVideoPlayerAdapter.ViewHolder>() {

    private lateinit var onItemClickListner: OnItemClickListner

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_ruth_video_cell, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return videoPlayerList.size
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(/*broadcastData[position],*/ videoPlayerList[position])

    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(/*channelBroadcastData: ChannelBroadcastData, */videoPlayerType: String) {

            if (videoPlayerType.equals("RuthVideo", true)) {
                Glide.with(mContext).load(R.drawable.ghost_film)
                        .apply(RequestOptions().error(R.drawable.logoeye).placeholder(R.drawable.background)).into(itemView.imgCover)
            } else if (videoPlayerType.equals("SitTalk", true)) {
                Glide.with(mContext).load(R.drawable.sitn_talk_dummy_icon)
                        .apply(RequestOptions().error(R.drawable.logoeye).placeholder(R.drawable.background)).into(itemView.imgCover)
            }
            setEmojisList(itemView, 5)

//            itemView.lblVideoTitle.text = channelBroadcastData.broadcastTitle
//            itemView.lblViews.text = channelBroadcastData.broadcastViews.toString() + " views"
//            itemView.lblPastTime.text = RuthUtility.getTimeAgo("2017-09-06T07:57:29.872Z")
            //itemView.lblVideoDuration.text = convertTime(channelBroadcastData.broadcastUrl)

        }
    }

    private fun setEmojisList(view: View, count: Int?) {
        view.listScaryEmojis.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        val adapter = EmojisHomeAdapter(mContext, count!!)
        view.listScaryEmojis!!.adapter = adapter
    }


    fun setOnItemClickListner(onItemClickListner: OnItemClickListner) {
        this.onItemClickListner = onItemClickListner
    }

    interface OnItemClickListner {
        fun onItemClick(pos: Int, selectedId: String)
    }

}
