package com.ruth.app.home.eightbutton.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class VipSharePresenter @Inject constructor(): BasePresenter<VipSharePresenter.VipSharePresenterView>() {
    interface VipSharePresenterView : BaseView{

    }
}