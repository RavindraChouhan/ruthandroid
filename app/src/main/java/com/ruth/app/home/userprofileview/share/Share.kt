package com.ruth.app.home.userprofileview.share

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Share(var shareType: String) : Parcelable