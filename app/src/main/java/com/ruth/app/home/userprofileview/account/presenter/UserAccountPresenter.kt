package com.ruth.app.home.userprofileview.account.presenter

import android.widget.Toast
import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.network.config.RuthApi
import javax.inject.Inject

class UserAccountPresenter @Inject constructor(var ruthApi: RuthApi) : BasePresenter<UserAccountPresenter.UserAccountView>() {

    fun updateInfo(etUserName: String, etNickname: String,
                   etAbout: String, etBirthday: String,
                   gender: String, etEmail: String) {

        val hashMap: HashMap<String, String> = HashMap()
        hashMap["type"] = "account"
        hashMap["userId"] = preferences.getUserInfo().id
        if (etUserName.isNotEmpty()) hashMap["userName"] = etUserName.trim().replace("@", "")
        if (etNickname.isNotEmpty()) hashMap["nickname"] = etNickname.trim()
        if (etEmail.isNotEmpty()) hashMap["email"] = etEmail.trim()
        if (gender.isNotEmpty()) hashMap["gender"] = gender
        if (etBirthday.isNotEmpty()) hashMap["dob"] = etBirthday.trim()
        if (etAbout.isNotEmpty()) hashMap["about"] = etAbout.trim()

        ruthApi.updateUserInfo(preferences.getUserInfo().userToken, hashMap)
                .subscribe {
                    view?.showProgressLoading()
                    if (it.status) {
                        getMyProfile(it.message)
                    } else if (it.message.contains(context.getString(R.string.device_login), true)
                            || it.zhHans.contains(context.getString(R.string.device_login), true)
                            || it.zhHant.contains(context.getString(R.string.device_login), true)) {
                        view?.invalidLogin()
                        view?.hideProgressLoading()
                    } else {
                        view?.hideProgressLoading()
                        Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                    }
                }
    }

    private fun getMyProfile(message: String) {
        ruthApi.getUserInfo(preferences.getUserInfo().userToken, preferences.getUserInfo().id)
                .subscribe({
                    view?.showProgressLoading()
                    if (it != null) {
                        if (it.status) {
                            preferences.setUserProfile(it.data)
                            view?.profileUpdated(message)
                            view?.hideProgressLoading()
                        } else {
                            view?.hideProgressLoading()
                            view?.invalidLogin()
                        }
                    }
                }, {
                    view?.hideProgressLoading()
                    view?.showToast(getString(R.string.connection_error))
                })
    }

    fun clearLogin() {
        val tempFb = utility.getBooleaPreferences(context, getString(R.string.isfbSignup))
        val tempGoogle = utility.getBooleaPreferences(context, getString(R.string.isGoogleSignup))
        val tempInsta = utility.getBooleaPreferences(context, getString(R.string.isInstaSignup))

        preferences.clearPreferences()

        utility.setBooleanPreferences(context, getString(R.string.isfbSignup), tempFb)
        utility.setBooleanPreferences(context, getString(R.string.isGoogleSignup), tempGoogle)
        utility.setBooleanPreferences(context, getString(R.string.isInstaSignup), tempInsta)

        view?.clearPreferences()
    }

    interface UserAccountView : BaseView {
        fun profileUpdated(message: String)
        fun invalidLogin()
        fun clearPreferences()
        fun showProgressLoading()
        fun hideProgressLoading()

    }
}