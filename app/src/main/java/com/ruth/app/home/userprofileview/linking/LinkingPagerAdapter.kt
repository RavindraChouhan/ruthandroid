package com.ruth.app.home.userprofileview.linking


import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class LinkingPagerAdapter(var supportFragmentManager: FragmentManager?,
                          var tabCount: Int) : FragmentStatePagerAdapter(supportFragmentManager) {

    override fun getItem(position: Int): Fragment {
        if (position == 0) {
            return PaymentMethodFragment()
        } else if (position == 1) {
            return SocialAccountsFragments()
        }
        return PaymentMethodFragment()
    }

    override fun getCount(): Int {
        return tabCount
    }


}
