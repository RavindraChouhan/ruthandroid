package com.ruth.app.home.presenter

import android.widget.Toast
import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.model.RuthSettingsData
import com.ruth.app.network.config.RuthApi
import com.ruth.app.utility.RuthUtility
import javax.inject.Inject

class UserProfileFragmentPresenter @Inject constructor(var ruthApi: RuthApi) : BasePresenter<UserProfileFragmentPresenter.UserProfileFragmentView>() {

    override fun onCreate(view: UserProfileFragmentView) {
        super.onCreate(view)
        getMyProfile()
    }

    fun uploadProfileImage(uploadedImageUrl: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap["type"] = "account"
        hashMap["userId"] = preferences.getUserInfo().id
        hashMap["photoUrl"] = uploadedImageUrl

        ruthApi.updateUserInfo(preferences.getUserInfo().userToken, hashMap)
                .onErrorReturn { (null) }
                .subscribe {
                    view?.showProgressLoading()
                    if (it != null) {
                        if (it.status) {
                            getMyProfile()
                        } else if (it.message.contains(context.getString(R.string.device_login), true)
                                || it.zhHans.contains(context.getString(R.string.device_login), true)
                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
                            view?.invalidLogin()
                            view?.hideProgressLoading()
                        } else {
                            view?.hideProgressLoading()
                            Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
    }

    fun updateOnOffStatus(status: Int) {
        val hashMap: HashMap<String, @JvmSuppressWildcards Any> = HashMap()
        hashMap["type"] = "logout"
        hashMap["userId"] = preferences.getUserInfo().id
        hashMap["status"] = status

        ruthApi.updateUserInfo(preferences.getUserInfo().userToken, hashMap)
                .onErrorReturn { (null) }
                .subscribe {
                    view?.showProgressLoading()
                    if (it != null) {
                        if (it.status) {
                            view?.updateStatus(status)
                            getMyProfile()
                        } else if (it.message.contains(context.getString(R.string.device_login), true)
                                || it.zhHans.contains(context.getString(R.string.device_login), true)
                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
                            view?.invalidLogin()
                            view?.hideProgressLoading()
                        } else {
                            view?.hideProgressLoading()
                            Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
    }

    private fun getMyProfile() {
        ruthApi.getUserInfo(preferences.getUserInfo().userToken, preferences.getUserInfo().id)
                .onErrorReturn { (null) }
                .subscribe({
                    view?.showProgressLoading()
                    if (it != null) {
                        if (it.status) {
                            preferences.setUserProfile(it.data)
                            if (it.data.pointEarn!!.isNotEmpty()){
                                RuthUtility.updatePoints(context,it.data.pointEarn)
                            }else{

                            }
                            view?.hideProgressLoading()
                        } else if (it.message.contains(context.getString(R.string.device_login), true)
                                || it.zhHans.contains(context.getString(R.string.device_login), true)
                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
                            view?.invalidLogin()
                            view?.hideProgressLoading()
                        } else {
                            view?.hideProgressLoading()
                            Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }, {
                    view?.hideProgressLoading()
                    view?.showToast(getString(R.string.connection_error))
                })
    }

    fun getSettings() {

        ruthApi.getSettings(preferences.getUserInfo().userToken)
                .onErrorReturn { (null) }
                .subscribe {
                    if (it != null) {
                        if (it.status) {
                            view?.settingsData(it.data)
                        } else if (it.message.contains(context.getString(R.string.device_login), true)
                                || it.zhHans.contains(context.getString(R.string.device_login), true)
                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
                            view?.invalidLogin()
                            view?.hideProgressLoading()
                        } else {
                            view?.hideProgressLoading()
                            Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                        }
                    }

                }
    }

    fun clearLogin() {
        val tempFb = utility.getBooleaPreferences(context, getString(R.string.isfbSignup))
        val tempGoogle = utility.getBooleaPreferences(context, getString(R.string.isGoogleSignup))
        val tempInsta = utility.getBooleaPreferences(context, getString(R.string.isInstaSignup))

        preferences.clearPreferences()

        utility.setBooleanPreferences(context, getString(R.string.isfbSignup), tempFb)
        utility.setBooleanPreferences(context, getString(R.string.isGoogleSignup), tempGoogle)
        utility.setBooleanPreferences(context, getString(R.string.isInstaSignup), tempInsta)

        view?.clearPreferences()
    }


    interface UserProfileFragmentView : BaseView {
        fun invalidLogin()
        fun clearPreferences()
        fun showProgressLoading()
        fun hideProgressLoading()
        fun updateStatus(it: Int)
        fun settingsData(data: RuthSettingsData)
    }
}