package com.ruth.app.home.userprofileview.linking

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import kotlinx.android.synthetic.main.fragment_social_accounts.*
import kotlinx.android.synthetic.main.fragment_social_accounts.view.*

class SocialAccountsFragments : Fragment(), View.OnClickListener {

    private var connectFb: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_social_accounts, container, false)

        view.btnConnectFb.setOnClickListener(this)
        view.btnDisConnectFb.setOnClickListener(this)
        view.btnConnectInsta.setOnClickListener(this)
        view.btnDisConnectInsta.setOnClickListener(this)

        return view
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.btnConnectFb) {
            connectFb = true
            btnDisConnectFb.visibility = View.VISIBLE
            btnConnectFb.visibility = View.GONE
        }

        if (v?.id == R.id.btnDisConnectFb) {
            connectFb = false
            btnDisConnectFb.visibility = View.GONE
            btnConnectFb.visibility = View.VISIBLE
        }

        if (v?.id == R.id.btnConnectInsta) {
            connectFb = true
            btnDisConnectInsta.visibility = View.VISIBLE
            btnConnectInsta.visibility = View.GONE
        }

        if (v?.id == R.id.btnDisConnectInsta) {
            connectFb = false
            btnDisConnectInsta.visibility = View.GONE
            btnConnectInsta.visibility = View.VISIBLE
        }
    }

}