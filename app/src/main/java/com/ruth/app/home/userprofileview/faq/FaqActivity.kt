package com.ruth.app.home.userprofileview.faq

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ruth.app.R
import com.ruth.app.utility.Utility
import kotlinx.android.synthetic.main.activity_user_faq.*
import javax.inject.Inject

class FaqActivity : AppCompatActivity(), View.OnClickListener {

    @Inject
    lateinit var utility: Utility
    private var faqPagerAdapter: FaqViewPagerAdapter? = null
    private var faqList: ArrayList<Faq>? = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_faq)

        ivFaqBack.setOnClickListener(this)

        setupTabLayout()

    }

    override fun onClick(v: View?) {
        if (v == ivFaqBack) {
            finish()
        }
    }

    private fun setupTabLayout() {
        faqList?.clear()
        for (i in 0 until 5) {
            faqList?.add(Faq("$i. 張大文", resources.getString(R.string.dummy_text)))
        }
        faqPagerAdapter = FaqViewPagerAdapter(supportFragmentManager, faqList)
        viewPagerFaq.adapter = faqPagerAdapter
        tabFaq.setupWithViewPager(viewPagerFaq)

        tabFaq.getTabAt(0)!!.setText(R.string.all)
        tabFaq.getTabAt(1)!!.setText(R.string.basic)
        tabFaq.getTabAt(2)!!.setText(R.string.live)
        tabFaq.getTabAt(3)!!.setText(R.string.videos)
        tabFaq.getTabAt(4)!!.setText(R.string.podcast)
        tabFaq.getTabAt(5)!!.setText(R.string.pictures)


        tabFaq.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                val position = tab.position

                when (position) {
                    0 -> {
                        faqList?.clear()
                        for (i in 0 until 5) {
                            faqList?.add(Faq("$i. 張大文", resources.getString(R.string.dummy_text)))
                        }
                    }
                    1 -> {
                        faqList?.clear()
                        for (i in 0 until 10) {
                            faqList?.add(Faq("$i. 張大文", resources.getString(R.string.dummy_text)))
                        }
                    }
                    2 -> {
                        faqList?.clear()
                        for (i in 0 until 6) {
                            faqList?.add(Faq("$i. 張大文", resources.getString(R.string.dummy_text)))
                        }
                    }
                    3 -> {
                        faqList?.clear()
                        for (i in 0 until 3) {
                            faqList?.add(Faq("$i. 張大文", resources.getString(R.string.dummy_text)))
                        }
                    }
                    4 -> {
                        faqList?.clear()
                        for (i in 0 until 1) {
                            faqList?.add(Faq("$i. 張大文", resources.getString(R.string.dummy_text)))
                        }
                    }
                    5 -> {
                        faqList?.clear()
                        for (i in 0 until 2) {
                            faqList?.add(Faq("$i. 張大文", resources.getString(R.string.dummy_text)))
                        }
                    }
                }
            }
        })
    }

}