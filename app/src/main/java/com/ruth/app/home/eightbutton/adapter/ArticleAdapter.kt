package com.ruth.app.home.eightbutton.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import com.ruth.app.home.adapter.FollowersAdapter
import com.ruth.app.home.eightbutton.activity.BibleDetailActivity
import kotlinx.android.synthetic.main.item_latest_story.view.*

class ArticleAdapter(val mContext: Context) : RecyclerView.Adapter<ArticleAdapter.ViewHolder>() {

    var list: ArrayList<Int> = ArrayList()

    override fun getItemCount(): Int {
        return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_article, parent, false)
        return ViewHolder(view)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

}
