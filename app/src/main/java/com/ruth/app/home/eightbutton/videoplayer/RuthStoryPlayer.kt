package com.ruth.app.home.eightbutton.videoplayer

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.PagerSnapHelper
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AccelerateDecelerateInterpolator
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.home.model.ChannelBroadcastData
import com.ruth.app.login.activity.LoginActivity
import com.ruth.app.utility.RuthUtility
import com.ruth.app.video.presenter.VideoPlayerPresenter
import com.ruth.app.widgets.ScrollZoomLayoutManager
import kotlinx.android.synthetic.main.activity_ruth_story_player.*

class RuthStoryPlayer : BaseActivity<VideoPlayerPresenter>(), VideoPlayerPresenter.VideoPlayerView, View.OnClickListener {

    lateinit var context: Context
    private var position: Int = 0
    private var listMain: ArrayList<ChannelBroadcastData>? = null
    private var listUpdated: ArrayList<ChannelBroadcastData>? = null
    private lateinit var updateHandler: Handler
    private var isExpand: Boolean = false
    private var invalidDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
//        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)
        setContentView(R.layout.activity_ruth_story_player)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        context = this
        updateHandler = Handler()
        listMain = ArrayList()
        listUpdated = ArrayList()

//        if (intent.getStringExtra("videoType").equals("RuthVideo", true)) {
//            ivVideoImage.setImageResource(R.drawable.ghost_film)
//        } else if (intent.getStringExtra("videoType").equals("SitTalk", true)) {
//            ivVideoImage.setImageResource(R.drawable.sitn_talk_dummy_icon)
//        }

        val list: ArrayList<String> = ArrayList()
        for (i in 0 until 5) {
            list.add(intent.getStringExtra("videoType"))
        }

        val adapter = RuthStoryVideoPlayerAdapter(context, list)
        recycleVideos!!.adapter = adapter
        recycleVideos!!.adapter.notifyDataSetChanged()

        //Click listeners
        imgShow.setOnClickListener(this)
        imgFollow.setOnClickListener(this)
        imgBack.setOnClickListener(this)

        val snapHelper = PagerSnapHelper()
        val scrollZoomLayoutManager = ScrollZoomLayoutManager(this, RuthUtility.Dp2px(this, 16F), 1.3f)
        viewPagerPlayer.layoutManager = scrollZoomLayoutManager
        snapHelper.attachToRecyclerView(viewPagerPlayer)
        viewPagerPlayer.setHasFixedSize(true)

        val ruthStoryPlayerAdapter = RuthStoryPlayerAdapter(this)
        viewPagerPlayer.adapter = ruthStoryPlayerAdapter
    }

    override fun onClick(v: View?) {
        when (v) {
            imgShow -> {
                presenter.showHideDescription()
            }
            imgFollow -> {
                val isFollowed: Boolean = !listMain!![position].broadcasterData.isFollow
                if (listMain!![position].broadcasterData.isFollow) {
                    alert("Do you really want to unsubscribe?", "Already Subscribed", {
                        presenter.followUnfollow(listMain!![position].broadcasterData.id, isFollowed)
                        imgFollow.setImageResource(R.drawable.icon_follow_button)
                    }, {

                    })
                } else {
                    presenter.followUnfollow(listMain!![position].broadcasterData.id, isFollowed)
                    imgFollow.setImageResource(R.drawable.icon_follow)
                }
            }
            imgBack -> {
                finish()
            }
        }
    }

    override fun showHideDescription() {
        if (lytDescription!!.isExpanded) {
            lytDescription!!.collapse()
            val deg = 180f
            imgShow.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
        } else {
            lytDescription!!.expand()
            val deg = 0f
            imgShow.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
        }
    }

    override fun invalidLogin() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Invalid Login")
        builder.setMessage(getString(R.string.invalid_login_error))

        builder.setPositiveButton(getString(R.string.log_in)) { dialog, _ ->
            presenter.clearLogin()
            startActivity(Intent(this, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            finish()
            overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
            dialog.dismiss()
        }

        if (!isFinishing && window.decorView.isShown) {
            invalidDialog = builder.create()
            invalidDialog!!.setCancelable(false)
            invalidDialog!!.show()
        }
    }

    override fun clearPreferences() {
        startActivity(Intent(this, LoginActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        finish()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.no_animation, R.anim.slide_down_dialog)
    }

}


