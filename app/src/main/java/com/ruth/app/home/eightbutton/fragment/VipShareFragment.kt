package com.ruth.app.home.eightbutton.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.ruth.app.App

import com.ruth.app.R
import com.ruth.app.base.fragment.BaseFragment
import com.ruth.app.home.adapter.HomeBannerPagerAdapter
import com.ruth.app.home.eightbutton.adapter.VipShareRecycleAdapter
import com.ruth.app.home.eightbutton.presenter.VipShareFragmentPresenter
import kotlinx.android.synthetic.main.fragment_vip_share.view.*


class VipShareFragment : BaseFragment<VipShareFragmentPresenter>(), VipShareFragmentPresenter.VipShareFragmentPresenterView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_vip_share, container, false)
        App.getComponent().inject(this)
        presenter.onCreate(this)
        setTopBannerImages(view)

        view.recycle_share.layoutManager = LinearLayoutManager(presenter.context)
        view.recycle_share.hasFixedSize()
        val adapter = VipShareRecycleAdapter(presenter.context)
        view.recycle_share.adapter = adapter
        return view
    }


    private lateinit var homeBannerAdapter: HomeBannerPagerAdapter

    private var dotscount: Int = 0

    private lateinit var dots: Array<ImageView?>

    private fun setTopBannerImages(view: View) {
        val homeBannerList: ArrayList<String> = ArrayList()
        homeBannerAdapter = HomeBannerPagerAdapter(presenter.context, homeBannerList)
        view.viewpagershareBanner.adapter = homeBannerAdapter

        dotscount = homeBannerAdapter.count

        dots = arrayOfNulls<ImageView>(dotscount)
        for (i in 0 until dotscount) {
            dots[i] = ImageView(presenter.context)
            dots[i]?.setImageDrawable(ContextCompat.getDrawable(presenter.context, R.drawable.nonactive_dot))
            val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.setMargins(8, 0, 8, 0)
            view.share_bannerSliderDots.addView(dots[i], params)
        }
        dots[0]?.setImageDrawable(ContextCompat.getDrawable(presenter.context, R.drawable.active_dot))

        view.viewpagershareBanner.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                //    currentPage = position

                for (i in 0 until dotscount) {
                    dots!![i]?.setImageDrawable(ContextCompat.getDrawable(presenter.context, R.drawable.nonactive_dot))
                }
                dots!![position]?.setImageDrawable(ContextCompat.getDrawable(presenter.context, R.drawable.active_dot))

            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })
    }

    fun newInstance(): Fragment {
        return VipShareFragment()
    }


}
