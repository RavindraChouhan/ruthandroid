package com.ruth.app.home.userprofileview.videos

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class UserVideo(var summonType: String) : Parcelable