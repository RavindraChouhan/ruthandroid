package com.ruth.app.home.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.ruth.app.R
import com.ruth.app.model.RecommandedUserDetail
import com.ruth.app.utility.RuthUtility
import kotlinx.android.synthetic.main.item_recommended_friends.view.*
import java.text.SimpleDateFormat
import java.util.*

class SeeAllFriendsAdapter(private var context: Context,
                           private var friendsList: ArrayList<RecommandedUserDetail>)
    : RecyclerView.Adapter<SeeAllFriendsAdapter.RecommendedViewHolder>() {

    private lateinit var recyclerView: RecyclerView
    private var birthDate: String = ""
    private var onItemClickListener: OnItemClickListener? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecommendedViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_recommended_friends, parent, false)
        return RecommendedViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecommendedViewHolder, position: Int) {
        holder.bind(friendsList[position])
    }

    override fun getItemCount(): Int {
        return 10/*friendsList.size*/
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    inner class RecommendedViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(recommendedFriends: RecommandedUserDetail) {

            if (recommendedFriends.isFollow) {
                itemView.layoutUnselected.visibility = View.GONE
                itemView.layoutSelected.visibility = View.VISIBLE
            } else {
                itemView.layoutUnselected.visibility = View.VISIBLE
                itemView.layoutSelected.visibility = View.GONE
            }

            if (recommendedFriends.name.equals("Empty", true)) {
                itemView.layoutEmpty.visibility = View.VISIBLE
                itemView.layoutRecommend.visibility = View.GONE
            } else {
                itemView.layoutEmpty.visibility = View.GONE
                itemView.layoutRecommend.visibility = View.VISIBLE

                itemView.tvRecommendName.text = recommendedFriends.name
                if (recommendedFriends.gender.isNotEmpty()) {
                    if (recommendedFriends.gender.equals(context.resources.getString(R.string.female), false) ||
                            recommendedFriends.gender.equals(context.resources.getString(R.string.femaleChinese), false)) {
                        itemView.tvRecommendGender.text = context.resources.getString(R.string.femaleChinese)
                    } else if (recommendedFriends.gender.equals(context.resources.getString(R.string.male), false)
                            || recommendedFriends.gender.equals(context.resources.getString(R.string.maleChinese), false)) {
                        itemView.tvRecommendGender.text = context.resources.getString(R.string.maleChinese)
                    }
                }

                itemView.tvRecommendFans.text = RuthUtility.getCounts(recommendedFriends.followers)
                itemView.tvRecommendAccountNo.text = recommendedFriends.id.substring(recommendedFriends.id.length - 4, recommendedFriends.id.length)
                Glide.with(context).load(recommendedFriends.photoUrl)
                        .apply(RequestOptions().error(R.drawable.logoeye)
                                .placeholder(R.drawable.com_facebook_profile_picture_blank_square)
                                .diskCacheStrategy(DiskCacheStrategy.NONE))
                        .listener(object : RequestListener<Drawable> {
                            override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                itemView.progressBarFriends.visibility = View.GONE
                                return false
                            }

                            override fun onResourceReady(resource: Drawable?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                itemView.progressBarFriends.visibility = View.GONE
                                return false
                            }

                        })
                        .into(itemView.ivRecommendUser)

                if (recommendedFriends.dob.isNotEmpty()) {
                    val dateString = recommendedFriends.dob
                    val date = SimpleDateFormat("dd/MM/yyyy").parse(dateString)
                    val calendar = Calendar.getInstance()
                    calendar.time = date
                    val y = calendar[Calendar.YEAR]
                    val m = calendar[Calendar.MONTH + 1]
                    val d = calendar[Calendar.DAY_OF_MONTH]

                    when {
                        d < 10 -> {
                            birthDate = RuthUtility.convertToChinese(d)
                        }
                        d in 11..19 -> {
                            val reminder = d % 10
                            birthDate = if (reminder == 0) {
                                RuthUtility.convertToChinese(10)
                            } else {
                                RuthUtility.convertToChinese(10) + RuthUtility.convertToChinese(reminder)
                            }
                        }
                        else -> {
                            val reminder = d % 10
                            val div = d / 10
                            birthDate = if (reminder == 0) {
                                RuthUtility.convertToChinese(div) + RuthUtility.convertToChinese(10)
                            } else {
                                RuthUtility.convertToChinese(div) + RuthUtility.convertToChinese(10) + RuthUtility.convertToChinese(reminder)
                            }
                        }
                    }

                    val dateBirth = SimpleDateFormat("MMMM").format(date) + birthDate

                    val yearString = RuthUtility.convertToChinese(y / 1000) +
                            RuthUtility.convertToChinese((y % 1000) / 100) +
                            RuthUtility.convertToChinese(((y % 1000) % 100) / 10) +
                            RuthUtility.convertToChinese(((y % 1000) % 100) % 10)
                    val birthYear = context.getString(R.string.born_in) + yearString + "年"
                    Log.d("year", "1 ->$birthYear")

                    itemView.tvRecommendRegisteredDate.text = dateBirth
                    itemView.tvRecommendRegisteredYear.text = birthYear
                }

//                itemView.tvRecommendFirstPlace.text = recommendedFriends.districtName1
//                itemView.tvRecommendSecondPlace.text = recommendedFriends.districtName2
            }

            /*itemView.layoutRecommend.setOnClickListener(View.OnClickListener {
                friendsList!![position].isSelected = !friendsList!![position].isSelected
                notifyDataSetChanged()
            })*/

            itemView.layoutFooterFollow.setOnClickListener {
                onItemClickListener?.onFollowClick(itemView, adapterPosition)
            }

            itemView.btn_Open_Profile.setOnClickListener {
                onItemClickListener?.onItemClick(itemView, adapterPosition)
            }
        }
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
        fun onFollowClick(view: View, position: Int)
    }
}
