package com.ruth.app.home.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import com.ruth.app.home.model.Favorite
import com.ruth.app.login.model.Friends
import kotlinx.android.synthetic.main.item_favorite_view.view.*
import java.util.*


class FavoriteListAdapter(private var context: Context,
                          private var friendsList: ArrayList<Favorite>?)
    : RecyclerView.Adapter<FavoriteListAdapter.RecommendedViewHolder>() {

    private lateinit var recyclerView: RecyclerView

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecommendedViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_favorite_view, parent, false)
        return RecommendedViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecommendedViewHolder, position: Int) {
        holder.bind(friendsList?.get(position)!!)
    }

    override fun getItemCount(): Int {
        return friendsList?.size!!
    }

    inner class RecommendedViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(recommendedFriends: Favorite) {

            if (recommendedFriends.isSelected) {
                itemView.favUnselected.visibility = View.GONE
                itemView.favSelected.visibility = View.VISIBLE
            } else {
                itemView.favUnselected.visibility = View.VISIBLE
                itemView.favSelected.visibility = View.GONE
            }

            itemView.tvName.text = recommendedFriends.name
            itemView.tvFavFans.text = context.getString(R.string.born_in) + ":" + recommendedFriends.fansCount.toString()
            itemView.ivFavProfile.setImageDrawable(recommendedFriends.userImage)

            itemView.layoutFavorite.setOnClickListener(View.OnClickListener {
                friendsList!![position].isSelected = !friendsList!![position].isSelected
                notifyDataSetChanged()
            })
        }
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int, answers: ArrayList<Friends>)
    }
}
