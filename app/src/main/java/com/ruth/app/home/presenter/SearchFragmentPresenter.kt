package com.ruth.app.home.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class SearchFragmentPresenter @Inject constructor() : BasePresenter<SearchFragmentPresenter.SearchFragmentView>() {

    interface SearchFragmentView : BaseView {

    }
}