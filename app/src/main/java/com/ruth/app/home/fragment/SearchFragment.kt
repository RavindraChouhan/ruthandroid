package com.ruth.app.home.fragment

import android.content.Context
import android.content.res.Resources
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.fragment.BaseFragment
import com.ruth.app.home.adapter.OptionsAdapter
import com.ruth.app.home.adapter.SearchPagerAdapter
import com.ruth.app.home.adapter.SearchRecycleAdapter
import com.ruth.app.home.presenter.SearchFragmentPresenter
import kotlinx.android.synthetic.main.fragment_search.view.*

class SearchFragment : BaseFragment<SearchFragmentPresenter>(), SearchFragmentPresenter.SearchFragmentView {

    private var mContext: Context? = null

    private var margin: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_search, container, false)
        App.getComponent().inject(this)
        presenter.onCreate(this)
        mContext = activity
        if (getScreenWidth() == 1080) {
            margin = getScreenWidth() * 16 / 100
        } else {
            margin = getScreenWidth() * 16 / 100
        }
        val pagerAdapter = SearchPagerAdapter(mContext!!)

        view.creepy_pager.adapter = pagerAdapter
        view.creepy_pager.offscreenPageLimit = 3
        view.creepy_pager.currentItem = 1
        view.creepy_pager.pageMargin = -margin
        view.creepy_pager.clipChildren = true

        view.recycle_latest_story.layoutManager = LinearLayoutManager(mContext)
        val adapter = SearchRecycleAdapter(mContext as FragmentActivity)
        view.recycle_latest_story.adapter = adapter

        view.recycle_options.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.HORIZONTAL)
        view.recycle_options.adapter = OptionsAdapter(mContext as FragmentActivity)
        return view
    }

    private fun getScreenWidth(): Int {
        return Resources.getSystem().displayMetrics.widthPixels
    }


}