package com.ruth.app.home.eightbutton.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MapModel {


    @SerializedName("routes")
    @Expose
    var routes: List<Routes>? = null


}