package com.ruth.app.home.userprofileview.videos

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ruth.app.R
import com.ruth.app.utility.Utility
import kotlinx.android.synthetic.main.activity_user_viewpager_tab.*
import javax.inject.Inject

class UserVideosActivity : AppCompatActivity(), View.OnClickListener {

    @Inject
    lateinit var utility: Utility
    private var usersVideoAdapter: UserVideosPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_viewpager_tab)

        tvHeader.text = getString(R.string.videos)
        ivUserTabBack.setOnClickListener(this)

        setupTabLayout()
    }

    override fun onClick(v: View?) {
        if (v == ivUserTabBack) {
            finish()
        }
    }

    private fun setupTabLayout() {

        usersVideoAdapter = UserVideosPagerAdapter(supportFragmentManager, 2)
        viewPagerUser.adapter = usersVideoAdapter
        tablayoutUserTab.setupWithViewPager(viewPagerUser)

        tablayoutUserTab.getTabAt(0)!!.setText(R.string.already_uploaded)
        tablayoutUserTab.getTabAt(1)!!.setText(R.string.draft)

        tablayoutUserTab.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                val position = tab.position

            }
        })
    }
}