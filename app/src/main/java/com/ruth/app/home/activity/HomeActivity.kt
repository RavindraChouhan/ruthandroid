package com.ruth.app.home.activity

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.home.adapter.CalendarAdapter
import com.ruth.app.home.fragment.FavoriteFragment
import com.ruth.app.home.fragment.HomeFragment
import com.ruth.app.home.fragment.SearchFragment
import com.ruth.app.home.fragment.UserProfileFragment
import com.ruth.app.home.model.EightButtonsData
import com.ruth.app.home.presenter.HomePresenter
import com.ruth.app.livestreaming.activity.EnterBroadcastingDetailsActivity
import com.ruth.app.login.activity.LoginActivity
import com.ruth.app.utility.RuthUtility
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_home.*
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class HomeActivity : BaseActivity<HomePresenter>(), HomePresenter.HomeView, View.OnClickListener {

    private var fragment: Fragment? = null
    private var adapter: CalendarAdapter? = null
    private var displayFragment: String? = "HomeFragment"

    companion object {
        var goldPoints: Int = 0
        var silverPoints: Int = 0
        var yuvanPoints: Int = 0
        var eightButtonsData: ArrayList<EightButtonsData> = ArrayList()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        App.getComponent().inject(this)
        presenter.onCreate(this)
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)
        setUpSocket()

        presenter.getMyProfile()
        presenter.getEightButtons()

        val fm = supportFragmentManager
        fragment = HomeFragment()
        val ft = fm.beginTransaction()
        ft.add(R.id.frameContainer, fragment)
        ft.commit()

        tab_home.setOnClickListener(this)
        tab_search.setOnClickListener(this)
        tab_favorite.setOnClickListener(this)
        tab_user.setOnClickListener(this)
        ivStartLive.setOnClickListener(this)

        getNumberOfDays()
    }

    override fun onClick(v: View?) {

        if (v?.id == R.id.tab_home) {
            val mFragment = supportFragmentManager.findFragmentById(R.id.frameContainer)
            if (mFragment is HomeFragment) {
                return
            }
            tab_user.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tab_profile)) //resources.getDrawable(R.drawable.tab_profile)
            tab_favorite.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tab_favourite)) //resources.getDrawable(R.drawable.tab_favourite)
            tab_search.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tab_search)) //resources.getDrawable(R.drawable.tab_search)
            tab_home.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tab_home_fill)) //resources.getDrawable(R.drawable.tab_home_fill)

            fragment = HomeFragment()
            replaceFragment(fragment as HomeFragment, "HomeFragment")
        }

        if (v?.id == R.id.tab_search) {
            val mFragment = supportFragmentManager.findFragmentById(R.id.frameContainer)
            if (mFragment is SearchFragment) {
                return
            }
            tab_user.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tab_profile)) //resources.getDrawable(R.drawable.tab_profile)
            tab_favorite.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tab_favourite)) //resources.getDrawable(R.drawable.tab_favourite)
            tab_search.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tab_search_fill)) //resources.getDrawable(R.drawable.tab_search_fill)
            tab_home.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tab_home)) //resources.getDrawable(R.drawable.tab_home)

            fragment = SearchFragment()
            replaceFragment(fragment as SearchFragment, "SearchFragment")

        }

        if (v?.id == R.id.tab_favorite) {
            val mFragment = supportFragmentManager.findFragmentById(R.id.frameContainer)
            if (mFragment is FavoriteFragment) {
                return
            }
            tab_user.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tab_profile))
            tab_favorite.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tab_favourite_fill))
            tab_search.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tab_search))
            tab_home.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tab_home))

            fragment = FavoriteFragment()
            replaceFragment(fragment as FavoriteFragment, "FavoriteFragment")
        }

        if (v?.id == R.id.tab_user) {
            val mFragment = supportFragmentManager.findFragmentById(R.id.frameContainer)
            if (mFragment is UserProfileFragment) {
                return
            }
            tab_user.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tab_profile_fill))
            tab_favorite.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tab_favourite))
            tab_search.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tab_search))
            tab_home.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tab_home))

            fragment = UserProfileFragment()
            replaceFragment(fragment as UserProfileFragment, "UserProfileFragment")
        }
        if (v?.id == R.id.ivStartLive) {
            if (preferences.getUserProfile().isAuthorized && preferences.getUserProfile().channelId.isNotEmpty()) {
                openGoLivePopup()
            } else {
                val builder = AlertDialog.Builder(this)
                builder.setMessage(getString(R.string.unauthorised_user))

                // add a button
                builder.setPositiveButton(getString(R.string.ok_btn)) { dialog, _ ->
                    dialog.dismiss()
                }
                val dialog = builder.create()
                dialog.setCancelable(false)
                dialog.show()
            }
        }
    }

    private fun replaceFragment(fragment: Fragment, fragmentName: String) {
        val fragmentTransaction: FragmentTransaction
        val fragmentManager = supportFragmentManager

        fragmentTransaction = fragmentManager.beginTransaction()
//        fragmentManager.executePendingTransactions()

        if (fragmentName.equals("HomeFragment", ignoreCase = true)) {
            fragmentTransaction.setCustomAnimations(R.anim.fragment_slide_right_enter,
                    R.anim.fragment_slide_right_exit,
                    R.anim.fragment_slide_left_enter,
                    R.anim.fragment_slide_left_exit)
        } else if (fragmentName.equals("SearchFragment", ignoreCase = true)) {

            if (displayFragment.equals("HomeFragment", ignoreCase = true)) {
                fragmentTransaction.setCustomAnimations(R.anim.fragment_slide_left_enter,
                        R.anim.fragment_slide_left_exit,
                        R.anim.fragment_slide_right_enter,
                        R.anim.fragment_slide_right_exit)
            } else {
                fragmentTransaction.setCustomAnimations(R.anim.fragment_slide_right_enter,
                        R.anim.fragment_slide_right_exit,
                        R.anim.fragment_slide_left_enter,
                        R.anim.fragment_slide_left_exit)
            }
        } else if (fragmentName.equals("FavoriteFragment", ignoreCase = true)) {

            if (displayFragment.equals("HomeFragment", ignoreCase = true) || displayFragment.equals("SearchFragment", ignoreCase = true)) {
                fragmentTransaction.setCustomAnimations(R.anim.fragment_slide_left_enter,
                        R.anim.fragment_slide_left_exit,
                        R.anim.fragment_slide_right_enter,
                        R.anim.fragment_slide_right_exit)
            } else {
                fragmentTransaction.setCustomAnimations(R.anim.fragment_slide_right_enter,
                        R.anim.fragment_slide_right_exit,
                        R.anim.fragment_slide_left_enter,
                        R.anim.fragment_slide_left_exit)
            }
        } else if (fragmentName.equals("UserProfileFragment", ignoreCase = true)) {
            fragmentTransaction.setCustomAnimations(R.anim.fragment_slide_left_enter,
                    R.anim.fragment_slide_left_exit,
                    R.anim.fragment_slide_right_enter,
                    R.anim.fragment_slide_right_exit)
        }

        displayFragment = fragmentName

        fragmentTransaction.addToBackStack(null)
        try {
            fragmentTransaction.replace(R.id.frameContainer, fragment, fragmentName)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        try {
            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {
            e.printStackTrace()
            try {
                fragmentTransaction.commit()
            } catch (e1: Exception) {
                e1.printStackTrace()
            }
        }
    }

    override fun setEightButtons(data: ArrayList<EightButtonsData>) {
        eightButtonsData.clear()
        eightButtonsData = data
    }

    /*Popups*/
    //            getNumberOfDays()
    /*------------------Number of days in month-----------------------------*/
    private fun getNumberOfDays() {
        val c = Calendar.getInstance()
        val totalMonthDays = c.getActualMaximum(Calendar.DAY_OF_MONTH)
        val currentDay = c.get(Calendar.DATE)
        openCalendarPopup(totalMonthDays, currentDay)
    }

    /*------------------Go Live Popup-----------------------------*/
    private fun openGoLivePopup() {
        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_go_live_view, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation

        val btnShareLive = dialogView.findViewById<View>(R.id.btn_share_live) as LinearLayout
        val btnGoLive = dialogView.findViewById<View>(R.id.btn_go_live) as LinearLayout
        val ivGoLive = dialogView.findViewById<View>(R.id.ivGoLive) as ImageView
        val ivShareLive = dialogView.findViewById<View>(R.id.ivShareLive) as ImageView
        val popupClose = dialogView.findViewById<View>(R.id.popupClose) as ImageView

        val mDelayHandler: Handler?
        val splashDelay: Long = 500 //0.5 second
        mDelayHandler = Handler()
        //Navigate with delay

        popupClose.setOnClickListener {
            b.dismiss()
        }

        btnGoLive.setOnClickListener {
            ivGoLive.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.live_bg_selected))
            ivShareLive.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.share_bg_unselected))
            mDelayHandler.postDelayed({
                showHomeProgess()
                presenter.getVerificationCode()
                b.dismiss()
            }, splashDelay)

        }

        btnShareLive.setOnClickListener {
            ivGoLive.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.live_bg_unselected))
            ivShareLive.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.share_bg_selected))
            mDelayHandler.postDelayed({
                openShareFunction()
                b.dismiss()
//                if (preferences.getUserProfile().isVip) {
//                    startActivity(Intent(this, ShareActivity::class.java))
//                } else {
//                    openShareFunction()
//                }
            }, splashDelay)
        }

        b.setCancelable(false)
        b.show()

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        val dialogWindowWidth = (displayWidth * 1f).toInt()
        val dialogWindowHeight = (displayHeight * 1f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        b.window.attributes = layoutParams
    }

    /*---------------------------------------CALENDAR---------------------------------------------*/
    private fun openCalendarPopup(totalMonthDays: Int, currentDay: Int) {
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_home_redeem_points, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation

        val ivCloseCalendar = dialogView.findViewById<View>(R.id.ivCloseCalendar) as ImageView
        val layoutRedeemPoints = dialogView.findViewById<View>(R.id.layoutRedeemPoints) as RelativeLayout
        val layoutAlreadyRedeemPoints = dialogView.findViewById<View>(R.id.layoutAlreadyRedeemPoints) as RelativeLayout

        val rvDays = dialogView.findViewById<View>(R.id.rvDays) as RecyclerView

        rvDays.layoutManager = GridLayoutManager(this, 5)
        adapter = CalendarAdapter(this, totalMonthDays, currentDay)
        rvDays.adapter = adapter
        adapter!!.setOnItemClickListener(object : CalendarAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                if (position + 1 > currentDay) {
                    layoutAlreadyRedeemPoints.visibility = View.VISIBLE
                    layoutRedeemPoints.visibility = View.GONE
                } else {
                    layoutRedeemPoints.visibility = View.VISIBLE
                    layoutAlreadyRedeemPoints.visibility = View.GONE
                }
            }
        })

        ivCloseCalendar.setOnClickListener {
            b.dismiss()
            openAfterRatePopupSmile()
        }

        b.setCancelable(false)
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.9f).toInt()
        val dialogWindowHeight = (displayHeight * 0.9f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        b.window.attributes = layoutParams

        if (!isFinishing) {
            b.show()
        }
    }

    /*------------------------------------SUBSCRIPTION--------------------------------------------*/
    private fun openSubscriptionPopup() {
        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_subscription, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation

        val subscriptionTerms = dialogView.findViewById<View>(R.id.tvSubscriptionTerms) as TextView
        customSubscriptionTerms(subscriptionTerms)

        val closeSubscription = dialogView.findViewById<View>(R.id.ivCloseSubscription) as ImageView
        val btnSubscribe = dialogView.findViewById<View>(R.id.btnSubscribe) as TextView
        closeSubscription.setOnClickListener {
            b.dismiss()
        }

        btnSubscribe.setOnClickListener {
            b.dismiss()
            openRateUsPopup()
        }

        b.setCancelable(false)
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.9f).toInt()
        val dialogWindowHeight = (displayHeight * 0.8f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        b.window.attributes = layoutParams

        if (!isFinishing) {
            b.show()
        }
    }

    /*-----------------------------------RATE US--------------------------------------------------*/
    private fun openRateUsPopup() {
        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_home_rate_us, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation

        val ivCloseRateUs = dialogView.findViewById<View>(R.id.ivCloseRateUs) as ImageView
        ivCloseRateUs.setOnClickListener {
            b.dismiss()
        }
        b.setCancelable(false)
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.9f).toInt()
        val dialogWindowHeight = (displayHeight * 0.8f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        b.window.attributes = layoutParams

        if (!isFinishing) {
            b.show()
        }
    }

    /*--------------------------------------------------------------------------------------------*/
    private fun customSubscriptionTerms(view: TextView) {
        val terms = getString(R.string.subscription_terms)
        val conditions = getString(R.string.termsConditions)

        val spanTxt = SpannableStringBuilder(terms)

        spanTxt.setSpan(UnderlineSpan(), terms.indexOf(conditions),
                terms.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spanTxt.setSpan(ForegroundColorSpan(ContextCompat.getColor(this, R.color.terms_textColor)),
                terms.indexOf(conditions), spanTxt.length, 0)
        view.movementMethod = LinkMovementMethod.getInstance()
        view.setText(spanTxt, TextView.BufferType.SPANNABLE)
    }

    /*------------------------------------------FIRST---------------------------------------------*/
    private fun openAfterRatePopupSmile() {
        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_on_close_rate_us_first, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation

        val ivCloseSmile = dialogView.findViewById<View>(R.id.ivCloseSmile) as ImageView
        val rateNowSmile = dialogView.findViewById<View>(R.id.rateNowSmile) as TextView
        val tvDecline = dialogView.findViewById<View>(R.id.tvDecline) as TextView

        ivCloseSmile.setOnClickListener {
            b.dismiss()
        }

        rateNowSmile.setOnClickListener {
            b.dismiss()
            openAfterRatePopupSad() //second
        }

        tvDecline.setOnClickListener {
            b.dismiss()
        }

        b.setCancelable(false)
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.8f).toInt()
        val dialogWindowHeight = (displayHeight * 0.8f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        b.window.attributes = layoutParams

        if (!isFinishing) {
            b.show()
        }
    }

    /*------------------------------------SECOND--------------------------------------------------*/
    private fun openAfterRatePopupSad() {
        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_on_close_rate_us_second, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation

        val ivCloseSad = dialogView.findViewById<View>(R.id.ivCloseSad) as ImageView
        val rateNowSad = dialogView.findViewById<View>(R.id.rateNowSad) as TextView
        val tvDecline = dialogView.findViewById<View>(R.id.tvDecline) as TextView

        ivCloseSad.setOnClickListener {
            b.dismiss()
        }

        rateNowSad.setOnClickListener {
            b.dismiss()
            openAfterRatePopup() // third
        }

        tvDecline.setOnClickListener {
            b.dismiss()
        }

        b.setCancelable(false)
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.8f).toInt()
        val dialogWindowHeight = (displayHeight * 0.8f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        b.window.attributes = layoutParams

        if (!isFinishing) {
            b.show()
        }
    }

    /*----------------------------------------THIRD-----------------------------------------------*/
    private fun openAfterRatePopup() {
        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_on_close_rate_us_third, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation

        val closeSubscription = dialogView.findViewById<View>(R.id.ivCloseAfterRate) as ImageView
        val rateNowScared = dialogView.findViewById<View>(R.id.rateNowScared) as TextView
        val tvDecline = dialogView.findViewById<View>(R.id.tvDecline) as TextView

        closeSubscription.setOnClickListener {
            b.dismiss()
        }

        rateNowScared.setOnClickListener {
            b.dismiss()
            openSubscriptionPopup()
        }

        tvDecline.setOnClickListener {
            b.dismiss()
        }

        b.setCancelable(false)
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.8f).toInt()
        val dialogWindowHeight = (displayHeight * 0.8f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        b.window.attributes = layoutParams

        if (!isFinishing) {
            b.show()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private var invalidDialog: AlertDialog? = null

    override fun invalidLogin() {

        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.invalid_login))
        builder.setMessage(getString(R.string.invalid_login_error))

        // add a button
        builder.setPositiveButton(getString(R.string.log_in)) { dialog, _ ->
            presenter.clear()
            startActivity(Intent(this, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            finish()
            overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
            dialog.dismiss()
        }

        if (!isFinishing && window.decorView.isShown) {
            invalidDialog = builder.create()
            invalidDialog!!.setCancelable(false)
            invalidDialog!!.show()
        }
    }

    override fun clearPreferences() {
        startActivity(Intent(this, LoginActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        finish()
    }

    private fun openShareFunction() {
        val dialog = Dialog(this, R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.popup_vip)
        dialog.setCanceledOnTouchOutside(false)
        dialog.findViewById<View>(R.id.btn_share).setOnClickListener {
            // presenter.postPayment(true)
            dialog.dismiss()
        }
        dialog.show()
    }

    /*-------------------------------------Verification Dialog------------------------------------*/
    override fun openVerificationCode(verificationCode: Int) {
        verificationDialog(verificationCode)
    }

    override fun hideProgressLoading() {
        hideHomeProgess()
    }

    private fun verificationDialog(verificationCode: Int) {
        val dialog = Dialog(this, R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.popup_verification)
        val one = dialog.findViewById<View>(R.id.et_one) as EditText
        val two = dialog.findViewById<View>(R.id.et_two) as EditText
        val three = dialog.findViewById<View>(R.id.et_three) as EditText
        val four = dialog.findViewById<View>(R.id.et_four) as EditText
        val img_back = dialog.findViewById<View>(R.id.img_back) as ImageView

        img_back.setOnClickListener {
            dialog.dismiss()
        }

        var code: String = ""
        one.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length == 1) {
                    // code += one.text.toString()
                    one.clearFocus()
                    two.requestFocus()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        })
        two.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length == 1) {
                    //code += two.text.toString()
                    two.clearFocus()
                    three.requestFocus()
                }

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        })
        three.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length == 1) {
                    //code += three.text.toString()
                    three.clearFocus()
                    four.requestFocus()
                }

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        })
        four.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length == 1) {
                    //code += four.text.toString()
                    hideKeyboardTask()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        })
        dialog.findViewById<View>(R.id.btnSend).setOnClickListener {
            four.clearFocus()
            code = one.text.toString().trim() + two.text.toString().trim() + three.text.toString().trim() + four.text.toString().trim()
            if (code.isEmpty() || code.length < 4) {
                Toast.makeText(this, "Please enter correct otp.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (!isFinishing && window.decorView.isShown) {
                if (code.toInt() == verificationCode) {
                    startActivity(Intent(this, EnterBroadcastingDetailsActivity::class.java))
                    dialog.dismiss()
                } else {
                    showToast("Please enter correct otp...")
                }
            }
        }
        hideHomeProgess()

        Handler().postDelayed({
            one.clearFocus()
            two.clearFocus()
            three.clearFocus()
            four.clearFocus()
            hideKeyboardTask()
            four.setText(((verificationCode / 1) % 10).toString())
            three.setText(((verificationCode / 10) % 10).toString())
            two.setText(((verificationCode / 100) % 10).toString())
            one.setText(((verificationCode / 1000) % 10).toString())
        },2000)


        if (!isFinishing) {
            dialog.show()
        }

    }

    fun hideKeyboardTask() {
        val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = this.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

//    override fun openSharePage() {
//        startActivity(Intent(this, ShareActivity::class.java))
//    }

    /*--------------------------------------SOCKET------------------------------------------------*/
    private var mSocket: Socket? = null
    private var isConnected: Boolean? = true

    private fun setUpSocket() {
        val app = this.application as App
        mSocket = app.socket
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
        mSocket!!.on("RECEIVE_AUTHORIZED", isAuthorized)
//        mSocket!!.on("RECEIVE_ELITE", onGetEliteUser)
        mSocket!!.connect()
    }

    private val onConnect = Emitter.Listener {
        this.runOnUiThread {
            if (!isConnected!!) {
                isConnected = true
            }
        }
    }

    private val onDisconnect = Emitter.Listener {
        this.runOnUiThread {
            Log.i("socket", "disconnected")
            isConnected = false
        }
    }

    private val onConnectError = Emitter.Listener {
        this.runOnUiThread {
            Log.e("socket", "Error connecting")
        }
    }

    private val isAuthorized = Emitter.Listener { args ->
        this.runOnUiThread {
            try {
                val data = args[0] as JSONObject
                val userId = data.getString("userId")
                val isAuthorized = data.getBoolean("isAuthorized")
                val userProfileData = preferences.getUserProfile()
                if (userId == preferences.getUserProfile().id) {
                    if (isAuthorized) {
                        userProfileData.isAuthorized = isAuthorized
                        preferences.setUserProfile(userProfileData)
                        val builder = AlertDialog.Builder(this)
                        builder.setMessage("You are officially authorized now.")
                        // add a button
                        builder.setPositiveButton(getString(R.string.ok_btn)) { dialog, _ ->
                            dialog.dismiss()
                        }
                        val dialog = builder.create()
                        dialog.setCancelable(false)
                        dialog.show()
                    } else {
                        userProfileData.isAuthorized = isAuthorized
                        preferences.setUserProfile(userProfileData)
                        val builder = AlertDialog.Builder(this)
                        builder.setMessage(getString(R.string.unauthorised_user))

                        // add a button
                        builder.setPositiveButton(getString(R.string.ok_btn)) { dialog, _ ->
                            dialog.dismiss()
                        }
                        val dialog = builder.create()
                        dialog.setCancelable(false)
                        dialog.show()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun leave() {
        mSocket!!.disconnect()
        mSocket!!.connect()
    }

    override fun onDestroy() {
        if (invalidDialog != null) {
            invalidDialog!!.dismiss()
            invalidDialog = null
        }
        leave()
        super.onDestroy()
    }

    private fun showHomeProgess() {
        RuthUtility.showLoader(this@HomeActivity)
    }

    private fun hideHomeProgess() {
        RuthUtility.hideLoader()
    }

    override fun savePoints() {
//        if (preferences.getUserProfile().pointEarn!!.isNotEmpty()) {
//            for (i in 0 until preferences.getUserProfile().pointEarn?.size!!) {
//                if (preferences.getUserProfile().pointEarn!![i].id.equals(getString(R.string.gold_points), true)) {
//                    goldPoints = preferences.getUserProfile().pointEarn!![i].total
//                }
//
//                if (preferences.getUserProfile().pointEarn!![i].id.equals(getString(R.string.silver_points), true)) {
//                    silverPoints = preferences.getUserProfile().pointEarn!![i].total
//                }
//
//                if (preferences.getUserProfile().pointEarn!![i].id.equals(getString(R.string.coin_points), true)) {
//                    yuvanPoints = preferences.getUserProfile().pointEarn!![i].total
//                }
//            }
//        }
    }

//    private val onGetEliteUser = Emitter.Listener { args ->
//        this.runOnUiThread {
//            try {
//                val data = args[0] as JSONArray
//                var liveUsers: JSONObject
//                var giftData: JSONObject
//                var liveViewers : LiveUsers
//
//                for (i in 0 until data.length()) {
//                    val jsonData = data[i] as JSONObject
//                    liveUsers = jsonData.getJSONObject("giftId")
//                    giftData = jsonData.getJSONObject("userId")
//                    Log.d("live users", " -> $liveUsers gift data -> $giftData")
//                }
//
//                Log.d("live_user", " -> $data")
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//        }
//    }
}