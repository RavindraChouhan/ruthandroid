package com.ruth.app.home.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class FavoriteFragmentPresenter @Inject constructor() : BasePresenter<FavoriteFragmentPresenter.FavoriteFragmentView>() {

    interface FavoriteFragmentView : BaseView {

    }
}