package com.ruth.app.home.userprofileview.share

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import com.ruth.app.home.adapter.FollowersAdapter
import kotlinx.android.synthetic.main.item_share_view.view.*
import java.util.*

class ShareListAdapter(val mContext: Context?,
                       var summonList: ArrayList<Share>?) : RecyclerView.Adapter<ShareListAdapter.SummonViewHolder>() {

    var list: ArrayList<Int> = ArrayList()
    private lateinit var recyclerView: RecyclerView
    private var onItemClicklistener: OnItemClickListener? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SummonViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_share_view, parent, false)
        return SummonViewHolder(view)
    }

    override fun onBindViewHolder(holder: SummonViewHolder, position: Int) {
        holder.bind(summonList?.get(position)!!)

        holder.itemView.rvFollowersList.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        holder.itemView.rvFollowersList.adapter = FollowersAdapter(mContext, list)

        when {
            summonList!![position].shareType == "Videos" -> {
                holder.itemView.ivPlay.visibility = View.VISIBLE
                holder.itemView.tvVideoTime.visibility = View.VISIBLE
            }
            summonList!![position].shareType == "Podcasts" -> {
                holder.itemView.ivPlay.visibility = View.VISIBLE
                holder.itemView.tvVideoTime.visibility = View.VISIBLE
            }
            summonList!![position].shareType == "Pictures" -> {
                holder.itemView.ivPlay.visibility = View.GONE
                holder.itemView.tvVideoTime.visibility = View.GONE
            }
            summonList!![position].shareType == "Articles" -> {
                holder.itemView.ivPlay.visibility = View.GONE
                holder.itemView.tvVideoTime.visibility = View.GONE
            }
        }
    }

    override fun getItemCount(): Int {
        return summonList?.size!!
    }

    inner class SummonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(summon: Share) {

        }
    }

    fun setOnItemClickListener(onItemClick: OnItemClickListener) {
        this.onItemClicklistener = onItemClick
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}
