package com.ruth.app.home.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import kotlinx.android.synthetic.main.item_option_cell.view.*

class OptionsAdapter(val mContext: Context) : RecyclerView.Adapter<OptionsAdapter.ViewHolder>() {
    val list = mutableListOf("邀請好友", "邀請好", "邀友","好友","邀請好友","邀請請好友")

    override fun getItemCount(): Int {
        return list.size
    }

    private var lastPosition: Int = -1

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
        holder.itemView.tv_option.text = list[position]
        if (lastPosition == position){
            holder.itemView.tv_option.setTextColor(mContext.resources.getColor(R.color.white))
            holder.itemView.main_view.setBackgroundResource(R.drawable.option_back_filled)
        }else {
            holder.itemView.main_view.setBackgroundResource(R.drawable.option_back_blank)
            holder.itemView.tv_option.setTextColor(mContext.resources.getColor(R.color.text_light))
        }
        holder.itemView.setOnClickListener {
            lastPosition = position
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_option_cell, parent, false)
        return ViewHolder(view)
    }

    inner class ViewHolder(view: View) :RecyclerView.ViewHolder(view){
        fun bind(position: Int) {
        }


    }
}
