package com.ruth.app.home.eightbutton.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class RuthKnowsFragmentPresenter @Inject constructor(): BasePresenter<RuthKnowsFragmentPresenter.RuthKnowsFragmentPresenterView>() {
    interface RuthKnowsFragmentPresenterView :BaseView{

    }
}