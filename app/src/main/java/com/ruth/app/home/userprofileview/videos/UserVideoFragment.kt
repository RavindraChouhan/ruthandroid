package com.ruth.app.home.userprofileview.videos

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import kotlinx.android.synthetic.main.fragment_recyclerview.view.*

class UserVideoFragment : Fragment() {

//    private val summonList: ArrayList<Summon>? by lazy { arguments?.getParcelableArrayList<Summon>(SUMMON) }
    private var adapter: UserVideoListAdapter? = null

    companion object {
        const val SUMMON = "FAQ"
    }

    /* fun newInstance(data: ArrayList<Summon>): UserVideoFragment {
         val args = Bundle()
         args.putParcelableArrayList(SUMMON, data)
         return UserVideoFragment().withArguments(args)
     }*/

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_recyclerview, container, false)

        view.rvUserTabList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        adapter = UserVideoListAdapter(this.activity)
        view.rvUserTabList!!.adapter = adapter
        adapter!!.setOnItemClickListener(object : UserVideoListAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {

            }
        })
        return view
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            fragmentManager!!.beginTransaction().detach(this).attach(this).commit()
        }
    }
}