package com.ruth.app.home.userprofileview.userPurchaseActivity

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import com.ruth.app.home.userprofileview.share.Share
import java.util.*

class PurchaseListAdapter(val mContext: Context?/*,
                          var summonList: ArrayList<Share>?*/) : RecyclerView.Adapter<PurchaseListAdapter.PurchaseViewHolder>() {

    var list: ArrayList<Int> = ArrayList()
    private lateinit var recyclerView: RecyclerView
    private var onItemClicklistener: OnItemClickListener? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PurchaseViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_purchase_view, parent, false)
        return PurchaseViewHolder(view)
    }

    override fun onBindViewHolder(holder: PurchaseViewHolder, position: Int) {
        /* holder.bind(summonList?.get(position)!!)*/



    }

    override fun getItemCount(): Int {
        return 2
    }

    inner class PurchaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(summon: Share) {

        }
    }

    fun setOnItemClickListener(onItemClick: OnItemClickListener) {
        this.onItemClicklistener = onItemClick
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}
