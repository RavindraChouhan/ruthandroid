package com.ruth.app.home.userprofileview.backpack

import android.graphics.drawable.Drawable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BackpackStickers(var titleName: String,
                       @SerializedName("stickerData")
                       @Expose
                       var stickersData: ArrayList<BackpackStickersData>? = null)

class BackpackStickersData(var stickerImage: Drawable,
                           var stickerName: String,
                           var giftType: String,
                           var giftValue: String)