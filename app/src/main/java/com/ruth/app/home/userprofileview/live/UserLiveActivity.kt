package com.ruth.app.home.userprofileview.live

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ruth.app.R
import com.ruth.app.utility.Utility
import kotlinx.android.synthetic.main.activity_user_live_video.*
import javax.inject.Inject

class UserLiveActivity : AppCompatActivity(), View.OnClickListener {

    @Inject
    lateinit var utility: Utility
    private var adapter: UserLiveListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_live_video)

        tvHeader.text = getString(R.string.live)
        ivUserTabBack.setOnClickListener(this)

        setListData()
    }

    override fun onClick(v: View?) {
        if (v == ivUserTabBack) {
            finish()
        }
    }

    private fun setListData() {
        adapter = UserLiveListAdapter()
        rvLiveVideos!!.adapter = adapter
        adapter!!.setOnItemClickListener(object : UserLiveListAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {

            }
        })
    }

}