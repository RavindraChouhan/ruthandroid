package com.ruth.app.home.userprofileview.userPurchaseActivity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import kotlinx.android.synthetic.main.fragment_recyclerview.view.*

class PurchaseFragment : Fragment() {

    private var adapter: PurchaseListAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_recyclerview, container, false)

        view.rvUserTabList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        adapter = PurchaseListAdapter(activity/*, summonList!!*/)
        view.rvUserTabList!!.adapter = adapter
        adapter!!.setOnItemClickListener(object : PurchaseListAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {

            }
        })
        return view
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            fragmentManager!!.beginTransaction().detach(this).attach(this).commit()
        }
    }
}