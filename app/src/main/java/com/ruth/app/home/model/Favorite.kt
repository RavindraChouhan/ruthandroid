package com.ruth.app.home.model

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parceler
import kotlinx.android.parcel.Parcelize

@Parcelize
class Favorite(var name: String?,
               var userImage: Drawable? = null,
               var fansCount: Int?,
               var isSelected: Boolean = true) : Parcelable {

    companion object : Parceler<Favorite> {
        // private val mMyDrawable: Drawable? = null

        override fun Favorite.write(out: Parcel, flags: Int) {
            // Convert Drawable to Bitmap first:
            val bitmap = (userImage as BitmapDrawable).getBitmap() as Bitmap
            // Serialize bitmap as Parcelable:
            out.writeParcelable(bitmap, flags)
        }

        override fun create(parcel: Parcel): Favorite = TODO()
    }
}