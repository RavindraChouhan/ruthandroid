package com.ruth.app.home.eightbutton.adapter

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import com.ruth.app.home.adapter.FollowersAdapter
import kotlinx.android.synthetic.main.item_latest_story.view.*

class StoryLatestAdapter(val mContext: Context) : RecyclerView.Adapter<StoryLatestAdapter.ViewHolder>() {
    var list: ArrayList<Int> = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_latest_story, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
        holder.itemView.recycle_follower.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        holder.itemView.recycle_follower.adapter = FollowersAdapter(mContext,list)
        holder.itemView.imgLike.setOnClickListener {
            if (holder.itemView.imgLike.drawable.constantState == mContext.resources.getDrawable(R.drawable.icon_like).constantState){
                holder.itemView.imgLike.setImageDrawable(mContext.resources.getDrawable(R.drawable.icon_like_unselected))
            }else{
                holder.itemView.imgLike.setImageDrawable(mContext.resources.getDrawable(R.drawable.icon_like))
            }
        }
    }

    class ViewHolder(view:View):RecyclerView.ViewHolder(view) {
        fun bind(position: Int) {

        }

    }

}
