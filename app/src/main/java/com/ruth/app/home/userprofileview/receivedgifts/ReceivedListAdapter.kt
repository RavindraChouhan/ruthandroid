package com.ruth.app.home.userprofileview.receivedgifts

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R

class ReceivedListAdapter(val mContext: Context?) : RecyclerView.Adapter<ReceivedListAdapter.SummonViewHolder>() {

    var list: ArrayList<Int> = ArrayList()
    private lateinit var recyclerView: RecyclerView
    private var onItemClicklistener: OnItemClickListener? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SummonViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_received_gift_cell, parent, false)
        return SummonViewHolder(view)
    }

    override fun onBindViewHolder(holder: SummonViewHolder, position: Int) {

    }

    override fun getItemCount(): Int {
        return 2
    }

    inner class SummonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    fun setOnItemClickListener(onItemClick: OnItemClickListener) {
        this.onItemClicklistener = onItemClick
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}
