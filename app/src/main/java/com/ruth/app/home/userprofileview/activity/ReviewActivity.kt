package com.ruth.app.home.userprofileview.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ruth.app.R
import com.ruth.app.utility.Utility
import kotlinx.android.synthetic.main.activity_user_reviews.*
import javax.inject.Inject

class ReviewActivity : AppCompatActivity(), View.OnClickListener {

    @Inject
    lateinit var utility: Utility

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_reviews)

        etReviews.setOnTouchListener { _, _ ->
            etReviews.isCursorVisible = true
            false
        }
        ivReviewBack.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v == ivReviewBack) {
            finish()
        }
    }
}