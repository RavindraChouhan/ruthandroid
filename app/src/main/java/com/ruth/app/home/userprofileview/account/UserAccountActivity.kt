package com.ruth.app.home.userprofileview.account

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ruth.app.R
import kotlinx.android.synthetic.main.activity_user_account.*

class UserAccountActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_account)

        setUpTabLayout()
        ivBackAccount.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        finish()
    }

    private fun setUpTabLayout() {
        val adapter = UserAccountAdapter(supportFragmentManager, 4)
        viewPagerAccount.adapter = adapter
        tabAccount.setupWithViewPager(viewPagerAccount)

        tabAccount.getTabAt(0)!!.text = resources.getString(R.string.personal_info)
        tabAccount.getTabAt(1)!!.text = resources.getString(R.string.identify_verify)
        tabAccount.getTabAt(2)!!.text = resources.getString(R.string.levels)
        tabAccount.getTabAt(3)!!.text = resources.getString(R.string.referral_code)

        tabAccount.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                val position = tab?.position
                viewPagerAccount.setCurrentItem(position!!, true)
            }
        })
    }

}
