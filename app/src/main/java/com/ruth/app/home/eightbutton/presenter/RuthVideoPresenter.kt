package com.ruth.app.home.eightbutton.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class RuthVideoPresenter@Inject constructor() : BasePresenter<RuthVideoPresenter.RuthVidView>() {

    override fun onCreate(view: RuthVidView) {
        super.onCreate(view)

    }

    interface RuthVidView : BaseView {

    }
}