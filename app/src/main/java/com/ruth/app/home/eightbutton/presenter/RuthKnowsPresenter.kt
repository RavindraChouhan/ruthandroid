package com.ruth.app.home.eightbutton.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class RuthKnowsPresenter @Inject constructor() : BasePresenter<RuthKnowsPresenter.RuthKnowsPresenterView>() {

    interface RuthKnowsPresenterView : BaseView {

    }
}