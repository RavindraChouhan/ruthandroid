package com.ruth.app.home.userprofileview.share

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ruth.app.R
import com.ruth.app.utility.Utility
import kotlinx.android.synthetic.main.activity_user_viewpager_tab.*
import javax.inject.Inject

class UserShareActivity : AppCompatActivity(), View.OnClickListener {

    @Inject
    lateinit var utility: Utility
    private var shareViewPagerAdapter: ShareViewPagerAdapter? = null
    private var shareList: ArrayList<Share>? = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_viewpager_tab)

        tvHeader.text = getString(R.string.share)
        ivUserTabBack.setOnClickListener(this)

        setupTabLayout()
    }

    override fun onClick(v: View?) {
        if (v == ivUserTabBack) {
            finish()
        }
    }

    private fun setupTabLayout() {
        shareList?.clear()
        for (i in 0 until 5) {
            shareList?.add(Share("Videos"))
            shareList?.add(Share("Podcasts"))
            shareList?.add(Share("Pictures"))
            shareList?.add(Share("Articles"))
        }
        shareViewPagerAdapter = ShareViewPagerAdapter(supportFragmentManager, shareList)
        viewPagerUser.adapter = shareViewPagerAdapter
        tablayoutUserTab.setupWithViewPager(viewPagerUser)

        tablayoutUserTab.getTabAt(0)!!.setText(R.string.all)
        tablayoutUserTab.getTabAt(1)!!.setText(R.string.videos)
        tablayoutUserTab.getTabAt(2)!!.setText(R.string.podcast)
        tablayoutUserTab.getTabAt(3)!!.setText(R.string.pictures)
        tablayoutUserTab.getTabAt(4)!!.setText(R.string.articles)
        tablayoutUserTab.getTabAt(5)!!.setText(R.string.others)

        tablayoutUserTab.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                val position = tab.position

                when (position) {
                    0 -> {
                        shareList?.clear()
                        shareList?.add(Share("Videos"))
                        shareList?.add(Share("Podcasts"))
                        shareList?.add(Share("Pictures"))
                        shareList?.add(Share("Articles"))
                    }
                    1 -> {
                        shareList?.clear()
                        for (i in 0 until 10) {
                            shareList?.add(Share("Videos"))
                        }
                    }
                    2 -> {
                        shareList?.clear()
                        for (i in 0 until 6) {
                            shareList?.add(Share("Podcasts"))
                        }
                    }
                    3 -> {
                        shareList?.clear()
                        for (i in 0 until 3) {
                            shareList?.add(Share("Pictures"))
                        }
                    }
                    4 -> {
                        shareList?.clear()
                        for (i in 0 until 3) {
                            shareList?.add(Share("Articles"))
                        }
                    }
                }
            }
        })
    }
}
