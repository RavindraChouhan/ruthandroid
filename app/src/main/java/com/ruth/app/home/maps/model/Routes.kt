package com.ruth.app.home.eightbutton.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Routes {
    @SerializedName("overview_polyline")
    @Expose
    var overview_polyline: OverviewPolyline? = null
}

class OverviewPolyline {
    @SerializedName("points")
    @Expose
    var points: String? = null
}
