package com.ruth.app.home.maps.activity

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.home.eightbutton.adapter.MapFilterAdapter
import com.ruth.app.home.eightbutton.presenter.MapFilterPresenter
import kotlinx.android.synthetic.main.activity_filter.*

class FilterActivity : BaseActivity<MapFilterPresenter>(), MapFilterPresenter.MapFilterPresenterView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)
        App.component.inject(this)
        presenter.onCreate(this)
        rv_filtermap.layoutManager = GridLayoutManager(presenter.context, 4)
        val adapter = MapFilterAdapter(presenter.context)
        rv_filtermap.adapter = adapter
        btn_back.setOnClickListener {
            finish()
        }
    }

}
