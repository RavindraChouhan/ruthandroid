package com.ruth.app.home.eightbutton.videoplayer

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ruth.app.R
import kotlinx.android.synthetic.main.item_ruth_story_player.view.*

class RuthStoryPlayerAdapter(private val mContext: Context) : RecyclerView.Adapter<RuthStoryPlayerAdapter.ViewHolder>() {

    private lateinit var onItemClickListner: OnItemClickListner

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_ruth_story_player, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 5
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(/*broadcastData[position],*/position)

    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(/*channelBroadcastData: ChannelBroadcastData, */videoPlayerType: Int) {

            Glide.with(mContext).load(R.drawable.story_demo_img)
                    .apply(RequestOptions().error(R.drawable.story_demo_img)
                            .placeholder(R.drawable.story_demo_img)).into(itemView.ivStoryCover)
        }
    }


    fun setOnItemClickListner(onItemClickListner: OnItemClickListner) {
        this.onItemClickListner = onItemClickListner
    }

    interface OnItemClickListner {
        fun onItemClick(pos: Int, selectedId: String)
    }

}