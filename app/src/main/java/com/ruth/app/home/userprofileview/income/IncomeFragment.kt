package com.ruth.app.home.userprofileview.income


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.ruth.app.R
import kotlinx.android.synthetic.main.fragment_income.view.*

class IncomeFragment : Fragment() {


    fun newInstance(): Fragment {

        return IncomeFragment()
    }

    private var mContext: Context? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view  = inflater.inflate(R.layout.fragment_income, container, false)
        mContext = activity
        view.rv_income.layoutManager = LinearLayoutManager(mContext)
        view.rv_income.adapter = IncomeAdapter(mContext!!)
        return view
    }


}
