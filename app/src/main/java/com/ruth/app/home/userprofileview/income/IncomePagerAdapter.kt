package com.ruth.app.home.userprofileview.income


import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class IncomePagerAdapter(supportFragmentManager: FragmentManager?, tabCount: Int) : FragmentStatePagerAdapter(supportFragmentManager) {
    override fun getItem(position: Int): Fragment {
        return IncomeFragment().newInstance()
    }

    override fun getCount(): Int {
        return 2
    }



}
