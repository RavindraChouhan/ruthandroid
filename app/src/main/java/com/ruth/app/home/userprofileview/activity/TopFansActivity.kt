package com.ruth.app.home.userprofileview.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.ruth.app.R
import com.ruth.app.rank.adapter.PopularityAdapter
import kotlinx.android.synthetic.main.activity_top_fans.*

class TopFansActivity : AppCompatActivity(), View.OnClickListener {

    private var adapter: PopularityAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_top_fans)

        adapter = PopularityAdapter(this)
        rvFavoriteList!!.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvFavoriteList!!.adapter = adapter

        ivBackFans.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.ivBackFans){
            finish()
        }
    }
}
