package com.ruth.app.home.userprofileview.backpack

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class BackpackStickersAdapter(supportFragmentManager: FragmentManager,
                              var tabCounts: Int) : FragmentStatePagerAdapter(supportFragmentManager) {

    override fun getItem(position: Int): Fragment {
        return BackpackStickerFragment()
    }

    override fun getCount(): Int {
        return tabCounts
    }
}