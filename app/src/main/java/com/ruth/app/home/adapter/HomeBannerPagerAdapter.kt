package com.ruth.app.home.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.ruth.app.R
import java.util.*

class HomeBannerPagerAdapter(var context: Context, private var bannerlList: ArrayList<String>?) : PagerAdapter() {

    private var layoutInflater: LayoutInflater? = null

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        if (bannerlList?.size != 0) {
            return bannerlList!!.size
        }
        return 4
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = layoutInflater?.inflate(R.layout.item_home_banner, container, false)!!

        val ivImage = view.findViewById(R.id.ivHomeBanner) as ImageView
        val progressBarBanner = view.findViewById(R.id.progressBarBanner) as ProgressBar
        progressBarBanner.visibility = View.GONE
        if (bannerlList?.size != 0) {
            progressBarBanner.visibility = View.VISIBLE
            Glide.with(context).load(bannerlList!![position])
                    .apply(RequestOptions().error(R.drawable.background)
                            .placeholder(R.drawable.home_banner_header)
                            .diskCacheStrategy(DiskCacheStrategy.NONE))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            progressBarBanner.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            progressBarBanner.visibility = View.GONE
                            return false
                        }

                    }).into(ivImage)
        }
        container.addView(view, 0)
        return view
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    init {
        layoutInflater = LayoutInflater.from(context)
    }
}
