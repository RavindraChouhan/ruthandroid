package com.ruth.app.home.userprofileview.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ruth.app.R
import kotlinx.android.synthetic.main.activity_vip.*

class VIPActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vip)

        ivBackVip.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.ivBackVip) {
            finish()
        }
    }
}
