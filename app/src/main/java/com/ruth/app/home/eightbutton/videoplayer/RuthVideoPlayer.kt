package com.ruth.app.home.eightbutton.videoplayer

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.RelativeLayout
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.home.model.ChannelBroadcastData
import com.ruth.app.login.activity.LoginActivity
import com.ruth.app.video.presenter.VideoPlayerPresenter
import kotlinx.android.synthetic.main.activity_ruth_video_player.*
import kotlin.collections.ArrayList

class RuthVideoPlayer : BaseActivity<VideoPlayerPresenter>(), VideoPlayerPresenter.VideoPlayerView, View.OnClickListener {

    lateinit var context: Context
    private var position: Int = 0
    private var listMain: ArrayList<ChannelBroadcastData>? = null
    private var listUpdated: ArrayList<ChannelBroadcastData>? = null
    private lateinit var updateHandler: Handler
    private var isExpand: Boolean = false
    private var invalidDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
//        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)
        setContentView(R.layout.activity_ruth_video_player)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        context = this
        updateHandler = Handler()
        listMain = ArrayList()
        listUpdated = ArrayList()

        if (intent.getStringExtra("videoType").equals("RuthVideo", true)) {
            ivVideoImage.setImageResource(R.drawable.ghost_film)
        } else  if (intent.getStringExtra("videoType").equals("SitTalk", true)) {
            ivVideoImage.setImageResource(R.drawable.sitn_talk_dummy_icon)
        }

        val list : ArrayList<String> = ArrayList()
        for (i in 0 until 5){
            list.add(intent.getStringExtra("videoType"))
        }

        val adapter = RuthVideoPlayerAdapter(context, list)
        recycleVideos!!.adapter = adapter
        recycleVideos!!.adapter.notifyDataSetChanged()

        //Click listeners
        imgShow.setOnClickListener(this)
        imgFollow.setOnClickListener(this)
        lytBroadcastHead.setOnClickListener(this)
        imgExpand.setOnClickListener(this)
        layoutBack.setOnClickListener(this)
//        imgPlayVideo.setOnClickListener(this)

//        listMain = BroadcastDetailActivity.pastBroadcastList
//
//        videoId = listMain!![position].id
//        for (i in 1 until listMain!!.size) {
//            listUpdated!!.add(listMain!![i])
//        }
//        updateVideoList(listUpdated)
//
//        video_player.setOnCompletionListener {
//            if (switchAutoPlay.isChecked) {
//                imgPlayVideo.visibility = View.GONE
//                position += 1
//                if (position < listMain!!.size) {
//                    playNextVideo(position)
//                } else {
//                    position = 0
//                    updatePlayerNull()
//                    imgPlayVideo.setImageResource(R.drawable.exo_controls_play)
//                }
//            } else {
//                updatePlayerNull()
//                imgPlayVideo.setImageResource(R.drawable.exo_controls_play)
//            }
//        }
//
//        video_player.setOnErrorListener { mp, what, extra ->
//            return@setOnErrorListener false
//        }
//
//        video_player.setOnInfoListener(MediaPlayer.OnInfoListener { mp, what, extra ->
//            when (what) {
//                MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START -> {
//                    progressBar.visibility = View.GONE
//                    imgPlayVideo.visibility = View.VISIBLE
//                    if (updateHandler != null) {
//                        updateHandler.postDelayed(updateVideoTime, 1000)
//                    }
//                    return@OnInfoListener true
//                }
//                MediaPlayer.MEDIA_INFO_BUFFERING_START -> {
//                    progressBar.visibility = View.VISIBLE
//                    imgPlayVideo.visibility = View.GONE
//                    if (updateHandler != null) {
//                        updateHandler.removeCallbacks(updateVideoTime)
//                    }
//                    return@OnInfoListener true
//                }
//                MediaPlayer.MEDIA_INFO_BUFFERING_END -> {
//                    progressBar.visibility = View.GONE
//                    imgPlayVideo.visibility = View.VISIBLE
//                    if (updateHandler != null) {
//                        updateHandler.postDelayed(updateVideoTime, 1000)
//                    }
//                    return@OnInfoListener true
//                }
//            }
//            false
//        })
//
//        layoutSwipe.setOnRefreshListener {
//            layoutSwipe.isRefreshing = false
//        }
//
//        videoSeek.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
//            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
//                if (fromUser) {
//                    videoSeek.progress = progress
//                    video_player.seekTo(getTimeByProcess(progress, video_player.duration.toLong()))
//                }
//            }
//
//            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
//
//            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
//
//        })


    }

//    private fun playNextVideo(pos: Int) {
//        if (listMain!![pos].broadcastUrl != "") {
//            updatePlayerNull()
//            playVideo()
//        } else {
//            position = pos + 1
//            if (position < listMain!!.size) {
//                playNextVideo(position)
//            } else {
//                position = 0
//                updatePlayerNull()
//                imgPlayVideo.setImageResource(R.drawable.exo_controls_play)
//            }
//        }
//    }
//
//    private fun updatePlayerNull() {
//        lblStartTime.text = "00:00"
//        lblEndTime.text = "00:00"
//        videoSeek.progress = 0
//        listUpdated?.clear()
//        for (i in 0 until listMain!!.size) {
//            if (i != position) {
//                listUpdated!!.add(listMain!![i])
//            }
//        }
//        updateVideoList(listUpdated)
//        imgPlayVideo.visibility = View.VISIBLE
//        video_player.stopPlayback()
//        video_player.setVideoURI(null)
//    }
//
//    private fun updateVideoList(pastBroadcastList: ArrayList<ChannelBroadcastData>?) {
//
//        //ThumbNailClass().execute(pastBroadcastList!![position].broadcastUrl)
//
//        tvChannelTitle.text = listMain!![position].broadcastTitle
//        tvChannelViews.text = listMain!![position].broadcastViews.toString()
//        lblLikeCount.text = listMain!![position].broadcastLikes.toString()
//        lblScaryCount.text = listMain!![position].scaryCount.toString()
//
//        playingUrl = /*listMain!![position].broadcastUrl*/"http://184.72.239.149/vod/mp4:BigBuckBunny_115k.mov/playlist.m3u8"
//
//        Glide.with(context).load(listMain!![position].broadcasterData.photoUrl)
//                .apply(RequestOptions().error(R.drawable.logoeye).placeholder(R.drawable.background)).into(ivProfileImage)
//        tvChannelName.text = listMain!![position].broadcasterData.name
//        tvSubscriberCount.text = listMain!![position].broadcasterData.followers.toString() + " 名粉絲"
//
//        recycleVideos.layoutManager = LinearLayoutManager(presenter.context)
//        recycleVideos.hasFixedSize()
//        val adapter = VideoPlayerAdapter(presenter.context, pastBroadcastList!!)
//        recycleVideos.adapter = adapter
//
//        adapter.setOnItemClickListner(object : VideoPlayerAdapter.OnItemClickListner {
//            override fun onItemClick(pos: Int, selectedId: String) {
//                listUpdated?.clear()
//                for (i in 0 until listMain!!.size) {
//                    if (listMain!![i].id != selectedId) {
//                        listUpdated!!.add(listMain!![i])
//                    } else {
//                        position = i
//                    }
//                }
//                updateVideoList(listUpdated)
//                video_player.stopPlayback()
//                video_player.setVideoURI(null)
//                imgPlayVideo.setImageResource(R.drawable.exo_controls_play)
//            }
//
//        })
//    }

    override fun onClick(v: View?) {
        when (v) {
            imgShow -> {
                presenter.showHideDescription()
            }
            imgFollow -> {
                val isFollowed: Boolean = !listMain!![position].broadcasterData.isFollow
                if (listMain!![position].broadcasterData.isFollow) {
                    alert("Do you really want to unsubscribe?", "Already Subscribed", {
                        presenter.followUnfollow(listMain!![position].broadcasterData.id, isFollowed)
                        imgFollow.setImageResource(R.drawable.icon_follow_button)
                    }, {

                    })
                } else {
                    presenter.followUnfollow(listMain!![position].broadcasterData.id, isFollowed)
                    imgFollow.setImageResource(R.drawable.icon_follow)
                }
            }
            imgPlayVideo -> {
//                if (video_player.isPlaying) {
//                    imgPlayVideo.visibility = View.VISIBLE
//                    imgPlayVideo.setImageResource(R.drawable.exo_controls_play)
//                    stopPosition = video_player.currentPosition
//                    video_player.pause()
//                    if (updateHandler != null) {
//                        updateHandler.removeCallbacks(updateVideoTime)
//                    }
//                    Handler().postDelayed({ imgPlayVideo.visibility = View.GONE }, 3000)
//                } else {
//                    if (stopPosition > 0) {
//                        imgPlayVideo.visibility = View.VISIBLE
//                        imgPlayVideo.setImageResource(R.drawable.exo_controls_pause)
//                        video_player.seekTo(stopPosition)
//                        video_player.start()
//                        if (updateHandler != null) {
//                            updateHandler.postDelayed(updateVideoTime, 1000)
//                        }
//                        Handler().postDelayed({ imgPlayVideo.visibility = View.GONE }, 3000)
//                    } else {
//                        imgPlayVideo.visibility = View.GONE
//                        imgPlayVideo.setImageResource(R.drawable.exo_controls_pause)
//                        playingUrl = /*listMain!![position].broadcastUrl*/"http://184.72.239.149/vod/mp4:BigBuckBunny_115k.mov/playlist.m3u8"
//                        playVideo()
//                    }
//                }
            }
            lytBroadcastHead -> {
                imgPlayVideo.visibility = View.VISIBLE
                Handler().postDelayed({ imgPlayVideo.visibility = View.GONE }, 3000)
            }
            imgExpand -> {
                expandPlayer(isExpand)
            }
            layoutBack -> {
                if (isExpand) {
                    expandPlayer(isExpand)
                    return
                }
                finish()
                overridePendingTransition(R.anim.no_animation, R.anim.slide_down_dialog)
            }
        }
    }

    private fun expandPlayer(expand: Boolean) {
        if (expand) {
            isExpand = false
            layout.visibility = View.VISIBLE
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            val metrics = DisplayMetrics()
            this.windowManager.defaultDisplay.getMetrics(metrics)
            val params = lytBroadcastHead.layoutParams as android.widget.RelativeLayout.LayoutParams
            params.width = RelativeLayout.LayoutParams.MATCH_PARENT
            params.height = 500
            params.leftMargin = 0
            lytBroadcastHead.layoutParams = params
            imgExpand.setImageResource(R.drawable.ic_fullscreen_white_48dp)
        } else {
            isExpand = true
            layout.visibility = View.GONE
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
            val metrics = DisplayMetrics()
            this.windowManager.defaultDisplay.getMetrics(metrics)
            val params = lytBroadcastHead.layoutParams as android.widget.RelativeLayout.LayoutParams
            params.width = RelativeLayout.LayoutParams.MATCH_PARENT
            params.height = RelativeLayout.LayoutParams.MATCH_PARENT
            params.leftMargin = 0
            lytBroadcastHead.layoutParams = params
            imgExpand.setImageResource(R.drawable.ic_fullscreen_exit_white_48dp)
        }
    }

    override fun showHideDescription() {
        if (lytDescription!!.isExpanded) {
            lytDescription!!.collapse()
            val deg = 180f
            imgShow.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
        } else {
            lytDescription!!.expand()
            val deg = 0f
            imgShow.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
        }
    }

    override fun invalidLogin() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Invalid Login")
        builder.setMessage(getString(R.string.invalid_login_error))

        builder.setPositiveButton(getString(R.string.log_in)) { dialog, _ ->
            presenter.clearLogin()
            startActivity(Intent(this, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            finish()
            overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
            dialog.dismiss()
        }

        if (!isFinishing && window.decorView.isShown) {
            invalidDialog = builder.create()
            invalidDialog!!.setCancelable(false)
            invalidDialog!!.show()
        }
    }

    override fun clearPreferences() {
        startActivity(Intent(this, LoginActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        finish()
    }

//    private fun playVideo() {
//        progressBar.visibility = View.VISIBLE
//        val uri = Uri.parse(playingUrl)
//        video_player.setVideoURI(uri)
//        video_player.setOnPreparedListener {
//            progressBar.visibility = View.GONE
//            video_player.start()
//            lblStartTime.text = RuthUtility.convertSecondsToHMmSs(video_player.currentPosition.toLong())
//            lblEndTime.text = RuthUtility.convertSecondsToHMmSs(video_player.duration.toLong())
//            updateHandler.postDelayed(updateVideoTime, 1000)
//        }
//    }

//    private val updateVideoTime = object : Runnable {
//        override fun run() {
//            val currentDuration = getProgressPercentage(video_player.currentPosition.toLong(), video_player.duration.toLong())
//            videoSeek.progress = currentDuration
//            lblStartTime.text = RuthUtility.convertSecondsToHMmSs(video_player.currentPosition.toLong())
//            updateHandler.postDelayed(this, 1000)
//        }
//    }

//    fun getProgressPercentage(currentDuration: Long, totalDuration: Long): Int {
//        var percentage: Double? = 0.toDouble()
//        val currentSeconds = (currentDuration / 1000).toInt().toLong()
//        val totalSeconds = (totalDuration / 1000).toInt().toLong()
//        percentage = currentSeconds.toDouble() / totalSeconds * 100
//        return percentage.toInt()
//    }
//
//    fun getTimeByProcess(progress: Int, totalDuration: Long): Int {
//        return ((progress * totalDuration) / 100).toInt()
//    }

    override fun onBackPressed() {
        if (isExpand) {
            expandPlayer(isExpand)
            return
        }
        super.onBackPressed()
        overridePendingTransition(R.anim.no_animation, R.anim.slide_down_dialog)
    }

//    @SuppressLint("StaticFieldLeak")
//    inner class ThumbnailClass : AsyncTask<String, String, Bitmap?>() {
//        override fun doInBackground(vararg params: String?): Bitmap? {
//            var bitmap: Bitmap? = null
//            var mediaMetadataRetriever: MediaMetadataRetriever? = null
//            try {
//                mediaMetadataRetriever = MediaMetadataRetriever()
//                if (Build.VERSION.SDK_INT >= 14)
//                    mediaMetadataRetriever.setDataSource(params[0], HashMap<String, String>())
//                else
//                    mediaMetadataRetriever.setDataSource(params[0])
//                bitmap = mediaMetadataRetriever.frameAtTime
//            } catch (e: Exception) {
//                e.printStackTrace()
//                if (mediaMetadataRetriever != null) {
//                    mediaMetadataRetriever!!.release()
//                }
//            } finally {
//                if (mediaMetadataRetriever != null) {
//                    mediaMetadataRetriever!!.release()
//                }
//            }
//            return bitmap
//        }
//
//        override fun onPostExecute(result: Bitmap?) {
//            super.onPostExecute(result)
//            val bitmapDrawable = BitmapDrawable(result)
//            video_player.setBackgroundDrawable(bitmapDrawable)
//        }
//    }
}

