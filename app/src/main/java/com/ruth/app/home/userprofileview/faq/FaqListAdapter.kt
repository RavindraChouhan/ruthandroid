package com.ruth.app.home.userprofileview.faq

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import com.ruth.app.R
import kotlinx.android.synthetic.main.item_faq_cell.view.*
import java.util.*

class FaqListAdapter(private var faqList: ArrayList<Faq>?)
    : RecyclerView.Adapter<FaqListAdapter.FaqViewHolder>() {

    private lateinit var recyclerView: RecyclerView
    private var onItemClicklistener: OnItemClickListener? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FaqViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_faq_cell, parent, false)
        return FaqViewHolder(view)
    }

    override fun onBindViewHolder(holder: FaqViewHolder, position: Int) {
        holder.bind(faqList?.get(position)!!)

        holder.itemView.ivExpandAnswer.setOnClickListener {
            if (holder.itemView.expandAnswer!!.isExpanded) {
                holder.itemView.expandAnswer!!.collapse()
                val deg = 180f
                holder.itemView.ivExpandAnswer.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
            } else {
                holder.itemView.expandAnswer!!.expand()
                val deg = 0f
                holder.itemView.ivExpandAnswer.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
            }
        }
    }

    override fun getItemCount(): Int {
        return faqList?.size!!
    }

    inner class FaqViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(faq: Faq) {

            itemView.tvQuestion.text = faq.faqQuestion
            itemView.tvAnswer.text = faq.faqAnswer

        }
    }

    fun setOnItemClickListener(onItemClick: OnItemClickListener) {
        this.onItemClicklistener = onItemClick
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}
