package com.ruth.app.home.model

import com.google.gson.annotations.SerializedName

class HomeBanners( @SerializedName("status")
                   val status: Boolean,
                   @SerializedName("message")
                   val message: String,
                   @SerializedName("data")
                   val data: ArrayList<String>,
                   @SerializedName("zhHant")
                   val zhHant: String,
                   @SerializedName("zhHans")
                   val zhHans: String)