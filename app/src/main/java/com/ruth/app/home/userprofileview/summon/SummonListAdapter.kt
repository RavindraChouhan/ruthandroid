package com.ruth.app.home.userprofileview.summon

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import kotlinx.android.synthetic.main.item_summons_view.view.*
import java.util.*

class SummonListAdapter(private var summonList: ArrayList<Summon>?)
    : RecyclerView.Adapter<SummonListAdapter.SummonViewHolder>() {

    private lateinit var recyclerView: RecyclerView
    private var onItemClicklistener: OnItemClickListener? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SummonViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_summons_view, parent, false)
        return SummonViewHolder(view)
    }

    override fun onBindViewHolder(holder: SummonViewHolder, position: Int) {
        holder.bind(summonList?.get(position)!!)

        when {
            summonList!![position].summonType == "Summoning" -> {
                holder.itemView.layoutSummoning.visibility = View.VISIBLE
                holder.itemView.layoutSucceeded.visibility = View.GONE
                holder.itemView.layoutFailed.visibility = View.GONE
            }
            summonList!![position].summonType == "Succeeded" -> {
                holder.itemView.layoutSummoning.visibility = View.GONE
                holder.itemView.layoutSucceeded.visibility = View.VISIBLE
                holder.itemView.layoutFailed.visibility = View.GONE
            }
            summonList!![position].summonType == "Failed" -> {
                holder.itemView.layoutSummoning.visibility = View.GONE
                holder.itemView.layoutSucceeded.visibility = View.GONE
                holder.itemView.layoutFailed.visibility = View.VISIBLE
            }
        }
    }

    override fun getItemCount(): Int {
        return summonList?.size!!
    }

    inner class SummonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(summon: Summon) {

        }
    }

    fun setOnItemClickListener(onItemClick: OnItemClickListener) {
        this.onItemClicklistener = onItemClick
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}
