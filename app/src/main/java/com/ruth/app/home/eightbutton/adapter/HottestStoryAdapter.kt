package com.ruth.app.home.eightbutton.adapter

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import com.ruth.app.home.adapter.FollowersAdapter
import kotlinx.android.synthetic.main.item_hottest_story.view.*

class HottestStoryAdapter(val mContext: Context) : RecyclerView.Adapter<HottestStoryAdapter.ViewHolder>() {

    private var list: ArrayList<Int> = ArrayList()
    private var onItemClicklistener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_hottest_story, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
        holder.itemView.recycle_followers.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        holder.itemView.recycle_followers.adapter = FollowersAdapter(mContext,list)

        holder.itemView.setOnClickListener{
            onItemClicklistener!!.onItemClick(holder.itemView, position)
        }

    }

    override fun getItemCount(): Int {
        return 10
    }

    class ViewHolder(view: View):RecyclerView.ViewHolder(view) {
        fun bind(position: Int) {

        }
    }

    fun setOnItemClickListener(onItemClick: OnItemClickListener) {
        this.onItemClicklistener = onItemClick
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

}
