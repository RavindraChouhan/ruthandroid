package com.ruth.app.home.eightbutton.activity

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AccelerateDecelerateInterpolator
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.home.adapter.FollowersAdapter
import com.ruth.app.home.eightbutton.presenter.BibleDetailPresenter
import com.ruth.app.widgets.expandable.ExpandableLayout
import kotlinx.android.synthetic.main.activity_bible_detail.*

class BibleDetailActivity : BaseActivity<BibleDetailPresenter>(), BibleDetailPresenter.BibleDetailPresenterView, View.OnClickListener {

    var list: ArrayList<Int> = ArrayList()

    private var expandableLayout: ExpandableLayout? = null
    var count: Int = 0
    var isFirstTimeLike: Boolean = false

    private var mWindow: Window? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mWindow = window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        setContentView(R.layout.activity_bible_detail)
        App.getComponent().inject(this)
        presenter.onCreate(this)
        expandableLayout = findViewById<View>(R.id.lytDescription) as ExpandableLayout

        imgLike.setOnClickListener(this)
        imgShow.setOnClickListener(this)
        imgBack.setOnClickListener(this)
        imgSharing.setOnClickListener(this)

        recycle_follower.layoutManager = LinearLayoutManager(presenter.context, LinearLayoutManager.HORIZONTAL, false)
        recycle_follower.adapter = FollowersAdapter(presenter.context, list)
    }

    override fun onClick(v: View?) {
        when (v) {
            imgLike -> {
                likeBroadCast()
                return
            }
            imgShow -> {
                showHideDescription()
                return
            }
            imgBack -> {
                finish()
                return
            }
            imgSharing -> {
                return
            }
        }
    }

    private fun showHideDescription() {
        if (expandableLayout!!.isExpanded) {
            expandableLayout!!.collapse()
            val deg = 180f
            imgShow.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
        } else {
            expandableLayout!!.expand()
            val deg = 0f
            imgShow.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
        }
    }


    private fun likeBroadCast() {
        if (!isFirstTimeLike) {
            isFirstTimeLike = true
            count = lblLikeCount.text.toString().toInt()
            count++
            lblLikeCount.text = count.toString()
            imgLike.setImageResource(R.drawable.icon_like)
            count = 0
        }
    }

}
