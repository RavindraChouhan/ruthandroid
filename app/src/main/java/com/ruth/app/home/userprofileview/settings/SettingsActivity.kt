package com.ruth.app.home.userprofileview.settings

import android.os.Bundle
import android.view.View
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : BaseActivity<SettingsPresenter>(), SettingsPresenter.SettingView, View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        App.getComponent().inject(this)

        ivSettingsBack.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v == ivSettingsBack) {
            finish()
        }
    }

}
