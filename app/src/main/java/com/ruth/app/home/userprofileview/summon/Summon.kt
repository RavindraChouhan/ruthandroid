package com.ruth.app.home.userprofileview.summon

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Summon(var summonType: String) : Parcelable