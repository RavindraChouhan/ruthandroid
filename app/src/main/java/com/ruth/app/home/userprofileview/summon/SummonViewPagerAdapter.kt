package com.ruth.app.home.userprofileview.summon

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class SummonViewPagerAdapter(fm: FragmentManager?, var summonList: ArrayList<Summon>?) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {

        return AllFragment().newInstance(summonList!!)
    }

    override fun getCount(): Int {
        return 4
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return super.getPageTitle(position)
    }

//    override fun saveState(): Parcelable {
//        val bundle = saveState() as Bundle
//        bundle.putParcelableArray("states", null) // Never maintain any states from the base class to avoid TransactionTooLargeException
//        return bundle
//    }
}