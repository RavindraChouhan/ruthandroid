package com.ruth.app.home.userprofileview.faq

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import com.ruth.app.utility.withArguments
import kotlinx.android.synthetic.main.fragment_list_favorite.view.*

class FaqListFragment : Fragment() {

    private val faqList: ArrayList<Faq>? by lazy { arguments?.getParcelableArrayList<Faq>(FAQ) }
    private var adapter: FaqListAdapter? = null

    companion object {
        const val FAQ = "FAQ"
    }

    fun newInstance(data: ArrayList<Faq>): FaqListFragment {
        val args = Bundle()
        args.putParcelableArrayList(FAQ, data)
        return FaqListFragment().withArguments(args)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_list_favorite, container, false)

        view.rvFavoriteList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        adapter = FaqListAdapter(faqList!!)
        view.rvFavoriteList!!.adapter = adapter
        adapter!!.setOnItemClickListener(object : FaqListAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {

            }
        })
        return view
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            fragmentManager!!.beginTransaction().detach(this).attach(this).commit()
        }
    }
}