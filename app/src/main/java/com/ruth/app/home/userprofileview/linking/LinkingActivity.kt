package com.ruth.app.home.userprofileview.linking

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ruth.app.R
import kotlinx.android.synthetic.main.activity_linking.*

class LinkingActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_linking)

        setupTabLayout()
        ivBackLinking.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.ivBackLinking) {
            finish()
        }
    }

    private fun setupTabLayout() {

        val adapter = LinkingPagerAdapter(supportFragmentManager, 2)
        viewPagerLink!!.adapter = adapter
        tabLinking.setupWithViewPager(viewPagerLink)

        tabLinking.getTabAt(0)!!.setText(R.string.payment_method)
        tabLinking.getTabAt(1)!!.setText(R.string.social_media_accounts)

        tabLinking.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                val position = tab.position
                viewPagerLink.setCurrentItem(position, true)
            }
        })
    }
}
