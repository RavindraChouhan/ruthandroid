package com.ruth.app.home.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.ruth.app.R
import com.ruth.app.home.model.EightButtonsData
import kotlinx.android.synthetic.main.item_home_eight_button.view.*
import org.jetbrains.anko.windowManager

class EightButtonAdapter(val context: Context, val data: ArrayList<EightButtonsData>) : RecyclerView.Adapter<EightButtonAdapter.ButtonViewHolder>() {

    private var onItemClicklistener: OnItemClickListner? = null

    private lateinit var recyclerView: RecyclerView
    var lastposition: Int = -1

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ButtonViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_home_eight_button, parent, false)
        return ButtonViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ButtonViewHolder, position: Int) {
        holder.bind(position, data[position])

        val displayMetrics = DisplayMetrics()
        context.windowManager.defaultDisplay.getMetrics(displayMetrics)
        val deviceWidth = displayMetrics.widthPixels / 2
        holder.itemView.ivEffectIcon.layoutParams.width = deviceWidth
        holder.itemView.ivEffectIconSelected.layoutParams.width = deviceWidth



        if (lastposition == position) {
            holder.itemView.tvButtonText.visibility = View.VISIBLE
            holder.itemView.tvButtonText.text = "困擾時刻"
            holder.itemView.ivIcon.visibility = View.INVISIBLE
            holder.itemView.ivEffectIconSelected.visibility = View.VISIBLE
            holder.itemView.ivEffectIcon.visibility = View.GONE
        } else {
            holder.itemView.tvButtonText.visibility = View.GONE
            holder.itemView.tvButtonText.text = "困擾時刻"
            holder.itemView.ivIcon.visibility = View.VISIBLE
            holder.itemView.ivEffectIconSelected.visibility = View.GONE
            holder.itemView.ivEffectIcon.visibility = View.VISIBLE
        }
    }

    inner class ButtonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int, eightButtonsData: EightButtonsData) {

            Glide.with(context).load(eightButtonsData.urls[0])
                    .apply(RequestOptions().error(R.drawable.logoeye)
                            .placeholder(R.drawable.background))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            itemView.progressBarEight.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            itemView.progressBarEight.visibility = View.GONE
                            return false
                        }

                    })
                    .into(itemView.ivEffectIcon)

            Glide.with(context).load(eightButtonsData.urls[1])
                    .apply(RequestOptions().error(R.drawable.logoeye)
                            .placeholder(R.drawable.background))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            itemView.progressBarEight.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            itemView.progressBarEight.visibility = View.GONE
                            return false
                        }

                    })
                    .into(itemView.ivEffectIconSelected)

            when (position) {
                0 -> {
                    itemView.ivIcon.setImageDrawable(context.resources.getDrawable(R.drawable.home_icon_haunting_hour))
                    itemView.ivDivider.setImageDrawable(context.resources.getDrawable(R.drawable.home_line_transparent_top))
                }
                1 -> {
                    itemView.ivIcon.setImageDrawable(context.resources.getDrawable(R.drawable.home_icon_walk_hunt))
                    itemView.ivDivider.setImageDrawable(context.resources.getDrawable(R.drawable.home_line_transparent_top))
                }
                2 -> {
                    itemView.ivIcon.setImageDrawable(context.resources.getDrawable(R.drawable.home_icon_ruth_video))
                    itemView.ivDivider.setImageDrawable(context.resources.getDrawable(R.drawable.home_line_transparent_top))
                }
                3 -> {
                    itemView.ivIcon.setImageDrawable(context.resources.getDrawable(R.drawable.home_icon_ruth_story))
                    itemView.ivDivider.setImageDrawable(context.resources.getDrawable(R.drawable.home_line_transparent_top))
                }
                4 -> {
                    itemView.ivIcon.setImageDrawable(context.resources.getDrawable(R.drawable.home_icon_ruth_knows))
                    itemView.ivDivider.setImageDrawable(context.resources.getDrawable(R.drawable.home_line_transparent_top))
                }
                5 -> {
                    itemView.ivIcon.setImageDrawable(context.resources.getDrawable(R.drawable.home_icon_sit_talk))
                    itemView.ivDivider.setImageDrawable(context.resources.getDrawable(R.drawable.home_line_transparent_top))
                }
                6 -> {
                    itemView.ivIcon.setImageDrawable(context.resources.getDrawable(R.drawable.home_icon_bible))
                    itemView.ivDivider.setImageDrawable(context.resources.getDrawable(R.drawable.home_line_transparent_top))
                }
                7 -> {
                    itemView.ivIcon.setImageDrawable(context.resources.getDrawable(R.drawable.home_icon_vip_share))
                    itemView.ivDivider.setImageDrawable(context.resources.getDrawable(R.drawable.home_line_transparent_top))
                }
            }

            itemView.button_layout.setOnClickListener(View.OnClickListener
            {

                notifyItemChanged(lastposition)
                lastposition = adapterPosition
                notifyItemChanged(lastposition)

                onItemClicklistener!!.onItemClick(itemView, adapterPosition)

            })
        }
    }

    fun setOnItemClickListner(onItemClickList: OnItemClickListner) {
        this.onItemClicklistener = onItemClickList
    }

    interface OnItemClickListner {
        fun onItemClick(view: View, position: Int)
    }
}


