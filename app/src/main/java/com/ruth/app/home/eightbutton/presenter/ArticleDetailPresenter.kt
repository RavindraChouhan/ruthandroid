package com.ruth.app.home.eightbutton.presenter

import android.util.Log
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.network.config.RuthApi
import java.util.ArrayList
import javax.inject.Inject

class ArticleDetailPresenter @Inject constructor(val ruthApi: RuthApi) : BasePresenter<ArticleDetailPresenter.ArticleDetailView>() {

    fun bannersApi() {
        ruthApi.getHomeBanners()
                .onErrorReturn {
                    null
                }
                .doOnError {
                    Log.d("Error", "error" + it.message)
                }
                .subscribe {
                    if (it != null) {
                        if (it.status) {
                            this.view?.setTopBannerImages(it.data)
                        } else {
                            if (it.message.equals("Invalid device login", true)) {
                            }
                        }
                    }
                }
    }

    interface ArticleDetailView : BaseView{
        fun setTopBannerImages(data: ArrayList<String>)
    }
}