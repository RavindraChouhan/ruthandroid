package com.ruth.app.home.eightbutton.activity

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.home.adapter.HomeBannerPagerAdapter
import com.ruth.app.home.eightbutton.adapter.SitnTalkHottestAdapter
import com.ruth.app.home.eightbutton.adapter.SitnTalkLatestAdapter
import com.ruth.app.home.eightbutton.presenter.SitnTalkPresenter
import com.ruth.app.home.eightbutton.videoplayer.RuthVideoPlayer
import kotlinx.android.synthetic.main.activity_sit_n_talk.*

class SitnTalkActivity : BaseActivity<SitnTalkPresenter>(), SitnTalkPresenter.SitnTalkView {


    private lateinit var homeBannerAdapter: HomeBannerPagerAdapter

    private var dotscount: Int = 0

    private lateinit var dots: Array<ImageView?>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)
        setContentView(R.layout.activity_sit_n_talk)
        App.getComponent().inject(this)

        presenter.onCreate(this)
        setTopBannerImages()

        rv_sitntalk_hottest.layoutManager = LinearLayoutManager(presenter.context, LinearLayoutManager.HORIZONTAL, false)
        val sitTalkAdapter = SitnTalkHottestAdapter(presenter.context, false)
        rv_sitntalk_hottest.adapter = sitTalkAdapter
        sitTalkAdapter.setOnItemClickListener(object : SitnTalkHottestAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                startActivity(Intent(this@SitnTalkActivity, RuthVideoPlayer::class.java)
                        .putExtra("videoType", "SitTalk"))
            }
        })


        rv_sitntalk_latest.layoutManager = LinearLayoutManager(presenter.context)
        val sitnTalkLatestAdapter = SitnTalkLatestAdapter(presenter.context, false)
        rv_sitntalk_latest.adapter = sitnTalkLatestAdapter
        sitnTalkLatestAdapter.setOnItemClickListener(object : SitnTalkLatestAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                startActivity(Intent(this@SitnTalkActivity, RuthVideoPlayer::class.java)
                        .putExtra("videoType", "SitTalk"))
            }
        })


        btn_back_sitn.setOnClickListener {
            finish()
            overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
        }


    }

    private fun setTopBannerImages() {
        val homeBannerList: ArrayList<String> = ArrayList()
        homeBannerAdapter = HomeBannerPagerAdapter(presenter.context, homeBannerList)
        viewpagerSitnTalk.adapter = homeBannerAdapter

        dotscount = homeBannerAdapter.count

        dots = arrayOfNulls<ImageView>(dotscount)
        for (i in 0 until dotscount) {
            dots[i] = ImageView(presenter.context)
            dots[i]?.setImageDrawable(ContextCompat.getDrawable(presenter.context, R.drawable.nonactive_dot))
            val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.setMargins(8, 0, 8, 0)
            bannerSliderDots.addView(dots[i], params)
        }
        dots[0]?.setImageDrawable(ContextCompat.getDrawable(presenter.context, R.drawable.active_dot))

        viewpagerSitnTalk.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                //    currentPage = position

                for (i in 0 until dotscount) {
                    dots!![i]?.setImageDrawable(ContextCompat.getDrawable(presenter.context, R.drawable.nonactive_dot))
                }
                dots!![position]?.setImageDrawable(ContextCompat.getDrawable(presenter.context, R.drawable.active_dot))

            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })
    }

}
