package com.ruth.app.home.eightbutton.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class BiblePresenter @Inject constructor() : BasePresenter<BiblePresenter.Bibleview>()  {

    interface Bibleview : BaseView {

    }
}
