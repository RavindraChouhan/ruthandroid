package com.ruth.app.home.adapter

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R

class SearchPagerAdapter(mContext: Context) : PagerAdapter() {

    private var layoutInflater: LayoutInflater? = null

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`

    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
    return 10
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = layoutInflater?.inflate(R.layout.item_creepy_view, container, false)!!

        container.addView(view, 0)

        return view
    }
    init {
        layoutInflater = LayoutInflater.from(mContext)
    }
}
