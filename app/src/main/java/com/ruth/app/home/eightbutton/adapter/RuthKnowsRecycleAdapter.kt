package com.ruth.app.home.eightbutton.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import com.ruth.app.home.adapter.FollowersAdapter
import com.ruth.app.home.eightbutton.activity.ArticleDetailActivity
import kotlinx.android.synthetic.main.item_ruthknows_cell.view.*

class RuthKnowsRecycleAdapter(val mContext: Context) : RecyclerView.Adapter<RuthKnowsRecycleAdapter.ViewHolder>() {
    var list: ArrayList<Int> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_ruthknows_cell, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 10
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.recycle_follower.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        holder.itemView.recycle_follower.adapter = FollowersAdapter(mContext, list)
        holder.itemView.tv_description.text = "三代之時，天下有國千萬，言語不通。周既王，始有雅言。諸子著作，皆以雅言。及周道微，天下紛亂，言語異聲，文字異形。尤是楚語至不同也。秦始皇帝混一車書，獨用秦篆，各地方言日合。然至於許慎之世，說文解字之所錄，亦有同義而音異之字"

        holder.itemView.layoutArticle.setOnClickListener {
            mContext.startActivity(Intent(mContext, ArticleDetailActivity::class.java))
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    }


}
