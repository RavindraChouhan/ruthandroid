package com.ruth.app.home.userprofileview.videos

import android.content.Context
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import com.ruth.app.home.adapter.FollowersAdapter
import com.ruth.app.otheruser.OtherUserProfileActivity
import kotlinx.android.synthetic.main.item_user_video_cell.view.*

class UserVideoListAdapter(var context: Context?)
    : RecyclerView.Adapter<UserVideoListAdapter.VideoViewHolder>() {

    var list: ArrayList<Int> = ArrayList()
    private lateinit var recyclerView: RecyclerView
    private var onItemClicklistener: OnItemClickListener? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_user_video_cell, parent, false)
        return VideoViewHolder(view)
    }

    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) {
        holder.itemView.recycle_followers.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        holder.itemView.recycle_followers.adapter = FollowersAdapter(context, list)

        holder.itemView.layoutUser.setOnClickListener {
            context?.startActivity(Intent(context, OtherUserProfileActivity::class.java))
        }

    }

    override fun getItemCount(): Int {
        return 5
    }

    inner class VideoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    fun setOnItemClickListener(onItemClick: OnItemClickListener) {
        this.onItemClicklistener = onItemClick
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}
