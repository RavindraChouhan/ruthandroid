package com.ruth.app.home.userprofileview.backpack

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.ruth.app.R
import kotlinx.android.synthetic.main.item_backpack_sticker.view.*

class BackpackStickerAdapter(var context: FragmentActivity?,
                             var stickerItemList: ArrayList<BackpackStickersData>) : RecyclerView.Adapter<BackpackStickerAdapter.BackpackViewHolder>() {


    private var onItemClicklistener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BackpackViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_backpack_sticker, parent, false)
        return BackpackViewHolder(view)
    }

    override fun getItemCount(): Int {
        return stickerItemList.size
    }

    override fun onBindViewHolder(holder: BackpackViewHolder, position: Int) {
       // Glide.with(context).load(stickerItemList[position].stickerImage).into(holder.itemView.ivBackpackSticker)
        holder.itemView.ivBackpackSticker.setImageDrawable(stickerItemList[position].stickerImage)

        holder.itemView.tvBackpackPointsValue.text = stickerItemList[position].giftValue.toString()
        when {
            stickerItemList[position].giftType.equals(context!!.getString(R.string.gold_points), true) -> {
                holder.itemView.ivBackpackPointsType.setImageDrawable(context!!.getDrawable(R.drawable.icon_user_in_got_yellow))
            }
            stickerItemList[position].giftType.equals(context!!.getString(R.string.silver_points), true) -> {
                holder.itemView.ivBackpackPointsType.setImageDrawable(context!!.getDrawable(R.drawable.icon_user_in_got_silver))
            }
            stickerItemList[position].giftType.equals(context!!.getString(R.string.coin_points), true) -> {
                holder.itemView.ivBackpackPointsType.setImageDrawable(context!!.getDrawable(R.drawable.icon_user_yuan))
            }
        }
        holder.itemView.tvBackpackStickerName.text = stickerItemList[position].stickerName
        holder.itemView.tvBackpackPointsValue.text = stickerItemList[position].giftValue

        holder.itemView.setOnClickListener {
            onItemClicklistener!!.onItemClick(holder.itemView, position)
        }
    }

    inner class BackpackViewHolder(view: View) : RecyclerView.ViewHolder(view)

    fun setOnItemClickListener(onItemClick: OnItemClickListener) {
        this.onItemClicklistener = onItemClick
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

}