package com.ruth.app.home.maps.adapter

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import com.ruth.app.home.adapter.FollowersAdapter
import kotlinx.android.synthetic.main.item_map_cell.view.*

class MapAdapter(val mContext: Context) : RecyclerView.Adapter<MapAdapter.ViewHolder>() {

    var list: ArrayList<Int> = ArrayList()

    private var onItemClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_map_cell, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.recycle_follower.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        holder.itemView.recycle_follower.adapter = FollowersAdapter(mContext,list)
        holder.itemView.setOnClickListener {
            onItemClickListener!!.onItemClick(it,position)
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}