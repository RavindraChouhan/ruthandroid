package com.ruth.app.home.fragment

import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.*
import android.widget.TextView
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.fragment.BaseFragment
import com.ruth.app.home.adapter.HomeViewPagerAdapter
import com.ruth.app.home.presenter.HomeFragmentPresenter
import com.ruth.app.login.activity.LoginActivity
import com.ruth.app.rank.activity.RankHomeActivity
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : BaseFragment<HomeFragmentPresenter>(), HomeFragmentPresenter.HomeFragmentView {

    private var pagerAdapter: HomeViewPagerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        view.tvSignOut.setOnClickListener {
            showLogoutPopup()
//            activity?.startActivity(Intent(activity, LiveVideoPlayerActivity::class.java))
        }
        pagerAdapter = HomeViewPagerAdapter(childFragmentManager)
        view.viewPagerHome!!.adapter = pagerAdapter
        view.tabLayoutHome.setupWithViewPager(view.viewPagerHome)

        // set icons
        view.tabLayoutHome.getTabAt(0)!!.setText(R.string.home_live_tab1)
        view.tabLayoutHome.getTabAt(1)!!.setText(R.string.home_video_picture_tab2)
//        view.tabLayoutHome.getTabAt(2)!!.setText(R.string.home_podcasts_audio_tab3)
        view.btn_rank.setOnClickListener()
        {
            activity?.startActivity(Intent(activity, RankHomeActivity::class.java))
        }
        return view
    }

    override fun clearPreferences() {
        activity?.startActivity(Intent(activity, LoginActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        activity?.finish()
    }

    private fun showLogoutPopup() {

        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(activity!!)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_logout, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val btnYes = dialogView.findViewById<View>(R.id.btnYes) as TextView
        val btnNo = dialogView.findViewById<View>(R.id.btnNo) as TextView

        btnYes.setOnClickListener {
            presenter.clear()
            startActivity(Intent(activity, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            activity?.finish()
            activity?.overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
            b.dismiss()

        }

        btnNo.setOnClickListener {
            b.dismiss()
        }

        b.setCancelable(false)
        b.show()

        val displayMetrics = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val displayWidth = displayMetrics.widthPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.9f).toInt()
        layoutParams.width = dialogWindowWidth
        b.window.attributes = layoutParams
        b.window.setGravity(Gravity.CENTER_VERTICAL)


//
//        val builder = AlertDialog.Builder(activity)
//        builder.setTitle(getString(R.string.logout_header))
//        builder.setMessage(getString(R.string.logout_message))
//        builder.setPositiveButton(getString(R.string.yes)) { dialog, which ->
//            presenter.clear()
//            startActivity(Intent(activity, LoginActivity::class.java)
//                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
//            activity?.finish()
//            activity?.overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
//            dialog.dismiss()
//        }
//        builder.setNegativeButton(getString(R.string.no)) { dialog, which ->
//            dialog.dismiss()
//        }
//
//        val dialog = builder.create()
//        dialog.setCancelable(false)
//        dialog.show()
    }

}