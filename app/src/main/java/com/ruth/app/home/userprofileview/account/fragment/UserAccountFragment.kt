package com.ruth.app.home.userprofileview.account.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.*
import android.widget.ImageView
import android.widget.RadioGroup
import android.widget.Toast
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.fragment.BaseFragment
import com.ruth.app.home.userprofileview.account.presenter.UserAccountPresenter
import com.ruth.app.login.activity.LoginActivity
import com.ruth.app.utility.RuthUtility
import com.ruth.app.widgets.datepicker.DatePickerPopWin
import kotlinx.android.synthetic.main.fragment_user_account.*
import kotlinx.android.synthetic.main.fragment_user_account.view.*
import java.util.*

class UserAccountFragment : BaseFragment<UserAccountPresenter>(), UserAccountPresenter.UserAccountView, View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private var gender: String = ""
    private var birthday: String = ""
    private var thisAYear: Int = 0
    private var thisADay: Int = 0
    private var thisAMonth: Int = 0
    private var username: String = ""
    private var email: String = ""
    private var nickname: String = ""
    private var about: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_user_account, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        App.getComponent().inject(this)
        presenter.onCreate(this)
//        dialogs = CustomDialog(activity!!)

        view.etUserName.setOnTouchListener { v, event ->
            view.etUserName.isCursorVisible = true
            false
        }

        view.etBirthday.setOnClickListener(this)
        view.radioGroup?.setOnCheckedChangeListener(this)
        view.btnUpdateAccount.setOnClickListener(this)

        if (preferences.getUserProfile() != null) {
            if (preferences.getUserProfile().userName.isNotEmpty()) view.etUserName.text = "@" + preferences.getUserProfile().userName
            if (preferences.getUserProfile().nickname.isNotEmpty()) view.etNickname.setText(preferences.getUserProfile().nickname)
            if (preferences.getUserProfile().email.isNotEmpty()) view.etEmail.text = preferences.getUserProfile().email
            if (preferences.getUserProfile().gender.isNotEmpty()) {
                when {
                    preferences.getUserProfile().gender.equals(resources.getString(R.string.male), true) -> view.radioMale.isChecked = true
                    preferences.getUserProfile().gender.equals(resources.getString(R.string.female), true) -> view.radioFemale.isChecked = true
                    preferences.getUserProfile().gender.equals(resources.getString(R.string.none), true) -> view.radioOthers.isChecked = true

                }
            } else {
                view.radioOthers.isChecked = true
            }
            if (preferences.getUserProfile().about.isNotEmpty()) view.etAbout.setText(preferences.getUserProfile().about)
            if (preferences.getUserProfile().dob.isNotEmpty()) view.etBirthday.text = preferences.getUserProfile().dob
        }
    }

    override fun onClick(view: View?) {
        if (view?.id == R.id.etBirthday) {
            openDatePicker(view)
        }

        if (view?.id == R.id.btnUpdateAccount) {

//            if (preferences.getUserProfile().userName != etUserName.text.toString().trim()) {
//                username = if (etUserName.text.toString().isNotEmpty()) etUserName.text.toString().trim() else ""
//            }

            if (preferences.getUserProfile().nickname != etNickname.text.toString().trim()) {
                nickname = if (etNickname.text.toString().isNotEmpty()) etNickname.text.toString().trim() else ""
            }

            if (preferences.getUserProfile().about != etAbout.text.toString().trim()) {
                about = if (etAbout != null) etAbout.text.toString().trim() else ""
            }

            if (preferences.getUserProfile().dob != etBirthday.text.toString().trim()) {
                birthday = if (etBirthday != null) etBirthday.text.toString().trim() else ""
            }

//            if (preferences.getUserProfile().email != etEmail.text.toString().trim()) {
//                email = if (etEmail != null) etEmail.text.toString().trim() else ""
//            }

            activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
            presenter.updateInfo(username, nickname, about, birthday, gender, email)
        }
    }

    override fun profileUpdated(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    private var invalidDialog: AlertDialog? = null

    override fun invalidLogin() {

        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Invalid Login")
        builder.setMessage(getString(R.string.invalid_login_error))

        builder.setPositiveButton(getString(R.string.log_in)) { dialog, _ ->
            presenter.clearLogin()
            startActivity(Intent(activity, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            activity?.finish()
            activity?.overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
            dialog.dismiss()
        }

        if (!activity?.isFinishing!!) {
            invalidDialog = builder.create()
            invalidDialog!!.setCancelable(false)
            invalidDialog!!.show()
        }
    }

    override fun clearPreferences() {
        startActivity(Intent(activity, LoginActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        activity?.finish()
    }

    override fun onCheckedChanged(radioGroup: RadioGroup?, checkedId: Int) {
        gender = if (R.id.radioMale == checkedId) "male" else if (R.id.radioFemale == checkedId) "female" else "none"
    }

    override fun showProgressLoading() {
        RuthUtility.showLoader(activity!!)
//        progressBarLoading.visibility = View.VISIBLE
    }

    override fun hideProgressLoading() {
        RuthUtility.hideLoader()
//        progressBarLoading.visibility = View.GONE
    }

    /*--------------------------------------------------------------------------------------------*/
    @SuppressLint("SetTextI18n")
    private fun openDatePicker(view: View) {
        val c = Calendar.getInstance()
        thisAYear = c.get(Calendar.YEAR) - 15
        thisADay = c.get(Calendar.DAY_OF_MONTH)
        thisAMonth = c.get(Calendar.MONTH)

        val currentDate = "" + thisAYear + "-" + (thisAMonth + 1) + "-" + thisADay
        val pickerPopWin = DatePickerPopWin.Builder(activity, DatePickerPopWin.OnDatePickedListener { year, month, day, date ->
            if (year > Calendar.getInstance().get(Calendar.YEAR)) {
                Toast.makeText(activity, "Please select correct date", Toast.LENGTH_SHORT).show()
                return@OnDatePickedListener
            }

            if (Calendar.getInstance().get(Calendar.YEAR) - year < 18) {
                if (!view.etBirthday.text.isEmpty()) {
                    view.etBirthday.text = ""
                }
                showErrorPopup()
            } else {
                val monthInt = utility.monthNameToInt(month)
                view.etBirthday.text = "$day/$monthInt/$year"
            }
        }).textConfirm(getString(R.string.done)) //text of confirm button
                .btnTextSize(16) // button text size
                .viewTextSize(40) // pick view text size
                .colorConfirm(Color.parseColor("#3183fd"))//color of confirm button
                .minYear(1900) //min year in loop
                .maxYear(c.get(Calendar.YEAR) + 1) // max year in loop
                .dateChose(currentDate) // date chose when init popwindow
                .showDayMonthYear(true)
                .build()
        pickerPopWin.showPopWin(activity)
    }

    private fun showErrorPopup() {
        val dialogBuilder = Dialog(activity)
        dialogBuilder.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogBuilder.setContentView(R.layout.popup_birthday_error)
        dialogBuilder.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val closeTermsCondition = dialogBuilder.findViewById<View>(R.id.ivBirthdayDone) as ImageView

        closeTermsCondition.setOnClickListener {
            dialogBuilder.dismiss()
        }
        dialogBuilder.show()

        val displayMetrics = DisplayMetrics()
        activity?.windowManager!!.defaultDisplay.getMetrics(displayMetrics)

        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(dialogBuilder.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.6f).toInt()
        val dialogWindowHeight = (displayHeight * 0.7f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        dialogBuilder.window.attributes = layoutParams
    }

    /*--------------------------------------------------------------------------------------------*/
    override fun onDestroy() {
        if (invalidDialog != null) {
            invalidDialog!!.dismiss()
            invalidDialog = null
        }
        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        super.onDestroy()
    }
}

//btnUpdateAccount.isEnabled = checkEmptyFields(view.etUserName, view.etNickname, view.etAbout, view.etBirthday, gender, view.etEmail)

//        AndroidDisposable.create(this, Observable.combineLatest(
//                RxViewUtils.nonEmptyChecker(view.etUserName),
//                RxViewUtils.nonEmptyChecker(view.etNickname),
//                RxViewUtils.nonEmptyChecker(view.etEmail),
//                RxViewUtils.nonEmptyChecker(view.etEmail),
//                RxViewUtils.nonEmptyChecker(view.etEmail),
//                Function5<Boolean, Boolean, Boolean, Boolean, Boolean, Boolean> { nameOk, lnameOk, memberIdOk, phoneOk, insuranceOk ->
//                    nameOk && lnameOk && memberIdOk && phoneOk && insuranceOk
//                }
//        ).subscribe({ fieldsOk ->
//            view.btnUpdateAccount.isEnabled = fieldsOk
//        }))
