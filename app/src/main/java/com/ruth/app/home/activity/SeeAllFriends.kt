package com.ruth.app.home.activity

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.home.adapter.SeeAllFriendsAdapter
import com.ruth.app.home.fragment.LiveFragment
import com.ruth.app.home.presenter.SeeAllFriendsPresenter
import com.ruth.app.login.activity.LoginActivity
import com.ruth.app.model.RecommandedUserDetail
import com.ruth.app.otheruser.OtherUserProfileActivity
import com.ruth.app.utility.RuthUtility
import kotlinx.android.synthetic.main.activity_see_friends.*

class SeeAllFriends : BaseActivity<SeeAllFriendsPresenter>(), SeeAllFriendsPresenter.SeeAllFriendsView, View.OnClickListener {

    private var seeAllFriendsAdapter: SeeAllFriendsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_see_friends)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        presenter.getRecommendedUsers()
//        setUsers(LiveFragment.friendsList)
        ivSeeAllBack.setOnClickListener(this)

    }

    override fun onClick(view: View?) {
        if (view?.id == R.id.ivSeeAllBack) {
            finish()
        }
    }


    /*--------------------------------------------------------------------------------------------*/
    override fun showLoadingProgress() {
        RuthUtility.showLoader(this@SeeAllFriends)

//        progressBarRecommended.visibility = View.VISIBLE
    }

    override fun hideLoadingProgress() {
        RuthUtility.hideLoader()
//        progressBarRecommended.visibility = View.GONE
    }

    override fun invalidLogin() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Invalid Login")
        builder.setMessage(getString(R.string.invalid_login_error))

        // add a button
        builder.setPositiveButton(getString(R.string.log_in)) { dialog, _ ->
            presenter.clearLogin()
            startActivity(Intent(this@SeeAllFriends, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            finish()
            overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
            dialog.dismiss()
        }

        if (!isFinishing && window.decorView.isShown) {
            val dialog = builder.create()
            dialog.setCancelable(false)
            dialog.show()
        }
    }

    override fun setRecommendedUsers(data: ArrayList<RecommandedUserDetail>) {
        //      rvRecommendedList.layoutManager = GridLayoutManager(this, 3)
        seeAllFriendsAdapter = SeeAllFriendsAdapter(this, LiveFragment.friendsList)
        rvRecommendedList.adapter = seeAllFriendsAdapter
        seeAllFriendsAdapter!!.setOnItemClickListener(object : SeeAllFriendsAdapter.OnItemClickListener {
            override fun onFollowClick(view: View, position: Int) {
                data[position].isFollow = !data[position].isFollow
                if (data[position].isFollow) {
                    data[position].followers = data[position].followers + 1
                } else {
                    data[position].followers = data[position].followers - 1
                }
                presenter.followUnfollow(data[position].id, data[position].isFollow)
                seeAllFriendsAdapter!!.notifyItemChanged(position)
            }

            override fun onItemClick(view: View, position: Int) {
                startActivity(Intent(this@SeeAllFriends, OtherUserProfileActivity::class.java)
                        .putExtra("otherUserId", data[position].id))
            }
        })
    }

    private fun setUsers(data: ArrayList<RecommandedUserDetail>) {
        seeAllFriendsAdapter = SeeAllFriendsAdapter(this, LiveFragment.friendsList)
        rvRecommendedList.adapter = seeAllFriendsAdapter
        seeAllFriendsAdapter!!.setOnItemClickListener(object : SeeAllFriendsAdapter.OnItemClickListener {
            override fun onFollowClick(view: View, position: Int) {
                data[position].isFollow = !data[position].isFollow
                if (data[position].isFollow) {
                    data[position].followers = data[position].followers + 1
                } else {
                    data[position].followers = data[position].followers - 1
                }
                presenter.followUnfollow(data[position].id, data[position].isFollow)
                seeAllFriendsAdapter!!.notifyItemChanged(position)
            }

            override fun onItemClick(view: View, position: Int) {
                startActivity(Intent(
                        this@SeeAllFriends,
                        OtherUserProfileActivity::class.java
                )
                        .putExtra("otherUserId", data[position].id))
            }
        })
    }
}