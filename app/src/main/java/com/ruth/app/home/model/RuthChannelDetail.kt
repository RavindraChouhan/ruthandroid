package com.ruth.app.home.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RuthChannelDetail(
        @SerializedName("status")
        @Expose
        var status: Boolean,
        @SerializedName("message")
        @Expose
        var message: String = "",
        @SerializedName("data")
        @Expose
        var data: ChannelDetail,
        @SerializedName("zh_Hans")
        @Expose
        var zhHans: String = "",
        @SerializedName("zh_Hant")
        @Expose
        var zhHant: String = "")

class ChannelDetail(
        @SerializedName("_id")
        @Expose
        var id: String = "",
        @SerializedName("endTime")
        @Expose
        var endTime: String = "",
        @SerializedName("startTime")
        @Expose
        var startTime: String = "",
        @SerializedName("profile")
        @Expose
        var profile: String = "",
        @SerializedName("cover")
        @Expose
        var cover: String = "",
        @SerializedName("description")
        @Expose
        var description: String = "",
        @SerializedName("title")
        @Expose
        var title: String = "",
        @SerializedName("isLike")
        @Expose
        var isLike: Boolean = false,
        @SerializedName("likes")
        @Expose
        var likes: Int = 0,
        @SerializedName("totalViews")
        @Expose
        var views: Int = 0,
        @SerializedName("subscribers")
        @Expose
        var subscribers: Int = 0,
        @SerializedName("isSubscribe")
        @Expose
        var isSubscribe: Boolean = false,
        @SerializedName("emoji")
        @Expose
        var emoji: Int = 0,
        @SerializedName("onAir")
        @Expose
        var onAir: String = "",
        @SerializedName("broadcastData")
        @Expose
        var broadcastData: ArrayList<ChannelBroadcastData>,
        @SerializedName("pastBroadcast")
        @Expose
        var pastBroadcastData: ArrayList<ChannelBroadcastData>,
        @SerializedName("activeUserData")
        @Expose
        var activeUserData: ArrayList<Followers>)

class ChannelBroadcastData(@SerializedName("_id")
                           @Expose
                           var id: String = "",
                           @SerializedName("broadcasterId")
                           @Expose
                           var broadcasterId: String = "",
                           @SerializedName("isActive")
                           @Expose
                           var isActive: Boolean = false,
                           @SerializedName("isLive")
                           @Expose
                           var isLive: Boolean = false,
                           @SerializedName("broadcastStreamId")
                           @Expose
                           var broadcastStreamId: String = "",
                           @SerializedName("broadcastTag")
                           @Expose
                           var broadcastTag: String = "",
                           @SerializedName("broadcastTitle")
                           @Expose
                           var broadcastTitle: String = "",
                           @SerializedName("coverUrl")
                           @Expose
                           var coverUrl: String = "",
                           @SerializedName("broadcastUrl")
                           @Expose
                           var broadcastUrl: String = "",
                           @SerializedName("broadcastLikes")
                           @Expose
                           var broadcastLikes: Int = 0,
                           @SerializedName("emoji")
                           @Expose
                           var scaryCount: Int = 0,
                           @SerializedName("broadcastViews")
                           @Expose
                           var broadcastViews: Int = 0,
                           @SerializedName("broadcastComments")
                           @Expose
                           var broadcastComments: Int = 0,
                           @SerializedName("broadcasterData")
                           @Expose
                           var broadcasterData: ChannelBroadcasterData,
                           @SerializedName("broadcastStartTime")
                           @Expose
                           var broadcastStartTime: String = "",
                           @SerializedName("broadcastEndTime")
                           @Expose
                           var broadcastEndTime: String = "")


class ChannelBroadcasterData(@SerializedName("_id")
                             @Expose
                             var id: String = "",
                             @SerializedName("followers")
                             @Expose
                             var followers: Int = 0,
                             @SerializedName("photoUrl")
                             @Expose
                             var photoUrl: String = "",
                             @SerializedName("userName")
                             @Expose
                             var userName: String = "",
                             @SerializedName("email")
                             @Expose
                             var email: String = "",
                             @SerializedName("name")
                             @Expose
                             var name: String = "",
                             @SerializedName("isFollow")
                             @Expose
                             var isFollow: Boolean = false)
