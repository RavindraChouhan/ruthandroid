package com.ruth.app.home.userprofileview.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ruth.app.R
import com.ruth.app.utility.Utility
import kotlinx.android.synthetic.main.activity_terms_conditions.*
import javax.inject.Inject

class TermsConditionsActivity : AppCompatActivity(), View.OnClickListener {

    @Inject
    lateinit var utility: Utility

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms_conditions)

        ivTermsBack.setOnClickListener(this)
        tvTermsConditions.text = intent.getStringExtra("termsCondition")
    }

    override fun onClick(v: View?) {
        if (v == ivTermsBack) {
            finish()
        }
    }
}