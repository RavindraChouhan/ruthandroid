package com.ruth.app.home.userprofileview.faq

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Faq(var faqQuestion: String,
          var faqAnswer: String) : Parcelable