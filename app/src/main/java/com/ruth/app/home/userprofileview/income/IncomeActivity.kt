package com.ruth.app.home.userprofileview.income

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import com.ruth.app.R
import kotlinx.android.synthetic.main.activity_income.*

class IncomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_income)

        setupTabLayout()
        ivincomeBack.setOnClickListener {
            finish()
        }
    }

    private fun setupTabLayout() {

        val adapter = IncomePagerAdapter(supportFragmentManager, tab_income.tabCount)
        viewPager_income!!.adapter = adapter
        tab_income.setupWithViewPager(viewPager_income)

        tab_income.getTabAt(0)!!.setText(R.string.gold_points)
        tab_income.getTabAt(1)!!.setText(R.string.gifts)

        tab_income.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                val position = tab.position
                viewPager_income.setCurrentItem(position, true)
            }
        })
    }
}
