package com.ruth.app.home.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class RuthChannels(
        @SerializedName("status")
        val status: Boolean,
        @SerializedName("message")
        val message: String,
        @SerializedName("data")
        val data: ArrayList<RuthChannelsData>,
        @SerializedName("zh_Hans")
        @Expose
        var zhHans: String? = null,
        @SerializedName("zh_Hant")
        @Expose
        var zhHant: String? = null)

class RuthChannelsData(
        @SerializedName("_id")
        @Expose
        var id: String? = null,
        @SerializedName("endTime")
        @Expose
        var endTime: String? = null,
        @SerializedName("startTime")
        @Expose
        var startTime: String? = null,
        @SerializedName("profile")
        @Expose
        var profile: String? = null,
        @SerializedName("cover")
        @Expose
        var cover: String? = null,
        @SerializedName("description")
        @Expose
        var description: String? = null,
        @SerializedName("title")
        @Expose
        var title: String? = null,
        @SerializedName("emoji")
        @Expose
        var emoji: Int? = 0,
        @SerializedName("likes")
        @Expose
        var likes: Int? = null,
        @SerializedName("views")
        @Expose
        var views: Int? = null,
        @SerializedName("subscribers")
        @Expose
        var subscribers: Int? = null,
        @SerializedName("isSubscribe")
        @Expose
        var isSubscribe: Boolean? = null,
        @SerializedName("onAir")
        @Expose
        var onAir: String? = null,
        @SerializedName("broadcastData")
        @Expose
        var broadcastData: ArrayList<Followers>? = null):Serializable



