package com.ruth.app.home.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class FavoriteListPresenter @Inject constructor() : BasePresenter<FavoriteListPresenter.FavoriteFragmentView>() {

    interface FavoriteFragmentView : BaseView {

    }
}