package com.ruth.app.home.userprofileview.summon

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ruth.app.R
import com.ruth.app.utility.Utility
import kotlinx.android.synthetic.main.activity_user_viewpager_tab.*
import javax.inject.Inject

class SummonActivity : AppCompatActivity(), View.OnClickListener {

    @Inject
    lateinit var utility: Utility
    private var summonViewPagerAdapter: SummonViewPagerAdapter? = null
    private var summonList: ArrayList<Summon>? = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_viewpager_tab)

        tvHeader.text = getString(R.string.summon)
        ivUserTabBack.setOnClickListener(this)

        setupTabLayout()
    }

    override fun onClick(v: View?) {
        if (v == ivUserTabBack) {
            finish()
        }
    }

    private fun setupTabLayout() {
        summonList?.clear()
        for (i in 0 until 5) {
            summonList?.add(Summon("Summoning"))
            summonList?.add(Summon("Succeeded"))
            summonList?.add(Summon("Failed"))
            summonList?.add(Summon("Summoning"))
            summonList?.add(Summon("Succeeded"))
            summonList?.add(Summon("Failed"))
        }
        summonViewPagerAdapter = SummonViewPagerAdapter(supportFragmentManager, summonList)
        viewPagerUser.adapter = summonViewPagerAdapter
        tablayoutUserTab.setupWithViewPager(viewPagerUser)

        tablayoutUserTab.getTabAt(0)!!.setText(R.string.all)
        tablayoutUserTab.getTabAt(1)!!.setText(R.string.summoing)
        tablayoutUserTab.getTabAt(2)!!.setText(R.string.succeeded)
        tablayoutUserTab.getTabAt(3)!!.setText(R.string.failed)

        tablayoutUserTab.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                val position = tab.position

                when (position) {
                    0 -> {
                        summonList?.clear()
                        summonList?.add(Summon("Summoning"))
                        summonList?.add(Summon("Succeeded"))
                        summonList?.add(Summon("Failed"))
                        summonList?.add(Summon("Summoning"))
                        summonList?.add(Summon("Succeeded"))
                        summonList?.add(Summon("Failed"))
                    }
                    1 -> {
                        summonList?.clear()
                        for (i in 0 until 10) {
                            summonList?.add(Summon("Summoning"))
                        }
                    }
                    2 -> {
                        summonList?.clear()
                        for (i in 0 until 6) {
                            summonList?.add(Summon("Succeeded"))
                        }
                    }
                    3 -> {
                        summonList?.clear()
                        for (i in 0 until 3) {
                            summonList?.add(Summon("Failed"))
                        }
                    }
                }
            }
        })
    }
}
