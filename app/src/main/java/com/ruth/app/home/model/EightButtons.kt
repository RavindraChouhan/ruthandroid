package com.ruth.app.home.model

import com.google.gson.annotations.SerializedName

class EightButtons(
        @SerializedName("status")
        val status: Boolean,
        @SerializedName("message")
        val message: String,
        @SerializedName("data")
        val data: ArrayList<EightButtonsData>)

class EightButtonsData(
        @SerializedName("urls")
        val urls: ArrayList<String>,
        @SerializedName("zhHant")
        val zhHant: String,
        @SerializedName("zhHans")
        val zhHans: String,
        @SerializedName("en")
        val en: String,
        @SerializedName("_id")
        val _id: String)
