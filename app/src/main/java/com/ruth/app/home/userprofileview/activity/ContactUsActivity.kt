package com.ruth.app.home.userprofileview.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ruth.app.R
import com.ruth.app.model.ContactUs
import com.ruth.app.utility.Utility
import kotlinx.android.synthetic.main.activity_user_contact_us.*
import javax.inject.Inject

class ContactUsActivity : AppCompatActivity(), View.OnClickListener {

    @Inject
    lateinit var utility: Utility

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_contact_us)

        val contactUs: ContactUs = intent.getSerializableExtra("contactUs") as ContactUs
        tvContactAddress.text = contactUs.address
        tvContactHotline.text = contactUs.hotline
        tvContactEmail.text = contactUs.email
        tvContactTime.text = contactUs.openingStartTime + "-" + contactUs.openingEndTime
        tvContactWebsite.text = contactUs.website

        ivContactBack.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v == ivContactBack) {
            finish()
        }
    }
}