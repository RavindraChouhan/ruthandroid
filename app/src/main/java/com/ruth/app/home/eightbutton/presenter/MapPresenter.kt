package com.ruth.app.home.eightbutton.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class MapPresenter @Inject constructor(): BasePresenter<MapPresenter.MapView>(){

    interface MapView : BaseView {

    }
}
