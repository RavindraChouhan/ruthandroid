package com.ruth.app.home.userprofileview.settings

import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.network.config.RuthApi
import javax.inject.Inject

class SettingsPresenter @Inject constructor(private val ruthApi: RuthApi) : BasePresenter<SettingsPresenter.SettingView>() {

    interface SettingView : BaseView
}