package com.ruth.app.home.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import kotlinx.android.synthetic.main.item_followers_cell.view.*

class FollowersAdapter(val mContext: Context?, val list: ArrayList<Int>) : RecyclerView.Adapter<FollowersAdapter.ViewHolder>() {

    var isForFirst: Boolean = false
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_followers_cell, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 4
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position > 2) {
            if (!isForFirst) {
                isForFirst = true
                holder.itemView.imgBroadCaster.visibility = View.GONE
                holder.itemView.layoutExtraBroadcaster.visibility = View.VISIBLE
                holder.itemView.lblNumBroadcaster.text = "+18"
            }else{
                holder.itemView.imgBroadCaster.visibility = View.GONE
            }
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    }
}