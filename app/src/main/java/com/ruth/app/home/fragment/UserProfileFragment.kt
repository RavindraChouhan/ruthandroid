package com.ruth.app.home.fragment

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.fragment.BaseFragment
import com.ruth.app.home.activity.HomeActivity
import com.ruth.app.home.presenter.UserProfileFragmentPresenter
import com.ruth.app.home.userprofileview.account.UserAccountActivity
import com.ruth.app.home.userprofileview.activity.*
import com.ruth.app.home.userprofileview.backpack.BackpackActivity
import com.ruth.app.home.userprofileview.faq.FaqActivity
import com.ruth.app.home.userprofileview.income.IncomeActivity
import com.ruth.app.home.userprofileview.linking.LinkingActivity
import com.ruth.app.home.userprofileview.live.UserLiveActivity
import com.ruth.app.home.userprofileview.receivedgifts.UserReceivedGiftsActivity
import com.ruth.app.home.userprofileview.settings.SettingsActivity
import com.ruth.app.home.userprofileview.share.UserShareActivity
import com.ruth.app.home.userprofileview.summon.SummonActivity
import com.ruth.app.home.userprofileview.userPurchaseActivity.UserPurchaseActivity
import com.ruth.app.home.userprofileview.videos.UserVideosActivity
import com.ruth.app.home.userprofileview.watch.WatchActivity
import com.ruth.app.login.activity.LoginActivity
import com.ruth.app.model.ContactUs
import com.ruth.app.model.RuthSettingsData
import com.ruth.app.utility.RuthUtility
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.fragment_user_profile.*
import kotlinx.android.synthetic.main.fragment_user_profile.view.*
import org.json.JSONException
import org.json.JSONObject
import java.io.*

@Suppress("VARIABLE_WITH_REDUNDANT_INITIALIZER")
class UserProfileFragment : BaseFragment<UserProfileFragmentPresenter>(), UserProfileFragmentPresenter.UserProfileFragmentView, View.OnClickListener {

    private var mPicCaptureUri: Uri? = null
    private val REQUEST_PERMISSIONS = 100
    private var hidden: Boolean = true
    private var termsConditions: String = ""
    private var contactUs: ContactUs? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_user_profile, container, false)
        setUpSocket()
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        App.getComponent().inject(this)
        presenter.onCreate(this)
        presenter.getSettings()

        //Perform All the click
        view.imgAddProfile.setOnClickListener(this)
        view.imgSetting.setOnClickListener(this)
        view.imgUserPicEdit.setOnClickListener(this)
        view.layoutTopUp.setOnClickListener(this)
        view.layoutVip.setOnClickListener(this)
        view.layoutIncome.setOnClickListener(this)
        view.layoutMission.setOnClickListener(this)
        view.layoutTopFans.setOnClickListener(this)
        view.layoutLinking.setOnClickListener(this)
        view.layoutBackpack.setOnClickListener(this)
        view.layoutAccount.setOnClickListener(this)
        view.layoutWatch.setOnClickListener(this)
        view.layoutPurchase.setOnClickListener(this)
        view.layoutReceviedGift.setOnClickListener(this)
        view.layoutGiveAway.setOnClickListener(this)
        view.layoutLive.setOnClickListener(this)
        view.layoutVideos.setOnClickListener(this)
        view.layoutShare.setOnClickListener(this)
        view.layoutSummon.setOnClickListener(this)
        view.layoutTermAndCondition.setOnClickListener(this)
        view.layoutReviews.setOnClickListener(this)
        view.layoutFaq.setOnClickListener(this)
        view.layoutContactUs.setOnClickListener(this)
        view.layoutOnline.setOnClickListener(this)
        view.layoutOffline.setOnClickListener(this)
        view.lblUserStatus.setOnClickListener(this)

        setUserData(view)
        return view
    }

    private fun setUserData(view: View) {

        if (preferences.getUserProfile() != null) {
            var status: String = ""
            if (preferences.getUserProfile().status == 1) {
                status = "online"
            } else if (preferences.getUserProfile().status == 0) {
                status = "offline"
            }

            view.lblUserStatus.text = preferences.getUserProfile().nickname + "(" + status + ")"
            view.lblUserName.text = "@" + preferences.getUserProfile().userName
            view.lblName.text = preferences.getUserProfile().nickname
            view.lblFans.text = RuthUtility.getCounts(preferences.getUserProfile().followers)

            view.progressBarImageLoading.visibility = View.VISIBLE
            Glide.with(context).load(preferences.getUserProfile().photoUrl)
                    .apply(RequestOptions().error(R.drawable.com_facebook_profile_picture_blank_square)
                            .placeholder(R.drawable.com_facebook_profile_picture_blank_square)
                            .diskCacheStrategy(DiskCacheStrategy.NONE))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            view.progressBarImageLoading.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            view.progressBarImageLoading.visibility = View.GONE
                            return false
                        }

                    }).into(view.imgUserPic)

            view.tvGiftCountGold.text = RuthUtility.getCounts(HomeActivity.goldPoints)
            view.tvGiftCountSilver.text = RuthUtility.getCounts(HomeActivity.silverPoints)
            view.tvGiftCountCoin.text = RuthUtility.getCounts(HomeActivity.yuvanPoints)


//            if (preferences.getUserProfile().pointEarn!!.isNotEmpty()) {
//                for (i in 0 until preferences.getUserProfile().pointEarn?.size!!) {
//                    if (preferences.getUserProfile().pointEarn!![i].id.equals(getString(R.string.gold_points), true)) {
//                        view.tvGiftCountGold.text = RuthUtility.getCounts(preferences.getUserProfile().pointEarn!![i].total)
//                    }
//
//                    if (preferences.getUserProfile().pointEarn!![i].id.equals(getString(R.string.silver_points), true)) {
//                        view.tvGiftCountSilver.text = RuthUtility.getCounts(preferences.getUserProfile().pointEarn!![i].total)
//                    }
//
//                    if (preferences.getUserProfile().pointEarn!![i].id.equals(getString(R.string.coin_points), true)) {
//                        view.tvGiftCountCoin.text = RuthUtility.getCounts(preferences.getUserProfile().pointEarn!![i].total)
//                    }
//                }
//            } else {
//                view.tvGiftCountGold.text = "0"
//                view.tvGiftCountSilver.text = "0"
//                view.tvGiftCountCoin.text = "0"
//            }
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            imgAddProfile -> {
                //Toast.makeText(context, "imgAddProfile", Toast.LENGTH_SHORT).show()
                //   context?.startActivity(Intent(context, UserProfileActivity::class.java))
                return
            }
            imgSetting -> {
                startActivity(Intent(activity, SettingsActivity::class.java))
                return
            }
            imgUserPicEdit -> {
                if (utility.permissionForStorageAndCamera(activity!!)) {
                    openSelectImagePopup()
                }
                return
            }
            layoutTopUp -> {
                startActivity(Intent(activity, TopUpActivity::class.java))
                return
            }
            layoutVip -> {
                startActivity(Intent(activity, VIPActivity::class.java))
                return
            }
            layoutIncome -> {
                startActivity(Intent(activity, IncomeActivity::class.java))
                return
            }
            layoutMission -> {
                startActivity(Intent(activity, MissionActivity::class.java))
                return
            }
            layoutTopFans -> {
                startActivity(Intent(activity, TopFansActivity::class.java))
                return
            }
            layoutLinking -> {
                startActivity(Intent(activity, LinkingActivity::class.java))
                return
            }
            layoutBackpack -> {
                startActivity(Intent(activity, BackpackActivity::class.java))
                return
            }

            layoutAccount -> {
                startActivity(Intent(activity, UserAccountActivity::class.java))
                return
            }

            layoutWatch -> {
                startActivity(Intent(activity, WatchActivity::class.java))
                return
            }
            layoutPurchase -> {
                startActivity(Intent(activity, UserPurchaseActivity::class.java).putExtra("addOthers", true))
                return
            }
            layoutReceviedGift -> {
                startActivity(Intent(activity, UserReceivedGiftsActivity::class.java))
                return
            }
            layoutGiveAway -> {
                startActivity(Intent(activity, UserPurchaseActivity::class.java).putExtra("addOthers", false))
                return
            }
            layoutLive -> {
                startActivity(Intent(activity, UserLiveActivity::class.java))
                return
            }
            layoutVideos -> {
                startActivity(Intent(activity, UserVideosActivity::class.java))
                return
            }
            layoutShare -> {
                startActivity(Intent(activity, UserShareActivity::class.java))
                return
            }
            layoutSummon -> {
                startActivity(Intent(activity, SummonActivity::class.java))
                return
            }
            layoutTermAndCondition -> {
                startActivity(Intent(activity, TermsConditionsActivity::class.java)
                        .putExtra("termsCondition", termsConditions))
                return
            }
            layoutReviews -> {
                startActivity(Intent(activity, ReviewActivity::class.java))
                return
            }
            layoutFaq -> {
                startActivity(Intent(activity, FaqActivity::class.java))
                return
            }
            layoutContactUs -> {
                startActivity(Intent(activity, ContactUsActivity::class.java)
                        .putExtra("contactUs", contactUs))
                return
            }
            layoutOnline -> {
                presenter.updateOnOffStatus(1)
                hidden = false
                updateOnlineOfflineMode()
                return
            }
            layoutOffline -> {
                presenter.updateOnOffStatus(0)
                hidden = false
                updateOnlineOfflineMode()
                return
            }

            lblUserStatus -> {
                updateOnlineOfflineMode()
                return
            }
        }
    }

    private fun updateProfilePopup(imageUri: Uri) {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Update Profile Picture")
        builder.setMessage(getString(R.string.update_profile))

        // add a button
        builder.setPositiveButton(getString(R.string.yes)) { dialog, _ ->
            awsUtility.deleteObjectFromAws(preferences.getUserProfile().photoUrl, "profileImage/")
            imgUserPic.setImageURI(imageUri)
            if (imageUri != null) {
                val uploadedImageUrl = selectedImage(imageUri)
                Log.d("upload", " -> $uploadedImageUrl")

                if (uploadedImageUrl.isEmpty()) {
                    showProgressLoading()
                    uploadCoverImage(imageUri)
                }
            }

            dialog.dismiss()
        }

        // add a button
        builder.setNegativeButton(getString(R.string.no)) { dialog, _ ->
            dialog.dismiss()
        }

        if (!activity?.isFinishing!!) {
            val dialog = builder.create()
            dialog.setCancelable(false)
            dialog.show()
        }

    }

    override fun updateStatus(it: Int) {
        val status: Int
        val activityStatus: String
        if (it == 0) {
            status = 0
            activityStatus = "offline"
        } else {
            status = 1
            activityStatus = "online"
        }
        updateStatusUser(status)
        view?.lblUserStatus?.text = preferences.getUserProfile().nickname + "(" + activityStatus + ")"
    }

    override fun settingsData(data: RuthSettingsData) {
        termsConditions = data.terms
        contactUs = data.contactUs
    }

    /*------------------------------------UPDATE PROFILE IMAGE------------------------------------*/

    private var tempFile: String? = null
    private var myBase64Image: String? = null
    private var bm: Bitmap? = null
    private var coverImageUrl: String = ""

    private fun openSelectImagePopup() {
        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(activity!!)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_take_picture, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation

        val layoutGallery = dialogView.findViewById<View>(R.id.layoutGallery) as LinearLayout
        val layoutCamera = dialogView.findViewById<View>(R.id.layoutCamera) as LinearLayout
        val ivGallery = dialogView.findViewById<View>(R.id.ivGallery) as ImageView
        val ivCamera = dialogView.findViewById<View>(R.id.ivCamera) as ImageView
        val popupClose = dialogView.findViewById<View>(R.id.popupClose) as ImageView

        layoutCamera.setOnClickListener {
            ivGallery.setImageDrawable(resources.getDrawable(R.drawable.unselected_phone))
            ivCamera.setImageDrawable(resources.getDrawable(R.drawable.selected_camera))
            b.dismiss()
            cameraIntent()
        }

        layoutGallery.setOnClickListener {
            ivGallery.setImageDrawable(resources.getDrawable(R.drawable.selected_phone))
            ivCamera.setImageDrawable(resources.getDrawable(R.drawable.unselected_camera))
            b.dismiss()
            galleryIntent()
        }

        popupClose.setOnClickListener {
            b.dismiss()
        }

        b.setCancelable(false)
        b.show()

        val displayMetrics = DisplayMetrics()
        activity?.windowManager!!.defaultDisplay.getMetrics(displayMetrics)
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        val dialogWindowWidth = (displayWidth * 1f).toInt()
        val dialogWindowHeight = (displayHeight * 1f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        b.window.attributes = layoutParams
    }

    private fun galleryIntent() {
        val intent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, 2)
    }


    private fun cameraIntent() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        mPicCaptureUri = utility.getOutputMediaFileUri(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, activity!!)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mPicCaptureUri)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        startActivityForResult(intent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == AppCompatActivity.RESULT_OK) {
            if (requestCode == 1) {
                if (mPicCaptureUri != null) {
                    updateProfilePopup(mPicCaptureUri!!)
                } else {
                    Toast.makeText(activity, "Error while capturing Image", Toast.LENGTH_LONG).show()
                }
            } else if (requestCode == 2) {
                val selectedImage = data?.data
                mPicCaptureUri = selectedImage
                if (mPicCaptureUri != null) {
                    updateProfilePopup(mPicCaptureUri!!)
                }
                val filePath = arrayOf(MediaStore.Images.Media.DATA)
                val c = activity?.contentResolver?.query(selectedImage, filePath, null, null, null)
                c!!.moveToFirst()
                val columnIndex = c.getColumnIndex(filePath[0])
                val picturePath = c.getString(columnIndex)
                c.close()
                val thumbnail = BitmapFactory.decodeFile(picturePath)
                if (thumbnail != null) {
                    //  updateProfilePopup(mPicCaptureUri!!)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                openSelectImagePopup()
            } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!, Manifest.permission.CAMERA)) {
                    ActivityCompat.requestPermissions(this.activity!!, arrayOf(Manifest.permission.CAMERA), REQUEST_PERMISSIONS)
                }
            } else if (grantResults[1] == PackageManager.PERMISSION_DENIED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    ActivityCompat.requestPermissions(this.activity!!, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_PERMISSIONS)
                }
            }
        }
    }

    private fun selectedImage(imageUri: Uri): String {
        val outputUri = imageUri
        var imageStream: InputStream? = null
        try {
            imageStream = activity?.contentResolver!!.openInputStream(outputUri)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                bm = BitmapFactory.decodeStream(imageStream)
                val file = storeImage(bm!!)
                tempFile = utility.compressImage(file!!.absolutePath)
                bm = null
            } else {
                myBase64Image = utility.getUriPath(this.activity!!, outputUri)
                tempFile = myBase64Image
                tempFile = utility.compressImage(tempFile!!)
            }
        } catch (e: NullPointerException) {
            e.printStackTrace()

        }
        myBase64Image = tempFile
        try {
            coverImageUrl = awsUtility.uploadCoverImage(myBase64Image!!, "profileImage")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return coverImageUrl
    }

    private fun storeImage(image: Bitmap): File? {
        val pictureFile = utility.getOutputMediaFile(1) ?: return null
        try {
            bm = image
            val fos = FileOutputStream(pictureFile)
            image.compress(Bitmap.CompressFormat.JPEG, 60, fos)
            fos.close()
            return pictureFile
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return pictureFile
    }

    private fun uploadCoverImage(imageUri: Uri) {
        val thread = Thread(Runnable {
            Runtime.getRuntime().gc()
            val internetConnection = utility.checkInternetWithoutDialog(this.activity!!)
            if (internetConnection) {
                val checkImageUploaded = selectedImage(imageUri)
                if (!checkImageUploaded.isEmpty()) {
                    hideProgressLoading()
                    val uploadedImageUrl = selectedImage(imageUri)
                    presenter.uploadProfileImage(uploadedImageUrl)
                } else {
                    uploadCoverImage(imageUri)
                }
            } else {
                uploadCoverImage(imageUri)
            }
        })
        thread.start()
    }

    private fun updateOnlineOfflineMode() {
        val radius = Math.max(view?.reveal_items?.width!!, view?.reveal_items?.height!!)
        val cx = view?.reveal_items?.right!!
        val cy = view?.reveal_items?.top
        if (hidden) {
            val anim = android.view.ViewAnimationUtils.createCircularReveal(view?.reveal_items, cx, cy!!, 0f, radius.toFloat())
            view?.reveal_items?.visibility = View.VISIBLE
            anim.start()
            hidden = false
        } else {
            val anim = android.view.ViewAnimationUtils.createCircularReveal(view?.reveal_items, cx, cy!!, radius.toFloat(), 0f)
            anim.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    view?.reveal_items?.visibility = View.INVISIBLE
                    hidden = true
                }
            })
            anim.start()
        }
    }

    private fun updateStatusUser(status: Int) {
        if (!mSocket!!.connected()) return

        val postData = JSONObject()
        try {
            postData.put("userId", preferences.getUserInfo().id)
            postData.put("status", status)

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        mSocket!!.emit("ACTIVITY_STATUS", postData)
    }

    /*--------------------------------------------------------------------------------------------*/
    private var invalidDialog: AlertDialog? = null

    override fun invalidLogin() {

        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Invalid Login")
        builder.setMessage(getString(R.string.invalid_login_error))

        builder.setPositiveButton(getString(R.string.log_in)) { dialog, _ ->
            presenter.clearLogin()
            startActivity(Intent(activity, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            activity?.finish()
            activity?.overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
            dialog.dismiss()
        }

        if (!activity?.isFinishing!!) {
            invalidDialog = builder.create()
            invalidDialog!!.setCancelable(false)
            invalidDialog!!.show()
        }
    }

    override fun onDestroy() {
        if (invalidDialog != null) {
            invalidDialog!!.dismiss()
            invalidDialog = null
        }
        super.onDestroy()
    }

    override fun clearPreferences() {
        startActivity(Intent(activity, LoginActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        activity?.finish()
    }

    override fun showProgressLoading() {
        RuthUtility.showLoader(activity!!)

        //progressBarProfile.visibility = View.VISIBLE
    }

    override fun hideProgressLoading() {
        RuthUtility.hideLoader()
        //  progressBarProfile.visibility = View.GONE
    }

    override fun onResume() {
        super.onResume()
//        view!!.tvGiftCountGold.text = RuthUtility.getCounts(HomeActivity.goldPoints)
//        view!!.tvGiftCountSilver.text = RuthUtility.getCounts(HomeActivity.silverPoints)
//        view!!.tvGiftCountCoin.text = RuthUtility.getCounts(HomeActivity.yuvanPoints)
    }

    /*--------------------------------------------------------------------------------------------*/
    private var mSocket: Socket? = null
    private var isConnected: Boolean? = true

    private fun setUpSocket() {
        val app = activity?.application as App
        mSocket = app.socket
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
        mSocket!!.connect()
    }

    private val onConnect = Emitter.Listener {
        activity?.runOnUiThread(Runnable {
            if (!isConnected!!) {
                isConnected = true
            }
        })
    }

    private val onDisconnect = Emitter.Listener {
        activity?.runOnUiThread {
            Log.i(TAG, "disconnected")
            isConnected = false
        }
    }

    private val onConnectError = Emitter.Listener {
        activity?.runOnUiThread {
            Log.e(TAG, "Error connecting")
        }
    }
}