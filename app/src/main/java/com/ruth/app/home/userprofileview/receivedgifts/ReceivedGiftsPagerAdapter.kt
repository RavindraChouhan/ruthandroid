package com.ruth.app.home.userprofileview.receivedgifts

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class ReceivedGiftsPagerAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {

        return ReceivedGiftFragment()
    }

    override fun getCount(): Int {
        return 4
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return super.getPageTitle(position)
    }

//    override fun saveState(): Parcelable {
//        val bundle = saveState() as Bundle
//        bundle.putParcelableArray("states", null) // Never maintain any states from the base class to avoid TransactionTooLargeException
//        return bundle
//    }
}