package com.ruth.app.home.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class PodcastsFragmentPresenter @Inject constructor() : BasePresenter<PodcastsFragmentPresenter.PodcastsFragmentView>() {

    override fun onCreate(view: PodcastsFragmentView) {
        super.onCreate(view)
    }
    interface PodcastsFragmentView : BaseView {

    }
}