package com.ruth.app.home.userprofileview.account

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.ruth.app.home.userprofileview.account.fragment.IdentifyVerification
import com.ruth.app.home.userprofileview.account.fragment.Levels
import com.ruth.app.home.userprofileview.account.fragment.ReferralCode
import com.ruth.app.home.userprofileview.account.fragment.UserAccountFragment

class UserAccountAdapter(supportFragmentManager: FragmentManager,
                         var tabCount: Int) : FragmentStatePagerAdapter(supportFragmentManager) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> UserAccountFragment()
            1 -> IdentifyVerification()
            2 -> Levels()
            3 -> ReferralCode()
            else -> UserAccountFragment()
        }
    }

    override fun getCount(): Int {
        return tabCount
    }
}