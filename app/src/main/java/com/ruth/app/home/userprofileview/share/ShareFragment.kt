package com.ruth.app.home.userprofileview.share

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import com.ruth.app.utility.withArguments
import kotlinx.android.synthetic.main.fragment_recyclerview.view.*

class ShareFragment : Fragment() {

    private val summonList: ArrayList<Share>? by lazy { arguments?.getParcelableArrayList<Share>(SUMMON) }
    private var adapter: ShareListAdapter? = null

    companion object {
        const val SUMMON = "FAQ"
    }

    fun newInstance(data: ArrayList<Share>): ShareFragment {
        val args = Bundle()
        args.putParcelableArrayList(SUMMON, data)
        return ShareFragment().withArguments(args)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_recyclerview, container, false)

        view.rvUserTabList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        adapter = ShareListAdapter(activity, summonList!!)
        view.rvUserTabList!!.adapter = adapter
        adapter!!.setOnItemClickListener(object : ShareListAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {

            }
        })
        return view
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            fragmentManager!!.beginTransaction().detach(this).attach(this).commit()
        }
    }
}