package com.ruth.app.home.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.livebroadcasting.utils.Utils
import com.ruth.app.R
import com.ruth.app.broadcaster.adapter.FollowerAdapter
import com.ruth.app.home.model.RuthChannelsData
import com.ruth.app.utility.RuthUtility
import kotlinx.android.synthetic.main.item_home_liveuser.view.*
import java.util.*

class HomeLiveUserAdapter(private val context: Context, var data: ArrayList<RuthChannelsData>)
    : RecyclerView.Adapter<HomeLiveUserAdapter.LiveUserViewHolder>() {

    private lateinit var recyclerView: RecyclerView
    private var onItemClicklistener: OnItemClickListener? = null
    private var adapter: FollowerAdapter? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LiveUserViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_home_liveuser, parent, false)
        return LiveUserViewHolder(view)
    }

    override fun onBindViewHolder(holder: LiveUserViewHolder, position: Int) {
        holder.bind(data[position])
        setEmojisList(holder.itemView, data[position].emoji)
        holder.itemView.setOnClickListener {
            onItemClicklistener!!.onItemClick(holder.itemView, position)
        }
    }

    inner class LiveUserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(ruthChannelsData: RuthChannelsData) {

            Glide.with(context).load(ruthChannelsData.cover)
                    .apply(RequestOptions().error(R.drawable.background)
                            .placeholder(R.drawable.background)
                            .diskCacheStrategy(DiskCacheStrategy.NONE))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            itemView.progressBarCoverImage.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            itemView.progressBarCoverImage.visibility = View.GONE
                            return false
                        }

                    })
                    .into(itemView.videoCoverImage)
            itemView.video_title.text = ruthChannelsData.title
            when {
                ruthChannelsData.views != 0 -> {
                    itemView.tvViewCount.visibility = View.VISIBLE
                    itemView.tvViewCount.text = RuthUtility.getCounts(ruthChannelsData.views!!)
                }
            }

            when {
                ruthChannelsData.likes != 0 -> {
                    itemView.tvLikesCount.visibility = View.VISIBLE
                    itemView.tvLikesCount.text = RuthUtility.getCounts(ruthChannelsData.likes!!)
                }
            }

            when {
                ruthChannelsData.subscribers != 0 -> {
                    itemView.lbl_subscriber_count.visibility = View.VISIBLE
                    itemView.lbl_subscriber_count.text = RuthUtility.getCounts(ruthChannelsData.subscribers!!)
                }
            }

            itemView.video_subtitle.text = ruthChannelsData.description
            itemView.lbl_timeTV.text = (Utils.convertTime(ruthChannelsData.startTime!!.toInt())).toString() + "-" + (Utils.convertTime(ruthChannelsData.endTime!!.toInt())).toString() + " " + Utils.findAmOrPm(ruthChannelsData.endTime!!.toInt())

            if (ruthChannelsData.onAir == "live") {
                itemView.shadow_view.visibility = View.VISIBLE
                itemView.shadow_view.shadowColor = context.resources.getColor(R.color.red)
                itemView.shadow_view.backgroundClr = context.resources.getColor(R.color.red)
            } else if (ruthChannelsData.onAir == "upcoming") {
                itemView.shadow_view.visibility = View.VISIBLE
            }

            itemView.recycle_followers.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = FollowerAdapter(context, ruthChannelsData.broadcastData!!)
            itemView.recycle_followers.adapter = adapter
        }
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    private fun setEmojisList(view: View, count: Int?) {
        view.rvEmojis_Home.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        val adapter = EmojisHomeAdapter(context, count!!)
        view.rvEmojis_Home!!.adapter = adapter
    }


    override fun getItemCount(): Int {
        return data.size
    }

    fun setOnItemClickListener(onItemClickList: OnItemClickListener) {
        this.onItemClicklistener = onItemClickList
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}