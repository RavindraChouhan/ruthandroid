package com.ruth.app.home.eightbutton.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class BibleDetailPresenter @Inject constructor(): BasePresenter<BibleDetailPresenter.BibleDetailPresenterView>() {
    interface BibleDetailPresenterView : BaseView{

    }
}