package com.ruth.app.home.eightbutton.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class BiblePagerPresenter @Inject constructor() : BasePresenter<BiblePagerPresenter.BiblePagerPresenterView>() {

    interface BiblePagerPresenterView : BaseView {

    }
}