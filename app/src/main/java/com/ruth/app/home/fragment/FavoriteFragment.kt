package com.ruth.app.home.fragment

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.fragment.BaseFragment
import com.ruth.app.home.adapter.FavoriteViewPagerAdapter
import com.ruth.app.home.model.Favorite
import com.ruth.app.home.presenter.FavoriteFragmentPresenter
import kotlinx.android.synthetic.main.fragment_favorite.view.*

class FavoriteFragment : BaseFragment<FavoriteFragmentPresenter>(), FavoriteFragmentPresenter.FavoriteFragmentView {

    private var favoritePagerAdapter: FavoriteViewPagerAdapter? = null
    private var favoriteList: ArrayList<Favorite>? = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_favorite, container, false)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        setupTabLayout(view)

        return view
    }

    private fun setupTabLayout(view: View) {
        favoriteList?.clear()
        for (i in 0 until 5) {
            favoriteList?.add(Favorite("張大文", resources.getDrawable(R.drawable.com_facebook_profile_picture_blank_square), 1234))
        }
        favoritePagerAdapter = FavoriteViewPagerAdapter(childFragmentManager, favoriteList)
        view.viewPagerFavorite!!.adapter = favoritePagerAdapter
        view.tabFavorite.setupWithViewPager(view.viewPagerFavorite)

        view.tabFavorite.getTabAt(0)!!.setText(R.string.user)
        view.tabFavorite.getTabAt(1)!!.setText(R.string.live)
        view.tabFavorite.getTabAt(2)!!.setText(R.string.videos)
        view.tabFavorite.getTabAt(3)!!.setText(R.string.podcast)
        view.tabFavorite.getTabAt(4)!!.setText(R.string.pictures)
        view.tabFavorite.getTabAt(5)!!.setText(R.string.articles)


        view.tabFavorite.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                val position = tab.position

                when (position) {
                    0 -> {
                        favoriteList?.clear()
                        for (i in 0 until 5) {
                            favoriteList?.add(Favorite("張大文", resources.getDrawable(R.drawable.com_facebook_profile_picture_blank_square), 1234))
                        }
                    }
                    1 -> {
                        favoriteList?.clear()
                        for (i in 0 until 10) {
                            favoriteList?.add(Favorite("張大文", resources.getDrawable(R.drawable.contact_placeholder), 1234))
                        }
                    }
                    2 -> {
                        favoriteList?.clear()
                        for (i in 0 until 6) {
                            favoriteList?.add(Favorite("張大文", resources.getDrawable(R.drawable.com_facebook_profile_picture_blank_square), 1234))
                        }
                    }
                    3 -> {
                        favoriteList?.clear()
                        for (i in 0 until 3) {
                            favoriteList?.add(Favorite("張大文", resources.getDrawable(R.drawable.contact_placeholder), 1234))
                        }
                    }
                    4 -> {
                        favoriteList?.clear()
                        for (i in 0 until 1) {
                            favoriteList?.add(Favorite("張大文", resources.getDrawable(R.drawable.com_facebook_profile_picture_blank_square), 1234))
                        }
                    }
                    5 -> {
                        favoriteList?.clear()
                        for (i in 0 until 2) {
                            favoriteList?.add(Favorite("張大文", resources.getDrawable(R.drawable.contact_placeholder), 1234))
                        }
                    }
                }
            }
        })
    }
}