package com.ruth.app.home.eightbutton.activity

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.annotation.DrawableRes
import android.support.design.widget.BottomSheetBehavior
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.home.eightbutton.model.MapModel
import com.ruth.app.home.eightbutton.presenter.MapPresenter
import com.ruth.app.home.maps.activity.FilterActivity
import com.ruth.app.home.maps.adapter.MapAdapter
import com.ruth.app.utility.Constants
import com.ruth.app.utility.map.PolyUtil
import com.ruth.app.widgets.CalendarView
import kotlinx.android.synthetic.main.activity_map.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashSet


class MapsActivity : BaseActivity<MapPresenter>(), MapPresenter.MapView, OnMapReadyCallback, GoogleMap.OnMarkerClickListener, View.OnClickListener {


    private lateinit var mMap: GoogleMap
    var style: MapStyleOptions? = null
    private val latLagArr: ArrayList<LatLng>? = ArrayList()
    private val markerArr: ArrayList<Marker>? = ArrayList()
    private var previousMarker: Marker? = null
    private var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        btn_back.setOnClickListener(this)
        btn_join_now.setOnClickListener(this)
        filter_icon.setOnClickListener(this)
        et_search.setOnClickListener(this)

        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)

        recycle_map.layoutManager = LinearLayoutManager(presenter.context, LinearLayoutManager.HORIZONTAL, false)
        val adapter = MapAdapter(presenter.context)
        recycle_map.adapter = adapter
        adapter.setOnItemClickListener(object : MapAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_EXPANDED
                lyt_marker.visibility = View.VISIBLE
                lyt_search.visibility = View.GONE
                search_clear.visibility = View.GONE
                if (position > 3) return
                if (previousMarker != null) {
                    previousMarker!!.setIcon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.marker_black)))
                }
                markerArr!![position].setIcon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.marker_green)))
                previousMarker = markerArr[position]
                getRoutesEncoded("28.083642, 110.856109", markerArr[position].position.latitude.toString() + "," + markerArr[position].position.longitude.toString())
            }
        })

        bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_COLLAPSED
        bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_EXPANDED
        bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_HIDDEN

        bottomSheetBehavior!!.peekHeight = 340

        bottomSheetBehavior!!.setBottomSheetCallback(
                object : BottomSheetBehavior.BottomSheetCallback() {
                    override fun onSlide(bottomSheet: View, slideOffset: Float) {

                    }

                    override fun onStateChanged(bottomSheet: View, newState: Int) {
                        when (newState) {
                            BottomSheetBehavior.STATE_EXPANDED -> {
                                if (lyt_marker.visibility == View.VISIBLE) {
                                    btn_join_now.visibility = View.VISIBLE
                                    fab.visibility = View.VISIBLE
                                }
                                recycle_map.visibility = View.GONE
                                fab1.visibility = View.GONE
                            }
                            BottomSheetBehavior.STATE_HIDDEN -> {
                                btn_join_now.visibility = View.GONE
                                recycle_map.visibility = View.VISIBLE
                                fab1.visibility = View.VISIBLE
                                fab.visibility = View.GONE
                                search_clear.visibility = View.GONE
                                if (previousMarker != null) {
                                    previousMarker!!.setIcon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.marker_black)))
                                }
                                if (polyline != null) {
                                    polyline!!.remove()
                                    polyline = null
                                }
                            }
                        }
                    }
                })
    }

    override fun onClick(v: View?) {
        when (v) {
            btn_back -> {
                finish()
                overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)

            }
            btn_join_now -> {
                openApplicationForm()
            }
            filter_icon -> {
                startActivity(Intent(presenter.context, FilterActivity::class.java))
            }
            et_search -> {
                layoutSearchFriends.visibility = View.GONE
                search_view.visibility = View.VISIBLE
                lyt_marker.visibility = View.GONE
                lyt_search.visibility = View.VISIBLE
                btn_join_now.visibility = View.GONE
                search_clear.visibility = View.VISIBLE
                bottomSheetBehavior!!.peekHeight = 250
                bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_EXPANDED

            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        val tempLat = 27.919800
        val tempLng = 836.333014

        latLagArr!!.add(LatLng(tempLat, tempLng))
        latLagArr.add(LatLng(28.483177, 107.544771))
        latLagArr.add(LatLng(31.676758, 113.694424))
        latLagArr.add(LatLng(25.824617, 104.557796))

        mMap = googleMap
        mMap.setOnMapClickListener {
            bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_HIDDEN
        }
        try {
            style = MapStyleOptions.loadRawResourceStyle(this, R.raw.google_map_style)
            //val style = MapStyleOptions.loadRawResourceStyle(this.context, R.raw.my_map_style)
        } catch (e: Resources.NotFoundException) {
            // Oops, looks like the map style resource couldn't be found!
        }
        mMap.setMapStyle(style)
        markerArr!!.add(mMap.addMarker(MarkerOptions()
                .position(LatLng(28.083642, 110.856109))
                .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromViewSingle(R.drawable.user_marker)))))

        for (i in 0..3) {
            if (i == 1) {
                markerArr!!.add(mMap.addMarker(MarkerOptions()
                        .position(latLagArr[1])
                        .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.marker_black))))
                )
            } else {
                val marker = mMap.addMarker(MarkerOptions()
                        .position(latLagArr[i])
                        .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.marker_black))))
                markerArr!!.add(marker)

            }
        }

        mMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(28.083642, 110.856109)))
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(28.083642, 110.856109), 5.0f))
        mMap.setOnMarkerClickListener(this)
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        if (marker!!.id == markerArr!![0].id) return true
        clickOnMarker(marker)
        return true
    }

    private fun clickOnMarker(marker: Marker?) {
        if (previousMarker != null) {
            previousMarker!!.setIcon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.marker_black)))
        }
        marker!!.setIcon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.marker_green)))
        previousMarker = marker
        getRoutesEncoded(markerArr!![0].position.latitude.toString() + "," + markerArr[0].position.longitude.toString(), marker.position.latitude.toString() + "," + marker.position.longitude.toString())
    }

    private fun getMarkerBitmapFromView(@DrawableRes resId: Int): Bitmap {

        val customMarkerView = (getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.marker_cell, null)
        val markerImageView = customMarkerView.findViewById(R.id.profile_image) as ImageView
        markerImageView.setImageResource(resId)
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        customMarkerView.layout(0, 0, customMarkerView.measuredWidth, customMarkerView.measuredHeight)
        customMarkerView.buildDrawingCache()
        val returnedBitmap = Bitmap.createBitmap(customMarkerView.measuredWidth, customMarkerView.measuredHeight,
                Bitmap.Config.ARGB_4444)
        val canvas = Canvas(returnedBitmap)
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
        val drawable = customMarkerView.background
        if (drawable != null)
            drawable.draw(canvas)
        customMarkerView.draw(canvas)
        return returnedBitmap
    }

    private fun getMarkerBitmapFromViewSingle(@DrawableRes resId: Int): Bitmap {

        val customMarkerView = (getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.user_marker_cell, null)
        val markerImageView = customMarkerView.findViewById(R.id.profile_image) as ImageView
        markerImageView.setImageResource(resId)
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        customMarkerView.layout(0, 0, customMarkerView.measuredWidth, customMarkerView.measuredHeight)
        customMarkerView.buildDrawingCache()
        val returnedBitmap = Bitmap.createBitmap(customMarkerView.measuredWidth, customMarkerView.measuredHeight,
                Bitmap.Config.ARGB_4444)
        val canvas = Canvas(returnedBitmap)
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
        val drawable = customMarkerView.background
        if (drawable != null)
            drawable.draw(canvas)
        customMarkerView.draw(canvas)
        return returnedBitmap
    }

    private var polyline: Polyline? = null

    fun getRoutesEncoded(origin: String, destination: String) {
        var ret: String
        val call = Constants.googldirectionService.getDirection(origin, destination, resources.getString(R.string.google_maps_key))
        call.enqueue(object : Callback<MapModel> {
            override fun onFailure(call: Call<MapModel>?, t: Throwable?) {
                t!!.printStackTrace()
            }

            override fun onResponse(call: Call<MapModel>?, response: Response<MapModel>?) {
                if (response!!.isSuccessful) {
                    if (response.body()!!.routes!!.isEmpty()) return
                    ret = response.body()!!.routes!![0].overview_polyline!!.points!!
                    val decodedPath = PolyUtil.decode(ret)
                    val polyLineOptions = PolylineOptions()
                    polyLineOptions.color(resources.getColor(R.color.green))
                    if (polyline != null) {
                        polyline!!.remove()
                        polyline = null
                    }
                    polyLineOptions.addAll(decodedPath).geodesic(true)
                    polyline = mMap.addPolyline(polyLineOptions)
                }
            }
        })
    }

    override fun onBackPressed() {
        if (b != null) {
            if (b!!.isShowing) {
                b?.dismiss()
            } else {
                super.onBackPressed()
            }
        } else if (bottomSheetBehavior!!.state == BottomSheetBehavior.STATE_EXPANDED) {
            if (search_view.visibility == View.VISIBLE) {
                search_view.visibility = View.GONE
                layoutSearchFriends.visibility = View.VISIBLE
            }
            bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_HIDDEN

        } else {
            super.onBackPressed()
        }
    }

    private var b: AlertDialog? = null

    private fun openApplicationForm() {
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_map_application_form, null)
        dialogBuilder.setView(dialogView)
        b = dialogBuilder.create()
        b!!.window.attributes.windowAnimations = R.style.DialogAnimation

        val layoutDate = dialogView.findViewById<View>(R.id.layoutDate) as RelativeLayout

        layoutDate.setOnClickListener {
            openDateCalendar()
        }


        b?.setCancelable(true)
        b?.show()

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)

        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b!!.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.9f).toInt()
        val dialogWindowHeight = (displayHeight * 0.7f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        b?.window?.attributes = layoutParams
    }

    private fun openDateCalendar() {
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_date_calendar, null)
        dialogBuilder.setView(dialogView)
        b = dialogBuilder.create()
        b!!.window.attributes.windowAnimations = R.style.DialogAnimation

        val ivCloseCalendar = dialogView.findViewById<View>(R.id.ivCloseCalendar) as ImageView
        val cv = dialogView.findViewById<View>(R.id.calendar_view) as CalendarView

        val events: HashSet<Date> = HashSet()
        events.add(Date())

        cv.updateCalendar(events)

        // assign event handler
        cv.setEventHandler(object : CalendarView.EventHandler {
            override fun onDayLongPress(date: Date) {
                // show returned day
                val df = SimpleDateFormat.getDateInstance()
                Toast.makeText(this@MapsActivity, df.format(date), Toast.LENGTH_SHORT).show()
            }
        })

        ivCloseCalendar.setOnClickListener {
            b?.dismiss()
        }

        b?.setCancelable(false)
        b?.show()

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)

        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b!!.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.9f).toInt()
        val dialogWindowHeight = (displayHeight * 0.8f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        b?.window?.attributes = layoutParams
    }
}