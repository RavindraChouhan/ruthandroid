package com.ruth.app.home.eightbutton.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class SitnTalkPresenter@Inject constructor() : BasePresenter<SitnTalkPresenter.SitnTalkView>() {

    override fun onCreate(view: SitnTalkView) {
        super.onCreate(view)

    }

    interface SitnTalkView : BaseView {

    }
}