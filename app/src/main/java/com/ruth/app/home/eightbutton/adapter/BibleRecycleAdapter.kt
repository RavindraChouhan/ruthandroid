package com.ruth.app.home.eightbutton.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import com.ruth.app.home.adapter.FollowersAdapter
import com.ruth.app.home.eightbutton.activity.BibleDetailActivity
import kotlinx.android.synthetic.main.item_latest_story.view.*

class BibleRecycleAdapter(val  mContext: Context) : RecyclerView.Adapter<BibleRecycleAdapter.ViewHolder>() {

    var list: ArrayList<Int> = ArrayList()

    override fun getItemCount(): Int {
        return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.recycle_follower.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        holder.itemView.recycle_follower.adapter = FollowersAdapter(mContext,list)

        holder.itemView.setOnClickListener {
            mContext.startActivity(Intent(mContext, BibleDetailActivity::class.java))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_bible_cell, parent, false)
        return ViewHolder(view)
    }

    inner class ViewHolder(view: View):RecyclerView.ViewHolder(view) {


    }

}
