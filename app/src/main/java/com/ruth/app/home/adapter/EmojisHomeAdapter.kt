package com.ruth.app.home.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R

class EmojisHomeAdapter(context: Context,var count:Int): RecyclerView.Adapter<EmojisHomeAdapter.EmojisViewHolder>() {


    private lateinit var recyclerView: RecyclerView

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmojisViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_emojis_home, parent, false)
        return EmojisViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (count>=5){
            5
        }else {
            count
        }
    }

    override fun onBindViewHolder(holder: EmojisViewHolder, position: Int) {

    }


    inner class EmojisViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    }
}


