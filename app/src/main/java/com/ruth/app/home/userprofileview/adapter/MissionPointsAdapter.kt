package com.ruth.app.home.userprofileview.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import kotlinx.android.synthetic.main.item_cell_mission.view.*

class MissionPointsAdapter(val mContext: Context, private val missionPointsList: ArrayList<String>?)
    : RecyclerView.Adapter<MissionPointsAdapter.ViewHolder>() {

    private var isRedeemed: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_cell_mission, parent, false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(missionPointsList!![position])

        holder.itemView.btnRedeem.setOnClickListener {
            if (isRedeemed) {
                isRedeemed = false
                holder.itemView.btnRedeem.setBackgroundResource(R.drawable.popup_calendar_points_timeout)
            } else {
                isRedeemed = true
                holder.itemView.btnRedeem.setBackgroundResource(R.drawable.btn_rect_green_bg)
            }
        }
    }

    override fun getItemCount(): Int {
        return missionPointsList?.size!!
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(missionPointsList: String) {
            itemView.tvCheckinType.text = missionPointsList
        }
    }
}