package com.ruth.app.home.userprofileview.activity

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.ruth.app.R
import com.ruth.app.home.userprofileview.adapter.TopUpAdapter
import kotlinx.android.synthetic.main.activity_top_up.*

class TopUpActivity : AppCompatActivity() {

    private var mContext: Context? = null

    private var adapter: TopUpAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_top_up)
        mContext = this

        ivtopupBack.setOnClickListener {
            finish()
        }

        rv_topup.layoutManager = LinearLayoutManager(this)
        adapter = TopUpAdapter(mContext!!)
        rv_topup!!.adapter = adapter
    }
}
