package com.ruth.app.home.userprofileview.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.ruth.app.R
import com.ruth.app.home.userprofileview.adapter.MissionPointsAdapter
import kotlinx.android.synthetic.main.activity_mission.*

class MissionActivity : AppCompatActivity(), View.OnClickListener {

    var missionPointsList: ArrayList<String>? = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mission)

        ivBackMission.setOnClickListener(this)

        rvMissions.layoutManager = LinearLayoutManager(this)
        val adapter = MissionPointsAdapter(this, missionPointsList)
        rvMissions!!.adapter = adapter

        missionPointsList?.clear()
        missionPointsList?.add("First day on Ruth")
        missionPointsList?.add("Share a live broadcast")
        missionPointsList?.add("First time going live")

        rvMissions!!.adapter.notifyDataSetChanged()

    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.ivBackMission) {
            finish()
        }
    }
}
