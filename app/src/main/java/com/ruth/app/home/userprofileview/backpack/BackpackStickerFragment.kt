package com.ruth.app.home.userprofileview.backpack

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import kotlinx.android.synthetic.main.fragment_backpack.view.*

class BackpackStickerFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_backpack, container, false)

        for (i in 0 until BackpackActivity.stickerItemList.size) {
            if (BackpackActivity.BACKPACK_TYPE == BackpackActivity.stickerItemList[i].titleName) {
                val adapter = BackpackStickerAdapter(activity, BackpackActivity.stickerItemList[i].stickersData!!)
                view.rvBackpackStickers.adapter = adapter
            }
        }
        return view
    }
}