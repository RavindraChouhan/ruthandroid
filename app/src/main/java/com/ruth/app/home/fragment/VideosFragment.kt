package com.ruth.app.home.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.fragment.BaseFragment
import com.ruth.app.home.presenter.VideoFragmentPresenter

class VideosFragment : BaseFragment<VideoFragmentPresenter>(), VideoFragmentPresenter.VideoFragmentView {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_home_videos, container, false)
        App.getComponent().inject(this)
        presenter.onCreate(this)
        return view
    }

}