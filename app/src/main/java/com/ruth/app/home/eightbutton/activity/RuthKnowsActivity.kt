package com.ruth.app.home.eightbutton.activity

import android.os.Bundle
import android.support.design.widget.TabLayout
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.home.eightbutton.adapter.RuthKnowsPagerAdapter
import com.ruth.app.home.eightbutton.presenter.RuthKnowsPresenter
import kotlinx.android.synthetic.main.activity_ruth_knows.*

class RuthKnowsActivity : BaseActivity<RuthKnowsPresenter>(), RuthKnowsPresenter.RuthKnowsPresenterView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)
        setContentView(R.layout.activity_ruth_knows)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        setupTabLayout()
        btn_back_ruthknows.setOnClickListener {
            finish()
        }
    }

    private fun setupTabLayout() {


        val adapter = RuthKnowsPagerAdapter(supportFragmentManager, tab_ruthknows.tabCount)
        viewPager_ruthknows!!.adapter = adapter
        tab_ruthknows.setupWithViewPager(viewPager_ruthknows)

        tab_ruthknows.getTabAt(0)!!.setText(R.string.all)
        tab_ruthknows.getTabAt(1)!!.setText(R.string.pictures)
        tab_ruthknows.getTabAt(2)!!.setText(R.string.articles)
        tab_ruthknows.getTabAt(3)!!.setText(R.string.picturesplusarticle)



        tab_ruthknows.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {


            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                val position = tab.position
                viewPager_ruthknows.setCurrentItem(position, true)

            }
        })
    }

}
