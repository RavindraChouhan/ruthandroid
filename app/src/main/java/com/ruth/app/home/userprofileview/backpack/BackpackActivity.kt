package com.ruth.app.home.userprofileview.backpack

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ruth.app.R
import kotlinx.android.synthetic.main.activity_backpack.*

class BackpackActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        val stickerItemList: ArrayList<BackpackStickers> = ArrayList()
        val allStickers: ArrayList<BackpackStickersData>? = ArrayList()
        var BACKPACK_TYPE: String = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_backpack)

        createStickerList()
        setupTabLayout()
        ivBackpack.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.ivBackpack) {
            finish()
        }
    }

    fun setupTabLayout() {
        BACKPACK_TYPE = stickerItemList[0].titleName
        val adapter = BackpackViewPagerAdapter(supportFragmentManager, stickerItemList.size)
        viewPagerBackpack.adapter = adapter
        tabBackpack.setupWithViewPager(viewPagerBackpack)

        for (i in 0 until stickerItemList.size) {
            tabBackpack.getTabAt(i)!!.text = stickerItemList[i].titleName
        }

//        tabBackpack.getTabAt(0)!!.text = resources.getString(R.string.all)
//        tabBackpack.getTabAt(1)!!.text = resources.getString(R.string.positive_gift)
//        tabBackpack.getTabAt(2)!!.text = resources.getString(R.string.negative_gift)
//        tabBackpack.getTabAt(3)!!.text = resources.getString(R.string.special_editions)

        tabBackpack.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                val position = tab!!.position
                BACKPACK_TYPE = stickerItemList[position].titleName
                viewPagerBackpack.setCurrentItem(position, true)
            }
        })
    }

    private fun createStickerList() {
        allStickers!!.clear()
        stickerItemList.clear()

        for (i in 0 until 6) {
            allStickers.add(BackpackStickersData(resources.getDrawable(R.drawable.icon_sticker_one), "sticker$i", "Gold", "10"))
        }
        stickerItemList.add(0, BackpackStickers(resources.getString(R.string.all), allStickers))
        stickerItemList.add(1, BackpackStickers(resources.getString(R.string.positive_gift), allStickers))
        stickerItemList.add(2, BackpackStickers(resources.getString(R.string.negative_gift), allStickers))
        stickerItemList.add(3, BackpackStickers(resources.getString(R.string.special_editions), allStickers))

//        allStickers.clear()
//        backpackStickers.clear()
//        allStickers.add(BackpackStickersData(resources.getDrawable(R.drawable.icon_sticker_one), "pg1", "Gold", "10"))
//        allStickers.add(BackpackStickersData(resources.getDrawable(R.drawable.icon_sticker_one), "pg2", "Gold", "10"))
//        allStickers.add(BackpackStickersData(resources.getDrawable(R.drawable.icon_sticker_one), "pg3", "Gold", "10"))
//        allStickers.add(BackpackStickersData(resources.getDrawable(R.drawable.icon_sticker_one), "pg4", "Gold", "10"))
//        for (i in 0 until allStickers.size) {
//            backpackStickers.add(allStickers[i])
//        }
//        stickerItemList.add(1, BackpackStickers(resources.getString(R.string.positive_gift), backpackStickers))
//
//        allStickers.clear()
//        backpackStickers.clear()
//        allStickers.add(BackpackStickersData(resources.getDrawable(R.drawable.icon_sticker_one), "ng1", "Gold", "10"))
//        allStickers.add(BackpackStickersData(resources.getDrawable(R.drawable.icon_sticker_one), "ng2", "Gold", "10"))
//        allStickers.add(BackpackStickersData(resources.getDrawable(R.drawable.icon_sticker_one), "ng3", "Gold", "10"))
//        allStickers.add(BackpackStickersData(resources.getDrawable(R.drawable.icon_sticker_one), "ng4", "Gold", "10"))
//        for (i in 0 until allStickers.size) {
//            backpackStickers.add(allStickers[i])
//        }
//        stickerItemList.add(2, BackpackStickers(resources.getString(R.string.negative_gift), backpackStickers))
//
//        allStickers.clear()
//        backpackStickers.clear()
//        allStickers.add(BackpackStickersData(resources.getDrawable(R.drawable.icon_sticker_one), "se1", "Gold", "10"))
//        allStickers.add(BackpackStickersData(resources.getDrawable(R.drawable.icon_sticker_one), "se2", "Gold", "10"))
//        allStickers.add(BackpackStickersData(resources.getDrawable(R.drawable.icon_sticker_one), "se3", "Gold", "10"))
//        allStickers.add(BackpackStickersData(resources.getDrawable(R.drawable.icon_sticker_one), "se4", "Gold", "10"))
//        for (i in 0 until allStickers.size) {
//            backpackStickers.add(allStickers[i])
//        }
//        stickerItemList.add(3, BackpackStickers(resources.getString(R.string.special_editions), backpackStickers))
    }
}
