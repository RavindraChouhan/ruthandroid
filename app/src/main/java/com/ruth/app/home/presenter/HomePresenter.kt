package com.ruth.app.home.presenter

import android.widget.Toast
import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.home.model.EightButtonsData
import com.ruth.app.network.config.RuthApi
import com.ruth.app.utility.RuthUtility
import javax.inject.Inject

class HomePresenter @Inject constructor(private val ruthApi: RuthApi) : BasePresenter<HomePresenter.HomeView>() {

    override fun onCreate(view: HomeView) {
        super.onCreate(view)
    }

    fun clear() {
        val tempFb = utility.getBooleaPreferences(context, getString(R.string.isfbSignup))
        val tempGoogle = utility.getBooleaPreferences(context, getString(R.string.isGoogleSignup))
        val tempInsta = utility.getBooleaPreferences(context, getString(R.string.isInstaSignup))

        preferences.clearPreferences()

        utility.setBooleanPreferences(context, getString(R.string.isfbSignup), tempFb)
        utility.setBooleanPreferences(context, getString(R.string.isGoogleSignup), tempGoogle)
        utility.setBooleanPreferences(context, getString(R.string.isInstaSignup), tempInsta)

        view?.clearPreferences()
    }

//    fun postPayment(isPayment: Boolean) {
//        val hm = HashMap<String, @JvmSuppressWildcards Any>()
//        hm["isPayment"] = isPayment
//        hm["userId"] = preferences.getUserInfo().id
//        ruthApi.postPayment(preferences.getUserInfo().userToken, hm)
//                .subscribe {
//                    if (it != null) {
//                        if (it.status) {
//                            getMyProfile()
//                            view?.openSharePage()
//                        } else if (it.message.contains(context.getString(R.string.device_login), true)
//                                || it.zh_Hans.contains(context.getString(R.string.device_login), true)
//                                || it.zh_Hant.contains(context.getString(R.string.device_login), true)) {
//                            view?.invalidLogin()
//                        } else {
//                            Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
//                        }
//                    }
//                }
//    }

    fun getMyProfile() {
        ruthApi.getUserInfo(preferences.getUserInfo().userToken, preferences.getUserInfo().id)
                .subscribe({
                    if (it != null) {
                        if (it.status) {
                            preferences.setUserProfile(it.data)
                            if (it.data.pointEarn!!.isNotEmpty()) {
                                RuthUtility.updatePoints(context, it.data.pointEarn)
                            }
                            // view?.savePoints()
                        } else {
                            // view?.invalidLogin()
                        }
                    }
                }, {
                    view?.showToast(getString(R.string.connection_error))
                })
    }

    fun getEightButtons() {
        if (utility.isConnectingToInternet(context)) {
            eightButtonApi()
        } else {
            val thread = Thread(Runnable {
                Runtime.getRuntime().gc()
                val internetConnection = utility.checkInternetWithoutDialog(context)
                if (internetConnection) {
                    eightButtonApi()
                } else {
                    getEightButtons()
                }
            })
            thread.start()
        }
    }

    private fun eightButtonApi() {
        ruthApi.getButtonEffect()
                .onErrorReturn { (null) }
                .subscribe {
                    if (it != null) {
                        if (it.status) {
                            view?.setEightButtons(it.data)
                        }
                    }
                }
    }

    fun getVerificationCode() {
        ruthApi.getVerificationCode(preferences.getUserInfo().userToken)
                .onErrorReturn { null }
                .subscribe {
                    if (it != null) {
                        Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                        if (it.status) {
                            view!!.openVerificationCode(it.data!!.verificationCode)
                        } else {
                            view!!.hideProgressLoading()
                        }
                    } else {
                        view!!.hideProgressLoading()
                    }
                }
    }

    interface HomeView : BaseView {
        fun clearPreferences()
        fun invalidLogin()
        fun savePoints()
        fun setEightButtons(data: ArrayList<EightButtonsData>)
        fun openVerificationCode(verificationCode: Int)
        fun hideProgressLoading()
//        fun openSharePage()
    }

}