package com.ruth.app.home.eightbutton.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class MapFilterPresenter @Inject constructor() : BasePresenter<MapFilterPresenter.MapFilterPresenterView>() {


    interface MapFilterPresenterView : BaseView {

    }
}