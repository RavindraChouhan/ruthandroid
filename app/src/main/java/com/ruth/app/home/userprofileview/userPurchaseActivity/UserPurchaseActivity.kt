package com.ruth.app.home.userprofileview.userPurchaseActivity

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ruth.app.R
import com.ruth.app.home.userprofileview.share.Share
import com.ruth.app.utility.Utility
import kotlinx.android.synthetic.main.activity_user_viewpager_tab.*
import javax.inject.Inject

class UserPurchaseActivity : AppCompatActivity(), View.OnClickListener {

    @Inject
    lateinit var utility: Utility
    private var purchasePagerAdapter: PurchasePagerAdapter? = null
    private var shareList: ArrayList<Share>? = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_viewpager_tab)

        tvHeader.text = getString(R.string.purchase)
        ivUserTabBack.setOnClickListener(this)

        setupTabLayout()
    }

    override fun onClick(v: View?) {
        if (v == ivUserTabBack) {
            finish()
        }
    }


    private fun setupTabLayout() {

        val tabCount: Int
        val tabCheck = intent.getBooleanExtra("addOthers", false)
        tabCount = if (tabCheck) {
            5
        } else {
            4
        }

        purchasePagerAdapter = PurchasePagerAdapter(supportFragmentManager/*, shareList*/, tabCount)
        viewPagerUser.adapter = purchasePagerAdapter
        tablayoutUserTab.setupWithViewPager(viewPagerUser)

        if (tabCheck) {
            tablayoutUserTab.getTabAt(0)!!.setText(R.string.all)
            tablayoutUserTab.getTabAt(1)!!.setText(R.string.positive_gift)
            tablayoutUserTab.getTabAt(2)!!.setText(R.string.negative_gift)
            tablayoutUserTab.getTabAt(3)!!.setText(R.string.special_editions)
            tablayoutUserTab.getTabAt(4)!!.setText(R.string.others)
        } else {
            tablayoutUserTab.getTabAt(0)!!.setText(R.string.all)
            tablayoutUserTab.getTabAt(1)!!.setText(R.string.positive_gift)
            tablayoutUserTab.getTabAt(2)!!.setText(R.string.negative_gift)
            tablayoutUserTab.getTabAt(3)!!.setText(R.string.special_editions)
        }

        tablayoutUserTab.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                val position = tab.position

            }
        })
    }

}
