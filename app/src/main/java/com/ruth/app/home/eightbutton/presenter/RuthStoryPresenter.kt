package com.ruth.app.home.eightbutton.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class RuthStoryPresenter@Inject constructor() : BasePresenter<RuthStoryPresenter.RuthStoryView>() {

    override fun onCreate(view: RuthStoryView) {
        super.onCreate(view)

    }

    interface RuthStoryView : BaseView {

    }
}