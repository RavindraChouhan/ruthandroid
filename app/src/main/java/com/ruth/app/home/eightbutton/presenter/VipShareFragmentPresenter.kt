package com.ruth.app.home.eightbutton.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class VipShareFragmentPresenter @Inject constructor() : BasePresenter<VipShareFragmentPresenter.VipShareFragmentPresenterView>() {
    interface VipShareFragmentPresenterView : BaseView {

    }
}