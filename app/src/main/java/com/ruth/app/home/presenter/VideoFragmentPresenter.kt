package com.ruth.app.home.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class VideoFragmentPresenter @Inject constructor() : BasePresenter<VideoFragmentPresenter.VideoFragmentView>() {

    override fun onCreate(view: VideoFragmentView) {
        super.onCreate(view)
    }

    interface VideoFragmentView : BaseView {

    }
}