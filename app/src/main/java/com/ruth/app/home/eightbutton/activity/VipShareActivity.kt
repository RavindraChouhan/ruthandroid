package com.ruth.app.home.eightbutton.activity

import android.os.Bundle
import android.support.design.widget.TabLayout
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.home.eightbutton.adapter.VipSharePagerAdapter
import com.ruth.app.home.eightbutton.presenter.VipSharePresenter
import kotlinx.android.synthetic.main.activity_vip_share.*

class VipShareActivity : BaseActivity<VipSharePresenter>(),VipSharePresenter.VipSharePresenterView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)
        setContentView(R.layout.activity_vip_share)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        setupTabLayout()
        btn_back_share.setOnClickListener {
            finish()
        }
    }

    private fun setupTabLayout() {

        val adapter = VipSharePagerAdapter(supportFragmentManager, tab_share.tabCount)
        viewPager_share!!.adapter = adapter
        tab_share.setupWithViewPager(viewPager_share)

        tab_share.getTabAt(0)!!.setText(R.string.all)
        tab_share.getTabAt(1)!!.setText(R.string.videos)
        tab_share.getTabAt(2)!!.setText(R.string.podcast)
        tab_share.getTabAt(3)!!.setText(R.string.pictures)
        tab_share.getTabAt(4)!!.setText(R.string.articles)
        tab_share.getTabAt(5)!!.setText(R.string.others)



        tab_share.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {


            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                val position = tab.position
                viewPager_share.setCurrentItem(position, true)

            }
        })
    }

}
