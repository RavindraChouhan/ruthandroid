package com.ruth.app.home.presenter

import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.home.activity.HomeActivity
import javax.inject.Inject

class HomeFragmentPresenter @Inject constructor() : BasePresenter<HomeFragmentPresenter.HomeFragmentView>() {

    override fun onCreate(view: HomeFragmentView) {
        super.onCreate(view)
    }

    fun clear() {
        val tempFb = utility.getBooleaPreferences(context, getString(R.string.isfbSignup))
        val tempGoogle = utility.getBooleaPreferences(context, getString(R.string.isGoogleSignup))
        val tempInsta = utility.getBooleaPreferences(context, getString(R.string.isInstaSignup))

        preferences.clearPreferences()

        utility.setBooleanPreferences(context, getString(R.string.isfbSignup), tempFb)
        utility.setBooleanPreferences(context, getString(R.string.isGoogleSignup), tempGoogle)
        utility.setBooleanPreferences(context, getString(R.string.isInstaSignup), tempInsta)

        view?.clearPreferences()
        HomeActivity.goldPoints = 0
        HomeActivity.silverPoints = 0
        HomeActivity.yuvanPoints = 0

    }

    interface HomeFragmentView : BaseView {
        fun clearPreferences()
    }
}