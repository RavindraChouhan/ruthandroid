package com.ruth.app.home.userprofileview.videos

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class UserVideosPagerAdapter(fm: FragmentManager?, var size: Int) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return UserVideoFragment()
    }

    override fun getCount(): Int {
        return size
    }
}