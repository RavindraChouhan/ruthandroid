package com.ruth.app.home.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.ruth.app.home.fragment.FavoriteListFragment
import com.ruth.app.home.model.Favorite

class FavoriteViewPagerAdapter(fm: FragmentManager?, var favoriteList: ArrayList<Favorite>?) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
//        when (position) {
//            0 -> return FavoriteListFragment().newInstance(favoriteList!!)
//            1 -> return FavoriteListFragment().newInstance(favoriteList!!)
//            2 -> return PodcastsFragment()
//            3 -> return VideosFragment()
//            4 -> return PodcastsFragment()
//            5 -> return VideosFragment()
//        }
        return FavoriteListFragment().newInstance(favoriteList!!)
    }

    override fun getCount(): Int {
        return 6
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return super.getPageTitle(position)
    }

//    override fun saveState(): Parcelable {
//        val bundle = saveState() as Bundle
//        bundle.putParcelableArray("states", null) // Never maintain any states from the base class to avoid TransactionTooLargeException
//        return bundle
//    }
}