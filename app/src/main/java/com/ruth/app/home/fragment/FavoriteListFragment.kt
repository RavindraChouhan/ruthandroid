package com.ruth.app.home.fragment

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.fragment.BaseFragment
import com.ruth.app.home.adapter.FavoriteListAdapter
import com.ruth.app.home.model.Favorite
import com.ruth.app.home.presenter.FavoriteListPresenter
import com.ruth.app.utility.withArguments
import kotlinx.android.synthetic.main.fragment_list_favorite.view.*

class FavoriteListFragment : BaseFragment<FavoriteListPresenter>(), FavoriteListPresenter.FavoriteFragmentView {

    private val favoriteList: ArrayList<Favorite>? by lazy { arguments?.getParcelableArrayList<Favorite>(FAVORITE) }
    private var adapter: FavoriteListAdapter? = null

    companion object {
        const val FAVORITE = "FAVORITE"
    }

    fun newInstance(data: ArrayList<Favorite>): FavoriteListFragment {
        val args = Bundle()
        args.putParcelableArrayList(FAVORITE, data)
        return FavoriteListFragment().withArguments(args)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_list_favorite, container, false)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        view.rvFavoriteList.layoutManager = GridLayoutManager(this.activity!!, 3)
        adapter = FavoriteListAdapter(this.activity!!, favoriteList!!)
        view.rvFavoriteList!!.adapter = adapter

        return view
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            fragmentManager!!.beginTransaction().detach(this).attach(this).commit()
        }
    }
}