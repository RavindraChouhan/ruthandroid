package com.ruth.app.home.eightbutton.adapter

import android.os.Bundle
import android.os.Parcelable
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.ruth.app.home.eightbutton.fragment.RuthKnowsFragment


class RuthKnowsPagerAdapter(supportFragmentManager: FragmentManager?, tabCount: Int) : FragmentStatePagerAdapter(supportFragmentManager) {
    override fun getItem(position: Int): Fragment {
        return RuthKnowsFragment().newInstance()

    }

    override fun getCount(): Int {
    return 4
    }

//    override fun saveState(): Parcelable {
//        val bundle = saveState() as Bundle
//        bundle.putParcelableArray("states", null) // Never maintain any states from the base class to avoid TransactionTooLargeException
//        return bundle
//    }
}
