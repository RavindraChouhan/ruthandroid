package com.ruth.app.home.eightbutton.activity

import android.os.Bundle
import android.support.design.widget.TabLayout
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.home.eightbutton.adapter.BiblePagerAdapter
import com.ruth.app.home.eightbutton.presenter.BiblePresenter
import kotlinx.android.synthetic.main.activity_bible.*

class BibleActivity : BaseActivity<BiblePresenter>(), BiblePresenter.Bibleview {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)
        setContentView(R.layout.activity_bible)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        setupTabLayout()
        btn_back.setOnClickListener {
            finish()
        }
    }

    private fun setupTabLayout() {

        val adapter = BiblePagerAdapter(supportFragmentManager, tabBible.tabCount)
        viewPagerBible!!.adapter = adapter
        tabBible.setupWithViewPager(viewPagerBible)

        tabBible.getTabAt(0)!!.setText(R.string.all)
        tabBible.getTabAt(1)!!.setText(R.string.feng_shui)
        tabBible.getTabAt(2)!!.setText(R.string.fortune_telling)
        tabBible.getTabAt(3)!!.setText(R.string.bazi)
        tabBible.getTabAt(4)!!.setText(R.string.exorcism)
        tabBible.getTabAt(5)!!.setText(R.string.others)


        tabBible.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {


            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                val position = tab.position
                viewPagerBible.setCurrentItem(position, true)
                when (position) {

                }
            }
        })
    }
}
