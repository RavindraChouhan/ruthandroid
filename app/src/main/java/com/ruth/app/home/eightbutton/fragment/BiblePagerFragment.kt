package com.ruth.app.home.eightbutton.fragment

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.fragment.BaseFragment
import com.ruth.app.home.adapter.HomeBannerPagerAdapter
import com.ruth.app.home.adapter.HomeViewPagerAdapter
import com.ruth.app.home.eightbutton.adapter.BibleRecycleAdapter
import com.ruth.app.home.eightbutton.presenter.BiblePagerPresenter
import kotlinx.android.synthetic.main.fragment_biblepager.view.*

class BiblePagerFragment : BaseFragment<BiblePagerPresenter>(), BiblePagerPresenter.BiblePagerPresenterView {

    fun newInstance(): BiblePagerFragment {
        return BiblePagerFragment()
    }

    private var pagerAdapter: HomeViewPagerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_biblepager, container, false)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        setTopBannerImages(view)

        view.recycle_recommended.layoutManager = LinearLayoutManager(presenter.context)
        val adapter = BibleRecycleAdapter(this.activity!!)
        view.recycle_recommended.adapter = adapter

        return view
    }

    private lateinit var homeBannerAdapter: HomeBannerPagerAdapter

    private var dotscount: Int = 0

    private lateinit var dots: Array<ImageView?>

    private fun setTopBannerImages(view: View) {
        val homeBannerList: ArrayList<String> = ArrayList()
        homeBannerAdapter = HomeBannerPagerAdapter(presenter.context, homeBannerList)
        view.viewpagerBibleBanner.adapter = homeBannerAdapter

        dotscount = homeBannerAdapter.count

        dots = arrayOfNulls<ImageView>(dotscount)
        for (i in 0 until dotscount) {
            dots[i] = ImageView(presenter.context)
            dots[i]?.setImageDrawable(ContextCompat.getDrawable(presenter.context, R.drawable.nonactive_dot))
            val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.setMargins(8, 0, 8, 0)
            view.bannerSliderDots.addView(dots[i], params)
        }
        dots[0]?.setImageDrawable(ContextCompat.getDrawable(presenter.context, R.drawable.active_dot))

        view.viewpagerBibleBanner.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                //    currentPage = position

                for (i in 0 until dotscount) {
                    dots!![i]?.setImageDrawable(ContextCompat.getDrawable(presenter.context, R.drawable.nonactive_dot))
                }
                dots!![position]?.setImageDrawable(ContextCompat.getDrawable(presenter.context, R.drawable.active_dot))

            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })
    }

}
