package com.ruth.app.home.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import kotlinx.android.synthetic.main.item_calendar_days.view.*

class CalendarAdapter(private var context: Context, var totalMonthDays: Int, var currentDay: Int)
    : RecyclerView.Adapter<CalendarAdapter.CalendarViewHolder>() {

    private lateinit var recyclerView: RecyclerView
    private var lastPosition: Int = -1
    private lateinit var onItemClickListener: OnItemClickListener

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalendarViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_calendar_days, parent, false)
        return CalendarViewHolder(view)
    }

    override fun onBindViewHolder(holder: CalendarViewHolder, position: Int) {
        holder.bind(position, currentDay)

        if (lastPosition == position) {
            holder.itemView.tvDaysNo.setTextColor(context.resources.getColor(R.color.timer_text))
        } else {
            holder.itemView.tvDaysNo.setTextColor(context.resources.getColor(R.color.white))
        }
    }

    override fun getItemCount(): Int {
        return totalMonthDays
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    inner class CalendarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(totalMonthDays: Int, currentDay: Int) {
            itemView.tvDaysNo.text = ((totalMonthDays) + 1).toString()
            if (((totalMonthDays) + 1) == currentDay) {
                itemView.tvDaysNo.setTextColor(context.resources.getColor(R.color.timer_text))
                itemView.tvDaysNo.background = context.resources.getDrawable(R.drawable.popup_calendar_selected_date)
            } else {
                itemView.tvDaysNo.background = null
            }

            if (((totalMonthDays) + 1) < currentDay) {
                itemView.ivCalendarHeader.visibility = View.VISIBLE
                itemView.ivCalendarBg.background = context.getDrawable(R.drawable.popup_calendar_selected_date_bg)
            } else {
                itemView.ivCalendarHeader.visibility = View.GONE
                itemView.ivCalendarBg.background = context.getDrawable(R.drawable.popup_calendar_unselected_date_bg)
            }

            itemView.layoutSelectedDate.setOnClickListener(View.OnClickListener {
                notifyItemChanged(lastPosition)
                lastPosition = position
                notifyItemChanged(lastPosition)
                onItemClickListener.onItemClick(itemView, adapterPosition)
            })

        }
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}
