package com.ruth.app.home.eightbutton.activity

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AccelerateDecelerateInterpolator
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.home.adapter.FollowersAdapter
import com.ruth.app.home.adapter.HomeBannerPagerAdapter
import com.ruth.app.home.eightbutton.adapter.ArticleAdapter
import com.ruth.app.home.eightbutton.presenter.ArticleDetailPresenter
import com.ruth.app.widgets.expandable.ExpandableLayout
import kotlinx.android.synthetic.main.activity_article_detail.*
import com.ruth.app.widgets.menu_button.CustomArcMenu


class ArticleDetailActivity : BaseActivity<ArticleDetailPresenter>(), ArticleDetailPresenter.ArticleDetailView, View.OnClickListener {

    private lateinit var homeBannerAdapter: HomeBannerPagerAdapter
    private var runnable: Runnable? = null
    private val handler: Handler? = Handler()
    private var currentPage: Int = 0
    private var expandableLayout: ExpandableLayout? = null
    private var isLiked: Boolean = false

    private lateinit var mContext: Context

    private lateinit var menu:CustomArcMenu

    private var listItem:ArrayList<Int> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
//        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_article_detail)
        App.getComponent().inject(this)
        presenter.onCreate(this)
        mContext = this
        expandableLayout = findViewById<View>(R.id.lytDescription) as ExpandableLayout
        menu = findViewById<View>(R.id.arcMenu) as CustomArcMenu

        presenter.bannersApi()

        recycle_followers.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        recycle_followers.adapter = FollowersAdapter(mContext, listItem)

        rvComments.layoutManager = LinearLayoutManager(presenter.context)
        val adapter = ArticleAdapter(this)
        rvComments.adapter = adapter

        imgLike.setOnClickListener(this)
        imgScary.setOnClickListener(this)
        imgShow.setOnClickListener(this)
        imgBack.setOnClickListener(this)
        imgSharing.setOnClickListener(this)
        fab_gift.setOnClickListener(this)
        fab_chat.setOnClickListener(this)

        layoutSwipe.setOnRefreshListener {
            layoutSwipe.isRefreshing = false
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            imgLike -> {
                if (isLiked) {
                    isLiked = false
                    imgLike.setImageResource(R.drawable.tab_like)
                } else {
                    isLiked = true
                    imgLike.setImageResource(R.drawable.icon_like)

                }
                return
            }
            imgScary -> {
                //updateEmojis()
                return
            }
            imgShow -> {
                showHideDescription()
                return
            }
            imgBack -> {
                finish()
                return
            }
            imgSharing -> {
                return
            }
            fab_gift ->{
                menu.toggleMenu()
            }
            fab_chat ->{
                menu.toggleMenu()
            }
        }
    }

    fun showHideDescription() {
        if (expandableLayout!!.isExpanded) {
            expandableLayout!!.collapse()
            val deg = 180f
            imgShow.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
        } else {
            expandableLayout!!.expand()
            val deg = 0f
            imgShow.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
        }
    }

    override fun setTopBannerImages(data: ArrayList<String>) {
        homeBannerAdapter = HomeBannerPagerAdapter(this, data)
        viewpagerHomeBanner.adapter = homeBannerAdapter

        val density = resources.displayMetrics.density
        indicator.setViewPager(viewpagerHomeBanner)
        indicator.radius = 6 * density
        indicator.fillColor = resources.getColor(R.color.indicator_active_color)
        indicator.pageColor = resources.getColor(R.color.indicator_inactive_color)
        indicator.strokeColor = resources.getColor(R.color.transparent)

        /*runnable = Runnable {
            if (currentPage >= data.size) {
                currentPage = 0
            } else {
                currentPage += 1
            }
            viewpagerHomeBanner.setCurrentItem(currentPage, true)
        }*/

        /*val timer = Timer()                                                                         // This will create a new Thread
        timer.schedule(object : TimerTask() {                                                       // task to be scheduled
            override fun run() {
                handler!!.post(runnable)
            }
        }, 2000, 3000)*/

        indicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageSelected(position: Int) {
                currentPage = position
            }

            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {}

            override fun onPageScrollStateChanged(pos: Int) {}
        })
    }

    fun changeImage(){
        arcMenu.rotation = 45f
        /*arcMenu.animate().rotationY(45f).setListener(object : Animator.AnimatorListener {

            override fun onAnimationStart(animation: Animator) {}

            override fun onAnimationRepeat(animation: Animator) {}

            override fun onAnimationEnd(animation: Animator) {
                arcMenu.rotationY = 45f
                arcMenu.animate().rotationY(360f).setListener(null)

            }

            override fun onAnimationCancel(animation: Animator) {}
        })*/
    }
}
