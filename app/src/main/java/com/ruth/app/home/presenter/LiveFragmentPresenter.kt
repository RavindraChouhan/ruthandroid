package com.ruth.app.home.presenter

import android.util.Log
import android.view.View
import android.widget.Toast
import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.home.model.EightButtonsData
import com.ruth.app.home.model.RuthChannelsData
import com.ruth.app.model.BroadcastCustomOrder
import com.ruth.app.model.InvitedFansData
import com.ruth.app.model.RecommandedUserDetail
import com.ruth.app.model.UserGiftData
import com.ruth.app.network.config.RuthApi
import com.ruth.app.utility.RuthUtility
import javax.inject.Inject

class LiveFragmentPresenter @Inject constructor(private val ruthApi: RuthApi) : BasePresenter<LiveFragmentPresenter.LiveFragmentView>() {

    fun getHomeBanners(view1: View) {
        if (utility.isConnectingToInternet(context)) {
            bannersApi(view1)
        } else {
            val thread = Thread(Runnable {
                Runtime.getRuntime().gc()
                val internetConnection = utility.checkInternetWithoutDialog(context)
                if (internetConnection) {
                    bannersApi(view1)
//                    this.view!!.hideNoInternet(view1)
                } else {
                    getHomeBanners(view1)
                    this.view!!.showNoInternet(view1)
                }
            })
            thread.start()
        }
    }

    private fun bannersApi(view1: View) {
        ruthApi.getHomeBanners()
                .onErrorReturn {
                    null
                }
                .doOnError {
                    Log.d("Error", "error" + it.message)
                }
                .subscribe {
                    if (it != null) {
                        if (it.status) {
                            this.view?.setTopBannerImages(it.data, view1)
                        } else {
                            if (it.message.equals("Invalid device login", true)) {
                                this.view?.invalidLogin()
                            }
                        }
                    }
                }
    }

    fun getEightButtons(view: View) {
        if (utility.isConnectingToInternet(context)) {
            eightButtonApi(view)
        } else {
            val thread = Thread(Runnable {
                Runtime.getRuntime().gc()
                val internetConnection = utility.checkInternetWithoutDialog(context)
                if (internetConnection) {
                    eightButtonApi(view)
                } else {
                    getEightButtons(view)
                }
            })
            thread.start()
        }
    }

    private fun eightButtonApi(view1: View) {
        ruthApi.getButtonEffect()
                .onErrorReturn { (null) }
                .subscribe {
                    if (it != null) {
                        if (it.status) {
                            this.view?.setEightButtons(it.data, view1)
                        } else {
                            view?.hideLoadingProgress(view1)
                        }
                    }
                }
    }

    fun getAllChannels(view1: View?) {
        if (utility.isConnectingToInternet(context)) {
            getChannelsApi(view1)
        } else {
            val thread = Thread(Runnable {
                Runtime.getRuntime().gc()
                val internetConnection = utility.checkInternetWithoutDialog(context)
                if (internetConnection) {
                    getChannelsApi(view1)
                } else {
                    getAllChannels(view1)
                }
            })
            thread.start()
        }
    }

    private fun getChannelsApi(view1: View?) {
        ruthApi.getAllChannels(preferences.getUserInfo().userToken)
                .onErrorReturn { (null) }
                .subscribe {
                    //view?.showLoadingProgress(view1)
                    if (it != null) {
                        if (it.status) {
//                        preferences.setAllChannels(it.data)
                            view?.setChannelsList(it.data, preferences.getUserInfo().id, view1)
                            view?.hideLoadingProgress(view1)
                        } else if (it.message.contains(context.getString(R.string.device_login), true)
                                || it.zhHans!!.contains(context.getString(R.string.device_login), true)
                                || it.zhHant!!.contains(context.getString(R.string.device_login), true)) {
                            view?.invalidLogin()
                            view?.hideLoadingProgress(view1)
                        } else {
                            view?.hideLoadingProgress(view1)
                        }
                    }

                }
    }

    fun getRecommendedUsers(view1: View?) {
        ruthApi.recommandedUsers(preferences.getUserInfo().userToken)
                .onErrorReturn { (null) }
                .subscribe {
                    // view?.showLoadingProgress(view1)
                    if (it != null) {
                        if (it.status) {
                            view?.setRecommandedUsers(it.data, view1)
                            view?.hideLoadingProgress(view1)
                        } else if (it.message.contains(context.getString(R.string.device_login), true)
                                || it.zhHans.contains(context.getString(R.string.device_login), true)
                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
                            view?.invalidLogin()
                            view?.hideLoadingProgress(view1)
                        } else {
                            view?.hideLoadingProgress(view1)
                        }
                    } else {
                        view?.hideLoadingProgress(view1)
                    }
                }
    }

    fun followUnfollow(followingId: String, isFollowed: Boolean) {
        val hm = HashMap<String, @JvmSuppressWildcards Any>()
        hm["type"] = "follow"
        hm["isFollow"] = isFollowed
        hm["followingId"] = followingId
        hm["userId"] = preferences.getUserInfo().id

        ruthApi.followUnfollowUser(preferences.getUserInfo().userToken, hm)
                .onErrorReturn { (null) }
                .subscribe {
                    if (it != null) {
                        if (it.status) {
                            view?.updateRecomandedView()
                        } else if (it.message.contains(context.getString(R.string.device_login), true)
                                || it.zhHans.contains(context.getString(R.string.device_login), true)
                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
                            view?.invalidLogin()
                        } else {
                            Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
    }

    fun getUsersGifts(broadcastId: String)/*: ArrayList<UserGiftData> */ {
        var giftsList: ArrayList<UserGiftData> = ArrayList()
        ruthApi.getUsersGifts(preferences.getUserInfo().userToken, broadcastId)
                .subscribe {
                    if (it.status) {
                        giftsList = it.data!!
                        view?.updateGifts(giftsList)
                        Log.d("status", " -> " + it.data)
                    } else {
                        Log.d("status", " -> $it")
                    }
                }
    }

    fun clearLogin() {
        val tempFb = utility.getBooleaPreferences(context, getString(R.string.isfbSignup))
        val tempGoogle = utility.getBooleaPreferences(context, getString(R.string.isGoogleSignup))
        val tempInsta = utility.getBooleaPreferences(context, getString(R.string.isInstaSignup))

        preferences.clearPreferences()

        utility.setBooleanPreferences(context, getString(R.string.isfbSignup), tempFb)
        utility.setBooleanPreferences(context, getString(R.string.isGoogleSignup), tempGoogle)
        utility.setBooleanPreferences(context, getString(R.string.isInstaSignup), tempInsta)

    }

    fun getMyProfile() {
        ruthApi.getUserInfo(preferences.getUserInfo().userToken, preferences.getUserInfo().id)
                .subscribe({
                    if (it != null) {
                        if (it.status) {
                            preferences.setUserProfile(it.data)
                            if (it.data.pointEarn!!.isNotEmpty()) {
                                RuthUtility.updatePoints(context, it.data.pointEarn)
                            }
                        } else {
                            // view?.invalidLogin()
                        }
                    }
                }, {
                    view?.showToast(getString(R.string.connection_error))
                })
    }

//    fun getCustomOrders(broadcastId: String) {
//        ruthApi.getCustomOrders(preferences.getUserInfo().userToken, broadcastId)
//                .onErrorReturn { (null) }
//                .doOnError {
//                    Log.d("Error", "error" + it.message)
//                }
//                .subscribe {
//                    if (it != null) {
//                        if (it.status) {
//                            if (it.data!!.isNotEmpty()) {
//                                view?.updateOrdersData(it.data)
//                            }
//                        } else {
//                            Log.d("status", " -> $it")
//                        }
//                    }
//                }
//    }


    fun getInvitedUsers(broadcastId: String) {
        ruthApi.getInvitedUsers(preferences.getUserInfo().userToken, broadcastId)
                .onErrorReturn { (null) }
                .doOnError {
                    Log.d("Error", "error" + it.message)
                }
                .subscribe {
                    if (it != null) {
                        if (it.status) {
                            view?.updateFansData(it.data)
                        } else {
                            Log.d("status", " -> $it")
                        }
                    }
                }
    }

//    private lateinit var userIds: Array<String>
//    fun inviteUser(selectedFansId: ArrayList<String>, broadcastId: String) {
//
//        for(i in 0 until  selectedFansId.size){
//            userIds = arrayOf(selectedFansId[i])
//        }
//
//        val hashMap: HashMap<String, @JvmSuppressWildcards Any> = HashMap()
//        hashMap["broadcastId"] = broadcastId
//        hashMap["userIds"] = userIds
//
//        ruthApi.postInviteFans(preferences.getUserInfo().userToken, hashMap)
//                .subscribe {
//                    if (it != null) {
//                        if (it.status) {
//                            Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
//                            Log.d("inviteUsers", "status -> " + it.message)
//                        } else if (it.message.contains(context.getString(R.string.device_login), true)
//                                || it.zhHans.contains(context.getString(R.string.device_login), true)
//                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
//                            view?.invalidLogin()
//                        } else {
//                            Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
//                        }
//                    }
//                }
//    }

    interface LiveFragmentView : BaseView {
        fun setEightButtons(data: ArrayList<EightButtonsData>, view: View)
        fun setChannelsList(data: ArrayList<RuthChannelsData>, userId: String, view: View?)
        fun invalidLogin()
        fun showLoadingProgress(view: View?)
        fun hideLoadingProgress(view: View?)
        fun setTopBannerImages(data: ArrayList<String>, view: View)
        fun setRecommandedUsers(data: ArrayList<RecommandedUserDetail>, view1: View?)
        fun updateRecomandedView()
        fun updateGifts(giftsList: ArrayList<UserGiftData>)
        fun showNoInternet(view1: View)
        fun hideNoInternet(view1: View)
        fun updateFansData(data: ArrayList<InvitedFansData>?)
//        fun updateOrdersData(data: ArrayList<BroadcastCustomOrder>?)
    }
}