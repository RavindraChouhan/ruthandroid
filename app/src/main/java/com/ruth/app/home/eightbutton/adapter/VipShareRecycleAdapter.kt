package com.ruth.app.home.eightbutton.adapter

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import com.ruth.app.home.adapter.FollowersAdapter
import kotlinx.android.synthetic.main.item_vipshare_cell.view.*

class VipShareRecycleAdapter(val mContext: Context) : RecyclerView.Adapter<VipShareRecycleAdapter.ViewHolder>() {

    var list: ArrayList<Int> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_vipshare_cell, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 10
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.recycle_followers.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        holder.itemView.recycle_followers.adapter = FollowersAdapter(mContext,list)
        holder.itemView.img_follow.setOnClickListener {
            if (holder.itemView.img_follow.drawable.constantState == mContext.resources.getDrawable(R.drawable.icon_follow).constantState) {
                holder.itemView.img_follow.setImageDrawable(mContext.resources.getDrawable(R.drawable.icon_unfollow))
            } else {
                holder.itemView.img_follow.setImageDrawable(mContext.resources.getDrawable(R.drawable.icon_follow))
            }
        }
    }

    inner class ViewHolder(view: View):RecyclerView.ViewHolder(view) {

    }

}
