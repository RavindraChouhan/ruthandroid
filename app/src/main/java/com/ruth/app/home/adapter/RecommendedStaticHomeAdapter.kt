package com.ruth.app.home.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import com.ruth.app.login.model.RecommendedFriends
import kotlinx.android.synthetic.main.item_home_recommended_user.view.*
import java.util.*

class RecommendedStaticHomeAdapter(private var context: Context,
                                   private var friendsList: ArrayList<RecommendedFriends>)
    : RecyclerView.Adapter<RecommendedStaticHomeAdapter.RecommendedViewHolder>() {

    private lateinit var recyclerView: RecyclerView
    private var onItemClickListener: OnItemClickListener? = null
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecommendedViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_home_recommended_user, parent, false)
        return RecommendedViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecommendedViewHolder, position: Int) {
        holder.bind(friendsList?.get(position)!!)
    }

    override fun getItemCount(): Int {
        return friendsList?.size!!
    }

    inner class RecommendedViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(recommendedFriends: RecommendedFriends) {

            if (recommendedFriends.isSelected) {
                itemView.layoutUnselected.visibility = View.GONE
                itemView.layoutSelected.visibility = View.VISIBLE
            } else {
                itemView.layoutUnselected.visibility = View.VISIBLE
                itemView.layoutSelected.visibility = View.GONE
            }

            if (recommendedFriends.name.equals("Empty", true)) {
                itemView.layoutEmpty.visibility = View.VISIBLE
                itemView.layoutRecommend.visibility = View.GONE
            } else {
                itemView.layoutEmpty.visibility = View.GONE
                itemView.layoutRecommend.visibility = View.VISIBLE

                itemView.tvRecommendName.text = recommendedFriends.name
                itemView.tvRecommendAccountNo.text = recommendedFriends.accountNo
                itemView.tvRecommendFans.text = recommendedFriends.fansCount.toString()
                itemView.tvRecommendGender.text = recommendedFriends.gender

                val date = recommendedFriends.registeredDate?.split("年".toRegex())
                itemView.tvRecommendRegisteredYear.text = /*context.getString(R.string.born_in)*/"生於" + date!![0] + "年"/*context.getString(R.string.year)*/
                itemView.tvRecommendRegisteredDate.text = date[1]
                itemView.tvRecommendFirstPlace.text = recommendedFriends.districtName1
                itemView.tvRecommendSecondPlace.text = recommendedFriends.districtName2
            }

            itemView.layoutRecommend.setOnClickListener {
                recommendedFriends.isSelected = !recommendedFriends.isSelected
                notifyDataSetChanged()
            }

            itemView.btn_Open_Profile.setOnClickListener {
                onItemClickListener?.onItemClick(itemView, adapterPosition)
            }
        }
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}