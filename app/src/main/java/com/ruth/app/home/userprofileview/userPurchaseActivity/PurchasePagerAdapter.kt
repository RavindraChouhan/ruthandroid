package com.ruth.app.home.userprofileview.userPurchaseActivity

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class PurchasePagerAdapter(fm: FragmentManager?/*, var shareList: ArrayList<Share>?*/,
                           var tabCount: Int) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {

        return PurchaseFragment()/*.newInstance(shareList!!)*/
    }

    override fun getCount(): Int {
        return tabCount
    }
}