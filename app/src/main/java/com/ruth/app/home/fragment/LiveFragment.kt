package com.ruth.app.home.fragment

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.fragment.BaseFragment
import com.ruth.app.broadcaster.activity.BroadcastDetailActivity
import com.ruth.app.home.activity.SeeAllFriends
import com.ruth.app.home.adapter.EightButtonAdapter
import com.ruth.app.home.adapter.HomeBannerPagerAdapter
import com.ruth.app.home.adapter.HomeLiveUserAdapter
import com.ruth.app.home.adapter.RecommendedHomeAdapter
import com.ruth.app.home.eightbutton.activity.*
import com.ruth.app.home.model.EightButtonsData
import com.ruth.app.home.model.RuthChannelsData
import com.ruth.app.home.presenter.LiveFragmentPresenter
import com.ruth.app.livestreaming.adapter.UsersInvitationAdapter
import com.ruth.app.login.activity.LoginActivity
import com.ruth.app.model.InvitedFansData
import com.ruth.app.model.RecommandedUserDetail
import com.ruth.app.model.UserGiftData
import com.ruth.app.otheruser.OtherUserProfileActivity
import com.ruth.app.utility.RuthUtility
import com.ruth.app.widgets.StartSnap
import kotlinx.android.synthetic.main.fragment_home_live.view.*
import java.util.*
import kotlin.collections.ArrayList

class LiveFragment : BaseFragment<LiveFragmentPresenter>(), LiveFragmentPresenter.LiveFragmentView, View.OnClickListener {

    private var adapter: HomeLiveUserAdapter? = null
    private var buttonAdapter: EightButtonAdapter? = null
    private lateinit var homeBannerAdapter: HomeBannerPagerAdapter
    private var recomAdapter: RecommendedHomeAdapter? = null
    private var currentPage: Int = 0
    private var handler: Handler? = Handler()
    private var runnable: Runnable? = null
    private var mDelayHandler: Handler? = Handler()
    private val splashDelay: Long = 200 //0.3 second

    companion object {
        var friendsList: ArrayList<RecommandedUserDetail> = ArrayList()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_home_live, container, false)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        view.layoutProgress!!.visibility = View.VISIBLE
        showLiveProgess()

        presenter.getMyProfile()
        presenter.getHomeBanners(view)
        presenter.getEightButtons(view)
        presenter.getAllChannels(view)
        presenter.getRecommendedUsers(view)
//        presenter.getUsersGifts("5b92567aad43682a9373450c")
        presenter.getInvitedUsers(/*intent.getStringExtra("broadcastId")*/"5bb202768f4ac93d5c9042a3")
//        presenter.getCustomOrders(/*intent.getStringExtra("broadcastId")*/"5bc57ccdf0ef721a062f98a6")

        view.tvSeeAllFriends.setOnClickListener(this)
        return view
    }

    override fun onClick(view: View?) {
        if (view?.id == R.id.tvSeeAllFriends) {
            startActivity(Intent(activity, SeeAllFriends::class.java))
        }
    }

    private var usersGiftList: java.util.ArrayList<UserGiftData> = java.util.ArrayList()
    override fun updateGifts(giftsList: java.util.ArrayList<UserGiftData>) {
        usersGiftList = giftsList
    }

    override fun setEightButtons(data: ArrayList<EightButtonsData>, view: View) {
        val mSnapHelper = StartSnap()
        mSnapHelper.attachToRecyclerView(view.rv8btnHome)
        view.layoutEightView.visibility = View.VISIBLE
        view.rv8btnHome.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        buttonAdapter = EightButtonAdapter(presenter.context, data)
        view.rv8btnHome!!.adapter = buttonAdapter

        buttonAdapter!!.setOnItemClickListner(object : EightButtonAdapter.OnItemClickListner {
            override fun onItemClick(view: View, position: Int) {
                when (position) {
                    0 -> {
//                        showInvitationPopup()
                        showLiveProgess()
                        mDelayHandler!!.postDelayed({
                            activity?.startActivity(Intent(activity, MapsActivity::class.java))
                            hideLiveProgess()
                        }, splashDelay)
                    }
                    1 -> {
                        showLiveProgess()
                        mDelayHandler!!.postDelayed({
                            activity?.startActivity(Intent(activity, MapsActivity::class.java))
                            hideLiveProgess()
                        }, splashDelay)
                    }
                    2 -> {
                        showLiveProgess()
                        mDelayHandler!!.postDelayed({
                            activity?.startActivity(Intent(activity, RuthVideoActivity::class.java))
                            hideLiveProgess()
                        }, splashDelay)
                    }
                    3 -> {
                        showLiveProgess()
                        mDelayHandler!!.postDelayed({
                            activity?.startActivity(Intent(activity, RuthStoryActivity::class.java))
                            hideLiveProgess()
                        }, splashDelay)
                    }
                    4 -> {
                        showLiveProgess()
                        mDelayHandler!!.postDelayed({
                            activity?.startActivity(Intent(activity, SitnTalkActivity::class.java))
                            hideLiveProgess()
                        }, splashDelay)
                    }
                    5 -> {
                        showLiveProgess()
                        mDelayHandler!!.postDelayed({
                            activity?.startActivity(Intent(activity, RuthKnowsActivity::class.java))
                            hideLiveProgess()
                        }, splashDelay)
                    }
                    6 -> {
                        showLiveProgess()
                        mDelayHandler!!.postDelayed({
                            activity?.startActivity(Intent(activity, BibleActivity::class.java))
                            hideLiveProgess()
                        }, splashDelay)
                    }
                    7 -> {
                        showLiveProgess()
                        mDelayHandler!!.postDelayed({
                            activity?.startActivity(Intent(activity, VipShareActivity::class.java))
                            hideLiveProgess()
                        }, splashDelay)
                    }
                }
            }
        })
    }

    override fun setTopBannerImages(data: ArrayList<String>, view: View) {
        homeBannerAdapter = HomeBannerPagerAdapter(activity!!, data)
        view.viewpagerHomeBanner.adapter = homeBannerAdapter

        val density = resources.displayMetrics.density
        view.indicator.setViewPager(view.viewpagerHomeBanner)
        view.indicator.radius = 6 * density
        view.indicator.fillColor = ContextCompat.getColor(activity!!, R.color.indicator_active_color)
        view.indicator.pageColor = ContextCompat.getColor(activity!!, R.color.indicator_inactive_color)
        view.indicator.strokeColor = ContextCompat.getColor(activity!!, R.color.transparent)

        runnable = Runnable {
            if (currentPage >= data.size) {
                currentPage = 0
            } else {
                currentPage += 1
            }
            view.viewpagerHomeBanner.setCurrentItem(currentPage, true)
        }

        val timer = Timer()                                                                         // This will create a new Thread
        timer.schedule(object : TimerTask() {                                                       // task to be scheduled
            override fun run() {
                handler!!.post(runnable)
            }
        }, 2000, 3000)

        // Auto start of viewpager
        // Pager listener over indicator
        view.indicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageSelected(position: Int) {
                currentPage = position
            }

            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {}

            override fun onPageScrollStateChanged(pos: Int) {}
        })
    }

    override fun setChannelsList(data: ArrayList<RuthChannelsData>, userId: String, view: View?) {
        val liveData = data.filter { it.onAir == "live" } as ArrayList
        view?.liveUserRecyclerView!!.layoutManager = LinearLayoutManager(activity)
        adapter = HomeLiveUserAdapter(this.activity!!, liveData)
        view.liveUserRecyclerView!!.adapter = adapter
        adapter!!.setOnItemClickListener(object : HomeLiveUserAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                activity?.startActivity(Intent(activity, BroadcastDetailActivity::class.java)
                        .putExtra("channelId", liveData[position].id)
                        .putExtra("userId", userId))
            }
        })

        val upcomData = data.filter { it.onAir == "upcoming" } as ArrayList
        view.upComingUserRecyclerView.layoutManager = LinearLayoutManager(activity)
        adapter = HomeLiveUserAdapter(this.activity!!, upcomData)
        view.upComingUserRecyclerView!!.adapter = adapter
        adapter!!.setOnItemClickListener(object : HomeLiveUserAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                activity?.startActivity(Intent(activity, BroadcastDetailActivity::class.java)
                        .putExtra("channelId", upcomData[position].id)
                        .putExtra("userId", userId))
            }
        })
    }

    private fun setRecommendedUserList(view: View?, data: ArrayList<RecommandedUserDetail>) {
        if (data.size > 15) {
            view!!.tvSeeAllFriends.visibility = View.VISIBLE
        } else {
            view!!.tvSeeAllFriends.visibility = View.GONE
        }

        friendsList.clear()
        friendsList.addAll(data)
        view.rvHome_recommendedUser.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recomAdapter = RecommendedHomeAdapter(this.activity!!, data)
        view.rvHome_recommendedUser.adapter = recomAdapter
        recomAdapter!!.setOnItemClickListener(object : RecommendedHomeAdapter.OnItemClickListener {
            override fun onFollowClick(view: View, position: Int) {
                data[position].isFollow = !data[position].isFollow
                if (data[position].isFollow) {
                    data[position].followers = data[position].followers + 1
                } else {
                    data[position].followers = data[position].followers - 1
                }
                presenter.followUnfollow(data[position].id, data[position].isFollow)
            }

            override fun onItemClick(view: View, position: Int) {
                context?.startActivity(Intent(context, OtherUserProfileActivity::class.java)
                        .putExtra("otherUserId", data[position].id))
            }
        })
        view.recommendedUserView.visibility = View.VISIBLE
        view.layoutProgress!!.visibility = View.GONE

        hideLiveProgess()
    }

    override fun setRecommandedUsers(data: ArrayList<RecommandedUserDetail>, view1: View?) {
        setRecommendedUserList(view1, data)
    }

    private var invalidDialog: AlertDialog? = null

    override fun invalidLogin() {

        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Invalid Login")
        builder.setMessage(getString(R.string.invalid_login_error))

        // add a button
        builder.setPositiveButton(getString(R.string.log_in)) { dialog, _ ->
            presenter.clearLogin()
            startActivity(Intent(activity, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            activity?.finish()
            activity?.overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
            dialog.dismiss()
        }

        try {
            if (!activity?.isFinishing!! && activity!!.window.decorView.isShown) {
                invalidDialog = builder.create()
                if (invalidDialog != null) {
                    invalidDialog!!.setCancelable(false)
                    invalidDialog!!.show()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        if (invalidDialog != null) {
            invalidDialog!!.dismiss()
            invalidDialog = null
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (invalidDialog != null) {
            invalidDialog!!.dismiss()
            invalidDialog = null
        }
    }

    override fun showLoadingProgress(view: View?) {
        showLiveProgess()
    }

    override fun hideLoadingProgress(view: View?) {
        hideLiveProgess()
    }

    private fun showLiveProgess() {
        RuthUtility.showLoader(activity!!)
    }

    private fun hideLiveProgess() {
        RuthUtility.hideLoader()
    }

    override fun updateRecomandedView() {
        view!!.rvHome_recommendedUser.adapter.notifyDataSetChanged()
    }

    override fun showNoInternet(view1: View) {
//        view1.tvNoInternet.visibility = View.VISIBLE
    }

    override fun hideNoInternet(view1: View) {
        //      view1.tvNoInternet.visibility = View.GONE
    }

    private var invitedFansList: ArrayList<InvitedFansData>? = ArrayList()
    override fun updateFansData(data: ArrayList<InvitedFansData>?) {
//        invitedFansList!!.clear()
//        invitedFansList = data
        if (data != null) {
            for (i in 0 until 2){
                invitedFansList!!.addAll(data)
            }
        }
    }

    private fun showInvitationPopup() {
        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(activity!!)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.popup_new_user_layout1, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        b.window.attributes.windowAnimations = R.style.DialogAnimation
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val ivCloseOrders = dialogView.findViewById<View>(R.id.ivCloseOrders) as ImageView
        val rvInvitationList = dialogView.findViewById<View>(R.id.rvInvitationList) as RecyclerView
        val btnInviteAll = dialogView.findViewById<View>(R.id.btnInviteAll) as TextView
        val btnInviteSelected = dialogView.findViewById<View>(R.id.btnInviteSelected) as TextView
        val selectedFansId: ArrayList<String> = ArrayList()

        val adapter = UsersInvitationAdapter(activity!!, invitedFansList)
        rvInvitationList.adapter = adapter

        ivCloseOrders.setOnClickListener {
            b.dismiss()
            //resetButtonsBackground()
        }

        b.setCancelable(false)
        b.show()

        val displayMetrics = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
        b.window.setBackgroundDrawableResource(R.color.transparent)

        val displayWidth = displayMetrics.widthPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(b.window.attributes)

        val dialogWindowWidth = (displayWidth * 0.9f).toInt()
        layoutParams.width = dialogWindowWidth
        b.window.attributes = layoutParams
        b.window.setGravity(Gravity.CENTER_VERTICAL)
    }
}