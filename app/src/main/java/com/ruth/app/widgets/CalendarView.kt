package com.ruth.app.widgets

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.ruth.app.R
import java.text.SimpleDateFormat
import java.util.*

class CalendarView : LinearLayout {

    // date format
    private var dateFormat: String? = null

    // current displayed month
    private val currentDate = Calendar.getInstance()

    //event handling
    private var eventHandler: EventHandler? = null

    // internal components
    private var header: LinearLayout? = null
    private var btnPrev: TextView? = null
    private var btnNext: TextView? = null
    private var txtDate: TextView? = null
    private var grid: GridView? = null
    private var selectedDateList = ArrayList<Date>()
    private var selectedDate: Date? = Date()

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initControl(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initControl(context, attrs)
    }

    /**
     * Load control xml layout
     */
    private fun initControl(context: Context, attrs: AttributeSet) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.control_calendar, this)

        loadDateFormat(attrs)
        assignUiElements()
        assignClickHandlers()

        updateCalendar()
    }

    private fun loadDateFormat(attrs: AttributeSet) {
        val ta = context.obtainStyledAttributes(attrs, R.styleable.CalendarView)

        try {
            // try to load provided date format, and fallback to default otherwise
            dateFormat = ta.getString(R.styleable.CalendarView_dateFormat)
            if (dateFormat == null)
                dateFormat = DATE_FORMAT
        } finally {
            ta.recycle()
        }
    }

    private fun assignUiElements() {
        // layout is inflated, assign local variables to components
        header = findViewById<View>(R.id.calendar_header) as LinearLayout
        btnPrev = findViewById<View>(R.id.calendar_prev_button) as TextView
        btnNext = findViewById<View>(R.id.calendar_next_button) as TextView
        txtDate = findViewById<View>(R.id.calendar_date_display) as TextView
        grid = findViewById<View>(R.id.calendar_grid) as GridView
    }

    private fun assignClickHandlers() {
        // add one month and refresh UI
        btnNext!!.setOnClickListener {
            currentDate.add(Calendar.MONTH, 1)
            updateCalendar()
        }

        // subtract one month and refresh UI
        btnPrev!!.setOnClickListener {
            currentDate.add(Calendar.MONTH, -1)
            updateCalendar()
        }

        grid!!.onItemClickListener = AdapterView.OnItemClickListener { view, cell, position, id ->
            // handle click
            if (eventHandler == null)
                return@OnItemClickListener

            selectedDate = view.getItemAtPosition(position) as Date
            updateCalendar(null)
        }
    }

    /**
     * Display dates correctly in grid
     */
    @JvmOverloads
    fun updateCalendar(events: HashSet<Date>? = null) {
        val cells = ArrayList<Date>()
        val calendar = currentDate.clone() as Calendar

        // determine the cell for current month's beginning
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        val monthBeginningCell = calendar.get(Calendar.DAY_OF_WEEK) - 1

        // move calendar backwards to the beginning of the week
        calendar.add(Calendar.DAY_OF_MONTH, -monthBeginningCell)

        // fill cells
        while (cells.size < DAYS_COUNT) {
            cells.add(calendar.time)
            calendar.add(Calendar.DAY_OF_MONTH, 1)
        }

        selectedDateList.clear()
        selectedDateList = createSelectedDatesList()

        // update grid
        grid!!.adapter = CalendarAdapter(context, cells, events, selectedDateList, selectedDate!!)

        // update title
        val sdf = SimpleDateFormat(dateFormat!!)
        txtDate!!.text = sdf.format(currentDate.time)

        // set header color according to current season

        val month = currentDate.get(Calendar.MONTH) + 1
        val currentMonth = Calendar.getInstance().get(Calendar.MONTH) + 1
        val currentYear = Calendar.getInstance().get(Calendar.YEAR)
        val changedYear = currentDate.get(Calendar.YEAR)

        txtDate!!.text = month.toString() + "月"
        if (month == 12) {
            btnNext!!.text = "1月"
        } else {
            btnNext!!.text = (month + 1).toString() + "月"
        }

        if (month == 1) {
            btnPrev!!.text = "12月"
        } else {
            btnPrev!!.text = (month - 1).toString() + "月"
        }

        if (month - 1 < currentMonth && currentYear == changedYear) {
            btnPrev!!.visibility = View.INVISIBLE
        } else {
            btnPrev!!.visibility = View.VISIBLE
        }

        //        int season = monthSeason[month];
        //        int color = rainbow[season];
        //        header.setBackgroundColor(getResources().getColor(R.color.today));
    }

    private fun createSelectedDatesList(): ArrayList<Date> {
        val selectedDateList1 = ArrayList<Date>()

        var calendar1 = Calendar.getInstance()
        calendar1.add(Calendar.DAY_OF_MONTH, -1)
        var newDate = calendar1.time
        selectedDateList1.add(newDate)

        calendar1 = Calendar.getInstance()
        calendar1.add(Calendar.DAY_OF_MONTH, -2)
        newDate = calendar1.time
        selectedDateList1.add(newDate)

        calendar1 = Calendar.getInstance()
        calendar1.add(Calendar.DAY_OF_MONTH, -3)
        newDate = calendar1.time
        selectedDateList1.add(newDate)

        calendar1 = Calendar.getInstance()
        calendar1.add(Calendar.DAY_OF_MONTH, 4)
        newDate = calendar1.time
        selectedDateList1.add(newDate)

        calendar1 = Calendar.getInstance()
        calendar1.add(Calendar.DAY_OF_MONTH, 9)
        newDate = calendar1.time
        selectedDateList1.add(newDate)

        calendar1 = Calendar.getInstance()
        calendar1.add(Calendar.DAY_OF_MONTH, 16)
        newDate = calendar1.time
        selectedDateList1.add(newDate)

        calendar1 = Calendar.getInstance()
        calendar1.add(Calendar.DAY_OF_MONTH, 3)
        newDate = calendar1.time
        selectedDateList1.add(newDate)

        calendar1 = Calendar.getInstance()
        calendar1.add(Calendar.DAY_OF_MONTH, 2)
        newDate = calendar1.time
        selectedDateList1.add(newDate)

        return selectedDateList1

    }

    private inner class CalendarAdapter(context: Context, days: ArrayList<Date>, // days with events
                                        private val eventDays: HashSet<Date>?, private val selectedDates: ArrayList<Date>, selectedDate: Date) : ArrayAdapter<Date>(context, R.layout.control_calendar_day, days) {
        private var selectedDate: Date? = null

        // for view inflation
        private val inflater: LayoutInflater

        init {
            this.selectedDate = selectedDate
            inflater = LayoutInflater.from(context)
        }

        override fun getView(position: Int, view: View?, parent: ViewGroup): View {
            var view = view
            // day in question
            val date = getItem(position)
            val day = date!!.date
            val month = date.month
            val year = date.year

            // today
            val today = Date()

            // inflate item if it does not exist yet
            if (view == null)
                view = inflater.inflate(R.layout.control_calendar_day, parent, false)

            val tvDate = view!!.findViewById<View>(R.id.tvDate) as TextView


            // if this day has an event, specify event image
            view.setBackgroundResource(0)
            if (eventDays != null) {
                for (eventDate in eventDays) {
                    if (eventDate.date == day &&
                            eventDate.month == month &&
                            eventDate.year == year) {
                        // mark this day for event
                        // view.setBackgroundResource(R.drawable.reminder);
                        break
                    }
                }
            }

            // clear styling
            tvDate.setTypeface(null, Typeface.NORMAL)
            tvDate.setTextColor(Color.WHITE)
            //            view.setBackgroundResource(R.color.bg_calendar_cell);

            if (month != today.month || year != today.year) {
                // if this day is outside current month, grey it out
                tvDate.setBackgroundResource(R.color.greyed_out)
                tvDate.setTextColor(resources.getColor(R.color.white))
            } else if (day == today.date) {
                // if it is today, set it to blue/bold
                tvDate.setTypeface(null, Typeface.BOLD)
                tvDate.setTextColor(Color.GREEN)
            }

            // set text
            tvDate.text = date.date.toString()

            for (i in selectedDates.indices) {
                val date1 = selectedDates[i]
                if (date.date == date1.date && date.month == date1.month && date.year == date1.year) {
                    tvDate.setTypeface(null, Typeface.BOLD)
                    tvDate.setBackgroundResource(R.color.selected_cell)
                }
            }

            if (selectedDate != null) {
                if (date.date == selectedDate!!.date && date.month == selectedDate!!.month && date.year == selectedDate!!.year) {
                    tvDate.setTypeface(null, Typeface.BOLD)
                    tvDate.setTextColor(resources.getColor(R.color.white))
                    tvDate.setBackgroundResource(R.drawable.bg_selected_calendar_date)
                }
            }
            return view
        }
    }

    /**
     * Assign event handler to be passed needed events
     */
    fun setEventHandler(eventHandler: EventHandler) {
        this.eventHandler = eventHandler
    }

    /**
     * This interface defines what events to be reported to
     * the outside world
     */
    interface EventHandler {
        fun onDayLongPress(date: Date)
    }

    companion object {
        // for logging
        private val LOGTAG = "Calendar View"

        // how many days to show, defaults to six weeks, 42 days
        private val DAYS_COUNT = 42

        // default date format
        private val DATE_FORMAT = "MMM yyyy"
    }
}
/**
 * Display dates correctly in grid
 */