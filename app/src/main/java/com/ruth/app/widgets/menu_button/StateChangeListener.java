package com.ruth.app.widgets.menu_button;

public interface StateChangeListener {

    void onMenuOpened();

    void onMenuClosed();
}