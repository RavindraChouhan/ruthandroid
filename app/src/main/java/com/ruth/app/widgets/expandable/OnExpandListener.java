package com.ruth.app.widgets.expandable;

public interface OnExpandListener {

    public void onExpanded(ExpandableLayout view);

    public void onCollapsed(ExpandableLayout view);
}
