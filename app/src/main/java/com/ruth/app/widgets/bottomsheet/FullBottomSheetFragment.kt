package com.ruth.app.widgets.bottomsheet

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.BottomSheetDialogFragment
import android.support.design.widget.CoordinatorLayout
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import com.ruth.app.R

abstract class FullBottomSheetFragment : BottomSheetDialogFragment() {

    protected var progressDialog: ProgressDialog? = null
    protected var errorDialog: AlertDialog? = null
    private var mBehavior: BottomSheetBehavior<*>? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog

        val contentView = View.inflate(context, R.layout.full_bottom_sheet, null)
        val frame = contentView.findViewById<FrameLayout>(R.id.btdContent)
        val childView = onCreateDialogView(savedInstanceState, frame)
        frame.addView(childView)

        dialog.setContentView(contentView)
        mBehavior = BottomSheetBehavior.from(contentView.parent as View)
        return dialog
    }

    protected abstract fun onCreateDialogView(savedInstanceState: Bundle?, parent: ViewGroup): View

    override fun onStart() {
        super.onStart()
        mBehavior!!.state = BottomSheetBehavior.STATE_EXPANDED
    }

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)

        val marginTop = resources.getDimension(R.dimen.bottomsheet_margin_top).toInt()
        val dialogHeight = Tools.getDisplayContentHeight(activity!!) - marginTop

        val bottomSheet = dialog.window!!.findViewById<FrameLayout>(android.support.design.R.id.design_bottom_sheet)
        bottomSheet.setBackgroundResource(R.drawable.shape_bottomsheet_background)
        bottomSheet.minimumHeight = dialogHeight
        val params = bottomSheet.layoutParams as CoordinatorLayout.LayoutParams
        params.height = dialogHeight
        bottomSheet.layoutParams = params

        mBehavior!!.peekHeight = dialogHeight
    }

    protected open fun hideKeyboard() {
        val context = context
        val rootView = this.view
        if (context != null && rootView != null && rootView.windowToken != null) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(rootView.windowToken, 0)
        }
    }

    protected open fun showErrorDialog(error: Throwable, onCancelListener: (() -> Unit)? = null) {
        errorDialog = AlertDialog.Builder(context!!)
                .setMessage(error.message)
                .setPositiveButton(R.string.ok_btn) { dialog: DialogInterface?, _: Int ->
                    dialog?.dismiss()
                    onCancelListener?.invoke()
                }
                .show()
    }

    protected open fun showProgress(show: Boolean) {
        // todo: ProgressDialog was deprecated in Android O, figure out a better alternative
        if ((progressDialog == null || !progressDialog!!.isShowing) && show) {
            progressDialog = ProgressDialog(context, R.style.AppTheme_ProgressDialog)
            progressDialog?.setMessage(getString(R.string.loading))
            progressDialog?.setCancelable(true)
            progressDialog?.setOnCancelListener { onCancelRequestProgressDialog() }
            progressDialog?.setCanceledOnTouchOutside(false)
            progressDialog?.show()
        } else if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog?.dismiss()
            progressDialog = null
        }
    }

    protected open fun onCancelRequestProgressDialog() {
        if (isAdded) {
            activity!!.onBackPressed()
        }
    }

}