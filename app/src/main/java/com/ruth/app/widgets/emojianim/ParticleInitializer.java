package com.ruth.app.widgets.emojianim;

import java.util.Random;

public interface ParticleInitializer {

	void initParticle(Particle p, Random r);

}
