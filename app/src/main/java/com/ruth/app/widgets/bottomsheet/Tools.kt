package com.ruth.app.widgets.bottomsheet

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.graphics.Typeface
import android.os.Build
import android.support.annotation.FontRes
import android.support.v4.content.res.ResourcesCompat
import android.text.Html
import android.text.Spanned
import android.util.Log

object Tools {

    fun getDisplayContentHeight(activity: Activity): Int {
        val screenHeight = activity.resources.displayMetrics.heightPixels
        var statusBarHeight = 0
        val resource = activity.resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resource > 0) {
            statusBarHeight = activity.resources.getDimensionPixelSize(resource)
        }
        return screenHeight - statusBarHeight
    }

    private fun fromHtml(html: String): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            @Suppress("DEPRECATION")
            Html.fromHtml(html)
        }
    }

    /**
     * Using reflection to override default typeface
     * NOTICE: DO NOT FORGET TO SET TYPEFACE FOR APP THEME AS DEFAULT TYPEFACE WHICH WILL BE OVERRIDDEN
     * @param context to work with assets
     * @param defaultFontNameToOverride for example "monospace"
     * @param fontResId font resource id
     */
    @Deprecated("An EXTREMELY ugly workaround")
    fun overrideFont(context: Context, defaultFontNameToOverride: String, @FontRes fontResId: Int) {
        try {
            val customFontTypeface = ResourcesCompat.getFont(context, fontResId)
            val defaultFontTypefaceField = Typeface::class.java.getDeclaredField(defaultFontNameToOverride)
            defaultFontTypefaceField.isAccessible = true
            defaultFontTypefaceField.set(null, customFontTypeface)
        } catch (e: Exception) {
            Log.e("TypeFace", "Can not set custom font ${Resources.getSystem().getResourceName(fontResId)} instead of $defaultFontNameToOverride")
        }

    }
}
