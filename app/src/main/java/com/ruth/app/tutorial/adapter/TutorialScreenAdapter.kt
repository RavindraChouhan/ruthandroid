package com.ruth.app.tutorial.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.ruth.app.R
import com.ruth.app.model.TutorialsData
import com.ruth.app.widgets.progressbar.AVLoadingIndicatorView
import java.util.*

class TutorialScreenAdapter(context: Context, private var tutorialList: ArrayList<TutorialsData>?) : PagerAdapter() {

    private var layoutInflater: LayoutInflater? = null
    private var context: Context? = context

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return tutorialList!!.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = layoutInflater?.inflate(R.layout.item_tutorial_view, container, false)!!

        val tutorial_title = view.findViewById(R.id.tutorial_title) as TextView
        val tutorial_desc = view.findViewById(R.id.tutorial_desc) as TextView
        val ivImage = view.findViewById(R.id.ivImage) as ImageView
        val progressBarTutorial = view.findViewById(R.id.progressBarTutorial) as AVLoadingIndicatorView

        tutorial_title.text = tutorialList?.get(position)?.title
        tutorial_desc.text = tutorialList?.get(position)?.description

        progressBarTutorial.visibility = View.VISIBLE

        Glide.with(context)
                .load(tutorialList!![position].url)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        //TODO handle error images while loading photo
                        return false
                    }
                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        //TODO use "resource" as the photo for your ImageView
                        progressBarTutorial.visibility = View.GONE
                        return false
                    }
                })
                .into(ivImage)

        container.addView(view, 0)
        return view
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    init {
        layoutInflater = LayoutInflater.from(context)
    }
}
