package com.ruth.app.tutorial.activity

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.home.activity.HomeActivity
import com.ruth.app.login.activity.LoginActivity
import com.ruth.app.model.TutorialsData
import com.ruth.app.tutorial.adapter.TutorialScreenAdapter
import com.ruth.app.tutorial.presenter.TutorialPresenter
import com.ruth.app.utility.RuthUtility
import kotlinx.android.synthetic.main.activity_tutorial_screen.*

class TutorialActivity : BaseActivity<TutorialPresenter>(), TutorialPresenter.TutorialView {

    private var tutorialAdapter: TutorialScreenAdapter? = null
    private var currentPage: Int = 0
    private var tutorialList: ArrayList<TutorialsData>? = ArrayList()
    private var dotscount: Int = 0
    private var dots: Array<ImageView?>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial_screen)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        presenter.getTutorials()

        btnNext.setOnClickListener {
            if (currentPage == tutorialList!!.size.minus(1)) {
                presenter.openHome()
            }
            viewpagerLogin.setCurrentItem(currentPage + 1, true)

            btnNext.text = tutorialList!![currentPage].zhHans
            if (App.isChineseLanguage) {
                btnNext.text = tutorialList!![currentPage].zhHans
            } else {
                btnNext.text = tutorialList!![currentPage].en
            }
        }

        ivBackTutorial.setOnClickListener {
            viewpagerLogin.setCurrentItem(currentPage - 1, true)
        }

        ivNextTutorial.setOnClickListener {
            viewpagerLogin.setCurrentItem(currentPage + 1, true)

            if (App.isChineseLanguage) {
                btnNext.text = tutorialList!![currentPage].zhHans
            } else {
                btnNext.text = tutorialList!![currentPage].en
            }
        }

        tvSkipTutorial.setOnClickListener {
            presenter.openHome()
        }
    }

    override fun openHome() {
        startActivity(Intent(this, HomeActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
        finish()
    }

    override fun setTutorialView(data: ArrayList<TutorialsData>) {
        hideTutorialProgress()
//        progressBarTutorial.visibility = View.GONE

        for (i in 0 until data.size) {
            tutorialList?.add(data[i])
        }

        tvSkipTutorial.visibility = View.VISIBLE
        btnNext.visibility = View.VISIBLE

        if (tutorialList?.size!! > 1) {
            ivNextTutorial.visibility = View.VISIBLE
        }

        if (tutorialList?.size!! > 0) {
            tutorialAdapter?.notifyDataSetChanged()
        }

        if (App.isChineseLanguage) {
            btnNext.text = tutorialList!![currentPage].zhHans
        } else {
            btnNext.text = tutorialList!![currentPage].en
        }
        setIndicator()
    }

    private var invalidDialog: AlertDialog? = null

    override fun invalidLogin() {

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Invalid Login")
        builder.setMessage(getString(R.string.invalid_login_error))

        // add a button
        builder.setPositiveButton(getString(R.string.log_in), DialogInterface.OnClickListener { dialog, which ->
            presenter.clearLogin()
            startActivity(Intent(this, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            finish()
            overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
            dialog.dismiss()
        })

        if (!isFinishing && window.decorView.isShown) {
            invalidDialog = builder.create()
            invalidDialog!!.setCancelable(false)
            invalidDialog!!.show()
        }
    }

    override fun onDestroy() {
        if (invalidDialog != null) {
            invalidDialog!!.dismiss()
            invalidDialog = null
        }
        super.onDestroy()
    }

    private fun setIndicator() {
        tutorialAdapter = TutorialScreenAdapter(this, tutorialList)
        viewpagerLogin.adapter = tutorialAdapter

        dotscount = tutorialAdapter!!.count

        dots = arrayOfNulls<ImageView>(dotscount)
        for (i in 0 until dotscount) {
            dots!![i] = ImageView(this)
            dots!![i]?.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.nonactive_dot))
            val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.setMargins(8, 0, 8, 0)
            SliderDots.addView(dots!![i], params)
        }
        dots!![0]?.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.active_dot))

        viewpagerLogin.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                currentPage = position

                for (i in 0 until dotscount) {
                    dots!![i]?.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.nonactive_dot))
                }
                dots!![position]?.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.active_dot))

                if (position == 0) {
                    ivBackTutorial.visibility = View.GONE
                    ivNextTutorial.visibility = View.VISIBLE
                } else if (position == (tutorialList?.size?.minus(1))) {
                    ivNextTutorial.visibility = View.GONE
                    ivBackTutorial.visibility = View.VISIBLE
                } else {
                    ivBackTutorial.visibility = View.VISIBLE
                    ivNextTutorial.visibility = View.VISIBLE
                }

                if (App.isChineseLanguage) {
                    btnNext.text = tutorialList!![position].zhHans
                } else {
                    btnNext.text = tutorialList!![position].en
                }
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }


    override fun hideTutorialProgress() {
        RuthUtility.hideLoader()
//        progressBarTutorial.visibility = View.GONE
    }

    override fun showTutorialProgress() {
        RuthUtility.showLoader(this@TutorialActivity)

//        progressBarTutorial.visibility = View.VISIBLE

    }

}