package com.ruth.app.tutorial.presenter

import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.model.TutorialsData
import com.ruth.app.network.config.RuthApi
import javax.inject.Inject

class TutorialPresenter @Inject constructor(private val ruthApi: RuthApi) : BasePresenter<TutorialPresenter.TutorialView>() {

    override fun onCreate(view: TutorialView) {
        super.onCreate(view)
    }

    fun openHome() {
        view?.openHome()
    }

    fun getTutorials() {
        ruthApi.tutorials(preferences.getUserInfo().userToken)
                .doOnTerminate { view?.hideTutorialProgress() }
                .doOnSubscribe { view?.showTutorialProgress() }
                .subscribe({
                    if (it != null) {
                        if (it.status) {
                            view?.setTutorialView(it.data)
                            view?.hideTutorialProgress()
                        } else {
                            if (it.message.contains("Invaild device login")) {
                                view?.invalidLogin()
                                view?.hideTutorialProgress()
                            }
                        }
                    }
                }, {
                    logError(it)
                    view?.hideTutorialProgress()
                    view?.showToast(context.getString(R.string.connection_error))
                })
    }

    fun clearLogin() {
        preferences.clearPreferences()
    }

    interface TutorialView : BaseView {
        fun openHome()
        fun setTutorialView(data: ArrayList<TutorialsData>)
        fun invalidLogin()
        fun hideTutorialProgress()
        fun showTutorialProgress()
    }
}