package com.ruth.app.tutorial.model

import android.graphics.drawable.Drawable

class TutorialModel(var text : String,
                    var image : Drawable)