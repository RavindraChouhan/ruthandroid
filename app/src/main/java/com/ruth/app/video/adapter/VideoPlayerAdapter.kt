package com.ruth.app.video.adapter

import android.content.Context
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ruth.app.R
import com.ruth.app.home.model.ChannelBroadcastData
import com.ruth.app.utility.RuthUtility
import kotlinx.android.synthetic.main.item_video.view.*

class VideoPlayerAdapter(val mContext: Context,
                         private val broadcastData: List<ChannelBroadcastData>) : RecyclerView.Adapter<VideoPlayerAdapter.ViewHolder>() {

    private lateinit var onItemClickListner: OnItemClickListner

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_video, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return broadcastData.size
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(broadcastData[position])

        holder.itemView.setOnClickListener {
            onItemClickListner.onItemClick(position, broadcastData[position].id)
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(channelBroadcastData: ChannelBroadcastData) {
            Glide.with(mContext).load(channelBroadcastData.coverUrl)
                    .apply(RequestOptions().error(R.drawable.logoeye).placeholder(R.drawable.background)).into(itemView.imgCover)

            itemView.lblVideoTitle.text = channelBroadcastData.broadcastTitle
            itemView.lblViews.text = channelBroadcastData.broadcastViews.toString() + " views"
            itemView.lblPastTime.text = RuthUtility.getTimeAgo("2017-09-06T07:57:29.872Z")
            //itemView.lblVideoDuration.text = convertTime(channelBroadcastData.broadcastUrl)

        }
    }

    fun setOnItemClickListner(onItemClickListner: OnItemClickListner) {
        this.onItemClickListner = onItemClickListner
    }

    interface OnItemClickListner {
        fun onItemClick(pos: Int, selectedId: String)
    }

    fun convertTime(stream: String): String {
        val mediaMetadataRetriever = MediaMetadataRetriever()
        mediaMetadataRetriever.setDataSource(mContext.applicationContext, Uri.parse(stream))
        val METADATA_KEY_DURATION = java.lang.Long.parseLong(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION))
        return METADATA_KEY_DURATION.toString()
    }
}
