package com.ruth.app;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.ruth.app.network.config.AppComponent;
import com.ruth.app.network.config.DaggerAppComponent;
import com.ruth.app.network.module.ApiModule;
import com.ruth.app.utility.FontsOverride;
import com.ruth.app.utility.RuthUtility;
import com.steelkiwi.instagramhelper.InstagramHelper;

import org.jetbrains.annotations.Nullable;

import java.net.URISyntaxException;
import java.util.Locale;

import io.socket.client.IO;
import io.socket.client.Socket;

public class App extends Application {

    public static boolean isChineseLanguage = true;
    public static AppComponent component;
    private static InstagramHelper instagramHelper;

    public static final String CLIENT_ID = "5c51e07bfd674c04a726891c088956b7";
    public static final String REDIRECT_URL = "http://appone.hk";
    @Nullable
    public static GoogleSignInAccount acct;
    private static App mInstance;

    public static AppComponent getComponent() {
        return component;
    }

    private Socket mSocket;

    {
        try {
            mSocket = IO.socket(BuildConfig.SOCKET_URL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        setAppLanguage(this);

        FontsOverride.setDefaultFont(this, "SERIF", "SF UI Text Regular.otf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "SF UI Text Light.otf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "SF UI Text Semibold.otf");

        component = DaggerAppComponent.builder()
                .apiModule(new ApiModule(this))
                .build();
        component.inject(this);

        new Thread(new Runnable() {
            public void run() {
                instagramHelper = new InstagramHelper.Builder()
                        .withClientId(CLIENT_ID)
                        .withRedirectUrl(REDIRECT_URL)
                        .withScope("likes+comments")
                        .build();
            }
        }).start();

//        RuthUtility.checkInternet(mInstance);
    }
    public static InstagramHelper getInstagramHelper() {
        return instagramHelper;
    }

    public static void setAppLanguage(Context mContext) {
        String languageToLoad = "zh";

//        if (isChineseLanguage) {
//            languageToLoad = "zh";
//        } else {
//            languageToLoad = "en";
//        }

//        Locale locale = new Locale(languageToLoad);
//        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = new Locale(languageToLoad);
        mContext.getResources().updateConfiguration(config, mContext.getResources().getDisplayMetrics());
    }
}
