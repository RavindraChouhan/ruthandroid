package com.ruth.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LiveUsersData(
        @SerializedName("status")
        @Expose
        var status: Boolean,
        @SerializedName("message")
        @Expose
        var message: String,
        @SerializedName("zh_Hans")
        @Expose
        var zhHans: String,
        @SerializedName("zh_Hant")
        @Expose
        var zhHant: String,
        @SerializedName("data")
        @Expose
        var data: ArrayList<UserViewersData>? = null)

class UserViewersData(@SerializedName("_id")
                   @Expose
                   var id: String,
                   @SerializedName("userData")
                   @Expose
                   var userData: ArrayList<ViewerData>? = null)

class ViewerData(@SerializedName("_id")
               @Expose
               var id: String,
               @SerializedName("email")
               @Expose
               var email: String,
               @SerializedName("userName")
               @Expose
               var userName: String,
               @SerializedName("followers")
               @Expose
               var followers: Int,
               @SerializedName("name")
               @Expose
               var name: String,
               @SerializedName("photoUrl")
               @Expose
               var photoUrl: String,
               @SerializedName("isFollow")
               @Expose
               var isFollow: Boolean,
               @SerializedName("viewData")
               @Expose
               var viewData: ViewData)

class ViewData(@SerializedName("_id")
               @Expose
               var id: String,
               @SerializedName("viewStartTime")
               @Expose
               var viewStartTime: String,
               @SerializedName("viewEndTime")
               @Expose
               var viewEndTime: String,
               @SerializedName("spendingTime")
               @Expose
               var spendingTime: Int)
