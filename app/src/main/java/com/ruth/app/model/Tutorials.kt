package com.ruth.app.model

import com.google.gson.annotations.SerializedName


class Tutorials(
        @SerializedName("status")
        val status: Boolean,
        @SerializedName("message")
        val message: String,
        @SerializedName("zh_Hant")
        val zh_Hant: String,
        @SerializedName("zh_Hans")
        val zh_Hans: String,
        @SerializedName("data")
        val data: ArrayList<TutorialsData>)

class TutorialsData(@SerializedName("zhHant")
                    val zhHant: String,
                    @SerializedName("zhHans")
                    val zhHans: String,
                    @SerializedName("en")
                    val en: String,
                    @SerializedName("url")
                    val url: String,
                    @SerializedName("title")
                    val title: String,
                    @SerializedName("description")
                    val description: String)