package com.ruth.app.model

import com.google.gson.annotations.SerializedName

class UserInfo(
        @SerializedName("status")
        val status: Boolean,
        @SerializedName("message")
        val message: String,
        @SerializedName("zh_Hant")
        val zhHant: String,
        @SerializedName("zh_Hans")
        val zhHans: String,
        @SerializedName("data")
        val data: Data,
        @SerializedName("token")
        val token: String) {

    class Data(
            @SerializedName("_id")
            val id: String,
            @SerializedName("updatedAt")
            val updatedAt: String,
            @SerializedName("createdAt")
            val createdAt: String,
            @SerializedName("__v")
            val __v: Int,
            @SerializedName("photoUrl")
            val photoUrl: String,
            @SerializedName("gender")
            val gender: String,
            @SerializedName("country")
            val country: String,
            @SerializedName("env")
            val env: String,
            @SerializedName("isBlock")
            val isBlock: Boolean,
            @SerializedName("activePlan")
            val activePlan: List<Object>,
            @SerializedName("offset")
            val offset: Int,
            @SerializedName("lastLogin")
            val lastLogin: String,
            @SerializedName("stripe")
            val stripe: List<Stripe>,
            @SerializedName("resetPasswordExpires")
            val resetPasswordExpires: String,
            @SerializedName("resetPasswordToken")
            val resetPasswordToken: String,
            @SerializedName("status")
            val status: Int,
            @SerializedName("deviceId")
            val deviceId: String,
            @SerializedName("deviceType")
            val deviceType: String,
            @SerializedName("deviceToken")
            val deviceToken: String,
            @SerializedName("role")
            val role: String,
            @SerializedName("mobileNo")
            val mobileNo: Long,
            @SerializedName("accountType")
            val accountType: String,
            @SerializedName("userName")
            val userName: String,
            @SerializedName("password")
            val password: String,
            @SerializedName("email")
            val email: String,
            @SerializedName("socialID")
            val socialID: String,
            @SerializedName("nickname")
            val name: String,
            @SerializedName("isNewUser")
            val isNewUser: Boolean,
            @SerializedName("userToken")
            var userToken: String
    ) {
        class Stripe(
                var paymentMethods: List<Object>?,
                var customerId: String?
        )
    }
}