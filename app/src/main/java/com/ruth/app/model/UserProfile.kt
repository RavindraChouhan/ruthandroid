package com.ruth.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.ruth.app.home.model.Followers

class UserProfile(@SerializedName("status")
                  @Expose
                  var status: Boolean,
                  @SerializedName("message")
                  @Expose
                  var message: String,
                  @SerializedName("data")
                  @Expose
                  val data: UserProfileData,
                  @SerializedName("zh_Hans")
                  @Expose
                  var zhHans: String,
                  @SerializedName("zh_Hant")
                  @Expose
                  var zhHant: String)

class UserProfileData(@SerializedName("_id")
                      @Expose
                      var id: String,
                      @SerializedName("dob")
                      @Expose
                      var dob: String,
                      @SerializedName("photoUrl")
                      @Expose
                      var photoUrl: String,
                      @SerializedName("gender")
                      @Expose
                      var gender: String,
                      @SerializedName("country")
                      @Expose
                      var country: String,
                      @SerializedName("isBlock")
                      @Expose
                      var isBlock: Boolean,
                      @SerializedName("mobileNo")
                      @Expose
                      var mobileNo: Long,
                      @SerializedName("userName")
                      @Expose
                      var userName: String,
                      @SerializedName("email")
                      @Expose
                      var email: String,
                      @SerializedName("socialID")
                      @Expose
                      var socialID: String,
                      @SerializedName("pointEarn")
                      @Expose
                      var pointEarn: List<PointsEarned>? = null,
                      @SerializedName("name")
                      @Expose
                      var nickname: String,
                      @SerializedName("about")
                      @Expose
                      var about: String,
                      @SerializedName("followers")
                      @Expose
                      var followers: Int,
                      @SerializedName("following")
                      @Expose
                      var following: Int,
                      @SerializedName("giftCount")
                      @Expose
                      var giftCount: Int,
                      @SerializedName("level")
                      @Expose
                      var level: Int,
                      @SerializedName("isFollow")
                      @Expose
                      var isFollow: Boolean,
                      @SerializedName("isVip")
                      @Expose
                      var isVip: Boolean = false,
                      @SerializedName("isLiveStatus")
                      @Expose
                      var isLiveStatus: Boolean = false,
                      @SerializedName("status")
                      var status: Int,
                      @SerializedName("live")
                      var live: ArrayList<LiveVideos>,
                      @SerializedName("videos")
                      var videos: ArrayList<LiveVideos>,
                      @SerializedName("isAuthorized")
                      @Expose
                      var isAuthorized: Boolean,
                      @SerializedName("channelId")
                      @Expose
                      var channelId: String)

class PointsEarned(@SerializedName("_id")
                   @Expose
                   var id: String,
                   @SerializedName("total")
                   @Expose
                   var total: Int)

class LiveVideos(@SerializedName("_id")
                 @Expose
                 var id: String,
                 @SerializedName("coverUrl")
                 @Expose
                 var coverUrl: String,
                 @SerializedName("broadcastTitle")
                 @Expose
                 var broadcastTitle: String,
                 @SerializedName("broadcastTag")
                 @Expose
                 var broadcastTag: String,
                 @SerializedName("broadcastUrl")
                 @Expose
                 var broadcastUrl: String,
                 @SerializedName("broadcastStreamId")
                 @Expose
                 var broadcastStreamId: String,
                 @SerializedName("isLive")
                 @Expose
                 var isLive: Boolean,
                 @SerializedName("isActive")
                 @Expose
                 var isActive: Boolean,
                 @SerializedName("broadcasterId")
                 @Expose
                 var broadcasterId: String,
                 @SerializedName("createdAt")
                 @Expose
                 var createdAt: String,
                 @SerializedName("broadcastLikes")
                 @Expose
                 var broadcastLikes: Int,
                 @SerializedName("broadcastViews")
                 @Expose
                 var broadcastViews: Int,
                 @SerializedName("broadcastComments")
                 @Expose
                 var broadcastComments: Int,
                 @SerializedName("emoji")
                 @Expose
                 var emoji: Int,
                 @SerializedName("viewsUsers")
                 @Expose
                 var viewsUsers: ArrayList<Followers>? = null,
                 @SerializedName("broadcastStartTime")
                 @Expose
                 var broadcastStartTime: String,
                 @SerializedName("broadcastEndTime")
                 @Expose
                 var broadcastEndTime: String)