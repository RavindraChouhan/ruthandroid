package com.ruth.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RecommandedUser(
        @SerializedName("status")
        @Expose
        var status: Boolean,
        @SerializedName("message")
        @Expose
        var message: String,
        @SerializedName("data")
        @Expose
        var data: ArrayList<RecommandedUserDetail>,
        @SerializedName("zh_Hans")
        @Expose
        var zhHans: String,
        @SerializedName("zh_Hant")
        @Expose
        var zhHant: String)

class RecommandedUserDetail(
        @SerializedName("_id")
        @Expose
        var id: String,
        @SerializedName("dob")
        @Expose
        var dob: String,
        @SerializedName("photoUrl")
        @Expose
        var photoUrl: String,
        @SerializedName("gender")
        @Expose
        var gender: String,
        @SerializedName("status")
        @Expose
        var status: Int,
        @SerializedName("mobileNo")
        @Expose
        var mobileNo: Long,
        @SerializedName("userName")
        @Expose
        var userName: String,
        @SerializedName("email")
        @Expose
        var email: String,
        @SerializedName("address")
        @Expose
        var address: String,
        @SerializedName("name")
        @Expose
        var name: String,
        @SerializedName("followers")
        @Expose
        var followers: Int,
        @SerializedName("isFollow")
        @Expose
        var isFollow: Boolean)
