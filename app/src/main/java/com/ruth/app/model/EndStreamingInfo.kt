package com.ruth.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class EndStreamingInfo(
        @SerializedName("status")
        @Expose
        var status: Boolean,
        @SerializedName("message")
        @Expose
        var message: String,
        @SerializedName("zh_Hans")
        @Expose
        var zhHans: String,
        @SerializedName("zh_Hant")
        @Expose
        var zhHant: String,
        @SerializedName("data")
        @Expose
        var data: StreamingInfo? = null)

class StreamingInfo(@SerializedName("giftCount")
                    @Expose
                    var giftCount: Int,
                    @SerializedName("likes")
                    @Expose
                    var likes: Int,
                    @SerializedName("spendingTime")
                    @Expose
                    var spendingTime: SpendingTime,
                    @SerializedName("customOrder")
                    @Expose
                    var customOrder: Int)

class SpendingTime(@SerializedName("viewStartTime")
                   @Expose
                   var viewStartTime: String,
                   @SerializedName("viewEndTime")
                   @Expose
                   var viewEndTime: String)
