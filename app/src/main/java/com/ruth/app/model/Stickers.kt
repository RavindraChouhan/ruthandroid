package com.ruth.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Stickers(@SerializedName("status")
               @Expose
               var status: Boolean? = null,
               @SerializedName("message")
               @Expose
               var message: String? = null,
               @SerializedName("data")
               @Expose
               var data: StickerType? = null,
               @SerializedName("zh_Hans")
               @Expose
               var zhHans: String? = null,
               @SerializedName("zh_Hant")
               @Expose
               var zhHant: String? = null)

class StickerType(@SerializedName("positive")
                  @Expose
                  var positive: List<PositiveNegative>? = null,
                  @SerializedName("negative")
                  @Expose
                  var negative: List<PositiveNegative>? = null,
                  @SerializedName("specificOrder")
                  @Expose
                  var specificOrder: List<PositiveNegative>? = null,
                  @SerializedName("customGift")
                  @Expose
                  var customGift: CustomGift? = null)

class PositiveNegative(@SerializedName("_id")
                       @Expose
                       var _id: String = "",
                       @SerializedName("stickerData")
                       @Expose
                       var stickersData: List<StickersData>? = null)

class StickersData(@SerializedName("_id")
                   @Expose
                   var _id: String = "",
                   @SerializedName("title")
                   @Expose
                   var title: String = "",
                   @SerializedName("stickerLabel")
                   @Expose
                   var stickerLabel: String = "",
                   @SerializedName("image")
                   @Expose
                   var image: String = "",
                   @SerializedName("stickerType")
                   @Expose
                   var stickerType: String = "",
                   @SerializedName("giftValue")
                   @Expose
                   var giftValue: Int = 0,
                   @SerializedName("giftType")
                   @Expose
                   var giftType: String = "",
                   @SerializedName("createdAt")
                   @Expose
                   var createdAt: String = "",
                   @SerializedName("updatedAt")
                   @Expose
                   var updatedAt: String = "",
                   @SerializedName("__v")
                   @Expose
                   var __v: Int = 0)

class CustomGift(@SerializedName("_id")
                 @Expose
                 var _id: String = "",
                 @SerializedName("giftType")
                 @Expose
                 var giftType: String = "",
                 @SerializedName("giftValue")
                 @Expose
                 var giftValue: String = "")