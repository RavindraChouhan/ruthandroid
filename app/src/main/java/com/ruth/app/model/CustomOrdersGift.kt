package com.ruth.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CustomOrdersGift(
        @SerializedName("status")
        @Expose
        var status: Boolean,
        @SerializedName("message")
        @Expose
        var message: String,
        @SerializedName("zh_Hans")
        @Expose
        var zhHans: String,
        @SerializedName("zh_Hant")
        @Expose
        var zhHant: String,
        @SerializedName("data")
        @Expose
        var data: ArrayList<BroadcastCustomOrder>? = null)

class BroadcastCustomOrder(@SerializedName("_id")
                           @Expose
                           var id: String,
                           @SerializedName("comment")
                           @Expose
                           var comment: String,
                           @SerializedName("currentTime")
                           @Expose
                           var currentTime: String,
                           @SerializedName("orderCompletionTime")
                           @Expose
                           var orderCompletionTime: Int,
                           @SerializedName("isCompleted")
                           @Expose
                           var isCompleted: Boolean,
                           @SerializedName("isRejected")
                           @Expose
                           var isRejected: Boolean,
                           @SerializedName("userId")
                           @Expose
                           var userId: String,
                           @SerializedName("userData")
                           @Expose
                           var userData: CustomOrdersUserData,
                           @SerializedName("giftData")
                           @Expose
                           var giftData: CustomOrdersGiftData)

class CustomOrdersUserData(@SerializedName("_id")
                           @Expose
                           var _id: String,
                           @SerializedName("userName")
                           @Expose
                           var userName: String,
                           @SerializedName("name")
                           @Expose
                           var name: String,
                           @SerializedName("photoUrl")
                           @Expose
                           var photoUrl: String)

class CustomOrdersGiftData(@SerializedName("_id")
                           @Expose
                           var _id: String,
                           @SerializedName("giftType")
                           @Expose
                           var giftType: String,
                           @SerializedName("giftValue")
                           @Expose
                           var giftValue: Int)