package com.ruth.app.model

import com.google.gson.annotations.SerializedName

class PostResponse(@SerializedName("status")
                    val status: Boolean,
                   @SerializedName("message")
                    val message: String,
                   @SerializedName("zh_Hant")
                    val zhHant: String,
                   @SerializedName("zh_Hans")
                    val zhHans: String)