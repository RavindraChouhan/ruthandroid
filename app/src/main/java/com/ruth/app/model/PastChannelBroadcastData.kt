package com.ruth.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.ruth.app.home.model.ChannelBroadcastData

class PastChannelBroadcastData (
        @SerializedName("status")
        @Expose
        var status: Boolean,
        @SerializedName("message")
        @Expose
        var message: String,
        @SerializedName("zh_Hans")
        @Expose
        var zhHans: String,
        @SerializedName("zh_Hant")
        @Expose
        var zhHant: String,
        @SerializedName("data")
        @Expose
        var data: ChannelBroadcastData
)