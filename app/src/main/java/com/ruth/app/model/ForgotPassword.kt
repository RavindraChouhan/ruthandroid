package com.ruth.app.model

class ForgotPassword(
        var status: Boolean,
        var message: String,
        val zh_Hant: String,
        val zh_Hans: String,
        var token: String

)