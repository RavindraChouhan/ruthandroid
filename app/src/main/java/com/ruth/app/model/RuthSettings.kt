package com.ruth.app.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class RuthSettings(@SerializedName("status")
                   val status: Boolean,
                   @SerializedName("message")
                   val message: String,
                   @SerializedName("zh_Hant")
                   val zhHant: String,
                   @SerializedName("zh_Hans")
                   val zhHans: String,
                   @SerializedName("data")
                   val data: RuthSettingsData)


class RuthSettingsData(@SerializedName("terms")
                       val terms: String,
                       @SerializedName("contactUs")
                       val contactUs: ContactUs)

class ContactUs(@SerializedName("address")
                val address: String,
                @SerializedName("hotline")
                val hotline: String,
                @SerializedName("email")
                val email: String,
                @SerializedName("openingStartTime")
                val openingStartTime: String,
                @SerializedName("openingEndTime")
                val openingEndTime: String,
                @SerializedName("website")
                val website: String) : Serializable