package com.ruth.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class InvitedFans(
        @SerializedName("status")
        @Expose
        var status: Boolean,
        @SerializedName("message")
        @Expose
        var message: String,
        @SerializedName("zh_Hans")
        @Expose
        var zhHans: String,
        @SerializedName("zh_Hant")
        @Expose
        var zhHant: String,
        @SerializedName("data")
        @Expose
        var data: ArrayList<InvitedFansData>? = null)

class InvitedFansData(@SerializedName("_id")
                      @Expose
                      var id: String,
                      @SerializedName("userName")
                      @Expose
                      var userName: String,
                      @SerializedName("name")
                      @Expose
                      var name: String,
                      @SerializedName("photoUrl")
                      @Expose
                      var photoUrl: String,
                      @SerializedName("isInvite")
                      @Expose
                      var isInvite: Boolean,
                      @SerializedName("isLiveStatus")
                      @Expose
                      var isLiveStatus: Boolean = false)