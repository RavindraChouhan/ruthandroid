package com.ruth.app.model

class SendOTPResponse(val status: Boolean,
                      val message: String,
                      val zh_Hant: String,
                      val zh_Hans: String)