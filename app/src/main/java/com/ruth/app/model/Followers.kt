package com.ruth.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Followers(@SerializedName("_id")
                @Expose
                var id: String,
                @SerializedName("photoUrl")
                @Expose
                var photoUrl: String)
