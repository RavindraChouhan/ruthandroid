package com.ruth.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BroadcastInfo(
        @SerializedName("status")
        @Expose
        var status: Boolean,
        @SerializedName("message")
        @Expose
        var message: String,
        @SerializedName("zh_Hans")
        @Expose
        var zhHans: String,
        @SerializedName("zh_Hant")
        @Expose
        var zhHant: String,
        @SerializedName("data")
        @Expose
        var data: BroadcastInfoData
)

class BroadcastInfoData(@SerializedName("cover")
                        @Expose
                        var cover: String,
                        @SerializedName("title")
                        @Expose
                        var title: String,
                        @SerializedName("tag")
                        @Expose
                        var tag: String,
                        @SerializedName("likes")
                        @Expose
                        var likes: List<Object>? = null,
                        @SerializedName("comments")
                        @Expose
                        var comments: List<Object>? = null,
                        @SerializedName("views")
                        @Expose
                        var views: List<Object>? = null,
                        @SerializedName("url")
                        @Expose
                        var url: String,
                        @SerializedName("_id")
                        @Expose
                        var id: String,
                        @SerializedName("createdBy")
                        @Expose
                        var createdBy: String,
                        @SerializedName("createdAt")
                        @Expose
                        var createdAt: String,
                        @SerializedName("updatedAt")
                        @Expose
                        var updatedAt: String,
                        @SerializedName("__v")
                        @Expose
                        var v: Integer)