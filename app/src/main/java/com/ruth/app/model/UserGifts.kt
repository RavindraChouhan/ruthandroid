package com.ruth.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserGifts(
        @SerializedName("status")
        @Expose
        var status: Boolean,
        @SerializedName("message")
        @Expose
        var message: String,
        @SerializedName("zh_Hans")
        @Expose
        var zhHans: String,
        @SerializedName("zh_Hant")
        @Expose
        var zhHant: String,
        @SerializedName("data")
        @Expose
        var data: ArrayList<UserGiftData>? = null)

class UserGiftData(@SerializedName("_id")
                   @Expose
                   var id: String,
                   @SerializedName("count")
                   @Expose
                   var count: Int,
                   @SerializedName("userId")
                   @Expose
                   var userId: String,
                   @SerializedName("userData")
                   @Expose
                   var userData: UserData)

class UserData(@SerializedName("_id")
               @Expose
               var id: String,
               @SerializedName("email")
               @Expose
               var email: String,
               @SerializedName("userName")
               @Expose
               var userName: String,
               @SerializedName("followers")
               @Expose
               var followers: Int,
               @SerializedName("name")
               @Expose
               var name: String,
               @SerializedName("photoUrl")
               @Expose
               var photoUrl: String,
               @SerializedName("isFollow")
               @Expose
               var isFollow: Boolean)


