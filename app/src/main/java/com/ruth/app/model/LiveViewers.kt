package com.ruth.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LiveViewers(
        var giftId: GiftId,
        var userId: UserId,
        var count: Int)

class GiftId(var id: String,
             var giftType: String)

class UserId(var id: String,
             var photoUrl: String)
