package com.ruth.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class CustomOrderStatus(
        @SerializedName("status")
        @Expose
        var status: Boolean,
        @SerializedName("message")
        @Expose
        var message: String,
        @SerializedName("zh_Hans")
        @Expose
        var zhHans: String,
        @SerializedName("zh_Hant")
        @Expose
        var zhHant: String,
        @SerializedName("data")
        @Expose
        var data: CustomOrderStatusData? = null)

class CustomOrderStatusData(@SerializedName("_id")
                            @Expose
                            var id: String,
                            @SerializedName("comment")
                            @Expose
                            var comment: String,
                            @SerializedName("currentTime")
                            @Expose
                            var currentTime: String,
                            @SerializedName("orderCompletionTime")
                            @Expose
                            var orderCompletionTime: Int,
                            @SerializedName("isCompleted")
                            @Expose
                            var isCompleted: Boolean,
                            @SerializedName("isRejected")
                            @Expose
                            var isRejected: Boolean,
                            @SerializedName("userId")
                            @Expose
                            var userId: String,
                            @SerializedName("giftId")
                            @Expose
                            var giftId: String,
                            @SerializedName("broadcastId")
                            @Expose
                            var broadcastId: String)