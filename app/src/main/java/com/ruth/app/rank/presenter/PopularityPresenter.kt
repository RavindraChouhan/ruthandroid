package com.ruth.app.rank.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class PopularityPresenter @Inject constructor() : BasePresenter<PopularityPresenter.PopularityFragmentView>() {

    override fun onCreate(view: PopularityFragmentView) {
        super.onCreate(view)
    }

    interface PopularityFragmentView : BaseView {

    }
}