package com.ruth.app.rank.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class RichPresenter @Inject constructor(): BasePresenter<RichPresenter.RichFragmentView>(){

    override fun onCreate(view: RichFragmentView) {
        super.onCreate(view)
    }

    interface RichFragmentView : BasePresenter.BaseView {

    }
}
