package com.ruth.app.rank.common

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parceler
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize

class Comman(var name: String?,
             var userImage: Drawable? = null,
             var fansCount: Int?,
             var isSelected: Boolean = true) : Parcelable {

    companion object : Parceler<Comman> {
        override fun Comman.write(parcel: Parcel, flags: Int) {
            val bitmap = (userImage as BitmapDrawable).bitmap as Bitmap
            parcel.writeParcelable(bitmap, flags)
        }

        override fun create(parcel: Parcel): Comman = TODO()
    }
}