package com.ruth.app.rank.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class RankHomePresenter @Inject constructor() : BasePresenter<RankHomePresenter.RankHomeView>() {

    override fun onCreate(view: RankHomeView) {
        super.onCreate(view)
    }

    interface RankHomeView : BaseView {

    }
}
