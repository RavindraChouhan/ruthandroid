package com.ruth.app.rank.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.ruth.app.home.fragment.LiveFragment
import com.ruth.app.rank.fragment.FansFragment
import com.ruth.app.rank.fragment.GiftFragment
import com.ruth.app.rank.fragment.PopularityFragment
import com.ruth.app.rank.fragment.RichFragment

class RankViewPagerAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return PopularityFragment()
            1 -> return GiftFragment()
            2 -> return FansFragment()
            3 -> return RichFragment()

        }
        return LiveFragment()
    }

    override fun getCount(): Int {
       return 4
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return super.getPageTitle(position)
    }

//    override fun saveState(): Parcelable {
//        val bundle = saveState() as Bundle
//        bundle.putParcelableArray("states", null) // Never maintain any states from the base class to avoid TransactionTooLargeException
//        return bundle
//    }
}