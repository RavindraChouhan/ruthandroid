package com.ruth.app.rank.common

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class CommanPresenter @Inject constructor() : BasePresenter<CommanPresenter.CommanView>() {

    interface CommanView : BaseView {

    }
}