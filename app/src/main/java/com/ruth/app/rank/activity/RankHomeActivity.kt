package com.ruth.app.rank.activity

import android.os.Bundle
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.rank.adapter.RankViewPagerAdapter
import com.ruth.app.rank.presenter.RankHomePresenter
import kotlinx.android.synthetic.main.activity_rank_home.*

class RankHomeActivity : BaseActivity<RankHomePresenter>(), RankHomePresenter.RankHomeView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rank_home)
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        var pagerAdapter = RankViewPagerAdapter(supportFragmentManager)
        viewPagerRank!!.adapter = pagerAdapter
        tabLayoutRank.setupWithViewPager(viewPagerRank)

        btn_back_rank.setOnClickListener()
        {
            finish()
            overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
        }

        // set title
        tabLayoutRank.getTabAt(0)!!.setText(R.string.rank_home_popularity_tab1)
        tabLayoutRank.getTabAt(1)!!.setText(R.string.rank_home_gift_tab2)
        tabLayoutRank.getTabAt(2)!!.setText(R.string.rank_home_fans_tab3)
        tabLayoutRank.getTabAt(3)!!.setText(R.string.rank_home_rich_tab4)
    }

}