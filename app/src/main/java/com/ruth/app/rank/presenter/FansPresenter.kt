package com.ruth.app.rank.presenter

import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.home.presenter.HomeFragmentPresenter
import javax.inject.Inject

class FansPresenter @Inject constructor() : BasePresenter<FansPresenter.FansFragmentView>() {

    override fun onCreate(view: FansFragmentView) {
        super.onCreate(view)
    }

    interface FansFragmentView : BaseView {

    }
}