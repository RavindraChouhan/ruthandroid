package com.ruth.app.rank.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import com.ruth.app.otheruser.OtherUserProfileActivity
import kotlinx.android.synthetic.main.item_popularity.view.*

class PopularityAdapter(val context: Context) : RecyclerView.Adapter<PopularityAdapter.PopularityViewHolder>() {

    var isFollow:Boolean = false
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PopularityViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.item_popularity, parent, false)
        return PopularityViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 10
    }

    override fun onBindViewHolder(holder: PopularityViewHolder, position: Int) {
        if (position == 0) {
            holder.itemView.lytBottom.visibility = View.GONE
            holder.itemView.lytTop.visibility = View.VISIBLE
        } else if (position == 1) {
            holder.itemView.lytBottom.visibility = View.VISIBLE
            holder.itemView.lytTop.visibility = View.GONE
            holder.itemView.imgFire.visibility = View.VISIBLE
            holder.itemView.imgFire.setImageResource(R.drawable.ic_fire_blue)
            holder.itemView.lblPosition.visibility = View.GONE
        } else if (position == 2) {
            holder.itemView.lytBottom.visibility = View.VISIBLE
            holder.itemView.lytTop.visibility = View.GONE
            holder.itemView.imgFire.visibility = View.VISIBLE
            holder.itemView.imgFire.setImageResource(R.drawable.ic_fire_gray)
            holder.itemView.lblPosition.visibility = View.GONE
        } else {
            holder.itemView.lytBottom.visibility = View.VISIBLE
            holder.itemView.lytTop.visibility = View.GONE
            holder.itemView.imgFire.visibility = View.GONE
            holder.itemView.lblPosition.visibility = View.VISIBLE
            holder.itemView.lblPosition.text = (position + 1).toString()
        }


        holder.itemView.img_follow.setOnClickListener {
            if (isFollow){
                isFollow = false
                holder.itemView.img_follow.setImageResource(R.drawable.icon_follow_button)
            }else{
                isFollow = true
                holder.itemView.img_follow.setImageResource(R.drawable.icon_follow)
            }
        }

        holder.itemView.layoutUser.setOnClickListener {
            context.startActivity(Intent(context, OtherUserProfileActivity::class.java))
        }


    }


    inner class PopularityViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
