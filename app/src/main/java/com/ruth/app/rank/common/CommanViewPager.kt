package com.ruth.app.rank.common

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class CommanViewPager(fm: FragmentManager?, var favoriteList: ArrayList<Comman>?) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return CommanFragment().newInstance(favoriteList!!)
    }

    override fun getCount(): Int {
        return 6
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return super.getPageTitle(position)
    }
}