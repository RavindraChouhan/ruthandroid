package com.ruth.app.rank.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class GiftPresenter @Inject constructor() : BasePresenter<GiftPresenter.GiftFragmentView>() {

    override fun onCreate(view: GiftFragmentView) {
        super.onCreate(view)
    }

    interface GiftFragmentView : BaseView {

    }
}