package com.ruth.app.rank.fragment

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.fragment.BaseFragment
import com.ruth.app.rank.adapter.PopularityAdapter
import com.ruth.app.rank.common.Comman
import com.ruth.app.rank.common.CommanViewPager
import com.ruth.app.rank.presenter.PopularityPresenter
import kotlinx.android.synthetic.main.fragment_rank_popularity.view.*

class PopularityFragment : BaseFragment<PopularityPresenter>(), PopularityPresenter.PopularityFragmentView {

    lateinit var adapter: PopularityAdapter
    lateinit var layoutManager: LinearLayoutManager

    private var favoritePagerAdapter: CommanViewPager? = null
    private var favoriteList: ArrayList<Comman>? = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_rank_popularity, container, false)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        setupTabLayout(view)
        return view
    }


    private fun setupTabLayout(view: View) {
        favoriteList?.clear()
        for (i in 0 until 5) {
            favoriteList?.add(Comman("張大文", resources.getDrawable(R.drawable.com_facebook_profile_picture_blank_square), 1234))
        }
        favoritePagerAdapter = CommanViewPager(childFragmentManager, favoriteList)
        view.rvPopular!!.adapter = favoritePagerAdapter
        view.tabsPopular.setupWithViewPager(view.rvPopular)

        view.tabsPopular.getTabAt(0)!!.setText(R.string.user)
        view.tabsPopular.getTabAt(1)!!.setText(R.string.live)
        view.tabsPopular.getTabAt(2)!!.setText(R.string.videos)
        view.tabsPopular.getTabAt(3)!!.setText(R.string.podcast)
        view.tabsPopular.getTabAt(4)!!.setText(R.string.pictures)
        view.tabsPopular.getTabAt(5)!!.setText(R.string.articles)


        view.tabsPopular.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                val position = tab.position

                when (position) {
                    0 -> {
                        favoriteList?.clear()
                        for (i in 0 until 5) {
                            favoriteList?.add(Comman("張大文", resources.getDrawable(R.drawable.com_facebook_profile_picture_blank_square), 1234))
                        }
                    }
                    1 -> {
                        favoriteList?.clear()
                        for (i in 0 until 10) {
                            favoriteList?.add(Comman("張大文", resources.getDrawable(R.drawable.contact_placeholder), 1234))
                        }
                    }
                    2 -> {
                        favoriteList?.clear()
                        for (i in 0 until 6) {
                            favoriteList?.add(Comman("張大文", resources.getDrawable(R.drawable.com_facebook_profile_picture_blank_square), 1234))
                        }
                    }
                    3 -> {
                        favoriteList?.clear()
                        for (i in 0 until 3) {
                            favoriteList?.add(Comman("張大文", resources.getDrawable(R.drawable.contact_placeholder), 1234))
                        }
                    }
                    4 -> {
                        favoriteList?.clear()
                        for (i in 0 until 1) {
                            favoriteList?.add(Comman("張大文", resources.getDrawable(R.drawable.com_facebook_profile_picture_blank_square), 1234))
                        }
                    }
                    5 -> {
                        favoriteList?.clear()
                        for (i in 0 until 2) {
                            favoriteList?.add(Comman("張大文", resources.getDrawable(R.drawable.contact_placeholder), 1234))
                        }
                    }
                }
            }
        })
    }

}