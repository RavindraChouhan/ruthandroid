package com.ruth.app.rank.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.fragment.BaseFragment
import com.ruth.app.rank.presenter.RichPresenter

class RichFragment : BaseFragment<RichPresenter>(), RichPresenter.RichFragmentView {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_rank_rich, container, false)
        App.getComponent().inject(this)
        presenter.onCreate(this)
        return view
    }
}