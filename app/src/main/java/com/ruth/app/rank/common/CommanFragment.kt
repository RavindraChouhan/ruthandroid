package com.ruth.app.rank.common

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.fragment.BaseFragment
import com.ruth.app.rank.adapter.PopularityAdapter
import kotlinx.android.synthetic.main.fragment_list_favorite.view.*

class CommanFragment : BaseFragment<CommanPresenter>(), CommanPresenter.CommanView {

    private val favoriteList: ArrayList<Comman>? by lazy { arguments?.getParcelableArrayList<Comman>(FAVORITE) }
    private var adapter: PopularityAdapter? = null
    private lateinit var layoutManager: LinearLayoutManager

    companion object {
        const val FAVORITE = "FAVORITE"
    }

    fun newInstance(data: ArrayList<Comman>): CommanFragment {
        /*val args = Bundle()
        args.putParcelableArrayList(FAVORITE, data)*/
        return CommanFragment()/*.withArguments(args)*/
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.item_comman, container, false)
        App.getComponent().inject(this)
        presenter.onCreate(this)

        adapter = PopularityAdapter(this.activity!!)
        layoutManager = LinearLayoutManager(this.activity, LinearLayoutManager.VERTICAL, false)
        view.rvFavoriteList!!.layoutManager = layoutManager
        view.rvFavoriteList!!.adapter = adapter

        return view
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            fragmentManager!!.beginTransaction().detach(this).attach(this).commit()
        }
    }
}