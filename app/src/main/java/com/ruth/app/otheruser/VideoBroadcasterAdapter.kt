package com.ruth.app.otheruser

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ruth.app.R
import com.ruth.app.broadcaster.adapter.FollowerAdapter
import com.ruth.app.model.LiveVideos
import com.ruth.app.utility.RuthUtility
import kotlinx.android.synthetic.main.item_other_user_live_view.view.*
import java.util.*


class VideoBroadcasterAdapter(val context: Context?,
                              var videoList: ArrayList<LiveVideos>,
                              var type: String) : RecyclerView.Adapter<VideoBroadcasterAdapter.VideoBroadcasterHolder>() {

    private var adapter: FollowerAdapter? = null
    var list: ArrayList<Int> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoBroadcasterHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.item_other_user_live_view, parent, false)
        return VideoBroadcasterHolder(view)
    }

    override fun onBindViewHolder(holder: VideoBroadcasterHolder, position: Int) {
        holder.bind(videoList[position])
    }

    override fun getItemCount(): Int {
        return videoList.size
    }

    inner class VideoBroadcasterHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(liveVideos: LiveVideos) {
            Glide.with(context).load(liveVideos.coverUrl)
                    .apply(RequestOptions().error(R.drawable.logoeye).placeholder(R.drawable.logoeye))
                    .into(itemView.ivCoverImage)

            if (type == "live") {
                itemView.tvVideoType.text = context!!.resources.getString(R.string.live)
            } else {
                itemView.tvVideoType.text = context!!.resources.getString(R.string.videos)
            }

            itemView.tvVideoName.text = liveVideos.broadcastTitle
            itemView.lbl_view_count.text = RuthUtility.getCounts(liveVideos.broadcastViews)
            itemView.lbl_favorite_count.text = RuthUtility.getCounts(liveVideos.broadcastLikes)
            itemView.lbl_scary_count.text = RuthUtility.getCounts(liveVideos.emoji)
            itemView.tvTimeAgo.text = "-" + RuthUtility.getTimeAgo(liveVideos.createdAt)
            if (liveVideos.broadcastStartTime!= null && liveVideos.broadcastEndTime!= null) {
                itemView.tvVideoTime.text = RuthUtility.printDifference(liveVideos.broadcastStartTime,
                        liveVideos.broadcastEndTime, "yyyy-MM-dd'T'HH:mm:ss'Z'")
            }

            itemView.recycle_followers.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = FollowerAdapter(context, liveVideos.viewsUsers!!)
            itemView.recycle_followers.adapter = adapter

        }
    }
}


