package com.ruth.app.otheruser

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class OtherUserLivePresenter @Inject constructor() : BasePresenter<OtherUserLivePresenter.LiveView>() {

    interface LiveView : BaseView {

    }
}