package com.ruth.app.otheruser

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.ruth.app.home.fragment.LiveFragment

class OtherUserViewPagerAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return OtherUserLiveFragment()
    }

    override fun getCount(): Int {
        return 4
    }
}