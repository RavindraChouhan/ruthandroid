package com.ruth.app.otheruser

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.util.Log
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.login.activity.LoginActivity
import com.ruth.app.model.LiveVideos
import com.ruth.app.model.UserProfileData
import com.ruth.app.utility.RuthUtility
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_other_user_profile.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class OtherUserProfileActivity : BaseActivity<OtherUserProfilePresenter>(), OtherUserProfilePresenter.UserProfileView, View.OnClickListener {

    private var context: Context? = null
    private var pagerAdapter: OtherUserViewPagerAdapter? = null
    private var isFollowed: Boolean = false
    private var noFollowers: Int = 0

    companion object {
        var otherUserData: ArrayList<LiveVideos>? = ArrayList()
        var type: String = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_other_user_profile)
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)
        App.getComponent().inject(this)
        presenter.onCreate(this)
        context = this

        setUpSocket()
        presenter.getUserInfo(intent.getStringExtra("otherUserId"))

        collapse_toolbar.isTitleEnabled = false

        imgBack.setOnClickListener(this)
        btnFollow.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            imgBack -> {
                finish()
                overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
                return
            }

            btnFollow -> {
                if (isFollowed) {
                    isFollowed = false
                    noFollowers -= 1
                    btnFollow.setImageResource(R.drawable.icon_follow_button)
                } else {
                    isFollowed = true
                    noFollowers += 1
                    btnFollow.setImageResource(R.drawable.icon_follow)
                }
                lblFans.text = RuthUtility.getCounts(noFollowers)
                presenter.followUnfollow(intent.getStringExtra("otherUserId"), isFollowed)
            }
        }
    }

    override fun onDestroy() {
        if (invalidDialog != null) {
            invalidDialog!!.dismiss()
            invalidDialog = null
        }
        leave()
        super.onDestroy()
    }

    /*--------------------------------------------------------------------------------------------*/

    override fun setUserProfile(data: UserProfileData) {
        progressBarImageLoading.visibility = View.VISIBLE
        Glide.with(context).load(data.photoUrl)
                .apply(RequestOptions().error(R.drawable.com_facebook_profile_picture_blank_square)
                        .placeholder(R.drawable.com_facebook_profile_picture_blank_square)
                        .diskCacheStrategy(DiskCacheStrategy.NONE))
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        progressBarImageLoading.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        progressBarImageLoading.visibility = View.GONE
                        return false
                    }

                })
                .into(imgUserPic)
        lblName.text = data.nickname
        lblUserName.text = "@" + data.userName
        lblUserInfo.text = data.nickname
        lblFans.text = RuthUtility.getCounts(data.followers)
        lblFollowing.text = RuthUtility.getCounts(data.following)

        if (data.isFollow) {
            btnFollow.setImageResource(R.drawable.icon_follow)
        } else {
            btnFollow.setImageResource(R.drawable.icon_follow_button)
        }
        isFollowed = data.isFollow
        noFollowers = data.followers

        //status - 1 = online/ 0 = offline
        if (data.status == 1) {
            imgUserOnline.setImageDrawable(resources.getDrawable(R.drawable.oval_green))
        } else {
            imgUserOnline.setImageDrawable(resources.getDrawable(R.drawable.oval_gray))
        }

        layoutLiveStatus.text = data.nickname + getString(R.string.is_live_now)
        if (data.isLiveStatus) {
            layoutLiveStatus.visibility = android.view.View.VISIBLE
        } else {
            layoutLiveStatus.visibility = android.view.View.GONE
        }

        setTabLayout(data)
    }

    private fun setTabLayout(data: UserProfileData) {
        tabs.setSelectedTabIndicatorHeight(0)
        otherUserData?.clear()

        otherUserData?.addAll(data.live)
        type = "live"
        pagerAdapter = OtherUserViewPagerAdapter(supportFragmentManager)
        viewPagerHome!!.adapter = pagerAdapter
        tabs.setupWithViewPager(viewPagerHome)

        // set icons
        tabs.getTabAt(0)!!.setText(R.string.home_live_tab1)
        tabs.getTabAt(1)!!.setText(R.string.home_video_picture_tab2)
        tabs.getTabAt(2)!!.setText(R.string.home_podcasts_audio_tab3)
        tabs.getTabAt(3)!!.setText(R.string.pictures)


        tabs.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                val position = tab!!.position

                when (position) {
                    0 -> {
                        otherUserData?.clear()
                        type = "live"
                        otherUserData?.addAll(data.live)
                    }
                    1 -> {
                        otherUserData?.clear()
                        type = "videos"
                        otherUserData?.addAll(data.videos)
                    }
                    2 -> {
                        otherUserData?.clear()
                        type = "videos"
                        otherUserData?.addAll(data.videos)
                    }
                    3 -> {
                        otherUserData?.clear()
                        type = "videos"
                        otherUserData?.addAll(data.videos)
                    }
                }
                viewPagerHome.setCurrentItem(position, true)
            }
        })
    }

    override fun showLoadingProgress() {
        //progressBarBroadcast.visibility = View.VISIBLE
    }

    override fun hideLoadingProgress() {
        //  progressBarBroadcast.visibility = View.GONE
    }

    private var invalidDialog: AlertDialog? = null

    override fun invalidLogin() {

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Invalid Login")
        builder.setMessage(getString(R.string.invalid_login_error))

        builder.setPositiveButton(getString(R.string.log_in)) { dialog, _ ->
            presenter.clearLogin()
            startActivity(Intent(this, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            finish()
            overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
            dialog.dismiss()
        }

        if (!isFinishing && window.decorView.isShown) {
            invalidDialog = builder.create()
            invalidDialog!!.setCancelable(false)
            invalidDialog!!.show()
        }
    }

    override fun clearPreferences() {
        startActivity(Intent(this, LoginActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        finish()
    }

    /*---------------------------------------START OF SOCKET--------------------------------------*/
    private var mSocket: Socket? = null
    private var isConnected: Boolean? = true

    private fun setUpSocket() {
        val app = this.application as App
        mSocket = app.socket
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
        mSocket!!.on("RECEIVE_LIVE", onUpdateStatus)
        mSocket!!.on("RECEIVE_ACTIVITY_STATUS", onActivityStatus)
        mSocket!!.on("RECEIVE_ELITE", onGetEliteUser)
        mSocket!!.connect()
    }

    private val onConnect = Emitter.Listener {
        this.runOnUiThread {
            if (!isConnected!!) {
                isConnected = true
            }
        }
    }

    private val onDisconnect = Emitter.Listener {
        this.runOnUiThread(Runnable {
            Log.i("socket", "disconnected")
            isConnected = false
        })
    }

    private val onConnectError = Emitter.Listener {
        this.runOnUiThread {
            Log.e("socket", "Error connecting")
        }
    }

    private val onUpdateStatus = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            try {
                val data = args[0] as JSONObject
                val broadcasterId: String
                val isLive: Boolean
                // Log.d("args", "-data :- $data")
                try {
                    broadcasterId = data.getString("broadcasterUserId")
                    isLive = data.getBoolean("isLiveStatus")
                    if (broadcasterId == intent.getStringExtra("otherUserId")) {
                        if (isLive) {
                            layoutLiveStatus.visibility = android.view.View.VISIBLE
                        } else {
                            layoutLiveStatus.visibility = android.view.View.GONE
                        }
                    }
                    Log.d("id", "->" + broadcasterId + "user id -> " + intent.getStringExtra("otherUserId"))
                } catch (e: JSONException) {
                    Log.e(TAG, e.message)
                    return@Runnable
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private val onActivityStatus = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            try {
                val data = args[0] as JSONObject
                val userId: String
                val status: Int
                try {
                    userId = data.getString("userId")
                    status = data.getInt("status")
                    if (userId == intent.getStringExtra("otherUserId")) {
                        if (status == 0) {
                            imgUserOnline.setImageResource(R.drawable.oval_gray)
                        } else {
                            imgUserOnline.setImageResource(R.drawable.oval_green)
                        }
                    }
                } catch (e: JSONException) {
                    Log.e(TAG, e.message)
                    return@Runnable
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private val onGetEliteUser = Emitter.Listener { args ->
        this.runOnUiThread {
            try {
                val data = args[0] as JSONArray
                var id: String
                var users: JSONObject
                var userId: String
                var userPhotoUrl: String

                var giftData: JSONObject
                var giftId: String
                var giftType: String

                for (i in 0 until data.length()) {
                    val jsonData = data[i] as JSONObject
                    id = jsonData.getString("_id")
                    users = jsonData.getJSONObject("users")
                    userId = users.getString("_id")
                    userPhotoUrl = users.getString("photoUrl")

                    giftData = jsonData.getJSONObject("giftData")
                    giftId = giftData.getString("_id")
                    giftType = giftData.getString("giftType")

                    Log.d("elite", " ->" + id + " users id-> " + userId +
                            " photo url -> " + userPhotoUrl +
                            " gift id -> " + giftId + " type -> " + giftType)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun leave() {
        mSocket!!.disconnect()
        mSocket!!.connect()
    }
/*---------------------------------------END OF SOCKET--------------------------------------*/

}
