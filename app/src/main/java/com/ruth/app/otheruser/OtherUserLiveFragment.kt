package com.ruth.app.otheruser

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_other_user_live.view.*

class OtherUserLiveFragment : BaseFragment<OtherUserLivePresenter>(), OtherUserLivePresenter.LiveView {

    private var adapter: VideoBroadcasterAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_other_user_live, container, false)
        App.getComponent().inject(this)
        presenter.onCreate(this)


        if (OtherUserProfileActivity.otherUserData!!.isEmpty()){
            view.tvEmptyState.visibility = View.VISIBLE
            view.rvLiveBroadcaster.visibility = View.GONE
        }

        adapter = VideoBroadcasterAdapter(context, OtherUserProfileActivity.otherUserData!!, OtherUserProfileActivity.type)
        view.rvLiveBroadcaster.adapter = adapter

        return view
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            fragmentManager!!.beginTransaction().detach(this).attach(this).commit()
        }
    }
}