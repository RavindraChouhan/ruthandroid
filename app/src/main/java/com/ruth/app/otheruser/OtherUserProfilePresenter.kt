package com.ruth.app.otheruser

import android.widget.Toast
import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.model.UserProfileData
import com.ruth.app.network.config.RuthApi
import java.util.*
import javax.inject.Inject

class OtherUserProfilePresenter @Inject constructor(private val ruthApi: RuthApi) : BasePresenter<OtherUserProfilePresenter.UserProfileView>() {

    fun getUserInfo(userId: String?) {
        ruthApi.getUserInfo(preferences.getUserInfo().userToken, userId!!)
                .subscribe({
                    if (it != null) {
                        if (it.status) {
                            view?.setUserProfile(it.data)
                        } else if (it.message.contains(context.getString(R.string.device_login), true)
                                || (it.zhHans.contains(context.getString(R.string.device_login), true)
                                        || it.zhHant.contains(context.getString(R.string.device_login), true))) {
                            view?.invalidLogin()
                            view?.hideLoadingProgress()
                        } else {
                            view?.hideLoadingProgress()
                        }
                    }
                }, {
                    view?.showToast(getString(R.string.connection_error))
                })
    }

    fun followUnfollow(followingId: String, isFollowed: Boolean) {
        val hm = HashMap<String, @JvmSuppressWildcards Any>()
        hm["type"] = "follow"
        hm["isFollow"] = isFollowed
        hm["followingId"] = followingId
        hm["userId"] = preferences.getUserInfo().id

        ruthApi.followUnfollowUser(preferences.getUserInfo().userToken, hm)
                .subscribe {
                    if (it != null) {
                        if (it.status) {

                        } else if (it.message.contains(context.getString(R.string.device_login), true)
                                || it.zhHans.contains(context.getString(R.string.device_login), true)
                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
                            view?.invalidLogin()
                        } else {
                            Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
    }

    fun clearLogin() {
        val tempFb = utility.getBooleaPreferences(context, getString(R.string.isfbSignup))
        val tempGoogle = utility.getBooleaPreferences(context, getString(R.string.isGoogleSignup))
        val tempInsta = utility.getBooleaPreferences(context, getString(R.string.isInstaSignup))

        preferences.clearPreferences()

        utility.setBooleanPreferences(context, getString(R.string.isfbSignup), tempFb)
        utility.setBooleanPreferences(context, getString(R.string.isGoogleSignup), tempGoogle)
        utility.setBooleanPreferences(context, getString(R.string.isInstaSignup), tempInsta)

        view?.clearPreferences()
    }


    interface UserProfileView : BaseView {
        fun setUserProfile(data: UserProfileData)
        fun invalidLogin()
        fun clearPreferences()
        fun showLoadingProgress()
        fun hideLoadingProgress()
    }
}