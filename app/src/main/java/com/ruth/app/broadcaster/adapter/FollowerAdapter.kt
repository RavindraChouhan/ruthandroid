package com.ruth.app.broadcaster.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.ruth.app.R
import com.ruth.app.home.model.Followers
import kotlinx.android.synthetic.main.item_followers_cell.view.*

class FollowerAdapter(val mContext: Context?, val list: ArrayList<Followers>) :
        RecyclerView.Adapter<FollowerAdapter.ViewHolder>() {

    private var isForFirst: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_followers_cell, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position > 2) {
            if (!isForFirst) {
                isForFirst = true
                holder.itemView.imgBroadCaster.visibility = View.GONE
                holder.itemView.layoutExtraBroadcaster.visibility = View.VISIBLE
                holder.itemView.lblNumBroadcaster.text = /*"+18"*/(list.size - 3).toString()
            } else {
                holder.itemView.imgBroadCaster.visibility = View.GONE
            }
        } else {
            Glide.with(mContext).load(list[position].photoUrl).into(holder.itemView.imgBroadCaster)
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}