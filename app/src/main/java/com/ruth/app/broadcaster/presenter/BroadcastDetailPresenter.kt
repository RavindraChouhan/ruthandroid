package com.ruth.app.broadcaster.presenter

import android.util.Log
import android.widget.Toast
import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.home.activity.HomeActivity
import com.ruth.app.home.model.ChannelDetail
import com.ruth.app.network.config.RuthApi
import com.ruth.app.utility.RuthUtility
import java.util.*
import javax.inject.Inject

class BroadcastDetailPresenter @Inject constructor(private val ruthApi: RuthApi) :
        BasePresenter<BroadcastDetailPresenter.BroadcastDetailView>() {

    fun showHideDescription() {
        view?.showHideDescription()
    }

    fun getChannelDetail(channelId: String) {
        if (utility.isConnectingToInternet(context)) {
            getChannelDetailApi(channelId)
        } else {
            val thread = Thread(Runnable {
                Runtime.getRuntime().gc()
                val internetConnection = utility.checkInternetWithoutDialog(context)
                if (internetConnection) {
                    getChannelDetailApi(channelId)
                } else {
                    getChannelDetail(channelId)
                }
            })
            thread.start()
        }
    }

    private fun getChannelDetailApi(channelId: String) {
        ruthApi.getChannelById(preferences.getUserInfo().userToken, channelId)
                .onErrorReturn { null }
                .doOnError {
                    view?.hideLoadingProgress()
                    Log.d("error", "-> $it")
                }
                .subscribe {
                    view?.showLoadingProgress()
                    if (it != null) {
                        if (it.status) {
                            view?.setChannelDetail(it.data)
//                            view?.hideLoadingProgress()
                        } else if (it.zhHans.contains(context.getString(R.string.device_login), true)
                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
                            view?.invalidLogin()
                            view?.hideLoadingProgress()
                        } else {
                            view?.hideLoadingProgress()
                        }
                    }
                }
    }

    fun getLiveStickers() {
        ruthApi.getLiveStickers(preferences.getUserInfo().userToken)
                .onErrorReturn { null }
                .subscribe {
                    if (it != null) {
                        if (it.status!!) {
                            preferences.saveStickers(it.data!!)
                            Log.d("stickers", "->" + it.data)
                        }
                    }
                }

        getMyProfile()
    }

    private fun getMyProfile() {
        ruthApi.getUserInfo(preferences.getUserInfo().userToken, preferences.getUserInfo().id)
                .subscribe({
                    if (it != null) {
                        if (it.status) {
                            preferences.setUserProfile(it.data)
                            if (it.data.pointEarn!!.isNotEmpty()){
                                RuthUtility.updatePoints(context,it.data.pointEarn)
                            }
                        } else {
                            // view?.invalidLogin()
                        }
                    }
                }, {
                    view?.showToast(getString(R.string.connection_error))
                })
    }

    fun clearLogin() {
        val tempFb = utility.getBooleaPreferences(context, getString(R.string.isfbSignup))
        val tempGoogle = utility.getBooleaPreferences(context, getString(R.string.isGoogleSignup))
        val tempInsta = utility.getBooleaPreferences(context, getString(R.string.isInstaSignup))

        preferences.clearPreferences()

        utility.setBooleanPreferences(context, getString(R.string.isfbSignup), tempFb)
        utility.setBooleanPreferences(context, getString(R.string.isGoogleSignup), tempGoogle)
        utility.setBooleanPreferences(context, getString(R.string.isInstaSignup), tempInsta)

        view?.clearPreferences()
    }

    fun postSubscriber(userSubscribedLiked: Boolean, channelId: String, type: String) {

        val hm = HashMap<String, @JvmSuppressWildcards Any>()
        hm["type"] = type
        if (type.equals("like", true)) {
            hm["isLike"] = userSubscribedLiked
        } else {
            hm["isSubscribe"] = userSubscribedLiked
        }
        hm["channelId"] = channelId
        hm["userId"] = preferences.getUserInfo().id

        ruthApi.postSubscribe(preferences.getUserInfo().userToken, hm)
                .subscribe {
                    if (it != null) {
                        if (it.status) {

                        } else if (it.message.contains(context.getString(R.string.device_login), true)
                                || it.zhHans.contains(context.getString(R.string.device_login), true)
                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
                            view?.invalidLogin()
                        } else {
                            Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
    }

    fun followUnFollow(followingId: String, isFollowed: Boolean) {
        val hm = HashMap<String, @JvmSuppressWildcards Any>()
        hm["type"] = "follow"
        hm["isFollow"] = isFollowed
        hm["followingId"] = followingId
        hm["userId"] = preferences.getUserInfo().id

        ruthApi.followUnfollowUser(preferences.getUserInfo().userToken, hm)
                .subscribe {
                    if (it != null) {
                        if (it.status) {

                        } else if (it.message.contains(context.getString(R.string.device_login), true)
                                || it.zhHans.contains(context.getString(R.string.device_login), true)
                                || it.zhHant.contains(context.getString(R.string.device_login), true)) {
                            view?.invalidLogin()
                        } else {
                            Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
    }

    interface BroadcastDetailView : BaseView {
        fun showHideDescription()
        fun setChannelDetail(data: ChannelDetail)
        fun invalidLogin()
        fun clearPreferences()
        fun showLoadingProgress()
        fun hideLoadingProgress()
    }
}