package com.ruth.app.broadcaster.activity

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SimpleItemAnimator
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AccelerateDecelerateInterpolator
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.broadcaster.adapter.FollowerAdapter
import com.ruth.app.broadcaster.adapter.LiveChannelBroadcastAdapter
import com.ruth.app.broadcaster.presenter.BroadcastDetailPresenter
import com.ruth.app.home.adapter.EmojisHomeAdapter
import com.ruth.app.home.model.ChannelBroadcastData
import com.ruth.app.home.model.ChannelDetail
import com.ruth.app.home.model.Followers
import com.ruth.app.livestreaming.activity.LiveVideoPlayerActivity
import com.ruth.app.livestreaming.activity.PastVideoPlayerActivity
import com.ruth.app.login.activity.LoginActivity
import com.ruth.app.utility.RuthUtility
import com.ruth.app.widgets.expandable.ExpandableLayout
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_broadcast_detail.*
import org.json.JSONException
import java.util.*

class BroadcastDetailActivity : BaseActivity<BroadcastDetailPresenter>(), BroadcastDetailPresenter.BroadcastDetailView, View.OnClickListener {

    lateinit var context: Context
    lateinit var adapter: LiveChannelBroadcastAdapter
    private lateinit var emojiAdapter: EmojisHomeAdapter
    private lateinit var followerAdapter: FollowerAdapter
    private var expandableLayout: ExpandableLayout? = null
    private var broadcastId: String = ""
    private var channelDetail: ChannelDetail? = null
    private var sortedList: List<ChannelBroadcastData>? = null

    companion object {
        var list: ArrayList<ChannelDetail> = ArrayList()
        var liveUpcomingBroadcastList: List<ChannelBroadcastData>? = ArrayList()
        var pastBroadcastList: ArrayList<ChannelBroadcastData>? = ArrayList()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
//        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)
        setContentView(R.layout.activity_broadcast_detail)
        App.getComponent().inject(this)
        presenter.onCreate(this)
        context = this
        expandableLayout = findViewById<View>(R.id.lytDescription) as ExpandableLayout
//        dialogs = CustomDialog(context)
        setUpSocket()

        imgLike.setOnClickListener(this)
        imgScary.setOnClickListener(this)
        imgShow.setOnClickListener(this)
        imgBack.setOnClickListener(this)
        imgSharing.setOnClickListener(this)
        lblSeeAll.setOnClickListener(this)
        lblSeeAllPast.setOnClickListener(this)
        imgFollow.setOnClickListener(this)

        presenter.getChannelDetail(intent.getStringExtra("channelId"))
        presenter.getLiveStickers()

        layoutSwipe.setOnRefreshListener {
            presenter.getChannelDetail(intent.getStringExtra("channelId"))
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            imgLike -> {
                var likeCount = channelDetail!!.likes
                if (channelDetail?.isLike!!) {
                    presenter.postSubscriber(false, intent.getStringExtra("channelId"), "like")
                    imgLike.setImageResource(R.drawable.tab_like)
                    if (channelDetail!!.likes != 0) {
                        likeCount -= 1
                    }
                } else {
                    likeCount += 1
                    presenter.postSubscriber(true, intent.getStringExtra("channelId"), "like")
                    imgLike.setImageResource(R.drawable.icon_like)

                }
                lblLikeCount.text = likeCount.toString()
                channelDetail!!.isLike = !channelDetail!!.isLike
                channelDetail!!.likes = likeCount
                return
            }
            imgScary -> {
                //updateEmojis()
                return
            }
            imgShow -> {
                presenter.showHideDescription()
                return
            }
            imgBack -> {
                finish()
                return
            }
            imgSharing -> {
                return
            }
            lblSeeAll -> {
                startActivity(Intent(this, AllBroadcastActivity::class.java)
                        .putExtra("channelId", intent.getStringExtra("channelId"))
                        .putExtra("isPastData", false))
            }
            imgFollow -> {
                if (channelDetail?.isSubscribe!!) {
                    alert("Do you really want to unsubscribe?", "Already Subscribed", {
                        presenter.postSubscriber(false, intent.getStringExtra("channelId"), "subscribe")
                        imgFollow.setImageResource(R.drawable.icon_follow_button)
                    }, {

                    })
                } else {
                    presenter.postSubscriber(true, intent.getStringExtra("channelId"), "subscribe")
                    imgFollow.setImageResource(R.drawable.icon_follow)
                }
                channelDetail!!.isSubscribe = !channelDetail!!.isSubscribe
            }
            lblSeeAllPast -> {
                startActivity(Intent(this, AllBroadcastActivity::class.java)
                        .putExtra("channelId", intent.getStringExtra("channelId"))
                        .putExtra("isPastData", true))
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun setChannelDetail(data: ChannelDetail) {
        channelDetail = data
        layoutSwipe.isRefreshing = false

        tvChannelName.text = data.title
        tvChannelTitle.text = data.title + "!"
        lblChannelHeader.text = data.title + "!"

        val startTime = StringBuilder(data.startTime)
        val endTime = StringBuilder(data.endTime)
        startTime.insert(2, ':')
        endTime.insert(2, ':')

        lblBroadcastTime.text = (utility.convertTime(data.startTime.toInt())).toString() + "-" + (utility.convertTime(data.endTime.toInt())).toString() + " " + utility.findAmOrPm(data.endTime.toInt())
        lblLikeCount.text = data.likes.toString()
        lblScaryCount.text = data.emoji.toString()
        tvChannelViews.text = data.views.toString()
        lblCurrentBroadcast.text = "參加者 (" + data.broadcastData.size.toString() + ")"
        lblPastBroadcast.text = "相關廣播(" + data.pastBroadcastData.size.toString() + ")"
        lblDescription.text = data.description
        tvSubscriberCount.text = data.subscribers.toString() + " 名粉絲"

        if (data.profile.isNotEmpty()) {
            Glide.with(this@BroadcastDetailActivity).load(data.profile)
                    .apply(RequestOptions().error(R.mipmap.ic_launcher_round)
                            .placeholder(R.mipmap.ic_launcher_round))
                    .into(ivProfileImage)
        }
        if (data.cover.isNotEmpty()) {
            Glide.with(this@BroadcastDetailActivity).load(data.cover)
                    .apply(RequestOptions().error(R.drawable.background).placeholder(R.drawable.background))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            progressBarBanner.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            progressBarBanner.visibility = View.GONE
                            return false
                        }

                    })
                    .into(imgChannelCover)
        }

        if (data.isSubscribe) {
            imgFollow.setImageResource(R.drawable.icon_follow)
        } else {
            imgFollow.setImageResource(R.drawable.icon_follow_button)
        }

        if (data.isLike) {
            imgLike.setImageResource(R.drawable.icon_like)
        } else {
            imgLike.setImageResource(R.drawable.tab_like)
        }

        setEmojisList(data.emoji)
        setBroadcaterData(data.activeUserData)

        if (data.broadcastData.size > 0 || data.pastBroadcastData.size > 0) {
            layoutEmptyView.visibility = View.GONE
            layoutAllBroadcast.visibility = View.VISIBLE

            if (data.onAir == "upcoming" || data.onAir == "live") {
                if (data.broadcastData.size > 0) {
                    setCurrentBroadcaster(data.broadcastData)
                    layoutCurrent.visibility = View.VISIBLE
                    lytCurrentBroadcaster.visibility = View.VISIBLE
                    if (data.broadcastData.size > 4) {
                        lblSeeAll.visibility = View.VISIBLE
                    } else {
                        lblSeeAll.visibility = View.GONE
                    }
                } else {
                    layoutCurrent.visibility = View.GONE
                    lytCurrentBroadcaster.visibility = View.GONE
                }
            }

            if (data.pastBroadcastData.size > 0) {
                layoutPast.visibility = View.VISIBLE
                lytPastBroadcaster.visibility = View.VISIBLE
                setPastBroadcaster(data.pastBroadcastData)
                if (data.pastBroadcastData.size > 4) {
                    lblSeeAllPast.visibility = View.VISIBLE
                } else {
                    lblSeeAllPast.visibility = View.GONE
                }
            } else {
                layoutPast.visibility = View.GONE
                lytPastBroadcaster.visibility = View.GONE
            }
        } else {
            layoutAllBroadcast.visibility = View.GONE
            layoutEmptyView.visibility = View.VISIBLE
        }
        hideLoadingProgress()
    }

    private fun setBroadcaterData(data: ArrayList<Followers>) {
        recycle_followers.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        followerAdapter = FollowerAdapter(context, data)
        recycle_followers.adapter = followerAdapter
    }

    override fun onResume() {
        super.onResume()
        if (liveUpcomingBroadcastList!!.isNotEmpty()) {
            setCurrentBroadcaster(liveUpcomingBroadcastList)
        }

        if (pastBroadcastList!!.isNotEmpty()) {
            setPastBroadcaster(pastBroadcastList)
        }
    }

    override fun onDestroy() {
        leave()
        pastBroadcastList!!.clear()
        liveUpcomingBroadcastList = ArrayList()
        if (invalidDialog != null) {
            invalidDialog!!.dismiss()
            invalidDialog = null
        }
        super.onDestroy()
    }

    override fun onPause() {
        super.onPause()
        overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
    }

    override fun showHideDescription() {
        if (expandableLayout!!.isExpanded) {
            expandableLayout!!.collapse()
            val deg = 180f
            imgShow.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
        } else {
            expandableLayout!!.expand()
            val deg = 0f
            imgShow.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
        }
    }

    private fun setEmojisList(emojiCount: Int) {
        listScaryEmojis.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        emojiAdapter = EmojisHomeAdapter(context, emojiCount)
        listScaryEmojis!!.adapter = emojiAdapter
    }

    private fun setCurrentBroadcaster(broadcastData: List<ChannelBroadcastData>?) {
        layoutCurrent.visibility = View.VISIBLE
        lytCurrentBroadcaster.visibility = View.VISIBLE
        Collections.reverse(broadcastData)
        sortedList = broadcastData?.sortedBy { !it.isLive }
        liveUpcomingBroadcastList = sortedList
        (rvLiveBroadcaster.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false

//        val mLayoutManager = LinearLayoutManager(this)
//        mLayoutManager.reverseLayout = true
//        mLayoutManager.stackFromEnd = true
//        rvLiveBroadcaster.layoutManager = mLayoutManager
        adapter = LiveChannelBroadcastAdapter(context, sortedList, true)
        rvLiveBroadcaster!!.adapter = adapter
        rvLiveBroadcaster!!.adapter.notifyDataSetChanged()


        adapter.setOnItemClickListener(object : LiveChannelBroadcastAdapter.OnItemClickListener {
            override fun onUpdateClick(view: View, position: Int) {
                val isFollowed: Boolean = !sortedList!![position].broadcasterData.isFollow
                sortedList!![position].broadcasterData.isFollow = isFollowed

                presenter.followUnFollow(sortedList!![position].broadcasterData.id, isFollowed)
                adapter.notifyItemChanged(position)

                updateFollower(sortedList!![position], isFollowed)
            }

            override fun onItemClick(view: View, position: Int) {
                if (sortedList!![position].isLive) {
                    broadcastId = sortedList!![position].id

                    startActivity(Intent(this@BroadcastDetailActivity, LiveVideoPlayerActivity::class.java)
                            .putExtra("position", position)
                            .putExtra("broadcastId", sortedList!![position].id)
                            .putExtra("streamName", sortedList!![position].broadcastStreamId)
                            .putExtra("userId", intent.getStringExtra("userId"))
                            .putExtra("broadcasterId", sortedList!![position].broadcasterData.id)
                            .putExtra("broadcastStartTime", sortedList!![position].broadcastStartTime)
                            .putExtra("isFromUserProfile", false))
                }
            }
        })
    }

    private fun updateFollower(channelBroadcastData: ChannelBroadcastData, isFollowed: Boolean) {
        for (i in 0 until pastBroadcastList!!.size) {
            if (pastBroadcastList!![i].broadcasterData.id == channelBroadcastData.broadcasterData.id) {
                pastBroadcastList!![i].broadcasterData.isFollow = isFollowed
                pastBroadcastList!![i].broadcasterData.followers = channelBroadcastData.broadcasterData.followers
                rvPastBroadcaster.adapter.notifyItemChanged(i)
            }
        }

        for (i in 0 until liveUpcomingBroadcastList!!.size) {
            if (liveUpcomingBroadcastList!![i].broadcasterData.id == channelBroadcastData.broadcasterData.id) {
                liveUpcomingBroadcastList!![i].broadcasterData.isFollow = isFollowed
                liveUpcomingBroadcastList!![i].broadcasterData.followers = channelBroadcastData.broadcasterData.followers
                rvLiveBroadcaster.adapter.notifyItemChanged(i)
            }
        }
    }

    private fun setPastBroadcaster(broadcastData: ArrayList<ChannelBroadcastData>?) {
//        Log.d("LIVE", "setPast")
        layoutPast.visibility = View.VISIBLE
        lytPastBroadcaster.visibility = View.VISIBLE
        pastBroadcastList = broadcastData
        (rvPastBroadcaster.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        adapter = LiveChannelBroadcastAdapter(context, broadcastData, true)
        rvPastBroadcaster!!.adapter = adapter
        rvPastBroadcaster!!.adapter.notifyDataSetChanged()

        adapter.setOnItemClickListener(object : LiveChannelBroadcastAdapter.OnItemClickListener {
            override fun onUpdateClick(view: View, position: Int) {
                // update is-followed or not in past list
                val isFollowed: Boolean = !broadcastData!![position].broadcasterData.isFollow
                broadcastData[position].broadcasterData.isFollow = isFollowed

                presenter.followUnFollow(broadcastData[position].broadcasterData.id, isFollowed)
                adapter.notifyItemChanged(position)

                updateFollower(broadcastData[position], isFollowed)
            }

            override fun onItemClick(view: View, position: Int) {
                startActivity(Intent(this@BroadcastDetailActivity, PastVideoPlayerActivity::class.java)
                        .putExtra("position", position)
                        .putExtra("broadcastUrl", broadcastData!![position].broadcastUrl)
                        .putExtra("broadcastId", broadcastData[position].id)
                        .putExtra("broadcasterId", broadcastData[position].broadcasterData.id))
            }
        })

    }

    override fun showLoadingProgress() {
        RuthUtility.showLoader(this@BroadcastDetailActivity)
    }

    override fun hideLoadingProgress() {
        RuthUtility.hideLoader()
    }

    private var invalidDialog: AlertDialog? = null
    override fun invalidLogin() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Invalid Login")
        builder.setMessage(getString(R.string.invalid_login_error))

        builder.setPositiveButton(getString(R.string.log_in)) { dialog, _ ->
            presenter.clearLogin()
            startActivity(Intent(this, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            finish()
            overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
            dialog.dismiss()
        }

        if (!isFinishing && window.decorView.isShown) {
            invalidDialog = builder.create()
            invalidDialog!!.setCancelable(false)
            invalidDialog!!.show()
        }
    }

    override fun clearPreferences() {
        startActivity(Intent(this, LoginActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        finish()
    }

    /*---------------------------------------START OF SOCKET--------------------------------------*/
    private var mSocket: Socket? = null
    private var isConnected: Boolean? = true

    private fun setUpSocket() {
        val app = this.application as App
        mSocket = app.socket
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
        mSocket!!.on("RECEIVE_LIVE", onLiveEnded)
        mSocket!!.connect()
    }

    private val onConnect = Emitter.Listener {
        this.runOnUiThread {
            if (!isConnected!!) {
                isConnected = true
            }
        }
    }

    private val onDisconnect = Emitter.Listener {
        this.runOnUiThread {
            Log.i("socket", "disconnected")
            isConnected = false
        }
    }

    private val onConnectError = Emitter.Listener {
        this.runOnUiThread {
            Log.e("socket", "Error connecting")
        }
    }

    private val onLiveEnded = Emitter.Listener { _ ->
        this.runOnUiThread {
            try {
                // val data = args[0] as JSONObject
                android.os.Handler().postDelayed({
                    try {
                        if (intent.getStringExtra("channelId") != null) {
                            if (!isFinishing) {
                                presenter.getChannelDetail(intent.getStringExtra("channelId"))
                            }
                        }
                    } catch (e: JSONException) {
                        Log.e(TAG, e.message)
                        return@postDelayed
                    }
                }, 1000)


            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    private fun leave() {
        mSocket!!.disconnect()
        mSocket!!.connect()
    }

    /*---------------------------------------END OF SOCKET--------------------------------------*/


}