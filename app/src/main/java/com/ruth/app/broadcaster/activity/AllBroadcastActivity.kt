package com.ruth.app.broadcaster.activity

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.SimpleItemAnimator
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.broadcaster.adapter.LiveChannelBroadcastAdapter
import com.ruth.app.broadcaster.presenter.AllBroadcastPresenter
import com.ruth.app.home.model.ChannelBroadcastData
import com.ruth.app.livestreaming.activity.LiveVideoPlayerActivity
import com.ruth.app.livestreaming.activity.PastVideoPlayerActivity
import com.ruth.app.login.activity.LoginActivity
import kotlinx.android.synthetic.main.activity_all_broadcast.*

class AllBroadcastActivity : BaseActivity<AllBroadcastPresenter>(), AllBroadcastPresenter.AllBroadcastView, View.OnClickListener {

    lateinit var context: Context
    lateinit var adapter: LiveChannelBroadcastAdapter
    lateinit var list: ArrayList<ChannelBroadcastData>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
//        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_all_broadcast)
        App.getComponent().inject(this)
        presenter.onCreate(this)
        context = this
        list = ArrayList()
        imgBack.setOnClickListener(this)

        /*layoutSwipe.setOnRefreshListener {
            presenter.getChannelDetail(intent.getStringExtra("channelId"))
        }*/
    }

    override fun onResume() {
        super.onResume()
        overridePendingTransition(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit)
        if (intent.getBooleanExtra("isPastData", false)) {
            setPastBroadcaster(BroadcastDetailActivity.pastBroadcastList)
        } else {
            setCurrentBroadcaster(/*data.broadcastData*/)
        }
    }

    override fun onPause() {
        super.onPause()
        overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
    }

    override fun onClick(v: View?) {
        when (v) {
            imgBack -> finish()
        }
    }

    private var invalidDialog: AlertDialog? = null
    override fun invalidLogin() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Invalid Login")
        builder.setMessage(getString(R.string.invalid_login_error))

        // add a button
        builder.setPositiveButton(getString(R.string.log_in)) { dialog, _ ->
            presenter.clearLogin()
            startActivity(Intent(this, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            finish()
            overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
            dialog.dismiss()
        }

        if (!isFinishing && window.decorView.isShown) {
            invalidDialog = builder.create()
            invalidDialog!!.setCancelable(false)
            invalidDialog!!.show()
        }
    }

    override fun onDestroy() {
        if (invalidDialog != null) {
            invalidDialog!!.dismiss()
            invalidDialog = null
        }
        super.onDestroy()
    }

    override fun clearPreferences() {
        startActivity(Intent(this, LoginActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        finish()
    }

    override fun showLoadingProgress() {
        progressBarBroadcast.visibility = View.VISIBLE
    }

    override fun hideLoadingProgress() {
        progressBarBroadcast.visibility = View.GONE
    }

    private fun setCurrentBroadcaster(/*broadcastData: List<ChannelBroadcastData>?*/) {
        val sortedList = BroadcastDetailActivity.liveUpcomingBroadcastList /*broadcastData?.sortedBy { !it.isLive }*/
        (rvLiveBroadcaster.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        adapter = LiveChannelBroadcastAdapter(context, BroadcastDetailActivity.liveUpcomingBroadcastList, false)
        rvLiveBroadcaster.adapter = adapter
        adapter.setOnItemClickListener(object : LiveChannelBroadcastAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                if (sortedList!![position].isLive) {
                    startActivity(Intent(this@AllBroadcastActivity, LiveVideoPlayerActivity::class.java)
                            .putExtra("position", position)
                            .putExtra("broadcastId", sortedList[position].id)
                            .putExtra("streamName", sortedList[position].broadcastStreamId)
                            .putExtra("userId", intent.getStringExtra("userId"))
                            .putExtra("broadcasterId", sortedList[position].broadcasterData.id)
                            .putExtra("broadcastStartTime", sortedList[position].broadcastStartTime)
                            .putExtra("isFromUserProfile", false))
                }
            }

            override fun onUpdateClick(view: View, position: Int) {
                val isFollowed: Boolean = !BroadcastDetailActivity.liveUpcomingBroadcastList!![position].broadcasterData.isFollow
                BroadcastDetailActivity.liveUpcomingBroadcastList!![position].broadcasterData.isFollow = isFollowed
                presenter.followUnFollow(BroadcastDetailActivity.liveUpcomingBroadcastList!![position].broadcasterData.id, isFollowed)
                adapter.notifyItemChanged(position)

                if (BroadcastDetailActivity.liveUpcomingBroadcastList!!.isNotEmpty()) {
                    for (i in 0 until BroadcastDetailActivity.liveUpcomingBroadcastList!!.size) {
                        if (BroadcastDetailActivity.liveUpcomingBroadcastList!![i].broadcasterData.id == BroadcastDetailActivity.liveUpcomingBroadcastList!![position].broadcasterData.id) {
                            BroadcastDetailActivity.liveUpcomingBroadcastList!![i].broadcasterData.isFollow = isFollowed
                            BroadcastDetailActivity.liveUpcomingBroadcastList!![i].broadcasterData.followers = BroadcastDetailActivity.liveUpcomingBroadcastList!![position].broadcasterData.followers
                            adapter.notifyItemChanged(i)
                        }
                    }
                }

                if (BroadcastDetailActivity.pastBroadcastList!!.isNotEmpty()) {
                    for (i in 0 until BroadcastDetailActivity.pastBroadcastList!!.size) {
                        if (BroadcastDetailActivity.pastBroadcastList!![i].broadcasterData.id == BroadcastDetailActivity.liveUpcomingBroadcastList!![position].broadcasterData.id) {
                            BroadcastDetailActivity.pastBroadcastList!![i].broadcasterData.isFollow = isFollowed
                            BroadcastDetailActivity.pastBroadcastList!![i].broadcasterData.followers = BroadcastDetailActivity.liveUpcomingBroadcastList!![position].broadcasterData.followers
                        }
                    }
                }
            }
        })
    }

    private fun setPastBroadcaster(broadcastData: ArrayList<ChannelBroadcastData>?) {
        (rvLiveBroadcaster.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        adapter = LiveChannelBroadcastAdapter(context, broadcastData, false)
        rvLiveBroadcaster.adapter = adapter
        adapter.setOnItemClickListener(object : LiveChannelBroadcastAdapter.OnItemClickListener {
            override fun onUpdateClick(view: View, position: Int) {
                val isFollowed: Boolean = !BroadcastDetailActivity.pastBroadcastList!![position].broadcasterData.isFollow
                presenter.followUnFollow(BroadcastDetailActivity.pastBroadcastList!![position].broadcasterData.id, isFollowed)
                adapter.notifyItemChanged(position)

                if (BroadcastDetailActivity.pastBroadcastList!!.isNotEmpty()) {
                    for (i in 0 until BroadcastDetailActivity.pastBroadcastList!!.size) {
                        if (BroadcastDetailActivity.pastBroadcastList!![i].broadcasterData.id == BroadcastDetailActivity.pastBroadcastList!![position].broadcasterData.id) {
                            BroadcastDetailActivity.pastBroadcastList!![i].broadcasterData.isFollow = isFollowed
                            BroadcastDetailActivity.pastBroadcastList!![i].broadcasterData.followers = BroadcastDetailActivity.pastBroadcastList!![position].broadcasterData.followers
                            adapter.notifyItemChanged(i)
                        }
                    }
                }

                if (BroadcastDetailActivity.liveUpcomingBroadcastList!!.isNotEmpty()) {
                    for (i in 0 until BroadcastDetailActivity.liveUpcomingBroadcastList!!.size) {
                        if (BroadcastDetailActivity.liveUpcomingBroadcastList!![i].broadcasterData.id == BroadcastDetailActivity.pastBroadcastList!![position].broadcasterData.id) {
                            BroadcastDetailActivity.liveUpcomingBroadcastList!![i].broadcasterData.isFollow = isFollowed
                            BroadcastDetailActivity.liveUpcomingBroadcastList!![i].broadcasterData.followers = BroadcastDetailActivity.pastBroadcastList!![position].broadcasterData.followers
                            // adapter.notifyItemChanged(i)
                        }
                    }
                }
            }

            override fun onItemClick(view: View, position: Int) {
                startActivity(Intent(this@AllBroadcastActivity, PastVideoPlayerActivity::class.java)
                        .putExtra("position", position)
                        .putExtra("broadcastId", broadcastData!![position].id)
                        .putExtra("broadcastUrl", broadcastData[position].broadcastUrl)
                        .putExtra("broadcasterId", broadcastData[position].broadcasterData.id))
            }
        })
    }

}
