package com.ruth.app.broadcaster.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.ruth.app.R
import com.ruth.app.home.model.ChannelBroadcastData
import com.ruth.app.otheruser.OtherUserProfileActivity
import kotlinx.android.synthetic.main.item_broadcaster.view.*

class LiveChannelBroadcastAdapter(val context: Context,
                                  private val broadcastData: List<ChannelBroadcastData>?,
                                  private val isFromDetail: Boolean) : RecyclerView.Adapter<LiveChannelBroadcastAdapter.LiveBroadcasterViewHolder>() {

    private var onItemClickListener: OnItemClickListener? = null
    private lateinit var recyclerView: RecyclerView

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LiveBroadcasterViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.item_broadcaster, parent, false)
        return LiveBroadcasterViewHolder(view)
    }

    override fun getItemCount(): Int {
        if (isFromDetail) {
            return if (broadcastData?.size!! > 4) {
                4
            } else {
                broadcastData.size
            }
        }
        return broadcastData?.size!!
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: LiveBroadcasterViewHolder, position: Int) {
        holder.itemView.lytBroadCast.viewTreeObserver.addOnGlobalLayoutListener {

            val displayMetrics = DisplayMetrics()
            (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
            val deviceWidth = displayMetrics.widthPixels / 2
            holder.itemView.layoutParams.width = deviceWidth
            holder.itemView.layoutParams.height = deviceWidth

            holder.itemView.ivBroadcastCoverImage.layoutParams.width = holder.itemView.layoutParams.width
            holder.itemView.ivBroadcastCoverImage.layoutParams.height = holder.itemView.layoutParams.height
        }

        holder.bind(broadcastData!![position])

        holder.itemView.lytBroadCast.setOnClickListener {
            onItemClickListener!!.onItemClick(holder.itemView, position)
        }

        holder.itemView.ivFollowUser.setOnClickListener {
            var followerCount = broadcastData[position].broadcasterData.followers

            if (broadcastData[position].broadcasterData.isFollow) {
                followerCount -= 1
                holder.itemView.ivFollowUser.setImageResource(R.drawable.icon_follow_button)
            } else {
                followerCount += 1
                holder.itemView.ivFollowUser.setImageResource(R.drawable.icon_follow)
            }
            broadcastData[position].broadcasterData.followers = followerCount
            holder.itemView.tvFollowersCount.text = broadcastData[position].broadcasterData.followers.toString() + " 名粉絲"
            onItemClickListener!!.onUpdateClick(holder.itemView.ivFollowUser, position)
        }
        holder.itemView.layoutBroadcaster.setOnClickListener {
            context.startActivity(Intent(context, OtherUserProfileActivity::class.java)
                    .putExtra("otherUserId", broadcastData[position].broadcasterId))
        }
    }

    inner class LiveBroadcasterViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bind(channelBroadcastData: ChannelBroadcastData) {

            Glide.with(context).load(channelBroadcastData.coverUrl)
                    .apply(RequestOptions().error(R.drawable.background)
                            .placeholder(R.drawable.background))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            itemView.progress.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            itemView.progress.visibility = View.GONE
                            return false
                        }

                    }).into(itemView.ivBroadcastCoverImage)

            Glide.with(context).load(channelBroadcastData.broadcasterData.photoUrl)
                    .apply(RequestOptions().error(R.drawable.com_facebook_profile_picture_blank_square)
                            .placeholder(R.drawable.com_facebook_profile_picture_blank_square)
                            .diskCacheStrategy(DiskCacheStrategy.NONE))
                    .into(itemView.profile_image)

            if (channelBroadcastData.broadcasterData.name.isNotEmpty()) {
                itemView.tvBroadcasterName.text = channelBroadcastData.broadcasterData.name
            } else {
                itemView.tvBroadcasterName.text = channelBroadcastData.broadcasterData.userName
            }
            itemView.tvFollowersCount.text = channelBroadcastData.broadcasterData.followers.toString() + " 名粉絲"
            itemView.tvBroadcastViewCount.text = channelBroadcastData.broadcastViews.toString()
            itemView.tvFavoriteViewCount.text = channelBroadcastData.broadcastLikes.toString()
            itemView.lbl_scary_count.text = channelBroadcastData.scaryCount.toString()

            if (channelBroadcastData.broadcasterData.isFollow) {
                itemView.ivFollowUser.setImageResource(R.drawable.icon_follow)
            } else {
                itemView.ivFollowUser.setImageResource(R.drawable.icon_follow_button)
            }

            if (channelBroadcastData.isLive) {
                itemView.shadow_view.visibility = View.VISIBLE
                itemView.shadow_view.shadowColor = ContextCompat.getColor(context, R.color.red)
                itemView.shadow_view.backgroundClr = ContextCompat.getColor(context, R.color.red)
            } else {
                itemView.shadow_view.visibility = View.VISIBLE
            }
        }
    }

    override fun getItemId(position: Int): Long {
        return broadcastData!![position].id.toLong()
    }


    fun setOnItemClickListener(onItemClick: OnItemClickListener) {
        this.onItemClickListener = onItemClick
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
        fun onUpdateClick(view: View, position: Int)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}

