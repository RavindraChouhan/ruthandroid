package com.ruth.app.base.presenter

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.EditText
import com.ruth.app.network.config.AppPreferences
import com.ruth.app.utility.Utility
import javax.inject.Inject


open class BasePresenter<T : BasePresenter.BaseView> @Inject constructor() {
    protected var view: T? = null
    protected val TAG = this.javaClass.simpleName

    @Inject
    protected lateinit var preferences: AppPreferences

    @Inject
    protected lateinit var utility: Utility

//  @Inject
//  protected lateinit var freebirdApi: FreebirdApi

    @Inject
    lateinit var context: Context


    fun getString(id: Int) = context.getString(id)

    open fun onCreate(view: T) {
        this.view = view
    }

    open fun onDestroy() {
        view = null
    }


    fun logError(err: Throwable) {
        logError(err.message)
    }

    fun logError(message: String?) {
        Log.e(TAG, message)
    }

    interface BaseView {
        fun hideKeyboard()
        fun showKeyboard(view : EditText)
        fun showProgress()
        fun showProgress(message: String, title: String)
        fun hideProgress()
        fun showToast(message: String)
        fun showToast(resId: Int)
        fun isAlive(): Boolean
        fun alert(message: String, title: String,
                  positiveFunc: () -> Unit, negativeFunc: () -> Unit)

        fun alert(message: String, title: String,
                  positiveButtonText: String, negativeButtonText: String,
                  positiveFunc: () -> Unit, negativeFunc: () -> Unit)
    }
}