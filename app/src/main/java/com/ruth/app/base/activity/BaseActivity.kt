package com.ruth.app.base.activity

import android.app.Dialog
import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.provider.Settings
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import com.ruth.app.R
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.network.config.AppPreferences
import com.ruth.app.utility.AwsUtility
import com.ruth.app.utility.RuthUtility
import com.ruth.app.utility.Utility
import kotlinx.android.synthetic.main.activity_invite_phone_friends.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import javax.inject.Inject


open class BaseActivity<T : BasePresenter<*>> : AppCompatActivity(), BasePresenter.BaseView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        RuthUtility.checkContinousInternet(this)
        RuthUtility.scheduleJob(this)
    }


    @Inject
    protected lateinit var presenter: T

    private var alive = false
    private var dialog: ProgressDialog? = null
    private var toast: Toast? = null
    protected val TAG: String? = javaClass.simpleName

    @Inject
    protected lateinit var utility: Utility

    @Inject
    protected lateinit var awsUtility: AwsUtility

    @Inject
    protected lateinit var preferences: AppPreferences

    override fun showProgress() {
        showProgress("Loading", "Please wait")
    }

    override fun showProgress(message: String, title: String) {
        dialog = indeterminateProgressDialog(message, title)
        dialog?.setCancelable(false)
    }

    override fun hideProgress() {
        dialog?.let { if (!it.isShowing) return else it.hide() }
    }

    override fun onDestroy() {
        super.onDestroy()
        alive = false
        dialog?.dismiss()
    }

    override fun onStart() {
        super.onStart()
        alive = true
    }

    fun logError(err: Throwable) {
        logError(err.message)
    }

    fun logError(message: String?) {
        Log.e(TAG, message)
    }

    override fun showToast(message: String) {
        if (toast == null) {
            toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
        }
        toast?.let {
            it.setText(message)
            it.show()
        }
    }

    override fun showToast(resId: Int) {
        showToast(getString(resId))
    }

    override fun alert(message: String, title: String,
                       positiveButtonText: String, negativeButtonText: String,
                       positiveFunc: () -> Unit, negativeFunc: () -> Unit) {
        alert(message, title) {
            positiveButton(positiveButtonText) { positiveFunc() }
            negativeButton(negativeButtonText) { negativeFunc() }
        }.show()
    }

    override fun alert(message: String, title: String,
                       positiveFunc: () -> Unit, negativeFunc: () -> Unit) {
        alert(message, title, "Yes", "No", positiveFunc, negativeFunc)
    }

    override fun isAlive() = alive

    fun getTag() = TAG

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this).registerReceiver(internetConnection, IntentFilter("internetConnection"))

    }

    var internetConnection = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (!intent!!.getBooleanExtra("isConnected", false)) {
                showNoInternetDialog(this@BaseActivity)
            } else {
                hideNoInternetDialog()
            }

//            Toast.makeText(this@BaseActivity, "" + intent.getBooleanExtra("isConnected", false),
//                    Toast.LENGTH_SHORT).show()
        }

    }

    private var internetDialog: Dialog? = null

    private fun showNoInternetDialog(mContext: Context) {
        internetDialog = Dialog(mContext)
        internetDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        internetDialog!!.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        internetDialog!!.setContentView(R.layout.popup_no_internet)
        internetDialog!!.setCancelable(false)

//        internetDialog!!.findViewById<View>(R.id.lblTurnOn).setOnClickListener {
//            internetDialog!!.dismiss()
//            val intent = Intent(Settings.ACTION_WIRELESS_SETTINGS)
//            startActivity(intent)
//        }

        if (!isFinishing) {
            internetDialog!!.show()
        }
    }

    private fun hideNoInternetDialog() {
        if (internetDialog != null) {
            internetDialog!!.cancel()
            internetDialog!!.dismiss()
        }
    }

    override fun hideKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    }

    override fun showKeyboard(view: EditText) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(etSearch, InputMethodManager.SHOW_IMPLICIT)
    }

    open fun userVerified(phone: String) {}
}