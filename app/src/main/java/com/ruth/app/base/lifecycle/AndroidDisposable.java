package com.ruth.app.base.lifecycle;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;

import io.reactivex.disposables.Disposable;

/**
 * Обертка для {@link Disposable}, автоматически отписывающаяся в onDestroy() или другой заданный
 * коллбек жизненного цикла соотествующих активити и фрагментов.
 * При попытке подписаться в уничтоженном фрагменте отпишется сразу в момент вызова
 * метода {@link #create(LifecycleOwner, Lifecycle.Event, Disposable)}
 *
 * @author Andrey Makeev
 */

public class AndroidDisposable implements LifecycleObserver, Disposable {
    private Disposable disposable;
    private Lifecycle.Event disposeEvent;

    // Private constructor, please use factory method
    private AndroidDisposable() {
    }

    /**
     * Метод для создания автоотписывающегося {@link Disposable} (отписывается в onDestroy)
     *
     * @param owner      {@link LifecycleOwner}, к жизненному циклу которого привязывается {@link Disposable}
     * @param disposable {@link Disposable}, от которого нужно отписываться при закрытии экрана
     * @return ссылка на объект {@link AndroidDisposable}
     */
    public static AndroidDisposable create(LifecycleOwner owner, Disposable disposable) {
        return create(owner, Lifecycle.Event.ON_DESTROY, disposable);
    }

    /**
     * Метод для создания автоотписывающегося {@link Disposable} (отписывается при заданном событии)
     *
     * @param owner        {@link LifecycleOwner}, к жизненному циклу которого привязывается {@link Disposable}
     * @param disposable   {@link Disposable}, от которого нужно отписываться при закрытии экрана
     * @param disposeEvent {@link Lifecycle.Event} при котором нужно отписаться
     * @return ссылка на объект {@link AndroidDisposable}
     */
    public static AndroidDisposable create(LifecycleOwner owner, Lifecycle.Event disposeEvent, Disposable disposable) {
        AndroidDisposable androidDisposable = new AndroidDisposable();
        androidDisposable.disposable = disposable;
        androidDisposable.disposeEvent = disposeEvent;

        boolean destroyed = !owner.getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.INITIALIZED);
        if (destroyed) {
            androidDisposable.dispose(); // If fragment or activity is already destroyed, dispose immediately
        } else {
            owner.getLifecycle().addObserver(androidDisposable); // otherwise - subscribe to lifecycle events
        }
        return androidDisposable;
    }

    /*
     * Automatic dispose implementation
     */
    @SuppressWarnings({"WeakerAccess", "unused"})
    @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
    void onStateChange(LifecycleOwner owner, Lifecycle.Event event) {
        // Dispose if event == specified event or if it is a DESTROYED event
        if (event == disposeEvent || owner.getLifecycle().getCurrentState() == Lifecycle.State.DESTROYED) {
            dispose();
            owner.getLifecycle().removeObserver(this);
        }
    }

    /**
     * {@link Disposable#dispose()} implementation
     */
    @Override
    public void dispose() {
        if (!isDisposed()) {
            disposable.dispose();
            disposable = null;
        }
    }

    /**
     * {@link Disposable#isDisposed()} implementation
     *
     * @return subscription status
     */
    @Override
    public boolean isDisposed() {
        return disposable == null || disposable.isDisposed();
    }
}
