package com.ruth.app.base.fragment

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import com.ruth.app.base.presenter.BasePresenter
import com.ruth.app.network.config.AppPreferences
import com.ruth.app.utility.AwsUtility
import com.ruth.app.utility.RuthUtility
import com.ruth.app.utility.Utility
import org.jetbrains.anko.alert
import org.jetbrains.anko.support.v4.indeterminateProgressDialog
import javax.inject.Inject

open class BaseFragment<T : Any> : Fragment(), BasePresenter.BaseView {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
     //   RuthUtility.checkContinousInternet(activity!!)
    }

    @Inject
    protected lateinit var presenter: T

    private var alive = false
    private var dialog: ProgressDialog? = null
    private var toast: Toast? = null
    protected val TAG: String? = javaClass.simpleName

    @Inject
    protected lateinit var utility: Utility

    @Inject
    protected lateinit var awsUtility: AwsUtility

    @Inject
    protected lateinit var preferences: AppPreferences

    override fun showProgress() {
        showProgress("Loading", "Please wait")
    }

    override fun showProgress(message: String, title: String) {
        dialog = indeterminateProgressDialog(message, title)
    }

    override fun hideProgress() {
        dialog?.let { if (!it.isShowing) return else it.hide() }
    }

    override fun onDestroy() {
        super.onDestroy()
        alive = false
        dialog?.dismiss()
    }

    override fun onStart() {
        super.onStart()
        alive = true
    }

    fun logError(err: Throwable) {
        logError(err.message)
    }

    fun logError(message: String?) {
        Log.e(TAG, message)
    }

    override fun showToast(message: String) {
        if (toast == null) {
            toast = Toast.makeText(context, message, Toast.LENGTH_SHORT)
        }
        toast?.let {
            it.setText(message)
            it.show()
        }
    }

    override fun showToast(resId: Int) {
        showToast(getString(resId))
    }

    override fun alert(message: String, title: String,
                       positiveButtonText: String, negativeButtonText: String,
                       positiveFunc: () -> Unit, negativeFunc: () -> Unit) {
        context?.alert(message, title) {
            positiveButton(positiveButtonText) { positiveFunc() }
            negativeButton(negativeButtonText) { negativeFunc() }
        }?.show()
    }

    override fun alert(message: String, title: String,
                       positiveFunc: () -> Unit, negativeFunc: () -> Unit) {
        alert(message, title, "Yes", "No", positiveFunc, negativeFunc)
    }

    override fun hideKeyboard() {
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(activity?.currentFocus!!.windowToken, 0)
    }

    override fun showKeyboard(view: EditText) {
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }

    override fun isAlive() = alive

}

