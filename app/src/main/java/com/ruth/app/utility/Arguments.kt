package com.ruth.app.utility

import android.os.Bundle
import android.support.v4.app.Fragment

/**
 * Start an Activity for given class T and allow to work on intent with "run" lambda function
 */
inline fun <reified T : Fragment> T.withArguments(bundle: Bundle): T {
    this.arguments = bundle
    return this
}

/**
 * Retrieve property from intent
 */
fun <T : Any> Fragment.argument(key: String) = lazy { arguments?.get(key) as? T }

/**
 * Retrieve property with default value from intent
 */
fun <T : Any> Fragment.argument(key: String, defaultValue: T) = lazy {
    arguments?.get(key)  as? T ?: defaultValue
}
