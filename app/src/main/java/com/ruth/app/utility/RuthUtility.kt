package com.ruth.app.utility

import android.app.Activity
import android.app.Dialog
import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.os.Handler
import android.support.annotation.RequiresApi
import android.util.Log
import android.view.Window
import android.widget.Toast
import com.ruth.app.R
import com.ruth.app.home.activity.HomeActivity
import com.ruth.app.model.PointsEarned
import com.ruth.app.network.internet.TestJobService
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


object RuthUtility {

    fun getCounts(count: Int): String {
        val totalCount: String
        if (count > 1000) {
            totalCount = (count / 1000).toString() + "k"
        } else {
            totalCount = "" + count
        }
        //   Log.d("total", "->$totalCount")
        return totalCount
    }

    fun convertToChinese(value: Int): String {
        return if (value > 9) {
            (value.toString() + "").replace("10".toRegex(), "十")
                    .replace("100".toRegex(), "百").replace("1000".toRegex(), "千")
        } else {
            (value.toString() + "")
                    .replace("1".toRegex(), "一").replace("2".toRegex(), "二")
                    .replace("3".toRegex(), "三").replace("4".toRegex(), "四")
                    .replace("5".toRegex(), "五").replace("6".toRegex(), "六")
                    .replace("7".toRegex(), "七").replace("8".toRegex(), "八")
                    .replace("9".toRegex(), "九").replace("0".toRegex(), "〇")

        }
    }

    /*--------------------------------------------------------------------------------------------*/
    private fun getDate(OurDate: String): String {
        var date = OurDate
        try {
            val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            df.timeZone = TimeZone.getTimeZone("UTC")
            val value = df.parse(OurDate)

            val dateFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss") //this format changeable
            dateFormatter.timeZone = TimeZone.getDefault()
            date = dateFormatter.format(value)
        } catch (e: Exception) {
            e.printStackTrace()
            date = "00-00-0000 00:00"
        }

        return date
    }

    fun getTimeAgo(timeString1: String): String? {

        val timeDate = getDate(timeString1)
        val datetime = timeDate.split("T")
        val date: Array<String>
        val time: Array<String>
        val am_pm: String
        val rightNow = Calendar.getInstance()
        if (datetime.size == 2) {
            date = datetime[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            time = datetime[1].split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val year = Integer.parseInt(date[0])
            if (rightNow.get(Calendar.YEAR) > year)
                return if (rightNow.get(Calendar.YEAR) - year === 1) {
                    (rightNow.get(Calendar.YEAR) - year).toString() + " year ago"
                } else {
                    (rightNow.get(Calendar.YEAR) - year).toString() + " years ago"
                }
            val month = Integer.parseInt(date[1])
            if (rightNow.get(Calendar.MONTH) + 1 > month)
                return if (rightNow.get(Calendar.MONTH) - month === 1) {
                    (rightNow.get(Calendar.MONTH) + 1 - month).toString() + " month ago"
                } else {
                    (rightNow.get(Calendar.MONTH) + 1 - month).toString() + " months ago"
                }
            val day = Integer.parseInt(date[2])
            if (rightNow.get(Calendar.DAY_OF_MONTH) > day)
                return if (rightNow.get(Calendar.DAY_OF_MONTH) - day === 1) {
                    (rightNow.get(Calendar.DAY_OF_MONTH) - day).toString() + " day ago"
                } else {
                    (rightNow.get(Calendar.DAY_OF_MONTH) - day).toString() + " days ago"
                }
            val hours = Integer.parseInt(time[0])
            if (rightNow.get(Calendar.HOUR_OF_DAY) > hours)
                return if (rightNow.get(Calendar.HOUR_OF_DAY) - hours === 1) {
                    (rightNow.get(Calendar.HOUR_OF_DAY) - hours).toString() + " hour ago"
                } else {
                    (rightNow.get(Calendar.HOUR_OF_DAY) - hours).toString() + " hours ago"
                }
            val minuts = Integer.parseInt(time[1])
            if (rightNow.get(Calendar.MINUTE) > minuts)
                return (rightNow.get(Calendar.MINUTE) - minuts).toString() + " minute ago"
            val second = Integer.parseInt(time[2])
            if (rightNow.get(Calendar.SECOND) > second)
                return (rightNow.get(Calendar.SECOND) - second).toString() + " second ago"
        } else {
            println("got null")
            return null
        }
        return null
    }

    fun printDifference(startDate: String, endDate: String, format: String): String {
        val format = SimpleDateFormat(format)

        val d1: Date
        val d2: Date
        var diffHours: Long = 0
        var diffSeconds: Long = 0
        var diffMinutes: Long = 0

        try {
            d1 = format.parse(startDate)
            d2 = format.parse(endDate)

            //in milliseconds
            val diff = d2.time - d1.time

            diffSeconds = diff / 1000 % 60
            diffMinutes = diff / (60 * 1000) % 60
            diffHours = diff / (60 * 60 * 1000) % 24

        } catch (e: Exception) {
            e.printStackTrace()
        }
        Log.e("time difference", "" + (diffHours.toString() + ":" + diffMinutes.toString() + ":" + diffSeconds.toString()))
        return twoDigitString(diffHours.toInt()) + ":" + twoDigitString(diffMinutes.toInt()) + ":" + twoDigitString(diffSeconds.toInt())
    }

    fun printTimeDifference(startDate: String, endDate: String, format: String): String {
        val formatDate = SimpleDateFormat(format)

        val d1: Date
        val d2: Date
        var diffHours: Long = 0
        var diffSeconds: Long = 0
        var diffMinutes: Long = 0

        try {
            d1 = formatDate.parse(startDate)
            d2 = formatDate.parse(endDate)

            //in milliseconds
            val diff = d2.time - d1.time

            diffSeconds = diff / 1000 % 60
            diffMinutes = diff / (60 * 1000) % 60
            diffHours = diff / (60 * 60 * 1000) % 24

        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (diffHours > 0) {
            return ""
        }
        return if (diffMinutes > 0) {
            twoDigitString(diffMinutes.toInt()) + ":" + twoDigitString(diffSeconds.toInt()) + "s"
        } else {
            "00 : " + twoDigitString(diffSeconds.toInt()) + "s"
        }
    }

    private fun twoDigitString(number: Int): String {
        if (number == 0) {
            return "00"
        }
        if (number / 10 == 0) {
            return "0$number"
        }
        return (number).toString()
    }

    fun dateToString(date: Date): String {
        var dateTime: String = ""
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        try {
            val date = Date()
            dateTime = dateFormat.format(date)
            println("Current Date Time : $dateTime")
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return dateTime
    }

    fun convertSecondsToHMmSs(seconds: Long): String {
        if (seconds != null) {
            val ms = seconds / 1000
            val s = ms % 60
            val m = ms / 60 % 60
            val h = ms / (60 * 60) % 24
            return if (h != 0L) {
                String.format("%02d:%02d:%02d", h, m, s)
            } else {
                String.format("%02d:%02d", m, s)
            }
        } else {
            return ""
        }
    }

    /*--------------------------------------------------------------------------------------------*/
    fun Dp2px(mContext: Context, dp: Float): Int {
        val scale = mContext.resources.displayMetrics.density
        return (dp * scale + 1f).toInt()
    }

    /*-----------------------------------INTERNET CONNECTION--------------------------------------*/
    @JvmStatic
    fun checkInternetWithoutDialog(mContext: Context): Boolean {
        val connectivity = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivity != null) {
            val info = connectivity.allNetworkInfo
            if (info != null)
                for (i in info.indices)
                    if (info[i].state == NetworkInfo.State.CONNECTED) {
                        return true
                    }
        }
        return false
    }

//    @JvmStatic
//    fun checkInternet(mContext: Context) {
//        Thread(Runnable {
//            Runtime.getRuntime().gc()
//            val internetConnection = RuthUtility.checkInternetWithoutDialog(mContext)
//            if (!internetConnection) {
//                Log.d("internet", "connection lost")
//            } else {
//                Log.d("internet", "connected")
//            }
//            checkInternet(mContext)
//        }).start()
//    }

    fun checkContinousInternet(activity: Activity): Boolean {
        var internetConnection = false
        activity.runOnUiThread {
            try {
                Handler().postDelayed({
                    try {
                        Runtime.getRuntime().gc()
                        internetConnection = RuthUtility.checkInternetWithoutDialog(activity)
                        if (!internetConnection) {
                            Toast.makeText(activity, activity.getString(R.string.no_internet_connection_error), Toast.LENGTH_SHORT).show()
                            Log.d("internet", "connection lost")
                        }
                        checkContinousInternet(activity)
                    } catch (ex: Exception) {
                        Log.e("internet", "exception ex - " + ex.message)
                        checkContinousInternet(activity)
                        return@postDelayed
                    }
                }, 5000)
            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("internet", "exception e - " + e.message)
                checkContinousInternet(activity)
            }
        }
        return internetConnection
    }

    /*--------------------------------------------------------------------------------------------*/

    fun updatePoints(context: Context, pointEarn: List<PointsEarned>?) {
        HomeActivity.goldPoints = 0
        HomeActivity.silverPoints = 0
        HomeActivity.yuvanPoints = 0
        if (pointEarn!!.isNotEmpty()) {
            for (i in 0 until pointEarn.size) {
                if (pointEarn[i].id.equals(context.getString(R.string.gold_points), true)) {
                    HomeActivity.goldPoints = pointEarn[i].total
                }

                if (pointEarn[i].id.equals(context.getString(R.string.silver_points), true)) {
                    HomeActivity.silverPoints = pointEarn[i].total
                }

                if (pointEarn[i].id.equals(context.getString(R.string.coin_points), true)) {
                    HomeActivity.yuvanPoints = pointEarn[i].total
                }
            }

            Log.e("points", "Update -> Gold ->" + HomeActivity.goldPoints +
                    " silver -> " + HomeActivity.silverPoints + " coins -> " + HomeActivity.yuvanPoints)
        }
    }

    fun getTimeLimit(currentTime: String, orderCompletionTime: Int): String {
        val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val date = formatter.parse(currentTime)
        val calendar = Calendar.getInstance()
        calendar.time = date
        calendar.add(Calendar.MINUTE, orderCompletionTime)
        return SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(calendar.time)
    }

    /*--------------------------------------------------------------------------------------------*/
    private lateinit var loaderDialog: Dialog
    private var internetDialog: Dialog? = null

    fun showLoader(activityContext: Context) {
        try {
            // Dialog instance for loader;
            loaderDialog = Dialog(activityContext)
            loaderDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            loaderDialog.setCancelable(false)
            loaderDialog.setCanceledOnTouchOutside(false)
            loaderDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            loaderDialog.setContentView(R.layout.custom_dialog)
            loaderDialog.show()
            Log.d("progress", "show progress")
        } catch (e: Throwable) {
            e.printStackTrace()
        }
    }

    fun hideLoader() {
        if (loaderDialog != null) {
            if (loaderDialog.isShowing) {
                Log.d("progress", "hide progress")
                loaderDialog.cancel()
                loaderDialog.dismiss()
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @JvmStatic
    fun scheduleJob(context: Context) {
        val myJob = JobInfo.Builder(0, ComponentName(context, TestJobService::class.java))
                .setRequiresCharging(true)
                .setMinimumLatency(1000)
                .setOverrideDeadline(2000)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(true)
                .build()

        val jobScheduler = context.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        jobScheduler.schedule(myJob)

    }

    fun internetDialog(mContext: Context){
        if (internetDialog == null) {
            showNoInternetDialog(mContext)
        }else{
            hideInternetDialog(mContext)
        }
    }

    private fun showNoInternetDialog(mContext: Context) {
        internetDialog = Dialog(mContext)
        internetDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        internetDialog!!.window.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        internetDialog!!.setContentView(R.layout.popup_no_internet)
        internetDialog!!.setCancelable(false)
        internetDialog!!.show()

    }

    private fun hideInternetDialog(mContext: Context) {
        if (internetDialog != null) {
            if (internetDialog!!.isShowing) {
                Log.d("progress", "hide progress")
                internetDialog!!.cancel()
                internetDialog!!.dismiss()
            }
        }

    }


/*0	零	Líng
1	一	Yī
2	二	Èr
3	三	Sān
4	四	Sì
5	五	Wǔ
6	六	Liù
7	七	Qī
8	八	Bā
9	九	Jiǔ
10	十	Shí
11	十一	Shí yī
12	十二	Shí èr
13	十三	Shí sān
14	十四	Shí sì
15	十五	Shí wǔ
16	十六	Shí liù
17	十七	Shí qī
18	十八	Shí bā
19	十九	Shí jiǔ
20	二十	Èr shí
21	二十一	Èr shí yī
22	二十二	Èr shí èr
23	二十三	Èr shí sān
24	二十四	Èr shí sì
25	二十五	Èr shí wǔ
26	二十六	Èr shí liù
27	二十七	Èr shí qī
28	二十八	Èr shí bā
29	二十九	Èr shí jiǔ
30	三十	Sān shí
31	三十一	Sān shí yī*/


}