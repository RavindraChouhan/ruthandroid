package com.ruth.app.utility

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.content.ContentUris
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.*
import android.media.ExifInterface
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.telephony.TelephonyManager
import com.ruth.app.R
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import javax.inject.Inject

class Utility @Inject constructor() {

    private val PREFERENCES = "Ruth"

    /*--------------------------------------------------------------------------------------------*/
    fun setStringPreferences(context: Context, key: String, value: String?) {
        val setting = context.getSharedPreferences(PREFERENCES, 0) as SharedPreferences
        val editor = setting.edit()
        editor.putString(key, value)
        editor.commit()
    }

    fun setBooleanPreferences(context: Context, key: String, value: Boolean?) {
        val setting = context.getSharedPreferences(PREFERENCES, 0) as SharedPreferences
        val editor = setting.edit()
        editor.putBoolean(key, value!!)
        editor.commit()
    }

    fun getBooleaPreferences(context: Context, key: String): Boolean {
        val setting = context.getSharedPreferences(PREFERENCES, 0) as SharedPreferences
        return setting.getBoolean(key, false)
    }

    fun getStringPreferences(context: Context, key: String): String {
        val setting = context.getSharedPreferences(PREFERENCES, 0) as SharedPreferences
        return setting.getString(key, "")
    }

    fun getDoublePreferences(context: Context, key: String): Float {
        val setting = context.getSharedPreferences(PREFERENCES, 0) as SharedPreferences
        return setting.getFloat(key, 0f)

    }

    fun setDoublePreferences(context: Context, key: String, value: Double) {
        val setting = context.getSharedPreferences(PREFERENCES, 0) as SharedPreferences
        val editor = setting.edit()
        editor.putFloat(key, value.toFloat())
        editor.commit()
    }

    /*--------------------------------------------------------------------------------------------*/
    fun GetCountryZipCode(mContext: Context): String {
        val CountryID: String
        var CountryZipCode = ""
        val manager = mContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        //getNetworkCountryIso
        CountryID = manager.simCountryIso.toUpperCase()
        if (CountryID.isEmpty()) {
            val locale = mContext.resources.configuration.locale.isO3Country
            CountryZipCode = locale
            return CountryZipCode

        }
        val rl = mContext.resources.getStringArray(R.array.CountryCodes)
        for (i in rl.indices) {
            val g = rl[i].split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (g[1].trim { it <= ' ' } == CountryID.trim { it <= ' ' }) {
                CountryZipCode = g[0]
                break
            }
        }
        return CountryZipCode
    }


    fun isEmailValid(email: String): Boolean {
        return Pattern.compile(
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

    /*Time conversion*/
    fun convertTime12(time: String): Date? {
        val sdf = SimpleDateFormat("H:mm")
        val dateObj = sdf.parse(time)
        System.out.println(dateObj)
        System.out.println(SimpleDateFormat("K:mm").format(dateObj))
        return dateObj
    }

    fun convertTime(time: Int): Int {
        var timeDate = time / 100
        timeDate %= 12
        return timeDate
    }

    fun findAmOrPm(time: Int): String {
        return if (time >= 1200) {
            "pm"
        } else {
            "am"
        }
    }

    fun validUserName(password: String): Boolean {
        var validUserName: Boolean = true
        var counter = 0
        for (i in 0 until password.length) {
            if (Character.isLetter(password[i]))
                counter++
        }
        if (counter < 3) {
            validUserName = false
        }
        return validUserName
    }

    fun monthNameToInt(monthName: String): Int {
        val date = SimpleDateFormat("MMMM", Locale.ENGLISH).parse(monthName)
        val cal = Calendar.getInstance()
        cal.time = date
        val monthNumber = cal.get(Calendar.MONTH) + 1
        return monthNumber
    }

    /*---------------------------------PERMISSION-------------------------------------------------*/

    private val PERMISSION_CALLBACK_CONSTANT = 100

    private var permissionsForVideoRequired = arrayOf(Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE)

    fun permissionForStorageAndCamera(context: Context): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(context as Activity, permissionsForVideoRequired, PERMISSION_CALLBACK_CONSTANT)
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }

    /*-------------------------------------FILE STORAGE-------------------------------------------*/
    val SHARED_PROVIDER_AUTHORITY = "com.ruth.app" + ".provider"

    fun getOutputMediaFileUri(mediaTypeImage: Int, mContext: Context): Uri {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            FileProvider.getUriForFile(mContext, SHARED_PROVIDER_AUTHORITY, createFile())
        } else {
            Uri.fromFile(getOutputMediaFile(mediaTypeImage))
        }
    }

    private fun createFile(): File {
        val mediaStorageDir = File(Environment.getExternalStorageDirectory().toString() + File.separator + "/Ruth/media/cover")

        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs()
        }

        var sharedFile: File? = null
        try {
            sharedFile = File.createTempFile("IMG_AN_", ".jpg", mediaStorageDir)
            sharedFile!!.createNewFile()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return sharedFile!!
    }

    fun getOutputMediaFile(type: Int): File? {
        val mediaStorageDir = File(Environment.getExternalStorageDirectory().toString() + File.separator + "/Ruth/Media/cover")
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs()
        }
        val mediaFile: File
        if (type == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE) {
            mediaFile = File(Environment.getExternalStorageDirectory().toString() + File.separator + "/Ruth/Media/cover/" + "IMG_AN_" + System.currentTimeMillis() + ".jpg")
        } else {
            return null
        }
        return mediaFile
    }

    fun compressImage(filePath: String): String {
        try {
            var scaledBitmap: Bitmap? = null
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = true
            var bmp = BitmapFactory.decodeFile(filePath, options)
            var actualHeight = options.outHeight
            var actualWidth = options.outWidth
            val maxHeight = 1280.0f
            val maxWidth = 720.0f
            var imgRatio = (actualWidth / actualHeight).toFloat()
            val maxRatio = maxWidth / maxHeight
            if (actualHeight > maxHeight || actualWidth > maxWidth) {
                if (imgRatio < maxRatio) {
                    imgRatio = maxHeight / actualHeight
                    actualWidth = (imgRatio * actualWidth).toInt()
                    actualHeight = maxHeight.toInt()
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth
                    actualHeight = (imgRatio * actualHeight).toInt()
                    actualWidth = maxWidth.toInt()
                } else {
                    actualHeight = maxHeight.toInt()
                    actualWidth = maxWidth.toInt()
                }
            }
            options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight)
            options.inJustDecodeBounds = false
            options.inPurgeable = true
            options.inInputShareable = true
            options.inTempStorage = ByteArray(16 * 1024)
            try {
                bmp = BitmapFactory.decodeFile(filePath, options)
            } catch (exception: OutOfMemoryError) {
                exception.printStackTrace()
            }

            try {
                scaledBitmap = Bitmap.createBitmap(actualWidth,
                        actualHeight, Bitmap.Config.ARGB_8888)
            } catch (exception: OutOfMemoryError) {
                exception.printStackTrace()
            }

            val ratioX = actualWidth / options.outWidth.toFloat()
            val ratioY = actualHeight / options.outHeight.toFloat()
            val middleX = actualWidth / 2.0f
            val middleY = actualHeight / 2.0f

            val scaleMatrix = Matrix()
            scaleMatrix.setScale(ratioX, ratioY, middleX, middleY)
            val canvas = Canvas(scaledBitmap!!)
            canvas.matrix = scaleMatrix
            canvas.drawBitmap(bmp, middleX - bmp.width / 2, middleY - bmp.height / 2, Paint(Paint.FILTER_BITMAP_FLAG))
            val exif: ExifInterface
            try {
                exif = ExifInterface(filePath)
                val orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION, 0)
                val matrix = Matrix()
                if (orientation == 6) {
                    matrix.postRotate(90f)
                } else if (orientation == 3) {
                    matrix.postRotate(180f)
                } else if (orientation == 8) {
                    matrix.postRotate(270f)
                }
                scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                        scaledBitmap.width, scaledBitmap.height, matrix,
                        true)
            } catch (e: IOException) {
                e.printStackTrace()
            }

            val out: FileOutputStream
            val filename = getFilename()
            try {
                out = FileOutputStream(filename)
                scaledBitmap!!.compress(Bitmap.CompressFormat.JPEG, 90, out)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }

            return filename
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }

    private fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth) {
            val heightRatio = Math.round(height.toFloat() / reqHeight.toFloat())
            val widthRatio = Math.round(width.toFloat() / reqWidth.toFloat())
            inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
        }
        val totalPixels = (width * height).toFloat()
        val totalReqPixelsCap = (reqWidth * reqHeight * 2).toFloat()
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++
        }
        return inSampleSize
    }

    private fun getFilename(): String {
        val file = File(Environment.getExternalStorageDirectory().path, "/Ruth/Media/cover")
        if (!file.exists()) {
            file.mkdirs()
        }
        val uriSting = file.absolutePath + "/" + "IMG_AN_" + System.currentTimeMillis() + ".jpg"
        return uriSting
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    fun getUriPath(context: Context, uri: Uri): String? {

        val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                if ("primary".equals(type, ignoreCase = true)) {
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }
            } else if (isDownloadsDocument(uri)) {
                val id = DocumentsContract.getDocumentId(uri)
                val contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id))

                return getDataColumn(context, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                var contentUri: Uri? = null
                if ("image" == type) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                } else if ("video" == type) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if ("audio" == type) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }

                val selection = "_id=?"
                val selectionArgs = arrayOf(split[1])

                return getDataColumn(context, contentUri, selection, selectionArgs)
            }// MediaProvider
            // DownloadsProvider
        } else if ("content".equals(uri.scheme, ignoreCase = true)) {

            // Return the remote address
            return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(context, uri, null, null)

        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            return uri.path
        }// File
        // MediaStore (and general)

        return null
    }

    private fun getDataColumn(context: Context, uri: Uri?, selection: String?, selectionArgs: Array<String>?): String? {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(column)

        try {
            cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            if (cursor != null)
                cursor.close()
        }
        return null
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }
    /*----------------------------------------------------------------------------------------------*/

    /*---------------------------------INTERNET CONNECTION----------------------------------------*/
    fun isConnectingToInternet(mContext: Context): Boolean {
        val connectivity = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivity != null) {
            val info = connectivity.allNetworkInfo
            if (info != null)
                for (i in info.indices)
                    if (info[i].state === NetworkInfo.State.CONNECTED) {
                        return true
                    }
        }
        //   Toast.makeText(mContext, "Please check internet connection", Toast.LENGTH_SHORT).show()
//        val alertDialog = AlertDialog.Builder(mContext)
//        alertDialog.setMessage(mContext.getString(R.string.no_internet_connection_error))
//        alertDialog.setPositiveButton(mContext.getString(R.string.ok_btn)) { dialog, _ -> dialog.dismiss() }
//        alertDialog.show()
        return false
    }

    fun checkInternetWithoutDialog(mContext: Context): Boolean {
        val connectivity = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivity != null) {
            val info = connectivity.allNetworkInfo
            if (info != null)
                for (i in info.indices)
                    if (info[i].state == NetworkInfo.State.CONNECTED) {
                        return true
                    }
        }
        return false
    }


}