package com.ruth.app.utility;

import com.ruth.app.network.config.Api;

import org.jetbrains.annotations.Nullable;

import java.util.concurrent.TimeUnit;

import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Constants {

    private static String FACEBOOK_URL = "https://graph.facebook.com/";
    private static String INSTAGRAM_URL = "https://api.instagram.com/v1/users/";
    private static String GOOGLE_DIRECTION_URL = "https://maps.googleapis.com/maps/api/";

    private static final OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
            .connectTimeout(6000, TimeUnit.SECONDS)
            .readTimeout(6000, TimeUnit.SECONDS)
            .writeTimeout(6000, TimeUnit.SECONDS).connectionPool(
                    new ConnectionPool(50, 50000, TimeUnit.SECONDS))
            .build();

    public static final Api facebookService = new Retrofit.Builder()
            .baseUrl(FACEBOOK_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(Api.class);

    public static final Api instagramService = new Retrofit.Builder()
            .baseUrl(INSTAGRAM_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(Api.class);

    public static final Api googldirectionService = new Retrofit.Builder()
            .baseUrl(GOOGLE_DIRECTION_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(Api.class);
}
