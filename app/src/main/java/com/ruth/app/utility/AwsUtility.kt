package com.ruth.app.utility

import android.os.Environment
import android.util.Log
import com.amazonaws.AmazonClientException
import com.amazonaws.AmazonServiceException
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.DeleteObjectRequest
import com.amazonaws.services.s3.model.PutObjectRequest
import com.amazonaws.services.s3.model.ResponseHeaderOverrides
import com.ruth.app.BuildConfig
import java.io.File
import javax.inject.Inject

class AwsUtility @Inject constructor() {

    /*----------------------------------UPLOAD TO AWS-------------------------------------------*/
    private var photoUrl = ""

    fun uploadCoverImage(url: String, folderName: String): String {
        val thread = Thread(Runnable {
            try {
                val s3Client = AmazonS3Client(BasicAWSCredentials(BuildConfig.AWS_ACCESS_KEY, BuildConfig.AWS_SECRET_KEY))
                s3Client.setRegion(Region.getRegion(Regions.US_EAST_2))
                val fileUpload = File(url)
                val por = PutObjectRequest(BuildConfig.SESSION_BUCKET_NAME, folderName + "/" + fileUpload.name, fileUpload)
                s3Client.putObject(por)
                val override = ResponseHeaderOverrides()
                override.contentType = "image/jpeg"


//                val urlRequest = GeneratePresignedUrlRequest(BuildConfig.SESSION_BUCKET_NAME, folderName + "/" + fileUpload.name)
//                urlRequest.responseHeaders = override
//                val awsUrl = s3Client.generatePresignedUrl(urlRequest)
//                val stringUri = awsUrl.toString()
//                val parts = stringUri.split("\\?".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
                photoUrl = "https://s3.us-east-2.amazonaws.com/ruthapp/coverImage/" + fileUpload.name
                try {
                    val dir = File(Environment.getExternalStorageDirectory().toString() + File.separator + "/Ruth/media/cover/")
                    if (dir.isDirectory) {
                        val children = dir.list()
                        for (i in children!!.indices) {
                            File(dir, children[i]).delete()
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
        thread.start()
        return photoUrl
    }

    /*----------------------------------DELETE IMAGE AWS------------------------------------------*/
    fun deleteObjectFromAws(photoUrl: String, folderName: String) {
        val parts = photoUrl.split(folderName.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val s3Client = AmazonS3Client(BasicAWSCredentials(BuildConfig.AWS_ACCESS_KEY, BuildConfig.AWS_SECRET_KEY))
        s3Client.setRegion(Region.getRegion(Regions.US_EAST_2))
        try {
            s3Client.deleteObject(DeleteObjectRequest(BuildConfig.SESSION_BUCKET_NAME, folderName + parts[1]))
        } catch (ase: AmazonServiceException) {
            Log.d("exception", "->" + ase.errorMessage)
        } catch (ace: AmazonClientException) {
            Log.d("exception", "->" + ace.message)
        } catch (ex: Exception) {
            Log.d("exception", "->" + ex.message)
        }
    }
}