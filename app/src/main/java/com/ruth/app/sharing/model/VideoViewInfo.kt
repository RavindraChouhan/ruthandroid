package com.ruth.app.sharing.model

class VideoViewInfo(var filePath: String? = null,
                    var mimeType: String? = null,
                    var thumbPath: String? = null,
                    var title: String? = null,
                    var duration : String? = null)
