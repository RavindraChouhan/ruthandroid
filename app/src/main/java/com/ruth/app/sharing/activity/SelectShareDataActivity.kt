package com.ruth.app.sharing.activity

import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ruth.app.R
import com.ruth.app.sharing.adapter.ShareItemAdapter
import com.ruth.app.sharing.model.VideoViewInfo
import kotlinx.android.synthetic.main.activity_select_share_data.*

class SelectShareDataActivity : AppCompatActivity(), View.OnClickListener {

    private var adapter: ShareItemAdapter? = null

    companion object {
        var VIDEO_DATA : VideoViewInfo? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_share_data)

        tvShareHeader.text = intent.getStringExtra("selectedOption")
        ivShareBack.setOnClickListener(this)
        ivShareNext.setOnClickListener(this)
        ivPlayShare.setOnClickListener(this)

        getGalleryVideos()
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.ivShareBack) {
            finish()
        }

        if (v?.id == R.id.ivShareNext) {

        }

        if (v?.id == R.id.ivPlayShare) {
            startActivity(Intent(this, ShareEditorActivity::class.java)
                    .putExtra("selectedOption", intent.getStringExtra("selectedOption")))
        }
    }

    private fun getGalleryVideos() {

        val thumbColumns = arrayOf<String>(MediaStore.Video.Thumbnails.DATA, MediaStore.Video.Thumbnails.VIDEO_ID)

        val mediaColumns = arrayOf<String>(MediaStore.Video.Media._ID, MediaStore.Video.Media.DATA, MediaStore.Video.Media.TITLE, MediaStore.Video.Media.MIME_TYPE)

        var cursor = managedQuery(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                mediaColumns, null, null, null)

        val videoRows = ArrayList<VideoViewInfo>()

        if (cursor.moveToFirst()) {
            do {
                val newVVI = VideoViewInfo()
                val id = cursor.getInt(cursor.getColumnIndex(MediaStore.Video.Media._ID))
                val thumbCursor = managedQuery(
                        MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI,
                        thumbColumns, MediaStore.Video.Thumbnails.VIDEO_ID
                        + "=" + id, null, null)
                if (thumbCursor.moveToFirst()) {
                    newVVI.thumbPath = thumbCursor.getString(thumbCursor
                            .getColumnIndex(MediaStore.Video.Thumbnails.DATA))
                }

                newVVI.filePath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA))
                newVVI.title = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.TITLE))
                newVVI.mimeType = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.MIME_TYPE))
                val mp = MediaPlayer.create(this, Uri.parse(newVVI.filePath))
                newVVI.duration = mp.duration.toString()
           //     newVVI.duration = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION))
                videoRows.add(newVVI)

            } while (cursor.moveToNext())
        }

        ivShareHeader.setImageURI(Uri.parse(videoRows[0].thumbPath))

        val galleryVideos = ArrayList<VideoViewInfo>()
        for (i in 1 until videoRows.size) {
            galleryVideos.add(videoRows[i])
        }

        adapter = ShareItemAdapter(this, galleryVideos)
        rvShareList.adapter = adapter
        adapter!!.setOnItemClickListener(object : ShareItemAdapter.OnItemClickListener{
            override fun onItemClick(view: View, position: Int) {
                VIDEO_DATA = videoRows[position]
                startActivity(Intent(this@SelectShareDataActivity, ShareEditorActivity::class.java)
                        .putExtra("selectedOption", intent.getStringExtra("selectedOption")))
            }

        })
    }


}