package com.ruth.app.sharing.adapter

import android.content.Context
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import com.ruth.app.sharing.model.VideoViewInfo
import kotlinx.android.synthetic.main.item_share.view.*

class ShareItemAdapter(val context: Context, var videoList: ArrayList<VideoViewInfo>) : RecyclerView.Adapter<ShareItemAdapter.ShareItemViewHolder>() {

    private var onItemClicklistener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShareItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_share, parent, false)
        return ShareItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return videoList.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: ShareItemViewHolder, position: Int) {
        if (videoList[position].thumbPath != null) {
            holder.itemView.ivShareItem.setImageURI(Uri.parse(videoList[position].thumbPath))
//            holder.itemView.tvVideoDuration.text = videoList[position].duration.toString()

            holder.itemView.setOnClickListener {
                onItemClicklistener!!.onItemClick(holder.itemView, position)
            }
        }
    }

    inner class ShareItemViewHolder(view: View) : RecyclerView.ViewHolder(view)

    fun setOnItemClickListener(onItemClick: OnItemClickListener) {
        this.onItemClicklistener = onItemClick
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }


}


