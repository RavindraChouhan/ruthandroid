package com.ruth.app.sharing.widgets;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.ruth.app.R;
import com.ruth.app.sharing.activity.ShareActivity;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * PopWindow for Topic Pick
 */
public class SelectOptionPopup extends PopupWindow implements OnClickListener {

    private static final int DEFAULT_MIN_YEAR = 1900;
    private TextView confirmBtn;
    private OptionLoopView optionLoopView;
    private View pickerContainerV;
    private View contentView;//root view
    private int optionPos = 0;
    private Context mContext;
    private String textConfirm;
    private int colorConfirm;
    private int btnTextsize;   //text btnTextsize of cancel and confirm button

    private ArrayList<String> optionList = new ArrayList();


    public static class Builder {

        //Required
        private Context context;
        private OptionSelectionListner listener;

        public Builder(Context context, OptionSelectionListner listener) {
            this.context = context;
            this.listener = listener;
        }

        //Option
        private boolean showDayMonthYear = false;
        private int minYear = DEFAULT_MIN_YEAR;
        private int maxYear = Calendar.getInstance().get(Calendar.YEAR) + 1;
        private String textCancel = "Cancel";
        private String textConfirm = "Confirm";
        private int colorCancel = Color.parseColor("#999999");
        private int colorConfirm = Color.parseColor("#303F9F");
        private int btnTextSize = 16;//text btnTextsize of cancel and confirm button
        private int viewTextSize = 25;

        public Builder textConfirm(String textConfirm) {
            this.textConfirm = textConfirm;
            return this;
        }

        public Builder colorConfirm(int colorConfirm) {
            this.colorConfirm = colorConfirm;
            return this;
        }

        /**
         * set btn text btnTextSize
         *
         * @param textSize dp
         */
        public Builder btnTextSize(int textSize) {
            this.btnTextSize = textSize;
            return this;
        }

        public Builder viewTextSize(int textSize) {
            this.viewTextSize = textSize;
            return this;
        }

        public SelectOptionPopup build() {
            if (minYear > maxYear) {
                throw new IllegalArgumentException();
            }
            return new SelectOptionPopup(this);
        }
    }

    public SelectOptionPopup(Builder builder) {
        this.textConfirm = builder.textConfirm;
        this.mContext = builder.context;
        this.mListener = builder.listener;
        this.colorConfirm = builder.colorConfirm;
        this.btnTextsize = builder.btnTextSize;
        initView();
    }

    private OptionSelectionListner mListener;

    private void initView() {

        contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_share_option, null);
        confirmBtn = (TextView) contentView.findViewById(R.id.btn_confirm);
        confirmBtn.setTextColor(colorConfirm);
        confirmBtn.setTextSize(btnTextsize);
        optionLoopView = (OptionLoopView) contentView.findViewById(R.id.picker_option);
        pickerContainerV = contentView.findViewById(R.id.container_option);

        optionLoopView.setLoopListener(new OptionLoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                optionPos = item;
            }
        });

        initDayPickerView();

        confirmBtn.setOnClickListener(this);
        contentView.setOnClickListener(this);

        if (!TextUtils.isEmpty(textConfirm)) {
            confirmBtn.setText(textConfirm);
        }

        setTouchable(true);
        setFocusable(true);
        setBackgroundDrawable(new BitmapDrawable());
        setAnimationStyle(R.style.FadeInPopWin);
        setContentView(contentView);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
    }

    /**
     * Init day item
     */
    private void initDayPickerView() {
        optionList.add("-" + mContext.getString(R.string.film) + "-");
        optionList.add("-" + mContext.getString(R.string.image) + "-");
        optionList.add("-" + mContext.getString(R.string.share_article) + "-");
        optionList.add("-" + mContext.getString(R.string.recording) + "-");
        optionList.add("-" + mContext.getString(R.string.other) + "-");

        optionLoopView.setDataList((ArrayList) optionList);

        for (int i = 0; i < optionList.size(); i++) {
            if (optionList.get(i).equalsIgnoreCase(ShareActivity.Companion.getUSER_SELECTED_OPTION())) {
                optionLoopView.setInitPosition(i);
                optionPos = i;
                return;
            }
        }
    }

    /**
     * Show picker popWindow
     *
     * @param activity
     */
    public void showPopWin(Activity activity) {

        if (null != activity) {

            TranslateAnimation trans = new TranslateAnimation(
                    Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF,
                    0, Animation.RELATIVE_TO_SELF, 1,
                    Animation.RELATIVE_TO_SELF, 0);

            showAtLocation(activity.getWindow().getDecorView(), Gravity.BOTTOM,
                    0, 0);
            trans.setDuration(400);
            trans.setInterpolator(new AccelerateDecelerateInterpolator());

            pickerContainerV.startAnimation(trans);
        }
    }

    /**
     * Dismiss picker popWindow
     */
    private void dismissPopWin() {

        TranslateAnimation trans = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 1);

        trans.setDuration(400);
        trans.setInterpolator(new AccelerateInterpolator());
        trans.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                dismiss();
            }
        });

        pickerContainerV.startAnimation(trans);
    }

    @Override
    public void onClick(View v) {

        if (v == contentView) {
            dismissPopWin();
        } else if (v == confirmBtn) {
            mListener.onOptionSelected(optionList.get(optionPos));
            dismissPopWin();
        }
    }

    public interface OptionSelectionListner {
        void onOptionSelected(String option);
    }
}
