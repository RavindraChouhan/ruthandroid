package com.ruth.app.sharing.cropvideo.interfaces;

public interface OnProgressVideoListener {

    void updateProgress(int time, int max, float scale);
}
