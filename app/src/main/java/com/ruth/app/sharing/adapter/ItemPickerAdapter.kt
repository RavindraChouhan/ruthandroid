package com.ruth.app.sharing.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R

class ItemPickerAdapter(val context: Context, var count: Int) : RecyclerView.Adapter<ItemPickerAdapter.ItemPickerViewHolder>() {

    private var onItemClicklistener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemPickerViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_picker, parent, false)
        return ItemPickerViewHolder(view)
    }

    override fun getItemCount(): Int {
        return count
    }

    override fun onBindViewHolder(holder: ItemPickerViewHolder, position: Int) {
        holder.itemView.setOnClickListener { onItemClicklistener!!.onItemClick(holder.itemView, position) }
    }

    inner class ItemPickerViewHolder(view: View) : RecyclerView.ViewHolder(view)

    fun setOnItemClickListener(onItemClick: OnItemClickListener) {
        this.onItemClicklistener = onItemClick
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }


}


