package com.ruth.app.sharing.activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.WindowManager
import com.ruth.app.App
import com.ruth.app.R
import com.ruth.app.base.activity.BaseActivity
import com.ruth.app.sharing.adapter.AddChapterAdapter
import com.ruth.app.sharing.adapter.ItemPickerAdapter
import com.ruth.app.sharing.presenter.SharePresenter
import com.ruth.app.sharing.widgets.SelectOptionPopup
import kotlinx.android.synthetic.main.activity_share.*

class ShareActivity : BaseActivity<SharePresenter>(), SharePresenter.ShareView {

    lateinit var adapter: ItemPickerAdapter
    lateinit var chapterAdapter: AddChapterAdapter
    var selectedOption: String = ""
    var count: Int = 0
    var chapterCount: Int = 0

    companion object {
        var USER_SELECTED_OPTION: String = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        setContentView(R.layout.activity_share)
        App.getComponent().inject(this)

        count = 1
        selectedOption = getString(R.string.film)
        rvItemSelector.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        adapter = ItemPickerAdapter(this, count)
        rvItemSelector!!.adapter = adapter
        adapter.setOnItemClickListener(object : ItemPickerAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                startActivity(Intent(this@ShareActivity, SelectShareDataActivity::class.java)
                        .putExtra("selectedOption", selectedOption))
            }
        })

        chapterCount = 1
        rvAddChapter.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        chapterAdapter = AddChapterAdapter(this, chapterCount)
        rvAddChapter!!.adapter = chapterAdapter

        layoutOption.setOnClickListener {
            hideKeyboard()
            openOptionPicker()
        }

        btnAddChapter.setOnClickListener {
            if (chapterCount < 10) {
                chapterCount++
                chapterAdapter = AddChapterAdapter(this, chapterCount)
                rvAddChapter!!.adapter = chapterAdapter
                lblChapterCount.text = chapterCount.toString()
                btnRemoveChapter.setColorFilter(ContextCompat.getColor(this, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY)
                if (chapterCount == 10) {
                    btnAddChapter.setColorFilter(ContextCompat.getColor(this, R.color.gray), android.graphics.PorterDuff.Mode.MULTIPLY)
                }
            }
        }

        btnRemoveChapter.setOnClickListener {
            if (chapterCount > 1) {
                chapterCount--
                chapterAdapter = AddChapterAdapter(this, chapterCount)
                rvAddChapter!!.adapter = chapterAdapter
                lblChapterCount.text = chapterCount.toString()
                btnAddChapter.setColorFilter(ContextCompat.getColor(this, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY)
                if (chapterCount == 1) {
                    btnRemoveChapter.setColorFilter(ContextCompat.getColor(this, R.color.gray), android.graphics.PorterDuff.Mode.MULTIPLY)
                }
            }
        }

        ivShareBack.setOnClickListener {
            finish()
        }
    }

    private fun openOptionPicker() {
        val pickerPopWin = SelectOptionPopup.Builder(this, SelectOptionPopup.OptionSelectionListner { text ->
            selectedOption = text
            selectedOption = selectedOption.replace("-", "")
            USER_SELECTED_OPTION = text
            lblSelectedOption.text = selectedOption
            selectedOption(selectedOption)
        }).textConfirm(getString(R.string.done))
                .btnTextSize(16)
                .viewTextSize(40)
                .colorConfirm(Color.parseColor("#3183fd"))
                .build()
        pickerPopWin.showPopWin(this)
    }

    private fun selectedOption(text: String) {
        when (text) {
            getString(R.string.film) -> {
                count = 1
                lblUpload.text = getString(R.string.upload_video)
                layoutForChapter.visibility = View.GONE
                layoutForOtherSharing.visibility = View.VISIBLE
            }
            getString(R.string.image) -> {
                count = 5
                lblUpload.text = getString(R.string.upload_images)
                layoutForChapter.visibility = View.GONE
                layoutForOtherSharing.visibility = View.VISIBLE
            }
            getString(R.string.share_article) -> {
                layoutForChapter.visibility = View.VISIBLE
                layoutForOtherSharing.visibility = View.GONE
                btnRemoveChapter.setColorFilter(ContextCompat.getColor(this, R.color.gray), android.graphics.PorterDuff.Mode.MULTIPLY)
                return
            }
            getString(R.string.recording) -> {
                count = 1
                lblUpload.text = getString(R.string.upper_communication)
                layoutForChapter.visibility = View.GONE
                layoutForOtherSharing.visibility = View.VISIBLE
            }
            getString(R.string.other) -> {
                count = 1
                lblUpload.text = getString(R.string.upload_related_info)
                layoutForChapter.visibility = View.GONE
                layoutForOtherSharing.visibility = View.VISIBLE
            }
        }
        adapter = ItemPickerAdapter(this, count)
        rvItemSelector!!.adapter = adapter
        adapter.setOnItemClickListener(object : ItemPickerAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                startActivity(Intent(this@ShareActivity, SelectShareDataActivity::class.java)
                        .putExtra("selectedOption", selectedOption))
            }

        })
    }

}
