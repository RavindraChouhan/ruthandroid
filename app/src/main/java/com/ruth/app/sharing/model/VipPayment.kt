package com.ruth.app.sharing.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VipPayment(@SerializedName("status")
                 @Expose
                 var status: Boolean,
                 @SerializedName("message")
                 @Expose
                 var message: String,
                 @SerializedName("zh_Hans")
                 @Expose
                 var zh_Hans: String,
                 @SerializedName("zh_Hant")
                 @Expose
                 var zh_Hant: String,
                 @SerializedName("data")
                 @Expose
                 var data: Payment)

class Payment(@SerializedName("isPayment")
              @Expose
              var isPayment: Boolean,
              @SerializedName("_id")
              @Expose
              var _id: String,
              @SerializedName("userId")
              @Expose
              var userId: String,
              @SerializedName("createdAt")
              @Expose
              var createdAt: String,
              @SerializedName("updatedAt")
              @Expose
              var updatedAt: String,
              @SerializedName("__v")
              @Expose
              var __v: Int)