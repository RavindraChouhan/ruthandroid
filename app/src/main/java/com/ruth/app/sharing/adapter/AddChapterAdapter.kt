package com.ruth.app.sharing.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruth.app.R
import kotlinx.android.synthetic.main.item_new_chapter.view.*

class AddChapterAdapter(context: Context, var count: Int) : RecyclerView.Adapter<AddChapterAdapter.AddChapterViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddChapterViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_new_chapter, parent, false)
        return AddChapterViewHolder(view)
    }

    override fun getItemCount(): Int {
        return count
    }

    override fun onBindViewHolder(holder: AddChapterViewHolder, position: Int) {
        val chapter_hint = "關於第" + (position + 1).toString() + "章......"
        holder.itemView.etChapter.hint = chapter_hint
    }

    inner class AddChapterViewHolder(view: View) : RecyclerView.ViewHolder(view)
}


