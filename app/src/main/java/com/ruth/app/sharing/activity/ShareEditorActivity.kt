package com.ruth.app.sharing.activity

import android.app.ProgressDialog
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.ruth.app.R
import com.ruth.app.sharing.cropvideo.interfaces.OnHgLVideoListener
import com.ruth.app.sharing.cropvideo.interfaces.OnTrimVideoListener
import kotlinx.android.synthetic.main.activity_share_editor.*

class ShareEditorActivity : AppCompatActivity(), View.OnClickListener, OnTrimVideoListener, OnHgLVideoListener {

    private var mProgressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_editor)

        tvEditorHeader.text = intent.getStringExtra("selectedOption")

        ivEditorBack.setOnClickListener(this)
        ivEditorNext.setOnClickListener(this)

        var path = SelectShareDataActivity.VIDEO_DATA!!.filePath
        /*"android.resource://" + packageName + "/" + R.raw.video_testtwo*/
        val mp = MediaPlayer.create(this, Uri.parse(path))
        val duration = mp.duration

        val maxDuration = duration

        mProgressDialog = ProgressDialog(this)                                     //setting progressbar
        mProgressDialog!!.setCancelable(false)
        mProgressDialog!!.setMessage("Progress")

        if (timeLine != null) {
            timeLine.setMaxDuration(maxDuration)                                           //get total duration of video file
            timeLine.setOnTrimVideoListener(this)
            timeLine.setOnHgLVideoListener(this)
            timeLine.setVideoURI(Uri.parse(path))
            timeLine.setVideoInformationVisibility(true)
        }
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.ivEditorBack) {
            finish()
        }

        if (v?.id == R.id.ivEditorNext) {

        }

    }

    override fun onVideoPrepared() {
        runOnUiThread { }
    }

    override fun onTrimStarted() {
        try {
            mProgressDialog!!.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        mProgressDialog!!.cancel()
    }

    override fun onResume() {
        super.onResume()
        Runtime.getRuntime().gc()
    }

    override fun getResult(contentUri: Uri) {
        mProgressDialog!!.cancel()
        Log.d("result", " -> $contentUri")
    }

    override fun cancelAction() {
        mProgressDialog!!.cancel()
        timeLine.destroy()
        finish()
    }

    override fun onError(message: String) {
        mProgressDialog!!.cancel()
        Log.d("error", "->$message")
    }

//    fun getMediaDuration(uriOfFile: Uri, mContext: Context): Int {
//        try {
//            val mp = MediaPlayer.create(mContext, uriOfFile)
//            return mp.duration
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//
//        return 0
//    }

}