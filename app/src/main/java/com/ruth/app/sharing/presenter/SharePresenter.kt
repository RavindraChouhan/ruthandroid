package com.ruth.app.sharing.presenter

import com.ruth.app.base.presenter.BasePresenter
import javax.inject.Inject

class SharePresenter @Inject constructor() : BasePresenter<SharePresenter.ShareView>() {

    interface ShareView : BaseView
}